﻿using HVEX.Model;
using HVEX.Service;
using HVEX.View.Interface;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Controller {
    class EquipmentController {

        private ITransformer view;
        private IEquipment choseTrafoView;

        public EquipmentController(IEquipment frm) {
            if (frm is ITransformer) {
                view = (ITransformer)frm;
            } else {
                choseTrafoView = frm;
            }
        }

        public async Task<bool> Create(string userId, string equipmentType) {
            try {
                switch (equipmentType) {
                    case "Transformer":
                        List<ObjectId> connectionGroups = new List<ObjectId>();
                        Transformer trafo = new Transformer(
                            view.SerialNumber,
                            view.Model,
                            view.TrafoType,
                            view.InternalNumber,
                            view.Description,
                            view.TrafoRegulationRange,
                            view.ManufactureDate,
                            ObjectId.Parse(view.Manufacturer),
                            ObjectId.Parse(view.Customer),
                            view.TrafoEfficiencyLevel,
                            view.TrafoPolarity,
                            view.TrafoFrequency,
                            view.TrafoPhases,
                            view.TrafoTemperatureLimit,
                            view.TrafoWeight,
                            view.TrafoResistanceTemperature,
                            view.TrafoInsulatingOil,
                            view.TrafoVoltageClass,
                            view.TrafoState,
                            view.TrafoConnection,
                            view.TrafoPower,
                            view.TrafoPowerMeasurement,
                            view.TrafoVoltage,
                            view.TrafoVoltageMeasurement,
                            view.TrafoCurrent,
                            view.TrafoCurrentMeasurement,
                            view.TrafoWindingMaterial,
                            view.TrafoResistance,
                            view.TrafoResistanceMeasurement,
                            view.TrafoTaps
                        );
                        await trafo.Create(ObjectId.Parse(userId));
                        break;
                    case "Generic":
                        GenericEquipment equipment = new GenericEquipment(
                            view.SerialNumber,
                            view.InternalNumber,
                            ObjectId.Parse(view.Manufacturer),
                            ObjectId.Parse(view.Customer),
                            view.Description,
                            view.ManufactureDate,
                            view.Model,
                            "Generic"
                        );
                        await equipment.Create(ObjectId.Parse(userId));
                        break;
                }
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR REGISTRAR NOVO EQUIPAMENTO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar cadastrar o equipamento.\n{e.Message}");
                return false;
            }
        }

        public async Task Read(string userId, string type) {
            try {
                switch (type) {
                    case "Transformer":
                        Transformer trafo = new Transformer();
                        trafo.Id = ObjectId.Parse(view.Id);
                        await trafo.Read();
                        view.SerialNumber = trafo.SerialNumber;
                        view.InternalNumber = trafo.InternalNumber;
                        view.Description = trafo.Description;
                        view.Model = trafo.Model;
                        view.TrafoType = trafo.Type;
                        view.Manufacturer = trafo.Manufacturer.ToString();
                        view.Customer = trafo.Customer.ToString();
                        view.ManufactureDate = trafo.ManufactureDate;
                        view.TrafoEfficiencyLevel = trafo.EfficiencyLevel;
                        view.TrafoInsulatingOil = trafo.InsulatingOil;
                        view.TrafoPolarity = trafo.Polarity;
                        view.TrafoFrequency = trafo.Frequency;
                        view.TrafoPhases = trafo.Phases;
                        view.TrafoState = trafo.State;
                        view.TrafoTemperatureLimit = trafo.TemperatureLimit;
                        view.TrafoResistanceTemperature = trafo.ResistanceTemperature;
                        view.TrafoVoltageClass = trafo.VoltageClass;
                        view.TrafoWindingMaterial = trafo.WindingMaterial;
                        view.TrafoWeight = trafo.Weight;
                        view.TrafoConnection = trafo.Connection;
                        view.TrafoRegulationRange = trafo.RegulationRange;
                        view.TrafoVoltage = trafo.Voltage;
                        view.TrafoVoltageMeasurement = trafo.VoltageMeasurement;
                        view.TrafoCurrent = trafo.Current;
                        view.TrafoCurrentMeasurement = trafo.CurrentMeasurement;
                        view.TrafoPower = trafo.Power;
                        view.TrafoPowerMeasurement = trafo.PowerMeasurement;
                        view.TrafoResistance = trafo.Resistance;
                        view.TrafoResistanceMeasurement = trafo.ResistanceMeasurement;
                        view.TrafoTaps = trafo.Taps;
                        view.SpecimenType = "Transformer";
                        if (trafo.ConnectionGroups != null) {
                            DataTable connectionGroups = new DataTable();
                            connectionGroups.Columns.Add("Id");
                            connectionGroups.Columns.Add("Description");
                            foreach (ObjectId connectionGroup in trafo.ConnectionGroups) {
                                DataRow row = connectionGroups.NewRow();
                                row["Id"] = connectionGroup.ToString();
                                row["Description"] = connectionGroup;
                            }
                        }
                        break;
                    case "Generic":
                        GenericEquipment equipment = new GenericEquipment();
                        equipment.Id = ObjectId.Parse(view.Id);
                        await equipment.Read();
                        view.SerialNumber = equipment.SerialNumber;
                        view.InternalNumber = equipment.InternalNumber;
                        view.ManufactureDate = equipment.ManufactureDate;
                        view.Manufacturer = equipment.Manufacturer.ToString();
                        view.Customer = equipment.Customer.ToString();
                        view.Description = equipment.Description;
                        view.Model = equipment.Model;
                        view.SpecimenType = "Generic";
                        break;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR RECUPERAR TRANSFORMADOR.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar o transformador selecionado.\n{e.Message}");
            }
        }

        public async Task<bool> Update(string userId, string equipmentType) {
            try {
                switch (equipmentType) {
                    case "Transformer":
                        Transformer trafo = new Transformer();
                        trafo.Id = ObjectId.Parse(view.Id);
                        trafo.SerialNumber = view.SerialNumber;
                        trafo.InternalNumber = view.InternalNumber;
                        trafo.Description = view.Description;
                        trafo.Model = view.Model;
                        trafo.Manufacturer = ObjectId.Parse(view.Manufacturer);
                        trafo.ManufactureDate = view.ManufactureDate;
                        trafo.Customer = ObjectId.Parse(view.Customer);
                        trafo.EfficiencyLevel = view.TrafoEfficiencyLevel;
                        trafo.InsulatingOil = view.TrafoInsulatingOil;
                        trafo.Connection = view.TrafoConnection;
                        trafo.Phases = view.TrafoPhases;
                        trafo.Frequency = view.TrafoFrequency;
                        trafo.Polarity = view.TrafoPolarity;
                        trafo.RegulationRange = view.TrafoRegulationRange;
                        trafo.ResistanceTemperature = view.TrafoResistanceTemperature;
                        trafo.TemperatureLimit = view.TrafoTemperatureLimit;
                        trafo.State = view.TrafoState;
                        trafo.Type = view.TrafoType;
                        trafo.VoltageClass = view.TrafoVoltageClass;
                        trafo.Weight = view.TrafoWeight;
                        trafo.Power = view.TrafoPower;
                        trafo.PowerMeasurement = view.TrafoPowerMeasurement;
                        trafo.Voltage = view.TrafoVoltage;
                        trafo.VoltageMeasurement = view.TrafoVoltageMeasurement;
                        trafo.Current = view.TrafoCurrent;
                        trafo.CurrentMeasurement = view.TrafoCurrentMeasurement;
                        trafo.Resistance = view.TrafoResistance;
                        trafo.ResistanceMeasurement = view.TrafoResistanceMeasurement;
                        trafo.Taps = view.TrafoTaps;
                        trafo.WindingMaterial = view.TrafoWindingMaterial;
                        await trafo.Update(ObjectId.Parse(userId));
                        break;
                    case "Generic":
                        GenericEquipment equipment = new GenericEquipment();
                        equipment.Id = ObjectId.Parse(view.Id);
                        equipment.SerialNumber = view.SerialNumber;
                        equipment.InternalNumber = view.InternalNumber;
                        equipment.Manufacturer = ObjectId.Parse(view.Manufacturer.ToString());
                        equipment.Customer = ObjectId.Parse(view.Customer); ;
                        equipment.ManufactureDate = view.ManufactureDate;
                        equipment.Model = view.Model;
                        equipment.Description = view.Description;
                        await equipment.Update(ObjectId.Parse(userId));
                        break;
                }
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR ALTERAR TRANSFORMADOR.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar alterar o transformador.\n{e.Message}");
                return false;
            }
        }

        public async Task<bool> Delete(string userId, string equipmentType) {
            try {
                switch (equipmentType) {
                    case "Transformer":
                        Transformer trafo = new Transformer();
                        trafo.Id = ObjectId.Parse(view.Id);
                        await trafo.Delete(ObjectId.Parse(userId));
                        break;
                    case "Generic":
                        GenericEquipment equipment = new GenericEquipment();
                        equipment.Id = ObjectId.Parse(view.Id);
                        await equipment.Delete(ObjectId.Parse(userId));
                        break;
                }
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR REMOVER TRANSFORMADOR.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar remover o transformador.\n{e.Message}");
                return false;
            }
        }

        public async Task ListManufacturers(string userId) {
            try {
                Manufacturer manufacturer = new Manufacturer();
                DataTable manufacturerData = AdaptDataTable(await manufacturer.List("Nome Fantasia"));
                view.ManufacturerList = manufacturerData;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR LISTAR FABRICANTES.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar listar os fabricantes.\n{e.Message}");
            }
        }

        public async Task ListCustomers(string userId) {
            try {
                Manufacturer customer = new Manufacturer();
                DataTable customerData = AdaptDataTable(await customer.List("Nome Fantasia"));
                view.CustomerList = customerData;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR LISTAR CLIENTES",
                    $"{e.Message}\n\n{e}",
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar listar os clientes.\n{e.Message}");
            }

        }

        public async Task List(
            string userId,
            string serialNumber,
            string description,
            string order = "Número de Série"
        ) {
            try {
                Transformer trafo = new Transformer();
                trafo.SerialNumber = serialNumber;
                trafo.Description = description;
                DataTable data = await trafo.List(order);
                if (view != null) {
                    view.List = data;
                } else {
                    choseTrafoView.List = data;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR LISTAR EQUIPAMENTOS.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar os equipamentos do banco de dados.\n{e.Message}");
            }
        }

        private DataTable AdaptDataTable(DataTable table) {
            table.Columns.Remove("Razão Social");
            table.Columns.Remove("Contato");
            table.Columns.Remove("Telefone");
            return table;
        }
    }
}
