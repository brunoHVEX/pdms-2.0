﻿using HVEX.Model;
using HVEX.Service;
using HVEX.View.Interface;
using MongoDB.Bson;
using System;
using System.Data;
using System.Threading.Tasks;

namespace HVEX.Controller {
    class InstrumentController {
        private IInstrument view;

        public InstrumentController(IInstrument frm) {
            view = frm;
        }
        public async Task<bool> Create(string userId, string equipmentType) {
            try {

                switch (equipmentType) {
                    case "Oscilloscope":
                        IOscilloscope oscilloscopeView = (IOscilloscope)view;
                        Oscilloscope oscilloscope = new Oscilloscope(
                            oscilloscopeView.SerialNumber,
                            oscilloscopeView.Model,
                            oscilloscopeView.Description,
                            oscilloscopeView.Brand,
                            oscilloscopeView.Ip,
                            oscilloscopeView.Port,
                            oscilloscopeView.Channels
                            //oscilloscopeView.Instrument
                        );
                        await oscilloscope.Create(ObjectId.Parse(userId));
                        break;
                    case "Plc":
                        IPlc plcView = (IPlc)view;
                        Plc plc = new Plc(
                            plcView.SerialNumber,
                            plcView.Model,
                            plcView.Description,
                            plcView.Brand,
                            plcView.Ip,
                            plcView.Port,
                            plcView.AcquisitionInterval
                        );
                        await plc.Create(ObjectId.Parse(userId));
                        break;
                }
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR REGISTRAR NOVO INSTRUMENTO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar cadastrar o instrumento.\n{e.Message}");
                return false;
            }
        }

        public async Task Read(string userId, string type) {
            try {
                switch (type) {
                    case "Oscilloscope":
                        IOscilloscope oscilloscopeView = (IOscilloscope)view;
                        Oscilloscope oscilloscope = new Oscilloscope();
                        oscilloscope.Id = ObjectId.Parse(oscilloscopeView.Id);
                        await oscilloscope.Read();
                        oscilloscopeView.SerialNumber = oscilloscope.SerialNumber;
                        oscilloscopeView.Model = oscilloscope.Model;
                        oscilloscopeView.Description = oscilloscope.Description;
                        oscilloscopeView.Brand = oscilloscope.Brand;
                        oscilloscopeView.Ip = oscilloscope.Ip;
                        oscilloscopeView.Port = oscilloscope.Port;
                        oscilloscopeView.Channels = oscilloscope.Channels;
                        //oscilloscopeView.Instrument = oscilloscope.Instrument;
                        oscilloscopeView.InstrumentType = "Oscilloscope";
                        break;
                    case "Plc":
                        IPlc plcView = (IPlc)view;
                        Plc plc = new Plc();
                        plc.Id = ObjectId.Parse(plcView.Id);
                        await plc.Read();
                        plcView.SerialNumber = plc.SerialNumber;
                        plcView.Model = plc.Model;
                        plcView.Description = plc.Description;
                        plcView.Brand = plc.Brand;
                        plcView.Ip = plc.Ip;
                        plcView.Port = plc.Port;
                        plcView.AcquisitionInterval = plc.AcquisitionInterval;
                        plcView.InstrumentType = "Plc";
                        break;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR RECUPERAR INSTRUMENTO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar o instrumento selecionado.\n{e.Message}");
            }
        }

        public async Task<bool> Update(string userId, string equipmentType) {
            try {
                switch (equipmentType) {
                    case "Oscilloscope":
                        IOscilloscope oscilloscopeView = (IOscilloscope)view;
                        Oscilloscope osciloscope = new Oscilloscope();
                        osciloscope.Id = ObjectId.Parse(oscilloscopeView.Id);
                        osciloscope.SerialNumber = oscilloscopeView.SerialNumber;
                        osciloscope.Model = oscilloscopeView.Model;
                        osciloscope.Description = oscilloscopeView.Description;
                        osciloscope.Brand = oscilloscopeView.Brand;
                        osciloscope.Ip = oscilloscopeView.Ip;
                        osciloscope.Port = oscilloscopeView.Port;
                        osciloscope.Channels = oscilloscopeView.Channels;
                        //osciloscope.Instrument = oscilloscopeView.Instrument;
                        await osciloscope.Update(ObjectId.Parse(userId));
                        break;
                    case "Plc":
                        IPlc plcView = (IPlc)view;
                        Plc plc = new Plc();
                        plc.Id = ObjectId.Parse(plcView.Id);
                        plc.SerialNumber = plcView.SerialNumber;
                        plc.Model = plcView.Model;
                        plc.Description = plcView.Description;
                        plc.Brand = plcView.Brand;
                        plc.Ip = plcView.Ip;
                        plc.Port = plcView.Port;
                        plc.AcquisitionInterval = plcView.AcquisitionInterval;
                        await plc.Update(ObjectId.Parse(userId));
                        break;
                }
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR ALTERAR INSTRUMENTO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar alterar o instrumento.\n{e.Message}");
                return false;
            }
        }

        public async Task<bool> Delete(string userId, string equipmentType) {
            try {
                switch (equipmentType) {
                    case "Oscilloscope":
                        IOscilloscope oscilloscopeView = (IOscilloscope)view;
                        Oscilloscope oscilloscope = new Oscilloscope();
                        oscilloscope.Id = ObjectId.Parse(oscilloscopeView.Id);
                        await oscilloscope.Delete(ObjectId.Parse(userId));
                        break;
                    case "Plc":
                        IPlc plcView = (IPlc)view;
                        Plc plc = new Plc();
                        plc.Id = ObjectId.Parse(plcView.Id);
                        await plc.Delete(ObjectId.Parse(userId));
                        break;
                }
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR REMOVER INSTRUMENTO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar remover o instrumento.\n{e.Message}");
                return false;
            }
        }

        public async Task List(
            string userId,
            string serialNumber,
            string brand,
            string order = "Número de Série"
        ) {
            try {
                Oscilloscope osciloscope = new Oscilloscope();
                osciloscope.SerialNumber = serialNumber;
                osciloscope.Brand = brand;
                Plc plc = new Plc();
                plc.SerialNumber = serialNumber;
                plc.Brand = brand;
                DataTable data = await osciloscope.List(order);
                DataTable dataPlc = await plc.List(order);
                if ((data != null) && (dataPlc != null)){
                    data.Merge(dataPlc);
                    view.List = data;
                } else if ((data != null) && (dataPlc == null)) {
                    view.List = data;
                } else if ((data == null) && (dataPlc != null)) {
                    view.List = dataPlc;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR LISTAR INSTRUMENTOS.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar os instrumentos da base de dados.\n{e.Message}");
            }
        }
    }
}
