﻿using HVEX.Model;
using HVEX.Service;
using HVEX.View.Interface;
using MongoDB.Bson;
using System;
using System.Data;
using System.Threading.Tasks;

namespace HVEX.Controller {
    class ManufacturerController {
        private IManufacturer view;

        public ManufacturerController(IManufacturer frm) {
            view = frm;
        }

        public async Task<bool> Create(string userId) {
            try {
                Manufacturer manufacturer = new Manufacturer(
                    view.CompanyName,
                    view.FantasyName,
                    view.Phone,
                    view.Email,
                    view.Contact,
                    view.Address,
                    view.District,
                    view.City,
                    view.State,
                    view.Country,
                    ImageHandler.ImageToByteArray(view.Image)
                );
                await manufacturer.Create(ObjectId.Parse(userId));
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR REGISTRAR FABRICANTE.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar registrar o novo fabricante.\n{e.Message}");
                return false;
            }
        }

        public async Task Read(string userId) {
            try {
                Manufacturer manufacturer = new Manufacturer();
                manufacturer.Id = ObjectId.Parse(view.Id);
                await manufacturer.Read();
                view.CompanyName = manufacturer.CompanyName;
                view.FantasyName = manufacturer.FantasyName;
                view.Phone = manufacturer.Phone;
                view.Email = manufacturer.Email;
                view.Contact = manufacturer.Contact;
                view.Address = manufacturer.Address;
                view.District = manufacturer.District;
                view.City = manufacturer.City;
                view.State = manufacturer.State;
                view.Country = manufacturer.Country;
                if (manufacturer.Image != null) {
                    view.Image = ImageHandler.ByteArrayToImage(manufacturer.Image);
                } else {
                    view.Image = Properties.Resources.company_image;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR RECUPERAR FABRICANTE.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar o fabricante.\n{e.Message}");
            }
        }

        public async Task<bool> Update(string userId) {
            try {
                Manufacturer manufacturer = new Manufacturer();
                manufacturer.Id = ObjectId.Parse(view.Id);
                manufacturer.CompanyName = view.CompanyName;
                manufacturer.FantasyName = view.FantasyName;
                manufacturer.Phone = view.Phone;
                manufacturer.Email = view.Email;
                manufacturer.Contact = view.Contact;
                manufacturer.Address = view.Address;
                manufacturer.District = view.District;
                manufacturer.City = view.City;
                manufacturer.State = view.State;
                manufacturer.Country = view.Country;
                manufacturer.Image = ImageHandler.ImageToByteArray(view.Image);
                await manufacturer.Update(ObjectId.Parse(userId));
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR ALTERAR FABRICANTE.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar alterar o fabricante.\n{e.Message}");
                return false;
            }
        }

        public async Task<bool> Delete(string userId) {
            try {
                Manufacturer manufacturer = new Manufacturer();
                manufacturer.Id = ObjectId.Parse(view.Id);
                await manufacturer.Delete(ObjectId.Parse(userId));
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR REMOVER FABRICANTE.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar remover o fabricante.\n{e.Message}");
                return false;
            }
        }

        public async Task List(string userId, string companyName, string order = "Razão Social") {
            try {
                Manufacturer manufacturer = new Manufacturer();
                manufacturer.CompanyName = companyName;
                DataTable data = await manufacturer.List(order);
                view.List = data;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR LISTAR FABRICANTES.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar os fabricantes da base de dados!\n{e.Message}");
            }
        }
    }
}
