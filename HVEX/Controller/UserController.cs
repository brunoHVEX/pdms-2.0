﻿using HVEX.Model;
using HVEX.Model.Enum;
using HVEX.Service;
using HVEX.View.Interface;
using MongoDB.Bson;
using System;
using System.Data;
using System.Threading.Tasks;

namespace HVEX.Controller {
    class UserController {
        private IUser view;

        public UserController(IUser form = null) {
            view = form;
        }
        public async Task<bool> Create(string userId) {
            try {
                User user = new User(
                    view.Name,
                    view.UserName,
                    view.Password,
                    view.AccessLevel,
                    ImageHandler.ImageToByteArray(view.Image)
                );
                await user.Create(ObjectId.Parse(userId));
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR RESITRAR NOVO USUÁRIO",
                    $"{e.Message}\n\n{e}",
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar registrar o novo usuário.\n{e.Message}");
                return false;
            }
        }
        public async Task Read(string userId) {
            try {
                User user = new User();
                user.Id = ObjectId.Parse(view.Id);
                await user.Read();
                view.Id = user.Id.ToString();
                view.Name = user.Name;
                view.UserName = user.UserName;
                view.AccessLevel = user.AccessLevel.ToString();
                view.Image = user.Image != null ? ImageHandler.ByteArrayToImage(user.Image) : null;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR RECUPERAR USUÁRIO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar o usuário.\n{e.Message}");
            }
        }
        public async Task<bool> Update(string userId) {
            try {
                User user = new User();
                user.Id = ObjectId.Parse(view.Id);
                user.Name = view.Name;
                user.AccessLevel = (AccessLevel)Enum.Parse(typeof(AccessLevel), view.AccessLevel);
                if (user.AccessLevel.ToString() == "Master") {
                    user.Hidden = true;
                }
                user.UserName = view.UserName;
                user.Password = view.Password;
                user.Image = ImageHandler.ImageToByteArray(view.Image);
                await user.Update(ObjectId.Parse(userId));
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR ALTERAR USUÁRIO",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar alterar o usuário.\n{e.Message}");
                return false;
            }
        }
        public async Task<bool> Delete(string userId) {
            try {
                User user = new User();
                user.Id = ObjectId.Parse(view.Id);
                await user.Delete(ObjectId.Parse(userId));
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR REMOVER USUÁRIO",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar remover o usuário.\n{e.Message}");
                return false;
            }
        }
        public async Task List(string userId, string fullName, string order = "Nome") {
            try {
                User user = new User();
                user.Name = fullName;
                DataTable data = await user.List(order);
                view.List = data;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR LISTAR USUÁRIOS.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar os usuários\n{e.Message}");
            }
        }
        public async Task<string> Login(string userName, string password) {
            try {
                User user = new User();
                user.UserName = userName;
                user.Password = password;
                ObjectId? userId = await user.Login();
                string result = userId == null ? null : userId.ToString();
                return result;
            } catch (Exception e) {
                new FormException(e.Message);
                return null;
            }
        }
    }
}
