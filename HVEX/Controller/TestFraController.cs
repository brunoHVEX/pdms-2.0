﻿using HVEX.Model;
using HVEX.Service;
using HVEX.View.Interface;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HVEX.Controller {
    class TestFraController {
        ITestFra view;
        Oscilloscope oscilloscope;
        TestFra test;
        public bool TestRunning { get; set; }

        public TestFraController(ITestFra frm) {
            view = frm;
        }

        public async Task<bool> Create(string userId) {
            try {
                if (test.Validate()) {
                    await test.Create(ObjectId.Parse(userId));
                    return true;
                } else {
                    Logger log = new Logger(
                        "ERRO AO TENTAR REGISTRAR NOVO ENSAIO.",
                        $"Ensaio não possui todos os parâmetros obrigatórios para ser persistido no banco de dados.",
                        Environment.MachineName,
                        DateTime.Now,
                        ObjectId.Parse(userId)
                    );
                    await log.Create();
                    return false;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR REGISTRAR ENSAIO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar registrar o ensaio.\n{e.Message}");
                return false;
            }
        }

        public async Task GetOsciloscope(string userId, string oscilloscopeId) {
            try {
                oscilloscope = new Oscilloscope();
                oscilloscope.Id = ObjectId.Parse(oscilloscopeId);
                await oscilloscope.Read();
                view.InstrumentIp = oscilloscope.Ip;
                view.InstrumentPort = oscilloscope.Port;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR RECUPERAR OSCILOSCÓPIO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar o osciloscópio.\n{e.Message}");
            }
        }

        public async Task ListTransformers(string userId) {
            Transformer trafo = new Transformer();
            DataTable data = await trafo.List("Número de Série");
        }

        public async Task ListOsciloscopes(string userId) {
            try {
                Oscilloscope oscilloscope = new Oscilloscope();
                DataTable data = await oscilloscope.List("Número de Série");
                if (data != null) {
                    data.Columns.Add("Descrição");
                    foreach (DataRow row in data.Rows) {
                        row["Descrição"] = row["Marca"].ToString() + " " + row["Modelo"].ToString();
                    }
                    data.Columns.Remove("Número de Série");
                    data.Columns.Remove("Marca");
                    data.Columns.Remove("Modelo");
                    view.OsciloscopeList = data;
                } else {
                    view.OsciloscopeList = null;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR LISTAR ENSAIOS.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar listar os osciloscópios.\n{e.Message}");
            }
        }

        public async Task<bool> Connect(string userId) {
            try {
                await oscilloscope.Read();
                if (oscilloscope.Ip != view.InstrumentIp) {
                    oscilloscope.Ip = view.InstrumentIp;
                }
                if (oscilloscope.Port != oscilloscope.Port) {
                    oscilloscope.Port = view.InstrumentPort;
                }
                await oscilloscope.Connect();
                view.InstrumentConnection = "Conectado";
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR SE CONECTAR AO OSCILOSCÓPIO {oscilloscope.Id}.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar se conectar ao osciloscópio.\n{e.Message}");
                view.InstrumentConnection = "Desconectado";
                return false;
            }
        }

        public async Task Disconnect(string userId) {
            try {
                if (oscilloscope != null && oscilloscope.Instrument != null) {
                    await oscilloscope.Read();
                    await oscilloscope.Disconnect();
                    view.InstrumentConnection = "Desconectado";
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR SE DESCONECTAR DO OSCILOSCÓPIO {oscilloscope.Id}.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar se desconectar do osciloscópio.\n{e.Message}");
                view.InstrumentConnection = "Desconectado";
            }
        }

        public async Task<bool> ConfigInitialSetup(string userId) {
            try {
                if (oscilloscope != null && oscilloscope.Instrument != null) {
                    await oscilloscope.ConfigureInitialSetup(
                        view.MinAmplitude,
                        MeasurementHandler.ConvertToHz(view.MinFrequency, view.MinFrequencyMeasurement)
                    );
                    return true;
                } else {
                    Logger log = new Logger(
                        $"ERRO AO TENTAR CONFIGURAR OSCILOSCÓPIO {oscilloscope.Id}.",
                        $"Objeto oscilloscope ou oscilloscope.Id não encontrado em TestController.",
                        DateTime.Now,
                        ObjectId.Parse(userId)
                    );
                    await log.Create();
                    new HardwareException($"Não foi possível localizar o osciloscópio.");
                    return false;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR CONFIGURAR OSCILOSCÓPIO {oscilloscope.Id}.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar aplicar as configurações iniciais ao osciloscópio.\n{e.Message}");
                return false;
            }
        }

        public async Task UpdateSignals(string userId, double voltage, double frequency) {
            int maxTries = 5;
            int tries = 1;
            while (tries <= maxTries) {
                try {
                    if (oscilloscope != null && oscilloscope.Instrument != null) {
                        await oscilloscope.UpdateSignals(
                           voltage,
                           view.MaxAmplitude,
                           frequency
                        );
                        await oscilloscope.DowngradeSignals(
                            voltage,
                           view.MaxAmplitude,
                           frequency
                            );
                        view.InputSignal = oscilloscope.GetSignal("Input");
                        view.OutputSignal = oscilloscope.GetSignal("Output");
                    } else {
                        Logger log = new Logger(
                            $"ERRO AO TENTAR ATUALIZAR O SINAL DO OSCILOSCÓPIO {oscilloscope.Instrument}.",
                            "Objecto oscilloscope ou oscilloscope.Instrument não encontrado em TestController.cs",
                            Environment.MachineName,
                            DateTime.Now,
                            ObjectId.Parse(userId)
                        );
                        await log.Create();
                        new HardwareException($"Não foi possível localizar o osciloscópio.");
                    }
                    tries = 6;
                } catch (Exception e) {
                    if (tries == maxTries) {
                        Logger log = new Logger(
                            $"ERRO AO TENTAR ATUALIZAR SINAL DO OSCILOSCÓPIO {oscilloscope.Id}.",
                            $"{e.Message}\n\n{e}",
                            Environment.MachineName,
                            DateTime.Now,
                            ObjectId.Parse(userId)
                        );
                        await log.Create();
                        new HardwareException($"Houve um erro ao tentar atualizar o sinal do osciloscópio com os novos valores de tensão e/ou frequência.");
                    }
                    tries++;
                }
            }
        }

        public async Task GetSignal(string userId, string type) {
            try {
                if (oscilloscope != null && oscilloscope.Instrument != null) {
                    if (type == "ENTRADA") {
                        var input = oscilloscope.GetSignal("Input");
                        view.InputSignal = input;
                    } else {
                        var output = oscilloscope.GetSignal("Output");
                        view.OutputSignal = output;
                    }
                } else {
                    Logger log = new Logger(
                        $"ERRO AO TENTAR RECUPERAR SINAL DE {type} DO OSCILOSCÓPIO {oscilloscope.Id}.",
                        $"Objeto oscillscope ou oscilloscope.Id não encontrado em TestController.cs.",
                        Environment.MachineName,
                        DateTime.Now,
                        ObjectId.Parse(userId)
                    );
                    await log.Create();
                    new HardwareException($"Não foi possível localizar o osciloscópio.");
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR RECUPERAR SINAL DE {type} DO OSCILOSCÓPIO {oscilloscope.Id}.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar recuperar o sinal do osciloscópio.\n{e.Message}");
            }
        }

        private bool CheckIfPowerOf10(double input) {
            try {
                while (input >= 10 && input % 10 == 0)
                    input /= 10;
                return input == 1;
            } catch {
                return false;
            }
        }

        public async Task<bool> RunTest(string userId) {
            test = new TestFra();

            List<double> tempX = new List<double>();
            List<double> tempFase = new List<double>();
            tempX.Clear();
            tempFase.Clear();

            if (await CheckInputslVoltage(view.MinAmplitude, view.MaxAmplitude) == false) {
                new HardwareException("Os campos de tensão de entrada e tensão de saída não podem ultrapassar 5V!");
            } else if (await CheckInitialVoltage(view.MinAmplitude, view.MinFrequency) == false) {
                new HardwareException("Ajuste a tensão de entrada do teste!");
            } else {
                try {
                    //Pegas os parâmetros definidos no formulário
                    double maxFrequency = MeasurementHandler.ConvertToHz(view.MaxFrequency, view.MaxFrequencyMeasurement);
                    double minFrequency = MeasurementHandler.ConvertToHz(view.MinFrequency, view.MinFrequencyMeasurement);
                    double frequencyRange = maxFrequency - minFrequency;
                    await oscilloscope.ConfigureInitialSetup(view.MinAmplitude, minFrequency);
                    double amplitudeRange = view.MaxAmplitude - view.MinAmplitude;
                    //Prenche um array com os pontos de frequência dentro do intervalo definido no formulário
                    int powers = minFrequency >= 100 ? Convert.ToInt32(Math.Truncate(Math.Log10(maxFrequency)) -
                                                                       Math.Truncate(Math.Log10(minFrequency))) :
                                                       Convert.ToInt32(Math.Truncate(Math.Log10(maxFrequency)) -
                                                                       Math.Truncate(Math.Log10(minFrequency)));
                    powers = CheckIfPowerOf10(maxFrequency) ? powers : powers - 1;
                    if (powers == -1) {
                        powers = 0;
                    }
                    int samplesPerInterval = CheckIfPowerOf10(maxFrequency) ? Convert.ToInt32(view.Samples / powers) : Convert.ToInt32(view.Samples / (powers + 1));
                    List<double> frequencyAxis = new List<double>();
                    //Prenche um array com os pontos de frequência dentro do intervalo definido no formulário
                    double highLimit = minFrequency;
                    for (int i = 0; i < powers; i++) {
                        double lowLimit = i == 0 ? minFrequency * Math.Pow(10, i) : highLimit;

                        highLimit = Math.Pow(10, Convert.ToInt32(Math.Truncate(Math.Log10(minFrequency)) + i + 1));

                        double interval = highLimit - lowLimit;
                        double step = interval / samplesPerInterval;
                        for (int j = 0; j < samplesPerInterval; j++) {
                            frequencyAxis.Add((lowLimit + (step * j)));
                        }
                    }
                    if (!CheckIfPowerOf10(maxFrequency)) {
                        for (int j = 1; j <= samplesPerInterval; j++) {
                            double step = (maxFrequency - highLimit) / samplesPerInterval;
                            frequencyAxis.Add(highLimit + step * j);
                        }
                    }
                    //Inicia os valores de aquisição no formulário
                    view.CurrentFrequency = 0;
                    view.CurrentInput = 0;
                    view.CurrentOutput = 0;
                    view.CurrentResult = 0;
                    view.CurrentPhase = 0;
                    test.Frequency = frequencyAxis;
                    int count = 0;
                    double[] auxPhase = new double[frequencyAxis.Count];
                    double[] auxAmplitude = new double[frequencyAxis.Count];

                    //Atualiza os sinais com cada posição no array de frequências enquanto o teste estiver rodando
                    while (count < frequencyAxis.Count && TestRunning) {
                        int waveCount = 0;

                        view.CurrentStatus = "Ajustando Tensão";

                        await UpdateSignals(userId, view.MinAmplitude, frequencyAxis[count]);

                        view.CurrentStatus = "Varrendo Frequência";

                        int totalWaves = count == 0 ? 25 : 50;
                        view.AnalysisTotal = totalWaves;
                        //Aguarda o oscilloscópio acumular o número definido de ondas analisadas
                        waveCount = oscilloscope.GetWaveCount();
                        while (waveCount < totalWaves) {
                            if (!TestRunning) {
                                Logger log = new Logger(
                                    $"ENSAIO INTERROMPIDO.",
                                    Environment.MachineName,
                                    DateTime.Now,
                                    ObjectId.Parse(userId)
                                );
                                await log.Create();
                                return false;
                            } else {
                                view.AnalysisProgress = waveCount;
                                waveCount = oscilloscope.GetWaveCount();
                            }
                        }
                        view.AnalysisProgress = totalWaves;
                        waveCount = 0;
                        //Calcula o valor do ganho em decibéis e a fase entre os dois sinais

                        double inputAmplitude = oscilloscope.GetInputAmplitude();
                        double outputAmplitude = oscilloscope.GetOutputAmplitude();
                        double phase = oscilloscope.GetPhaseBetweenSignals();
                        double amplitude = 20 * Math.Log10(outputAmplitude / inputAmplitude);
                        auxPhase[count] = phase;
                        auxAmplitude[count] = amplitude;

                        tempX.Add(inputAmplitude);
                        tempFase.Add(phase);

                        //Vefirifca se a tensão tem diferença de 10V entre as duas últimas medições
                        if (count > 2) {
                            double voltage1 = tempX[count];
                            double voltage2 = tempX[count - 1];
                            double fase1 = tempFase[count];
                            double fase2 = tempFase[count - 1];

                            if (Math.Abs(voltage2) < Math.Abs(voltage1 * 0.5) || Math.Abs(voltage2) > Math.Abs(voltage1 * 1.5) || phase > 180 || phase < -180 || (Math.Abs(fase2) > 30 && Math.Abs(fase2) > Math.Abs(fase1 * 2)) || (Math.Abs(fase2) > 30 && Math.Abs(fase2) < Math.Abs(fase1 * 0.5))) {
                                view.CurrentStatus = "Aguardando Amplificador";

                                Thread.Sleep(15000);
                                await UpdateSignals(userId, view.MinAmplitude, frequencyAxis[count]);
                                inputAmplitude = oscilloscope.GetInputAmplitude();
                                outputAmplitude = oscilloscope.GetOutputAmplitude();
                                phase = oscilloscope.GetPhaseBetweenSignals();
                                amplitude = 20 * Math.Log10(outputAmplitude / inputAmplitude);
                                auxPhase[count] = phase;
                                auxAmplitude[count] = amplitude;


                            }
                        } else {
                            Thread.Sleep(500);
                        }
                        //Verifica se as tensões de entrada e sáida estão entre 0 e 50V
                        if ((inputAmplitude > 0.02) && (inputAmplitude < 50) &&
                            (outputAmplitude > 0.02) && (outputAmplitude < 50)) {
                            test.Phase.Add(phase);
                            test.Amplitude.Add(amplitude);
                        } else {
                            test.Phase.Add(auxPhase[count - 1]);
                            test.Amplitude.Add(auxAmplitude[count - 1]);
                        }

                        view.CurrentStatus = "Varrendo Frequência";

                        inputAmplitude = oscilloscope.GetInputAmplitude();
                        outputAmplitude = oscilloscope.GetOutputAmplitude();
                        phase = oscilloscope.GetPhaseBetweenSignals();
                        amplitude = 20 * Math.Log10(outputAmplitude / inputAmplitude);
                        auxPhase[count] = phase;
                        auxAmplitude[count] = amplitude;

                        //Calcula a corrente RMS na saída, tensão RMS na entrada e fase entre os dois
                        //double rmsVoltageInput = (inputAmplitude / 2) / Math.Sqrt(2);
                        //double rmsVoltageOutput = (outputAmplitude / 2) / Math.Sqrt(2);
                        //double rmsPhase = oscilloscope.GetPhaseBetweenSignals();
                        //test.RmsPhase.Add(rmsPhase);
                        //Atualiza o formulário com os valores recuperados do osciloscópio]
                        view.CurrentFrequency = Math.Round(MeasurementHandler.ConvertTokHz(frequencyAxis[count], "Hz"), 2);
                        view.CurrentInput = Math.Round(inputAmplitude, 2);
                        view.CurrentOutput = Math.Round(outputAmplitude, 2);
                        view.CurrentResult = Math.Round(test.Amplitude[count], 2);
                        //view.Resistence = resistence;
                        view.CurrentPhase = Math.Round(test.Phase[count], 2);
                        view.CurrentSample = 1;
                        view.InjectedVoltage = await oscilloscope.GetInjectedVoltage();
                        count++;
                        view.CurrentSample = count;
                        view.AmplitudeSignal = new List<double>[2] { frequencyAxis, test.Amplitude };
                        view.PhaseSignal = new List<double>[2] { frequencyAxis, test.Phase };
                    }
                    view.CurrentStatus = "Ensaio Finalizado";
                    await StopTest();
                } catch (Exception e) {
                    await Disconnect(userId);
                    Logger log = new Logger(
                        $"ERRO AO TENTAR EXECUTAR ENSAIO.",
                        $"{e.Message}\n\n{e}",
                        Environment.MachineName,
                        DateTime.Now,
                        ObjectId.Parse(userId)
                    );
                    await log.Create();
                    new HardwareException($"Houve um erro durante a execução o ensaio.\n{e.Message}");
                    return false;
                }
            }
            test.Equipment = ObjectId.Parse(view.EquipmentId);
            test.Instrument = ObjectId.Parse(view.InstrumentId);
            await StopTest();
            if (await Create(userId)) {
                await StopTest();
                return true;
            } else {
                await StopTest();
                return false;
            }
        }

        public async Task StopTest() {
            if (oscilloscope != null) {
                TestRunning = false;
                await oscilloscope.StopTest();
            } else {
                TestRunning = false;
            }
        }

        public async Task<bool> CheckInitialVoltage(double initialVoltage, double initialFrequency) {
            if ((initialFrequency < 10000) && (initialVoltage > 0.5)) {
                return false;
            }
            if ((initialFrequency < 30000) && (initialVoltage > 0.6)) {
                return false;
            }
            if ((initialFrequency < 50000) && (initialVoltage > 0.7)) {
                return false;
            }
            if ((initialFrequency < 60000) && (initialVoltage > 0.8)) {
                return false;
            }
            if ((initialFrequency < 70000) && (initialVoltage > 0.9)) {
                return false;
            }
            if ((initialFrequency < 75000) && (initialVoltage > 1)) {
                return false;
            }
            if ((initialFrequency < 80000) && (initialVoltage > 1.1)) {
                return false;
            }
            if ((initialFrequency < 90000) && (initialVoltage > 1.2)) {
                return false;
            }
            if ((initialFrequency < 95000) && (initialVoltage > 1.3)) {
                return false;
            }
            if ((initialFrequency < 100000) && (initialVoltage > 1.4)) {
                return false;
            }
            if ((initialFrequency < 1000000) && (initialVoltage > 5)) {
                return false;
            }
            return true;
        }
        public async Task<bool> CheckInputslVoltage(double initialVoltage, double finalVoltage) {
            if ((finalVoltage > 5) || (initialVoltage > 5)) {
                return false;
            }
            return true;
        }
    }
}
