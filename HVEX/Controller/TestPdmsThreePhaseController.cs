﻿using HVEX.Model;
using HVEX.Service;
using HVEX.View.Interface;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HVEX.Controller {
    class TestPdmsThreePhaseController {

        ITestPdmsThreePhase view;
        OscilloscopePdms oscilloscope;
        TestPdmsThreePhase model = new TestPdmsThreePhase();

        public bool TestRunning { get; set; }
        //private int channels;
        private string channels;

        public TestPdmsThreePhaseController(ITestPdmsThreePhase frm) {
            view = frm;
        }
        public async Task ListOsciloscopes(string userId) {
            try {
                Oscilloscope oscilloscope = new Oscilloscope();
                DataTable data = await oscilloscope.List("Número de Série");
                if (data != null) {
                    data.Columns.Add("Descrição");
                    foreach (DataRow row in data.Rows) {
                        row["Descrição"] = row["Marca"].ToString() + " " + row["Modelo"].ToString();
                    }
                    data.Columns.Remove("Número de Série");
                    data.Columns.Remove("Marca");
                    data.Columns.Remove("Modelo");
                    view.OsciloscopeList = data;
                    view.Osciloscope = true;
                } else {
                    view.OsciloscopeList = null;
                    view.Osciloscope = false;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR LISTAR ENSAIOS.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar listar os osciloscópios.\n{e.Message}");
            }
        }

        public async Task Disconnect(string userId) {
            try {
                if (oscilloscope != null && oscilloscope.Instrument != null) {
                    await oscilloscope.Read();
                    await oscilloscope.Disconnect();
                    view.InstrumentConnection = "Desconectado";
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR SE DESCONECTAR DO OSCILOSCÓPIO {oscilloscope.Id}.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar se desconectar do osciloscópio.\n{e.Message}");
                view.InstrumentConnection = "Desconectado";
            }
        }

        public async Task GetOsciloscope(string userId, string oscilloscopeId) {
            try {
                oscilloscope = new OscilloscopePdms();
                oscilloscope.Id = ObjectId.Parse(oscilloscopeId);
                await oscilloscope.Read();
                view.InstrumentIp = oscilloscope.Ip;
                view.InstrumentPort = oscilloscope.Port;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR RECUPERAR OSCILOSCÓPIO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar o osciloscópio.\n{e.Message}");
            }
        }

        public async Task<bool> Connect(string userId) {
            try {
                await oscilloscope.Read();
                if (oscilloscope.Ip != view.InstrumentIp) {
                    oscilloscope.Ip = view.InstrumentIp;
                }
                if (oscilloscope.Port != oscilloscope.Port) {
                    oscilloscope.Port = view.InstrumentPort;
                }
                await oscilloscope.Connect();
                view.InstrumentConnection = "Conectado";
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR SE CONECTAR AO OSCILOSCÓPIO {oscilloscope.Id}.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar se conectar ao osciloscópio.\n{e.Message}");
                view.InstrumentConnection = "Desconectado";
                return false;
            }
        }

        public async Task<int> GetChannelsCount(string userId) {
            try {
                await oscilloscope.Read();
                return oscilloscope.Channels;
            } catch (Exception e) {
                Logger log = new Logger(
                    "ERRO AO TENTAR RECUPERAR QUANTIDADE DE CANAIS DO OSCILOSCÓPIO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new DatabaseException($"Houve um erro ao tentar recuperar o osciloscópio.\n{e.Message}");
                return 0;
            }
        }

        public async Task<bool> InitialConfigOscilloscope(string userId, int channels) {
            try {
                if (channels != 0) {
                    await oscilloscope.ConfigureInitialSetup(channels);
                    return true;
                } else {
                    Logger log = new Logger(
                        $"ERRO AO TENTAR CONFIGURAR OSCILOSCÓPIO {oscilloscope.Id}.",
                        $"Objeto oscilloscope ou oscilloscope.Id não encontrado em TestController.",
                        DateTime.Now,
                        ObjectId.Parse(userId)
                    );
                    await log.Create();
                    new HardwareException($"Não foi possível localizar o osciloscópio.");
                    return false;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR CONFIGURAR OSCILOSCÓPIO {oscilloscope.Id}.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar aplicar as configurações iniciais ao osciloscópio.\n{e.Message}");
                return false;
            }
        }

        public async Task<bool> CleanConfigOscilloscope(string userId, int channels) {
            try {
                if (channels != 0) {
                    await oscilloscope.CleanOscilloscopeSetupPdms(channels);
                    return true;
                } else {
                    Logger log = new Logger(
                        $"ERRO AO TENTAR CONFIGURAR OSCILOSCÓPIO {oscilloscope.Id}.",
                        $"Objeto oscilloscope ou oscilloscope.Id não encontrado em TestController.",
                        DateTime.Now,
                        ObjectId.Parse(userId)
                    );
                    await log.Create();
                    new HardwareException($"Não foi possível localizar o osciloscópio.");
                    return false;
                }
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR CONFIGURAR OSCILOSCÓPIO {oscilloscope.Id}.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar limpar as configurações iniciais ao osciloscópio.\n{e.Message}");
                return false;
            }
        }

        public async Task<bool> ReadOscilloscope(string userId, int channels) {
            try {
                int chan1 = 0;
                int chan2 = 0;
                int chan3 = 0;
                //Setando os canais
                switch (channels) {
                    case 1:
                        chan1 = 1;
                        break;
                    case 2:
                        chan1 = 2;
                        break;
                    case 3:
                        chan1 = 3;
                        break;
                    case 4:
                        chan1 = 4;
                        break;
                    case 6:
                        chan1 = 1;
                        chan2 = 2;
                        chan3 = 3;
                        break;
                    case 7:
                        chan1 = 1;
                        chan2 = 2;
                        chan3 = 4;
                        break;
                    case 8:
                        chan1 = 1;
                        chan2 = 3;
                        chan3 = 4;
                        break;
                    case 9:
                        chan1 = 2;
                        chan2 = 3;
                        chan3 = 4;
                        break;
                }
                //if (statusTest == 0) {
                    WaveForm wavePhase1 = oscilloscope.ReadWave(chan1);
                    WaveForm wavePhase2 = oscilloscope.ReadWave(chan2);
                    WaveForm wavePhase3 = oscilloscope.ReadWave(chan3);

                   // WaveForm teste = new WaveForm();
                    //teste.

                    string filter = view.Filter;

                    double frequencyUndercut = await ConvertFrequency(view.FrequencyUndercut, view.UnitFrequencyUndercut);

                    double frequencyTopCut = await ConvertFrequency(view.FrequencyTopCut, view.UnitFrequencyTopCut);

                    view.SignalPhase1 = wavePhase1;
                    view.SignalPhase2 = wavePhase2;
                    view.SignalPhase3 = wavePhase3;

                    double gainPhase1 = view.GainPhase1;
                    double gainPhase2 = view.GainPhase2;
                    double gainPhase3 = view.GainPhase3;

                    WaveForm waveFilteredPhase1 = oscilloscope.WaveFilter(wavePhase1, filter, frequencyTopCut, frequencyUndercut, view.Order, gainPhase1, chan1, channels);
                    WaveForm waveFilteredPhase2 = oscilloscope.WaveFilter(wavePhase2, filter, frequencyTopCut, frequencyUndercut, view.Order, gainPhase2, chan2, channels);
                    WaveForm waveFilteredPhase3 = oscilloscope.WaveFilter(wavePhase3, filter, frequencyTopCut, frequencyUndercut, view.Order, gainPhase3, chan3, channels);

                    List<double> pureWaveAxisYPhase1 = oscilloscope.filterType(wavePhase1, filter, frequencyTopCut, frequencyUndercut, view.Order);
                    List<double> pureWaveAxisYPhase2 = oscilloscope.filterType(wavePhase2, filter, frequencyTopCut, frequencyUndercut, view.Order);
                    List<double> pureWaveAxisYPhase3 = oscilloscope.filterType(wavePhase3, filter, frequencyTopCut, frequencyUndercut, view.Order);

                    WaveForm pureWavePhase1 = oscilloscope.PureWave(wavePhase1, filter, frequencyTopCut, frequencyUndercut, view.Order);
                    WaveForm pureWavePhase2 = oscilloscope.PureWave(wavePhase2, filter, frequencyTopCut, frequencyUndercut, view.Order);
                    WaveForm pureWavePhase3 = oscilloscope.PureWave(wavePhase3, filter, frequencyTopCut, frequencyUndercut, view.Order);

                    view.SignalFiltredPhase1 = waveFilteredPhase1;
                    view.SignalFiltredPhase2 = waveFilteredPhase2;
                    view.SignalFiltredPhase3 = waveFilteredPhase3;

                    List<double>[] dispersionPhase1 = oscilloscope.Dispersion(waveFilteredPhase1, view.GatePhase1, pureWavePhase1, chan1, channels);
                    List<double>[] dispersionPhase2 = oscilloscope.Dispersion(waveFilteredPhase2, view.GatePhase2, pureWavePhase2, chan2, channels);
                    List<double>[] dispersionPhase3 = oscilloscope.Dispersion(waveFilteredPhase3, view.GatePhase3, pureWavePhase3, chan3, channels);

                    view.DispersionPhase1 = new List<double>[4] { dispersionPhase1[0], dispersionPhase1[1], pureWavePhase1.x, pureWavePhase1.y };
                    view.DispersionPhase2 = new List<double>[4] { dispersionPhase2[0], dispersionPhase2[1], pureWavePhase2.x, pureWavePhase2.y };
                    view.DispersionPhase3 = new List<double>[4] { dispersionPhase3[0], dispersionPhase3[1], pureWavePhase3.x, pureWavePhase3.y };

                    view.DispersionDetailedPhase1 = new List<double>[4] { dispersionPhase1[0], dispersionPhase1[2], pureWavePhase1.x, pureWavePhase1.y };
                    view.DispersionDetailedPhase2 = new List<double>[4] { dispersionPhase2[0], dispersionPhase2[2], pureWavePhase2.x, pureWavePhase2.y };
                    view.DispersionDetailedPhase3 = new List<double>[4] { dispersionPhase3[0], dispersionPhase3[2], pureWavePhase3.x, pureWavePhase3.y };

                    List<double>[] PartialDischargeTrendPhase1 = oscilloscope.PartialDischargeTrend(oscilloscope.partialDischargesPhase1);
                    List<double>[] PartialDischargeTrendPhase2 = oscilloscope.PartialDischargeTrend(oscilloscope.partialDischargesPhase2);
                    List<double>[] PartialDischargeTrendPhase3 = oscilloscope.PartialDischargeTrend(oscilloscope.partialDischargesPhase3);

                    view.PartialDischargeTrendPhase1 = PartialDischargeTrendPhase1;
                    view.PartialDischargeTrendPhase2 = PartialDischargeTrendPhase2;
                    view.PartialDischargeTrendPhase3 = PartialDischargeTrendPhase3;

                    view.FftPhase1 = oscilloscope.AcquireFFt(wavePhase1, filter, frequencyTopCut, frequencyUndercut, view.Order);
                    view.FftPhase2 = oscilloscope.AcquireFFt(wavePhase2, filter, frequencyTopCut, frequencyUndercut, view.Order);
                    view.FftPhase3 = oscilloscope.AcquireFFt(wavePhase3, filter, frequencyTopCut, frequencyUndercut, view.Order);

                    if ((oscilloscope.actualMaxValuePhase1 != 0) && (!Double.IsNaN(oscilloscope.actualMaxValuePhase1))) {
                        view.ChargePhase1 = Math.Round(oscilloscope.actualMaxValuePhase1 * 10e11, 2);
                    }
                    if (oscilloscope.actualPartialDischargePhase1 != 0) {
                        view.OccurrencesPhase1 = oscilloscope.actualPartialDischargePhase1;
                    }

                    if ((oscilloscope.actualMaxValuePhase2 != 0) && (!Double.IsNaN(oscilloscope.actualMaxValuePhase2))) {
                        view.ChargePhase2 = Math.Round(oscilloscope.actualMaxValuePhase2 * 10e11, 2);
                    }
                    if (oscilloscope.actualPartialDischargePhase2 != 0) {
                        view.OccurrencesPhase2 = oscilloscope.actualPartialDischargePhase2;
                    }

                    if ((oscilloscope.actualMaxValuePhase3 != 0) && (!Double.IsNaN(oscilloscope.actualMaxValuePhase3))) {
                        view.ChargePhase3 = Math.Round(oscilloscope.actualMaxValuePhase3 * 10e11, 2);
                    }
                    if (oscilloscope.actualPartialDischargePhase3 != 0) {
                        view.OccurrencesPhase3 = oscilloscope.actualPartialDischargePhase3;
                    }

                    double actualMaxPositiveValue1 = 0;
                    double actualMaxPositiveValue2 = 0;
                    double actualMaxPositiveValue3 = 0;
                    if ((oscilloscope.actualMaxPositiveValuePhase1 != 0) && (!Double.IsNaN(oscilloscope.actualMaxPositiveValuePhase1))) {
                        actualMaxPositiveValue1 = oscilloscope.actualMaxPositiveValuePhase1;   
                    }
                    if ((oscilloscope.actualMaxPositiveValuePhase2 != 0) && (!Double.IsNaN(oscilloscope.actualMaxPositiveValuePhase2))) {
                        actualMaxPositiveValue2 = oscilloscope.actualMaxPositiveValuePhase2;
                    }
                    if ((oscilloscope.actualMaxPositiveValuePhase3 != 0) && (!Double.IsNaN(oscilloscope.actualMaxPositiveValuePhase3))) {
                        actualMaxPositiveValue3 = oscilloscope.actualMaxPositiveValuePhase3;
                    }
                    if ((actualMaxPositiveValue1 > actualMaxPositiveValue2) && (actualMaxPositiveValue1 > actualMaxPositiveValue3)) {
                        view.TotalPositiveDischarges = Math.Round(oscilloscope.actualMaxPositiveValuePhase1 * 10e11, 2);
                    } else if ((actualMaxPositiveValue2 > actualMaxPositiveValue1) && (actualMaxPositiveValue2 > actualMaxPositiveValue3)) {
                        view.TotalPositiveDischarges = Math.Round(oscilloscope.actualMaxPositiveValuePhase2 * 10e11, 2);
                    } else if ((actualMaxPositiveValue3 > actualMaxPositiveValue1) && (actualMaxPositiveValue3 > actualMaxPositiveValue2)) {
                        view.TotalPositiveDischarges = Math.Round(oscilloscope.actualMaxPositiveValuePhase3 * 10e11, 2);
                    }

                    double actualMaxNegativeValue1 = 0;
                    double actualMaxNegativeValue2 = 0;
                    double actualMaxNegativeValue3 = 0;
                    if ((oscilloscope.actualMaxNegativeValuePhase1 != 0) && (!Double.IsNaN(oscilloscope.actualMaxNegativeValuePhase1))) {
                        actualMaxNegativeValue1 = oscilloscope.actualMaxNegativeValuePhase1;
                    }
                    if ((oscilloscope.actualMaxNegativeValuePhase2 != 0) && (!Double.IsNaN(oscilloscope.actualMaxNegativeValuePhase2))) {
                        actualMaxNegativeValue2 = oscilloscope.actualMaxNegativeValuePhase2;
                    }
                    if ((oscilloscope.actualMaxNegativeValuePhase3 != 0) && (!Double.IsNaN(oscilloscope.actualMaxNegativeValuePhase3))) {
                        actualMaxNegativeValue3 = oscilloscope.actualMaxNegativeValuePhase3;
                    }
                    if ((actualMaxNegativeValue1 > actualMaxNegativeValue2) && (actualMaxNegativeValue1 > actualMaxNegativeValue3)) {
                        view.TotalNegativeDischarges = Math.Round(oscilloscope.actualMaxNegativeValuePhase1 * 10e11, 2);
                    } else if ((actualMaxNegativeValue2 > actualMaxNegativeValue1) && (actualMaxNegativeValue2 > actualMaxNegativeValue3)) {
                        view.TotalNegativeDischarges = Math.Round(oscilloscope.actualMaxNegativeValuePhase2 * 10e11, 2);
                    } else if ((actualMaxNegativeValue3 > actualMaxNegativeValue1) && (actualMaxNegativeValue3 > actualMaxNegativeValue2)) {
                        view.TotalNegativeDischarges = Math.Round(oscilloscope.actualMaxNegativeValuePhase3 * 10e11, 2);
                    }

                    view.TotalOccurrences = oscilloscope.actualPartialDischargePhase1 + oscilloscope.actualPartialDischargePhase2 + oscilloscope.actualPartialDischargePhase3;

                    return true;
                //} else {
                //    Logger log = new Logger(
                //        $"ERRO AO TENTAR CONFIGURAR OSCILOSCÓPIO {oscilloscope.Id}.",
                //        $"Objeto oscilloscope ou oscilloscope.Id não encontrado em TestController.",
                //        DateTime.Now,
                //        ObjectId.Parse(userId)
                //    );
                //    await log.Create();
                //    new HardwareException($"Não foi possível localizar o osciloscópio.");
                //    return false;
                //}
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR CONFIGURAR OSCILOSCÓPIO {oscilloscope.Id}.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar aquisitar as ondas do osciloscópio.\n{e.Message}");
                return false;
            }
        }

        public async Task<bool> RunTest(string userId) {
            try {
                //channels = await GetChannelsCount(userId);
                channels = view.Channels;
                int channel = 0;
                switch (channels) {
                    case "Canais 1,2,3":
                        channel = 6;
                        break;
                    case "Canais 1,2,4":
                        channel = 7;
                        break;
                    case "Canais 1,3,4":
                        channel = 8;
                        break;
                    case "Canais 2,3,4":
                        channel = 9;
                        break;
                }
                await InitialConfigOscilloscope(userId, channel);
                while (TestRunning){
                    await ReadOscilloscope(userId, channel);
                }
                return true;
            } catch {
                return false;
            }
        }

        public async Task<bool> StopTest(string userId) {
            try {
                //channels = await GetChannelsCount(userId);
                channels = view.Channels;
                int channel = 0;
                switch (channels) {
                    case "Canais 1,2,3":
                        channel = 6;
                        break;
                    case "Canais 1,2,4":
                        channel = 7;
                        break;
                    case "Canais 1,3,4":
                        channel = 8;
                        break;
                    case "Canais 2,3,4":
                        channel = 9;
                        break;
                }
                await CleanConfigOscilloscope(userId, channel);
                return true;
            } catch {
                return false;
            }
        }

        public async Task<double> ConvertFrequency(double value, string unit) {
            try {
                double result = 0.0;
                switch (unit) { 
                    case "Hz":
                        result = value;
                        break;
                    case "kHz":
                        result = value * 1000;
                        break;
                    case "MHz":
                        result = value * 1000000;
                        break;
                    case "GHz":
                        result = value * 1000000000;
                        break;
                }
                return result;
            } catch {
                return 0.0;
            }
        }

        public async Task UpGain(double gain, int channel) {
            try {
                oscilloscope.calibrating = true;
                switch (channel){
                    case 1:
                        view.GainPhase1 = oscilloscope.UpdateGain(gain);
                        view.GatePhase1 = oscilloscope.UpdateGate(channel);
                        break;
                    case 2:
                        view.GainPhase2 = oscilloscope.UpdateGain(gain);
                        view.GatePhase2 = oscilloscope.UpdateGate(channel);
                        break;
                    case 3:
                        view.GainPhase3 = oscilloscope.UpdateGain(gain);
                        view.GatePhase3 = oscilloscope.UpdateGate(channel);
                        break;
                }
                oscilloscope.calibrating = false;
            } catch {
                
            }
        }

        public async Task<bool> SaveConfigurations (string userId) {
            try {
                List<double> gain = new List<double>();
                gain.Add(view.GainPhase1);
                gain.Add(view.GainPhase2);
                gain.Add(view.GainPhase3);
                List<double> gate = new List<double>();
                gate.Add(view.GatePhase1);
                gate.Add(view.GatePhase2);
                gate.Add(view.GatePhase3);
                int filter = 0;
                if(view.Filter == "Passa Baixa") {
                    filter = 0;
                } else if (view.Filter == "Passa Alta") {
                    filter = 1;
                } else if (view.Filter == "Passa Banda") {
                    filter = 2;
                } else if (view.Filter == "Passa Faixa") {
                    filter = 3;
                }
                double order = view.Order;
                double unitFrequencyUndercut = 0;
                if (view.UnitFrequencyUndercut == "Hz") {
                    unitFrequencyUndercut = 0;
                } else if (view.UnitFrequencyUndercut == "kHz") {
                    unitFrequencyUndercut = 1;
                } else if (view.UnitFrequencyUndercut == "MHz") {
                    unitFrequencyUndercut = 2;
                } else if (view.UnitFrequencyUndercut == "GHz") {
                    unitFrequencyUndercut = 3;
                }
                List<double> frequencyUndercut = new List<double>();
                frequencyUndercut.Add(view.FrequencyUndercut);
                frequencyUndercut.Add(unitFrequencyUndercut);
                double unitFrequencyTopcut = 0;
                if (view.UnitFrequencyTopCut == "Hz") {
                    unitFrequencyTopcut = 0;
                } else if (view.UnitFrequencyTopCut == "kHz") {
                    unitFrequencyTopcut = 1;
                } else if (view.UnitFrequencyTopCut == "MHz") {
                    unitFrequencyTopcut = 2;
                } else if (view.UnitFrequencyTopCut == "GHz") {
                    unitFrequencyTopcut = 3;
                }
                List<double> frequencyTopCut = new List<double>();
                frequencyTopCut.Add(view.FrequencyTopCut);
                frequencyTopCut.Add(unitFrequencyTopcut);
                List<double> calibration = new List<double>();
                calibration.Add(view.CalibrationPhase1);
                calibration.Add(view.CalibrationPhase2);
                calibration.Add(view.CalibrationPhase3);
                ConfigurationThreePhase configurationThreePhase = new ConfigurationThreePhase(
                    ObjectId.Parse(view.IdConfiguration),
                    gain,
                    gate,
                    filter,
                    order,
                    frequencyUndercut,
                    frequencyTopCut,
                    calibration);
                await configurationThreePhase.Update(ObjectId.Parse(userId));
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR SALVAR AS CONFIGURAÇÕES DO TESTE TRIFÁSICO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar salvar as configurações do teste trifásico.\n{e.Message}");
                return false;
            }
        }


        public async Task<bool> ResetConfigurations(string userId) {
            try {
                List<double> gain = new List<double>();
                gain.Add(100);
                gain.Add(100);
                gain.Add(100);
                List<double> gate = new List<double>();
                gate.Add(0.5);
                gate.Add(0.5);
                gate.Add(0.5);
                int filter = 2;
                double order = 500;
                List<double> frequencyUndercut = new List<double>();
                frequencyUndercut.Add(40);
                frequencyUndercut.Add(1);
                List<double> frequencyTopCut = new List<double>();
                frequencyTopCut.Add(1);
                frequencyTopCut.Add(2);
                List<double> calibration = new List<double>();
                calibration.Add(0);
                calibration.Add(0);
                calibration.Add(0);
                ConfigurationThreePhase configurationThreePhase = new ConfigurationThreePhase(
                    ObjectId.Parse(view.IdConfiguration),
                    gain,
                    gate,
                    filter,
                    order,
                    frequencyUndercut,
                    frequencyTopCut,
                    calibration);
                await configurationThreePhase.Update(ObjectId.Parse(userId));

                view.GainPhase1 = 100;
                view.GainPhase2 = 100;
                view.GainPhase3 = 100;
                view.GatePhase1 = 0.5;
                view.GatePhase2 = 0.5;
                view.GatePhase3 = 0.5;
                view.Filter = "Passa Banda";
                view.Order = 500;
                view.FrequencyUndercut = 40;
                view.UnitFrequencyUndercut = "kHz";
                view.FrequencyTopCut = 1;
                view.UnitFrequencyTopCut = "MHz";
                view.CalibrationPhase1 = 0;
                view.CalibrationPhase2 = 0;
                view.CalibrationPhase3 = 0;
                return true;
            } catch (Exception e) {
                Logger log = new Logger(
                    $"ERRO AO TENTAR RESETAR AS CONFIGURAÇÕES DO TESTE TRIFÁSICO.",
                    $"{e.Message}\n\n{e}",
                    Environment.MachineName,
                    DateTime.Now,
                    ObjectId.Parse(userId)
                );
                await log.Create();
                new HardwareException($"Houve um erro ao tentar resetar as configurações do teste trifásico.\n{e.Message}");
                return false;
            }
        }
    }
}
