﻿using HVEX.Model;
using HVEX.Service;
using HVEX.View.Interface;
using HVEX.View.SubForms;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Controller {
    class LoggerController {

        private ILog view;

        public LoggerController(ILog frm) {
            view = frm;
        }

        public async Task Read() {
            Logger log = new Logger();
            log.Id = ObjectId.Parse(view.Id);
            await log.Read();
            if (log.Error != null) {
                FrmErrorDescription frm = new FrmErrorDescription();
                frm.Id = log.Id.ToString();
                frm.Description = log.Description;
                frm.Error = log.Error;
                frm.Source = log.Source;
                frm.Date = log.Date;
                frm.User = log.UserId.ToString();
                frm.Show();
            }
        }

        public async Task List(
            string description,
            DateTime? since = null,
            DateTime? until = null,
            string order = "Data"
        ) {
            if (since == null) {
                since = new DateTime(1901, 01, 01);
            }
            if (until == null) {
                until = new DateTime(2100, 01, 01);
            }
            try {
                Logger log = new Logger();
                log.Description = description;
                DataTable data = await log.List(order, since, until);
                view.List = data;
            } catch (Exception e) {
                new DatabaseException($"Houve um erro ao tentar recuperar os logs do banco de dados.\n{e.Message}");
            }
        }
    }
}
