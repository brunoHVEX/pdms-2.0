﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model {
    public class GraphNode {
          public int numConnections { get; set; } = 0;
        public double x { get; set; }
        public double y { get; set; }
        public int index { get; set; }

        public bool plot { get; set; } = true;

        public void AddNeighbor()
        {
            numConnections++;
        }

        public GraphNode(double x, double y, int index)
        {
            this.x = x;
            this.y = y;
            this.index = index;
        }
    }

    public class Graph : List<GraphNode>
    {

        public double maxDistance { get; set; }

        public void addNode(double x, double y, int index)
        {
            GraphNode newNode = new GraphNode(x, y, index);

            foreach (GraphNode node in this)
            {
                double distance = Math.Sqrt(Math.Pow((x - node.x), 2) + Math.Pow(y - node.y, 2));
                if(distance < maxDistance)
                {
                    newNode.plot = false;
                    newNode.AddNeighbor();
                    node.AddNeighbor();
                }
            }
            if(this.Count < 1000) this.Add(newNode);
        }

        public double Max()
        {
            double max = 0;
            foreach (GraphNode node in this)
            {
                if (Math.Abs(node.y) > Math.Abs(max))
                    max = node.y;
            }
            return max;
        }

        public double[] posNegMax()
        {
            double[] posNeg = { 0, 0 };
            foreach (GraphNode node in this)
            {
                if (node.y > posNeg[0])
                    posNeg[0] = node.y;
                else if (node.y < posNeg[1])
                    posNeg[1] = node.y;
            }
            return posNeg;
        }

        public double MaxMedia()
        {
            double sum = 0;
            List<GraphNode> list = this.OrderBy(a => a.y).ToList();
            int n = list.Count >= 5 ? 5 : list.Count;

            for(int i=0; i<n; i++)
            {
                sum += list[i].y;
            }

            return sum / n;
        }

        public double[] getValues()
        {
            // 0 - Maximum Positive charge
            // 1 - Maximum Negative charge
            // 2 - Number os discharges

            double[] values = new double[3];

            values[0] = 0;
            values[1] = 0;

            values[2] = this.Count;

            for(int i=0; i < values[2]; i++)
            {
                if(this.ElementAt(i).y > values[0])
                {
                    values[0] = this.ElementAt(i).y;
                }
                else if(this.ElementAt(i).y < values[1])
                {
                    values[1] = this.ElementAt(i).y;
                }
            }

            return values;
        }

        public List<GraphNode> GetOrdered()
        {
            return this.OrderBy(a => a.y).ToList();
        }
    }
}
