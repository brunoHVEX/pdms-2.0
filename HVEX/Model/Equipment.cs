﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace HVEX.Model {
    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(
        typeof(Transformer),
        typeof(GenericEquipment)
    )]
    abstract class Equipment {
        static protected readonly string collectionName = "equipment";

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("serialNumber")]
        public string SerialNumber { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("model")]
        public string Model { get; set; }

        [BsonElement("deleted")]
        public bool Deleted { get; set; }

        public Equipment() {
            Id = new ObjectId();
            Deleted = false;
        }

        public Equipment(string serialNumber, string model, string description) {
            Id = new ObjectId();
            SerialNumber = serialNumber;
            Description = description;
            Model = model;
            Deleted = false;
        }

        public virtual async Task<DataTable> List(string order) {
            try {
                List<Equipment> equipments = new List<Equipment>();
                switch (order) {
                    case "Número de Série":
                        order = "serialNumber";
                        break;
                    case "Descrição":
                        order = "description";
                        break;
                }
                List<Equipment> collection;
                if (!string.IsNullOrEmpty(SerialNumber) && !string.IsNullOrEmpty(Description)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Equipment>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower()) &&
                            t.Description.ToLower().Contains(Description.ToLower())
                        )
                    );
                } else if (!string.IsNullOrEmpty(SerialNumber)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Equipment>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower())
                        )
                    );
                } else if (!string.IsNullOrEmpty(Description)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Equipment>.Filter.Where(
                            t => !t.Deleted &&
                            t.Description.ToLower().Contains(Description.ToLower())
                        )
                    );
                } else {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Equipment>.Filter.Where(
                            t => !t.Deleted
                        )
                    );
                }
                equipments.AddRange(collection);
                DataTable table = new DataTable();
                table.Columns.Add("Id");
                table.Columns.Add("Tipo");
                table.Columns.Add("Número de Série");
                table.Columns.Add("Descrição");
                foreach (Equipment equipment in equipments) {
                    DataRow row = table.NewRow();
                    row.BeginEdit();
                    row["Id"] = equipment.Id;
                    if (equipment is Transformer) {
                        row["Tipo"] = "Transformer";
                    } else {
                        row["Tipo"] = "Generic";
                    }
                    row["Número de Série"] = equipment.SerialNumber;
                    row["Descrição"] = equipment.Description;
                    row.EndEdit();
                    table.Rows.Add(row);
                }
                return table;
            } catch (Exception e) {
                throw e;
            }
        }

        public abstract Task Create(ObjectId userId);
        public abstract Task Read();
        public abstract Task Update(ObjectId userId);
        public abstract Task Delete(ObjectId userId);
    }
}
