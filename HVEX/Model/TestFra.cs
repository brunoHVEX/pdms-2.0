﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVEX.Model {
    class TestFra : Test {

        public List<double> Frequency { get; set; }

        public List<double> Amplitude { get; set; }

        public List<double> Phase { get; set; }

        public TestFra() {
            Id = new ObjectId();
            Deleted = false;
        }

        public TestFra(ObjectId equipment,
                       ObjectId instrument,
                       ObjectId user,
                       DateTime date,
                       DateTime duration,
                       List<double> frequency,
                       List<double> amplitude,
                       List<double> phase) : base(equipment,
                                                  instrument,
                                                  user,
                                                  date,
                                                  duration) {
            Id = new ObjectId();
            Frequency = frequency;
            Amplitude = amplitude;
            Phase = phase;
            Deleted = false;
        }
        override public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"NOVO ENSAIO REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<TestFra>.Filter.Eq(u => u.Id, Id)
                );
                var test = collection.FirstOrDefault();
                if (test != null) {
                    Instrument = test.Instrument;
                    User = test.User;
                    Frequency = test.Frequency;
                    Amplitude = test.Amplitude;
                    Phase = test.Phase;
                    Date = test.Date;
                    Duration = test.Duration;
                    Deleted = test.Deleted;
                } else {
                    throw new Exception("Nenhum ensaio, que atenda aos parâmetros especificados, foi encontrado.");
                }
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"ENSAIO {Id} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<TestFra>.Filter.Eq(t => t.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"ENSAIO {Id} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<TestFra>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
        public bool Validate() {
            if (Instrument == null) {
                return false;
            }
            if (User == null) {
                return false;
            }
            if (Date == null) {
                return false;
            }
            if (Duration == null) {
                return false;
            }
            if (Frequency == null) {
                return false;
            }
            if (Amplitude == null) {
                return false;
            }
            if (Phase == null) {
                return false;
            }
            return true;
        }
    }
}
