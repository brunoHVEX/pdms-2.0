﻿using HVEX.Model.Enum;
using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model {
    class User {
        static private readonly string collectionName = "user";

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("userName")]
        public string UserName { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("accessLevel")]
        public AccessLevel AccessLevel { get; set; }

        [BsonElement("image")]
        public byte[] Image { get; set; }

        [BsonElement("deleted")]
        public bool Deleted { get; set; }

        [BsonElement("hidden")]
        public bool Hidden { get; set; }

        public User() {
            Id = new ObjectId();
            Deleted = false;
        }

        public User(string name, string username, string password, string accessLevel, byte[] image = null) {
            Id = new ObjectId();
            Name = name;
            UserName = username;
            Password = password;
            AccessLevel = (AccessLevel)System.Enum.Parse(typeof(AccessLevel), accessLevel, true);
            Image = image;
            Deleted = false;
            Hidden = false;
        }
        public User(string name, string username, string password, string accessLevel, bool hidden, byte[] image = null) {
            Id = new ObjectId();
            Name = name;
            UserName = username;
            Password = password;
            AccessLevel = (AccessLevel)System.Enum.Parse(typeof(AccessLevel), accessLevel, true);
            Image = image;
            Deleted = false;
            Hidden = hidden;
        }

        public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"USUÁRIO {UserName} REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                Password = EncryptPassword(Password);
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task CreateUserMaster(ObjectId userId) {
            try {
                Password = EncryptPassword(Password);
                await Mongo.Create(collectionName, this);
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<User>.Filter.Eq(u => u.Id, Id)
                );
                var user = collection.FirstOrDefault();
                if (user != null) {
                    Id = user.Id;
                    Name = user.Name;
                    UserName = user.UserName;
                    AccessLevel = user.AccessLevel;
                    Image = user.Image;
                } else {
                    if (Id.ToString() == "000000000000000000000000") {
                        Name = "Sistema";
                        UserName = "system";
                        AccessLevel = AccessLevel.Administrador;
                        Image = ImageHandler.ImageToByteArray(Properties.Resources.user);
                    } else {
                        throw new Exception("Nenhum usuário, que atenda aos parâmetros especificados, foi encontrado.");
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"USUÁRIO {UserName} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                Password = EncryptPassword(Password);
                await Mongo.Update(collectionName, Builders<User>.Filter.Eq(u => u.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"USUÁRIO {UserName} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<User>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task<DataTable> List(string order) {
            List<User> users = new List<User>();
            switch (order) {
                case "Nome":
                    order = "name";
                    break;
                case "Usuário":
                    order = "username";
                    break;
                case "Nível de Acesso":
                    order = "access_level";
                    break;
            }
            try {
                List<User> collection;
                if (string.IsNullOrEmpty(Name)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<User>.Filter.Where(u => !u.Deleted && !u.Hidden),
                        1000,
                        order
                    );
                } else {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<User>.Filter.Where(
                            u => !u.Deleted && !u.Hidden && u.Name.ToLower().Contains(Name.ToLower())
                        ),
                        1000,
                        order
                    );

                }
                users.AddRange(collection);
                DataTable table = new DataTable();
                table.Columns.Add(new DataColumn("Id"));
                table.Columns.Add(new DataColumn("Nome"));
                table.Columns.Add(new DataColumn("Usuário"));
                table.Columns.Add(new DataColumn("Nível de Acesso"));
                foreach (User user in users) {
                    DataRow row = table.NewRow();
                    row.BeginEdit();
                    row["Id"] = user.Id;
                    row["Nome"] = user.Name;
                    row["Usuário"] = user.UserName;
                    row["Nível de Acesso"] = user.AccessLevel;
                    row.EndEdit();
                    table.Rows.Add(row);
                }
                return table;
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task<ObjectId?> Login() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<User>.Filter.Eq(u => u.UserName, UserName) &
                    Builders<User>.Filter.Eq(u => u.Password, EncryptPassword(Password)) &
                    Builders<User>.Filter.Where(u => !u.Deleted)
                );
                if (collection.FirstOrDefault() != null) {
                    var userId = collection.FirstOrDefault().Id;
                    Logger log = new Logger(
                        $"ACESSO de {UserName}.",
                        Environment.MachineName,
                        DateTime.Now,
                        userId
                    );
                    return userId;
                } else {
                    new FormException("Login e/ou Senha incorretos! Verifique os dados inseridos e tente novamente.");
                    return null;
                }
            } catch (Exception e) {
                throw e;
            }
        }

        private string EncryptPassword(string pass) {
            MD5 md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(pass));
            byte[] result = md5.Hash;
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < result.Length; i++) {
                builder.Append(result[i].ToString("x2"));
            }
            return builder.ToString();
        }
    }
}
