﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using RohdeSchwarz.RsInstrument;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace HVEX.Model {
    class Oscilloscope : Instruments {

        private double updatedVoltage = 0;

        [BsonElement("channels")]
        public int Channels { get; set; }

        [BsonElement("instrument")]
        public RsInstrument Instrument { get; set; }

        public Oscilloscope() {
            Id = new ObjectId();
            Deleted = false;
        }
        public Oscilloscope(
            string serialNumber,
            string description,
            string brand,
            string model,
            string ip,
            int port,
            int channels) : base(
                serialNumber,
                description,
                brand,
                model,
                ip,
                port) {
            Id = new ObjectId();
            SerialNumber = serialNumber;
            Description = description;
            Brand = brand;
            Model = model;
            Ip = ip;
            Port = port;
            Channels = channels;
            Deleted = false;
        }
        override public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"OSCILOSCÓPIO {SerialNumber} REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<Oscilloscope>.Filter.Eq(u => u.Id, Id)
                );
                var osciloscope = collection.FirstOrDefault();
                if (osciloscope != null) {
                    SerialNumber = osciloscope.SerialNumber;
                    Description = osciloscope.Description;
                    Brand = osciloscope.Brand;
                    Model = osciloscope.Model;
                    Ip = osciloscope.Ip;
                    Port = osciloscope.Port;
                    Channels = osciloscope.Channels;
                } else {
                    throw new Exception("Nenhum osciloscópio, que atenda aos parâmetros especificados, foi encontrado.");
                }
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"OSCILOSCÓPIO {SerialNumber} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<Oscilloscope>.Filter.Eq(o => o.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"OSCILOSCÓPIO {SerialNumber} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<Oscilloscope>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task<DataTable> List(string order) {
            List<Oscilloscope> osciloscopes = new List<Oscilloscope>();
            switch (order) {
                case "Número de Série":
                    order = "serial_number";
                    break;
                case "Marca":
                    order = "brand";
                    break;
                case "Modelo":
                    order = "model";
                    break;
            }
            try {
                List<Oscilloscope> collection;
                if (!string.IsNullOrEmpty(SerialNumber) && !string.IsNullOrEmpty(Brand)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Oscilloscope>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower()) &&
                            t.Brand.ToLower().Contains(Brand.ToLower())
                        ) &
                        Builders<Oscilloscope>.Filter.OfType<Oscilloscope>(),
                        1000,
                        order
                    );
                } else if (!string.IsNullOrEmpty(SerialNumber)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Oscilloscope>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower())
                        ) &
                        Builders<Oscilloscope>.Filter.OfType<Oscilloscope>(),
                        1000,
                        order
                    );
                } else if (!string.IsNullOrEmpty(Brand)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Oscilloscope>.Filter.Where(
                            t => !t.Deleted &&
                            t.Brand.ToLower().Contains(Brand.ToLower())
                        ) &
                        Builders<Oscilloscope>.Filter.OfType<Oscilloscope>(),
                        1000,
                        order
                    );
                } else {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Oscilloscope>.Filter.Where(
                            t => !t.Deleted
                        ) &
                        Builders<Oscilloscope>.Filter.OfType<Oscilloscope>(),
                        1000,
                        order
                    );
                }
                if (collection.Count > 0) {
                    osciloscopes.AddRange(collection);
                    DataTable table = new DataTable();
                    table.Columns.Add(new DataColumn("Id"));
                    table.Columns.Add(new DataColumn("Tipo"));
                    table.Columns.Add(new DataColumn("Número de Série"));
                    table.Columns.Add(new DataColumn("Marca"));
                    table.Columns.Add(new DataColumn("Modelo"));
                    foreach (Oscilloscope osciloscope in osciloscopes) {
                        DataRow row = table.NewRow();
                        row.BeginEdit();
                        row["Id"] = osciloscope.Id;
                        row["Tipo"] = "Oscilloscope";
                        row["Número de Série"] = osciloscope.SerialNumber;
                        row["Marca"] = osciloscope.Brand;
                        row["Modelo"] = osciloscope.Model;
                        row.EndEdit();
                        table.Rows.Add(row);
                    }
                    return table;
                } else {
                    return null;
                }
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Connect() {
            try {
                var resourceString = "TCPIP::" + Regex.Replace(Ip, "0*([0-9]+)", "${1}") + "::" + Port + "::SOCKET";
                Instrument = new RsInstrument(resourceString, true, false, "SelectVisa=SocketIo");
                Instrument.VisaTimeout = 5000;
                Instrument.WriteString("WGEN:OUTP:ENAB OFF");
                Thread.Sleep(500);
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Disconnect() {
            try {
                Instrument.Dispose();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task<bool> CheckConnection() {
            try {
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public async Task ConfigureInitialSetup(double voltage, double frequency) {
            try {
                //Limpas as configurações do osciloscópio
                Instrument.WriteString("*RST");

                //Ativa os canais de entrada e saída
                //Instrument.WriteString($"CHAN{FirstChannel}:STAT ON");
                //Instrument.WriteString($"CHAN{SecondChannel}:STAT ON");
                Instrument.WriteString($"CHAN1:STAT ON");
                Instrument.WriteString($"CHAN2:STAT ON");

                //Ativa o gerador de funções com uma função seno, atribui os valor
                //de amplitude e frequência, posiciona as ondas em 0 e Redimensiona a tela
                Instrument.WriteString("WGEN:FUNC SIN");
                Instrument.WriteString($"WGEN:VOLT {voltage.ToString(CultureInfo.InvariantCulture)}Vpp");
                Instrument.WriteString($"WGEN:FREQ {frequency.ToString(CultureInfo.InvariantCulture)}Hz");
                Instrument.WriteString("WGEN:OUTP 1");
                Thread.Sleep(500);
                Instrument.WriteString("AUT");
                //Instrument.WriteString($"CHAN{FirstChannel}:OFFS 0V");
                //Instrument.WriteString($"CHAN{SecondChannel}:OFFS 0V");
                Instrument.WriteString($"CHAN1:OFFS 0V");
                Instrument.WriteString($"CHAN2:OFFS 0V");
                Instrument.WriteString("ACQ:HRES AUTO");

                //Ativa os canais de medição que calcularão a amplitude e fase
                //das ondas geradas e atribui os canais que servirão como fontes
                Instrument.WriteString("MEAS1:ENAB ON");
                Instrument.WriteString("MEAS2:ENAB ON");
                Instrument.WriteString("MEAS3:ENAB ON");
                Instrument.WriteString("MEAS1:MAIN PEAK");
                Instrument.WriteString("MEAS2:MAIN PEAK");
                Instrument.WriteString("MEAS3:MAIN PHAS");
                //Instrument.WriteString($"MEAS1:SOUR CH{FirstChannel}");
                //Instrument.WriteString($"MEAS2:SOUR CH{SecondChannel}");
                //Instrument.WriteString($"MEAS3:SOUR CH{SecondChannel},CH{FirstChannel}");
                Instrument.WriteString($"MEAS1:SOUR CH1");
                Instrument.WriteString($"MEAS2:SOUR CH2");
                Instrument.WriteString($"MEAS3:SOUR CH2,CH1");
                Instrument.WriteString($"MEAS:STAT:ENAB ON");

                //Configura o Trigger para o sinal de Entrada
                Instrument.WriteString($"TRIG:A:SOUR CH1");
                Instrument.WriteString($"TRIG:A:MODE AUTO");
                Instrument.WriteString($"TRIG:A:TYPE EDGE");
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public List<double>[] GetSignal(string channel) {
            int number;
            if (channel == "Input") {
                //number = FirstChannel;
                number = 1;
            } else {
                //number = SecondChannel;
                number = 2;
            }
            try {
                var header = Instrument.Binary.QueryBinOrAsciiFloatArray($"CHAN{number}:DATA:HEAD?");
                var interval = (header[1] - header[0]) / header[2];
                List<double> x = new List<double>();
                double lastX = header[0] - interval;
                var y = Instrument.Binary.QueryBinOrAsciiFloatArray($"CHAN{number}:DATA?");
                foreach (double point in y) {
                    x.Add(lastX + interval);
                    lastX += interval;
                }
                List<double>[] signal = new List<double>[2] { x, y.ToList() };
                return FilterSignal(signal);
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public async Task UpdateSignals(double voltage, double maxVoltage, double frequency) {
            try {
                if (updatedVoltage < voltage) {
                    updatedVoltage = voltage;
                }
                if (updatedVoltage > maxVoltage) {
                    updatedVoltage = maxVoltage;
                }
                if (updatedVoltage > 5) {
                    updatedVoltage = 5;
                }
                Instrument.WriteString($"WGEN:VOLT {updatedVoltage.ToString(CultureInfo.InvariantCulture)}Vpp");
                Instrument.WriteString($"WGEN:FREQ {Math.Round(frequency, 4).ToString(CultureInfo.InvariantCulture)}Hz");
                double voltageOutput = Instrument.QueryDouble($"MEAS2:RES:ACT?");
                double voltageInput = Instrument.QueryDouble($"MEAS1:RES:ACT?");
                //while ((Convert.ToDouble(Instrument.QueryDouble($"MEAS2:RES:ACT?")) < 35 && Convert.ToDouble(Instrument.QueryDouble($"MEAS1:RES:ACT?")) < 35) && updatedVoltage < maxVoltage) 
                while (voltageOutput < 35 && voltageInput < 35 && updatedVoltage < maxVoltage) {
                    updatedVoltage += 0.1;
                    Instrument.WriteString($"WGEN:VOLT {updatedVoltage.ToString(CultureInfo.InvariantCulture)}Vpp");
                    Thread.Sleep(500);
                    voltageOutput = Instrument.QueryDouble($"MEAS2:RES:ACT?");
                    voltageInput = Instrument.QueryDouble($"MEAS1:RES:ACT?");

                    if (voltageOutput >= 40 || voltageInput >= 40) {
                        await ScaleSignal(frequency);
                    }

                    voltageOutput = Instrument.QueryDouble($"MEAS2:RES:ACT?");
                    voltageInput = Instrument.QueryDouble($"MEAS1:RES:ACT?");
                }
                //await ScaleSignal(frequency);
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public async Task DowngradeSignals(double voltage, double maxVoltage, double frequency) {
            try {
                if (updatedVoltage < voltage) {
                    updatedVoltage = voltage;
                }
                if (updatedVoltage > maxVoltage) {
                    updatedVoltage = maxVoltage;
                }
                if (updatedVoltage > 5) {
                    updatedVoltage = 5;
                }
                double test = Instrument.QueryDouble($"MEAS2:RES:ACT?");
                Instrument.WriteString($"WGEN:VOLT {updatedVoltage.ToString(CultureInfo.InvariantCulture)}Vpp");
                Instrument.WriteString($"WGEN:FREQ {Math.Round(frequency, 4).ToString(CultureInfo.InvariantCulture)}Hz");
                while ((Convert.ToDouble(Instrument.QueryDouble($"MEAS2:RES:ACT?")) > 40 ||
                        Convert.ToDouble(Instrument.QueryDouble($"MEAS1:RES:ACT?")) > 40) && updatedVoltage > voltage) {
                    test = Instrument.QueryDouble($"MEAS2:RES:ACT?");
                    updatedVoltage -= 0.1;
                    Instrument.WriteString($"WGEN:VOLT {updatedVoltage.ToString(CultureInfo.InvariantCulture)}Vpp");
                    Thread.Sleep(500);
                }
                await ScaleSignal(frequency);
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public async Task<double> GetInjectedVoltage() {
            try {
                return updatedVoltage;
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task StopTest() {
            try {
                updatedVoltage = 0;
                Instrument.WriteString("WGEN:OUTP:ENAB OFF");
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task ScaleSignal(double frequency) {
            try {
                double voltageOutput = Instrument.QueryDouble($"MEAS2:RES:ACT?");
                double voltageInput = Instrument.QueryDouble($"MEAS1:RES:ACT?");
                //Verifica a tensão nos canais antes de fazer os ajustes do osciloscópio
                //if ((Instrument.QueryDouble($"MEAS2:RES:ACT?") >= 40) ||
                //    (Instrument.QueryDouble($"MEAS1:RES:ACT?") >= 40))
                if (voltageOutput >= 40 || voltageInput >= 40) {
                    //double peakToPeakFirst;
                    //Altera a escala para que a onda passe a ocupar três quadrantes do display
                    //if (Instrument.QueryDouble($"MEAS2:RES:ACT?") > Instrument.QueryDouble($"MEAS1:RES:ACT?")) {
                    //    peakToPeakFirst = Instrument.QueryDouble($"MEAS2:RES:ACT?") / 2;
                    //} else {
                    //    peakToPeakFirst = Instrument.QueryDouble($"MEAS1:RES:ACT?") / 2;
                    //}
                    //Instrument.WriteString($"CHAN{FirstChannel}:SCAL 5V");
                    //Instrument.WriteString($"CHAN{FirstChannel}:OFFS 0V");
                    Instrument.WriteString($"CHAN1:SCAL 5V");
                    Instrument.WriteString($"CHAN1:OFFS 0V");

                    //Altera a escala para que a onda passe a ocupar três quadrantes do display
                    //Instrument.WriteString($"CHAN{SecondChannel}:SCAL 5V");
                    //Instrument.WriteString($"CHAN{SecondChannel}:OFFS 0V");
                    Instrument.WriteString($"CHAN2:SCAL 5V");
                    Instrument.WriteString($"CHAN2:OFFS 0V");

                    //Altera a escala horizontal para plotar as senóides
                    Instrument.WriteString($"TIM:SCAL {(1 / (5 * frequency)).ToString(CultureInfo.InvariantCulture)}");
                    Instrument.WriteString($"TIM:POS 0");

                    //Ajusta o trigger de acordo com a maior amplitude de onda entra a entrada e saída
                    //Instrument.WriteString($"TRIG:A:LEV{FirstChannel} {(peakToPeakFirst / 5).ToString(CultureInfo.InvariantCulture)}");
                    Instrument.WriteString("TRIG:A:LEV 0");

                } else {
                    //Altera a escala para que a onda passe a ocupar três quadrantes do display
                    double peakToPeakFirst = Instrument.QueryDouble($"MEAS1:RES:ACT?") / 2;
                    double threeQuadrantsFirst = peakToPeakFirst / 4;
                    //Instrument.WriteString($"CHAN{FirstChannel}:SCAL {threeQuadrantsFirst.ToString(CultureInfo.InvariantCulture)}V");
                    //Instrument.WriteString($"CHAN{FirstChannel}:OFFS 0V");
                    Instrument.WriteString($"CHAN1:SCAL {threeQuadrantsFirst.ToString(CultureInfo.InvariantCulture)}V");
                    Instrument.WriteString($"CHAN1:OFFS 0V");

                    //Altera a escala para que a onda passe a ocupar três quadrantes do display
                    double peakToPeakSecond = Instrument.QueryDouble($"MEAS2:RES:ACT?") / 2;
                    double threeQuadrantSecond = peakToPeakSecond / 4;
                    //Instrument.WriteString($"CHAN{SecondChannel}:SCAL {threeQuadrantSecond.ToString(CultureInfo.InvariantCulture)}V");
                    //Instrument.WriteString($"CHAN{SecondChannel}:OFFS 0V");
                    Instrument.WriteString($"CHAN2:SCAL {threeQuadrantSecond.ToString(CultureInfo.InvariantCulture)}V");
                    Instrument.WriteString($"CHAN2:OFFS 0V");

                    //Altera a escala horizontal para plotar três senóides
                    Instrument.WriteString($"TIM:SCAL {(1 / (4 * frequency)).ToString(CultureInfo.InvariantCulture)}");
                    Instrument.WriteString($"TIM:POS 0");

                    //Ajusta o trigger de acordo com a maior amplitude de onda entra a entrada e saída
                    Instrument.WriteString("TRIG:A:LEV 0");
                    //if (peakToPeakFirst > peakToPeakSecond) {
                    //    Instrument.WriteString($"TRIG:A:LEV{FirstChannel} {(peakToPeakFirst / 4).ToString(CultureInfo.InvariantCulture)}");
                    //} else {
                    //    Instrument.WriteString($"TRIG:A:LEV{FirstChannel} {(peakToPeakSecond / 4).ToString(CultureInfo.InvariantCulture)}");
                    //}

                }
            } catch (Exception e) {
                Console.WriteLine(e);
                throw e;
            }
        }

        //Usa média móvel para filtrar o sinal recebido.
        private List<double>[] FilterSignal(List<double>[] signal) {
            try {
                int samples = 5; //Número de amostras a ser considerado pela média móvel
                double average = new double();
                for (int i = 0; i < signal[1].Count; i++) {
                    //Verifica se o número de amostras restantes é menor que o número definido de samples
                    if (i < signal[1].Count - samples) {
                        //Caso não seja, percorre e tira a média das próximas N amostras
                        for (int j = i; j < i + samples; j++) {
                            average += signal[1][j];
                        }
                        average = average / samples;
                    } else {
                        //Caso seja, percorre e tira a média das próximas amostras restantes
                        for (int j = i; j < signal[1].Count; j++) {
                            average += signal[1][j];
                        }
                        average = average / (signal[1].Count - i);
                    }
                    //Substitui o valor daquela posição pela média das próximas N posições
                    signal[1][i] = average;
                    average = 0;
                }
                return signal;
            } catch (Exception e) {
                throw e;
            }
        }

        public double GetInputAmplitude() {
            try {
                return Instrument.QueryDouble("MEAS1:RES:AVG?");
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public double GetOutputAmplitude() {
            try {
                return Instrument.QueryDouble("MEAS2:RES:AVG?");
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public double GetPhaseBetweenSignals() {
            try {
                return Instrument.QueryDouble("MEAS3:RES:AVG?");
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public int GetWaveCount() {
            try {
                return Instrument.QueryInteger("MEAS3:RES:WFMC?");
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public async Task ConfigureInitialSetupPdms(int channels) {
            try {
                int count = 1;
                string channel;
                Instrument.WriteString("*RST");
                while (count <= channels) {
                    channel = count.ToString();
                    Instrument.WriteString($"CHAN{channel}:STAT ON");
                    Instrument.WriteString($"PROB{channel}:SET:ATT:UNIT V");
                    Instrument.WriteString($"PROB{channel}:SET:ATT:MAN 1");
                    Instrument.WriteString($"CHAN{channel}:RANG 10.0");
                    Instrument.WriteString($"CHAN{channel}:POS 0");
                    Instrument.WriteString($"CHAN{channel}:BAND FULL");
                    count++;
                    Thread.Sleep(500);
                }
                Instrument.WriteString($"TIM:SCAL 0.002");
                Instrument.WriteString($"TIM:POS 0.0");
                Instrument.WriteString($"ACQ:HRES AUTO");
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public async Task CleanOscilloscopeSetupPdms(int channels) {
            try {
                int count = 1;
                string channel;
                Instrument.WriteString("*RST");
                while (count <= channels) {
                    Instrument.WriteString($"CHAN{count.ToString()}:STAT OFF");
                    count++;
                    Thread.Sleep(500);
                }
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public List<double>[] ReadOscilloscopePdms(int channel) {
            int number = channel;
            try {
                var header = Instrument.Binary.QueryBinOrAsciiFloatArray($"CHAN{number}:DATA:HEAD?");
                var interval = (header[1] - header[0]) / header[2];
                List<double> x = new List<double>();
                double lastX = header[0] - interval;
                var y = Instrument.Binary.QueryBinOrAsciiFloatArray($"CHAN{number}:DATA?");
                foreach (double point in y) {
                    x.Add(lastX + interval);
                    lastX += interval;
                }
                List<double>[] signal = new List<double>[2] { x, y.ToList() };
                return FilterSignal(signal);
                //return signal;
            } catch (RsInstrumentException e) {
                throw e;
            }
        }
        public int QuantidadeOndasPdms() {
            try {
                int result = Instrument.QueryInteger("MEAS2:RES:WFMC?");
                //int result = Instrument.QueryInteger("MEAS2:MAIN: PPC");
                return result;
            } catch (RsInstrumentException e) {
                throw e;
            }
        }
        public int QuantidadePontosPdms() {
            try {
                //int result = Instrument.QueryInteger("MEAS2:RES:WFMC?");
                int result = Instrument.QueryInteger("MEAS2:RES:ACT?  PPC");
                return result;
            } catch (RsInstrumentException e) {
                throw e;
            }
        }
        public double AmplitutePdms() {
            try {
                double result = Instrument.QueryDouble("MEAS2:RES:ACT? MEAN");
                return result;
            } catch (RsInstrumentException e) {
                throw e;
            }
        }
        public double PicoPicoPdms() {
            try {
                double result = Instrument.QueryDouble("MEAS2:RES:ACT? PEAK");
                return result;
            } catch (RsInstrumentException e) {
                throw e;
            }
        }
        public double TensaoRmsPdms() {
            try {
                double result = Instrument.QueryDouble("MEAS2:RES:ACT? RMS");
                return result;
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public int PicosPositivosPdms() {
            try {
                int result = Instrument.QueryInteger("MEAS2:RES:ACT? PPC");
                return result;
            } catch (RsInstrumentException e) {
                throw e;
            }
        }

        public int PicosNegativosPdms() {
            try {
                int result = Instrument.QueryInteger("MEAS2:RES:ACT? NPC");
                return result;
            } catch (RsInstrumentException e) {
                throw e;
            }
        }
        public double PeriodoPdms() {
            try {
                double result = Instrument.QueryDouble("MEAS2:RES:ACT? PER");
                return result;
            } catch (RsInstrumentException e) {
                throw e;
            }
        }
    }
}
