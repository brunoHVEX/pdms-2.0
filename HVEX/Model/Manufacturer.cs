﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model {
    class Manufacturer {
        private static readonly string collectionName = "manufacturer";

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("companyName")]
        public string CompanyName { get; set; }

        [BsonElement("fantasyName")]
        public string FantasyName { get; set; }

        [BsonElement("phone")]
        public string Phone { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("contact")]
        public string Contact { get; set; }

        [BsonElement("address")]
        public string Address { get; set; }

        [BsonElement("district")]
        public string District { get; set; }

        [BsonElement("city")]
        public string City { get; set; }

        [BsonElement("state")]
        public string State { get; set; }

        [BsonElement("country")]
        public string Country { get; set; }

        [BsonElement("image")]
        public byte[] Image { get; set; }

        [BsonElement("deleted")]
        public bool Deleted { get; set; }

        public Manufacturer() {
            Id = new ObjectId();
            Deleted = false;
        }

        public Manufacturer(
            string companyName,
            string fantasyName,
            string phone,
            string email,
            string contact,
            string address,
            string district,
            string city,
            string state,
            string country,
            byte[] image = null
        ) {
            Id = new ObjectId();
            CompanyName = companyName;
            FantasyName = fantasyName;
            Phone = phone;
            Contact = contact;
            Phone = phone;
            Email = email;
            Address = address;
            District = district;
            City = city;
            State = state;
            Country = country;
            if (image != null) {
                Image = image;
            }
            Deleted = false;
        }

        public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"FABRICANTE {CompanyName} REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<Manufacturer>.Filter.Eq(m => m.Id, Id)
                );
                var manufacturer = collection.FirstOrDefault();
                if (manufacturer != null) {
                    Id = manufacturer.Id;
                    CompanyName = manufacturer.CompanyName;
                    FantasyName = manufacturer.FantasyName;
                    Phone = manufacturer.Phone;
                    Email = manufacturer.Email;
                    Contact = manufacturer.Contact;
                    Address = manufacturer.Address;
                    District = manufacturer.District;
                    City = manufacturer.City;
                    State = manufacturer.State;
                    Country = manufacturer.Country;
                    if (manufacturer.Image != null) {
                        Image = manufacturer.Image;
                    }
                    Deleted = manufacturer.Deleted;
                } else {
                    throw new Exception("Não existe nenhum fabricante que atenda aos parâmetros especificados.");
                }
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"FABRICANTE {CompanyName} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<Manufacturer>.Filter.Eq(f => f.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"FABRICANTE {CompanyName} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<Manufacturer>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task<DataTable> List(string order) {
            List<Manufacturer> manufacturers = new List<Manufacturer>();
            switch (order) {
                case "Razão Social":
                    order = "company_name";
                    break;
                case "Nome Fantasia":
                    order = "fantasy_name";
                    break;
                case "Telefone":
                    order = "phone";
                    break;
                case "Contato":
                    order = "contact";
                    break;
            }
            try {
                List<Manufacturer> collection;
                if (string.IsNullOrEmpty(CompanyName)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Manufacturer>.Filter.Where(m => !m.Deleted),
                        1000,
                        order
                    );
                } else {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Manufacturer>.Filter.Where(
                            m => !m.Deleted && m.CompanyName.ToLower().Contains(CompanyName.ToLower())
                        ),
                        1000,
                        order
                    );
                }
                manufacturers.AddRange(collection);
                DataTable table = new DataTable();
                table.Columns.Add(new DataColumn("Id"));
                table.Columns.Add(new DataColumn("Razão Social"));
                table.Columns.Add(new DataColumn("Nome Fantasia"));
                table.Columns.Add(new DataColumn("Telefone"));
                table.Columns.Add(new DataColumn("Contato"));
                foreach (Manufacturer manufacturer in manufacturers) {
                    DataRow row = table.NewRow();
                    row.BeginEdit();
                    row["Id"] = manufacturer.Id;
                    row["Razão Social"] = manufacturer.CompanyName;
                    row["Nome Fantasia"] = manufacturer.FantasyName;
                    row["Telefone"] = manufacturer.Phone;
                    row["Contato"] = manufacturer.Contact;
                    row.EndEdit();
                    table.Rows.Add(row);
                }
                return table;
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
