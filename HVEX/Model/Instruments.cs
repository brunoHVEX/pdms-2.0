﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model {
    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(
        typeof(Oscilloscope),
        typeof(Plc)
    )]
    abstract class Instruments {
        static protected readonly string collectionName = "instrument";

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("serialNumber")]
        public string SerialNumber { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("brand")]
        public string Brand { get; set; }

        [BsonElement("model")]
        public string Model { get; set; }

        [BsonElement("ip")]
        public string Ip { get; set; }

        [BsonElement("port")]
        public int Port { get; set; }

        [BsonElement("deleted")]
        public bool Deleted { get; set; }

        public Instruments() {
            Id = new ObjectId();
            Deleted = false;
        }

        public Instruments(string serialNumber,
                          string model,
                          string description,
                          string brand,
                          string ip,
                          int port) {
            Id = new ObjectId();
            SerialNumber = serialNumber;
            Description = description;
            Model = model;
            Brand = brand;
            Ip = ip;
            Port = port;
            Deleted = false;
        }

        public virtual async Task<DataTable> List(string order) {
            try {
                List<Instruments> instruments = new List<Instruments>();
                switch (order) {
                    case "Número de Série":
                        order = "serialNumber";
                        break;
                    case "Descrição":
                        order = "description";
                        break;
                }
                List<Instruments> collection;
                if (!string.IsNullOrEmpty(SerialNumber) && !string.IsNullOrEmpty(Description)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Instruments>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower()) &&
                            t.Description.ToLower().Contains(Description.ToLower())
                        )
                    );
                } else if (!string.IsNullOrEmpty(SerialNumber)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Instruments>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower())
                        )
                    );
                } else if (!string.IsNullOrEmpty(Description)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Instruments>.Filter.Where(
                            t => !t.Deleted &&
                            t.Description.ToLower().Contains(Description.ToLower())
                        )
                    );
                } else {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Instruments>.Filter.Where(
                            t => !t.Deleted
                        )
                    );
                }
                instruments.AddRange(collection);
                DataTable table = new DataTable();
                table.Columns.Add("Id");
                table.Columns.Add("Tipo");
                table.Columns.Add("Número de Série");
                table.Columns.Add("Descrição");
                foreach (Instruments instrument in instruments) {
                    DataRow row = table.NewRow();
                    row.BeginEdit();
                    row["Id"] = instrument.Id;
                    if (instrument is Oscilloscope) {
                        row["Tipo"] = "Oscilloscope";
                    } else if (instrument is Plc) {
                        row["Tipo"] = "Plc";
                    }
                    row["Número de Série"] = instrument.SerialNumber;
                    row["Descrição"] = instrument.Description;
                    row.EndEdit();
                    table.Rows.Add(row);
                }
                return table;
            } catch (Exception e) {
                throw e;
            }
        }

        public abstract Task Create(ObjectId userId);
        public abstract Task Read();
        public abstract Task Update(ObjectId userId);
        public abstract Task Delete(ObjectId userId);
    }
}
