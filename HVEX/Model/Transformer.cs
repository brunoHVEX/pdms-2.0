﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVEX.Model {
    class Transformer : Equipment {

        [BsonElement("internalNumber")]
        public string InternalNumber { get; set; }

        [BsonElement("manufactureDate")]
        public DateTime ManufactureDate { get; set; }

        [BsonElement("manufacturer")]
        public ObjectId Manufacturer { get; set; }

        [BsonElement("type")]
        public string Type { get; set; }

        [BsonElement("customer")]
        public ObjectId Customer { get; set; }

        [BsonElement("efficiencyLevel")]
        public string EfficiencyLevel { get; set; }

        [BsonElement("regulationRange")]
        public double RegulationRange { get; set; }

        [BsonElement("weight")]
        public double Weight { get; set; }

        [BsonElement("polarity")]
        public string Polarity { get; set; }

        [BsonElement("phases")]
        public int Phases { get; set; }

        [BsonElement("frequency")]
        public int Frequency { get; set; }

        [BsonElement("temperatureLimit")]
        public double TemperatureLimit { get; set; }

        [BsonElement("resistanceTemperature")]
        public double ResistanceTemperature { get; set; }

        [BsonElement("insulatingOil")]
        public string InsulatingOil { get; set; }

        [BsonElement("voltageClass")]
        public double VoltageClass { get; set; }

        [BsonElement("state")]
        public string State { get; set; }

        [BsonElement("connection")]
        public string Connection { get; set; }

        [BsonElement("connectionGroups")]
        public List<ObjectId> ConnectionGroups { get; set; }

        [BsonElement("power")]
        public double[] Power { get; set; }

        [BsonElement("powerMeasurement")]
        public string[] PowerMeasurement { get; set; }

        [BsonElement("voltage")]
        public double[] Voltage { get; set; }

        [BsonElement("voltageMeasurement")]
        public string[] VoltageMeasurement { get; set; }

        [BsonElement("current")]
        public double[] Current { get; set; }

        [BsonElement("currentMeasurement")]
        public string[] CurrentMeasurement { get; set; }

        [BsonElement("windingMaterial")]
        public string[] WindingMaterial { get; set; }

        [BsonElement("resistance")]
        public double[] Resistance { get; set; }

        [BsonElement("resistanceMeasurement")]
        public string[] ResistanceMeasurement { get; set; }

        [BsonElement("taps")]
        public List<double> Taps { get; set; }

        public Transformer() {
            Id = new ObjectId();
            Deleted = false;
        }

        public Transformer(
            string serialNumber,
            string model,
            string type,
            string internalNumber,
            string description,
            double regulationRange,
            DateTime manufactureDate,
            ObjectId manufacturer,
            ObjectId customer,
            string efficiencyLevel,
            string polarity,
            int frequency,
            int phases,
            double temperatureLimit,
            double weight,
            double resistanceTemperature,
            string insulatingOil,
            double voltageClass,
            string state,
            string connection,
            double[] power,
            string[] powerMeasurement,
            double[] voltage,
            string[] voltageMeasurement,
            double[] current,
            string[] currentMeasurement,
            string[] windingMaterial,
            double[] resistance,
            string[] resistanceMeasurement,
            List<double> taps
        ) : base(
            serialNumber,
            model,
            description
        ) {
            Id = new ObjectId();
            InternalNumber = internalNumber;
            Type = type;
            ManufactureDate = manufactureDate;
            Manufacturer = manufacturer;
            Customer = customer;
            EfficiencyLevel = efficiencyLevel;
            Polarity = polarity;
            Phases = phases;
            Frequency = frequency;
            TemperatureLimit = temperatureLimit;
            RegulationRange = regulationRange;
            ResistanceTemperature = resistanceTemperature;
            InsulatingOil = insulatingOil;
            VoltageClass = voltageClass;
            State = state;
            Weight = weight;
            Connection = connection;
            Power = power;
            PowerMeasurement = powerMeasurement;
            Voltage = voltage;
            VoltageMeasurement = voltageMeasurement;
            Current = current;
            CurrentMeasurement = currentMeasurement;
            WindingMaterial = windingMaterial;
            Resistance = resistance;
            ResistanceMeasurement = resistanceMeasurement;
            Taps = taps;
            Deleted = false;
        }

        override public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"TRANSFORMADOR {SerialNumber} REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }


        override public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<Transformer>.Filter.Eq(u => u.Id, Id)
                );
                var transformer = collection.FirstOrDefault();
                if (transformer != null) {
                    SerialNumber = transformer.SerialNumber;
                    Model = transformer.Model;
                    Type = transformer.Type;
                    InternalNumber = transformer.InternalNumber;
                    Description = transformer.Description;
                    Manufacturer = transformer.Manufacturer;
                    Customer = transformer.Customer;
                    ManufactureDate = transformer.ManufactureDate;
                    EfficiencyLevel = transformer.EfficiencyLevel;
                    RegulationRange = transformer.RegulationRange;
                    Weight = transformer.Weight;
                    Polarity = transformer.Polarity;
                    Frequency = transformer.Frequency;
                    Phases = transformer.Phases;
                    TemperatureLimit = transformer.TemperatureLimit;
                    ResistanceTemperature = transformer.ResistanceTemperature;
                    InsulatingOil = transformer.InsulatingOil;
                    VoltageClass = transformer.VoltageClass;
                    State = transformer.State;
                    Connection = transformer.Connection;
                    Power = transformer.Power;
                    PowerMeasurement = transformer.PowerMeasurement;
                    Voltage = transformer.Voltage;
                    VoltageMeasurement = transformer.VoltageMeasurement;
                    Current = transformer.Current;
                    CurrentMeasurement = transformer.CurrentMeasurement;
                    Resistance = transformer.Resistance;
                    ResistanceMeasurement = transformer.ResistanceMeasurement;
                    WindingMaterial = transformer.WindingMaterial;
                    Taps = transformer.Taps;
                    ConnectionGroups = transformer.ConnectionGroups;
                } else {
                    throw new Exception("Nenhum transformador, que atenda aos parâmetros especificados, foi encontrado.");
                }
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"TRANSFORMADOR {SerialNumber} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<Transformer>.Filter.Eq(t => t.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"TRANSFORMADOR {SerialNumber} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<Transformer>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        public double GetSecondaryCurrent() {
            if (Current != null) {
                if (Current[2] != 0) {
                    return Current[2];
                } else if (Current[1] != 0) {
                    return Current[1];
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }

        public double GetSecondaryVoltage() {
            if (Voltage != null) {
                if (Voltage[2] != 0) {
                    return Voltage[2];
                } else if (Voltage[1] != 0) {
                    return Voltage[1];
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        }

        public string getSecondaryWinding() {
            if (WindingMaterial != null) {
                if (!string.IsNullOrEmpty(WindingMaterial[2])) {
                    return WindingMaterial[2];
                } else if (!string.IsNullOrEmpty(WindingMaterial[1])) {
                    return WindingMaterial[1];
                } else {
                    return "";
                }
            } else {
                return "";
            }

        }

        public static double CalculateCurrent(double power, double voltage) {
            try {
                return power / voltage;
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
