﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVEX.Model {
    class TestShunt:Test {

        public List<double> Frequency { get; set; }

        public List<double> Amplitude { get; set; }

        public double Resistence { get; set; }

        public List<double> RmsVoltageInput { get; set; }

        public List<double> RmsCurrentOutput { get; set; }

        public List<double> RmsPhase { get; set; }

        public List<double> Phase { get; set; }

        public TestShunt() {
            Id = new ObjectId();
            Deleted = false;
        }

        public TestShunt(ObjectId equipment,
                         ObjectId instrument,
                         ObjectId user,
                         DateTime date,
                         DateTime duration,
                         List<double> frequency,
                         List<double> amplitude,
                         double resistence,
                         List<double> rmsVoltageInput,
                         List<double> rmsCurrentOutput,
                         List<double> rmsPhase,
                         List<double> phase) : base(equipment,
                                                    instrument,
                                                    user,
                                                    date,
                                                    duration) {
            Id = new ObjectId();
            Frequency = frequency;
            Amplitude = amplitude;
            Resistence = resistence;
            RmsVoltageInput = rmsVoltageInput;
            RmsCurrentOutput = rmsCurrentOutput;
            RmsPhase = rmsPhase;
            Phase = phase;
            Deleted = false;
        }
        override public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"NOVO ENSAIO REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<TestShunt>.Filter.Eq(u => u.Id, Id)
                );
                var test = collection.FirstOrDefault();
                if (test != null) {
                    Instrument = test.Instrument;
                    User = test.User;
                    Frequency = test.Frequency;
                    Amplitude = test.Amplitude;
                    Phase = test.Phase;
                    Resistence = test.Resistence;
                    RmsVoltageInput = test.RmsVoltageInput;
                    RmsCurrentOutput = test.RmsCurrentOutput;
                    RmsPhase = test.RmsPhase;
                    Date = test.Date;
                    Duration = test.Duration;
                    Deleted = test.Deleted;
                } else {
                    throw new Exception("Nenhum ensaio, que atenda aos parâmetros especificados, foi encontrado.");
                }
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"ENSAIO {Id} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<TestShunt>.Filter.Eq(t => t.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"ENSAIO {Id} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<TestShunt>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
        public bool Validate() {
            if (Instrument == null) {
                return false;
            }
            if (User == null) {
                return false;
            }
            if (Date == null) {
                return false;
            }
            if (Duration == null) {
                return false;
            }
            if (Frequency == null) {
                return false;
            }
            if (Amplitude == null) {
                return false;
            }
            if (Resistence == 0) {
                return false;
            }
            if (RmsVoltageInput == null) {
                return false;
            }
            if (RmsCurrentOutput == null) {
                return false;
            }
            if (RmsPhase == null) {
                return false;
            }
            if (Phase == null) {
                return false;
            }
            return true;
        }
    }
}
