﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace HVEX.Model {
    internal class GenericEquipment : Equipment  {

        [BsonElement("internalNumber")]
        public string InternalNumber { get; set; }

        [BsonElement("manufacturer")]
        public ObjectId Manufacturer { get; set; }

        [BsonElement("customer")]
        public ObjectId Customer { get; set; }

        [BsonElement("manufactureDate")]
        public DateTime ManufactureDate { get; set; }

        public GenericEquipment() { }

        public GenericEquipment(
            string serialNumber,
            string internalNumber,
            ObjectId manufacturer,
            ObjectId customer,
            string description,
            DateTime manufactureDate,
            string model,
            string type
        ) : base(
            serialNumber,
            model,
            description
        ) {
            InternalNumber = internalNumber;
            Manufacturer = manufacturer;
            Customer = customer;
            ManufactureDate = manufactureDate;
        }

        public override async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"EQUIPAMENTO GENÉRICO {SerialNumber} REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }

        }

        public override async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<GenericEquipment>.Filter.Eq(g => g.Id, Id)
                );
                var equipment = collection.FirstOrDefault();
                if (equipment != null) {
                    SerialNumber = equipment.SerialNumber;
                    InternalNumber = equipment.InternalNumber;
                    Manufacturer = equipment.Manufacturer;
                    Customer = equipment.Customer;
                    Description = equipment.Description;
                    Model = equipment.Model;
                    ManufactureDate = equipment.ManufactureDate;
                } else {
                    throw new Exception("Nenhum equipmamento atende aos parâmentros especificados.");
                }
            } catch (Exception e) {
                throw e;
            }
        }

        public override async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"EQUIPAMENTO GENÉRICO {SerialNumber} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<GenericEquipment>.Filter.Eq(g => g.Id, Id), this);
                await log.Create();
                await log.Create();
            } catch (Exception e) {
                throw e;
            }


        }

        public override async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"EQUIPAMENTO GENÉRICO {SerialNumber} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<GenericEquipment>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
    }
}