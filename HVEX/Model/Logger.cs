﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace HVEX.Model {
    class Logger {
        public static readonly string collectionName = "logger";

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("error")]
        public string Error { get; set; }

        [BsonElement("source")]
        public string Source { get; set; }

        [BsonElement("date")]
        public DateTime Date { get; set; }

        [BsonElement("user")]
        public ObjectId UserId { get; set; }

        [BsonElement("deleted")]
        public bool Deleted { get; set; }

        public Logger() { }

        public Logger(
            string description,
            string source,
            DateTime date,
            ObjectId userId
        ) {
            Description = description;
            Error = null;
            Source = source;
            Date = date;
            UserId = userId;
        }

        public Logger(
            string description,
            string errorMessage,
            string source,
            DateTime date,
            ObjectId userId
        ) {
            Description = description;
            Error = errorMessage;
            Source = source;
            Date = date;
            UserId = userId;
        }

        public async Task<bool> Create() {
            try {
                await Mongo.Create(collectionName, this);
                return true;
            } catch (Exception e) {
                new DatabaseException($"Houve um erro ao tentar registrar o log!\n{e.Message}");
                return false;
            }
        }

        public async Task<bool> Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<Logger>.Filter.Eq(l => l.Id, Id) &
                    Builders<Logger>.Filter.Where(l => !l.Deleted)
                );
                var log = collection.FirstOrDefault();
                if (log != null) {
                    Description = log.Description;
                    Error = log.Error;
                    Source = log.Source;
                    Date = log.Date;
                    UserId = log.UserId;
                    return true;
                } else {
                    new FormException("Não existe nenhum log que atenda aos parâmetros especificados.");
                    return false;
                }
            } catch (Exception e) {
                new DatabaseException($"Houve um erro ao tentar recuperar o log do banco de dados!\n{e.Message}");
                return false;
            }
        }

        public async Task<bool> Update() {
            try {
                await Mongo.Update(collectionName, Builders<Logger>.Filter.Eq(l => l.Id, Id), this);
                return true;
            } catch (Exception e) {
                new DatabaseException($"Houve um erro ao tentar atualizar o log no banco de dados!\n{e.Message}");
                return false;
            }
        }

        public async Task<bool> Delete() {
            try {
                await Mongo.Delete<Logger>(collectionName, Id);
                return true;
            } catch (Exception e) {
                new DatabaseException($"Houve um erro ao tentar remover o log do banco de dados!\n{e.Message}");
                return false;
            }
        }

        public async Task<DataTable> List(string order, DateTime? since, DateTime? until) {
            List<Logger> logs = new List<Logger>();
            switch (order) {
                case "Data":
                    order = "date";
                    break;
                case "Descrição":
                    order = "description";
                    break;
                case "Fonte":
                    order = "source";
                    break;
                case "Usuário":
                    order = "user";
                    break;
            }
            try {
                List<Logger> collection;
                if (string.IsNullOrEmpty(Description)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Logger>.Filter.Where(
                            l => !l.Deleted &&
                            l.Date >= since
                            && l.Date <= until
                        ),
                        1000,
                        order,
                        -1
                    );
                } else {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Logger>.Filter.Where(
                            l => !l.Deleted &&
                            l.Description.ToLower().Contains(Description.ToLower()) &&
                            l.Date >= since && l.Date <= until
                        ),
                        10000,
                        order,
                        -1
                    );
                }
                logs.AddRange(collection);
                DataTable table = new DataTable();
                table.Columns.Add(new DataColumn("Id"));
                table.Columns.Add(new DataColumn("Descrição"));
                table.Columns.Add(new DataColumn("Usuário"));
                table.Columns.Add(new DataColumn("Local"));
                table.Columns.Add(new DataColumn("Data"));
                foreach (Logger log in logs) {
                    User user = new User();
                    user.Id = log.UserId;
                    await user.Read();
                    DataRow row = table.NewRow();
                    row.BeginEdit();
                    row["Id"] = log.Id;
                    row["Descrição"] = log.Description;
                    row["Usuário"] = user.UserName;
                    row["Local"] = log.Source;
                    row["Data"] = log.Date.Subtract(new TimeSpan(3, 0, 0));
                    row.EndEdit();
                    table.Rows.Add(row);
                }
                return table;
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
