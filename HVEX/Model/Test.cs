﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model {
    [BsonDiscriminator(RootClass = true)]
    [BsonKnownTypes(
        typeof(TestFra),
        typeof(TestShunt)
    )]
    abstract class Test {
        static protected readonly string collectionName = "test";

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("equipment")]
        public ObjectId Equipment { get; set; }

        [BsonElement ("instrument")]
        public ObjectId Instrument { get; set; }

        [BsonElement("user")]
        public ObjectId User { get; set; }

        [BsonElement ("date")]
        public DateTime Date { get; set; }

        [BsonElement ("duration")]
        public DateTime Duration { get; set; }

        [BsonElement ("deleted")]
        public bool Deleted { get; set; }

        public Test() {
            Id = new ObjectId();
            Deleted = false;
        }

        public Test(ObjectId equipment,
                    ObjectId instrument,
                    ObjectId user,
                    DateTime date,
                    DateTime duration) {
            Id = new ObjectId();
            Equipment = equipment;
            Instrument = instrument;
            User = user;
            Date = date;
            Duration = duration;
            Deleted = false;
        }
        public abstract Task Create(ObjectId userId);
        public abstract Task Read();
        public abstract Task Update(ObjectId userId);
        public abstract Task Delete(ObjectId userId);
        public async Task<DataTable> List(string order, DateTime? since, DateTime? until) {
            since = since == null ? new DateTime(1901, 01, 01) : since;
            until = until == null ? new DateTime(2100, 12, 30) : until;
            List<Test> tests = new List<Test>();
            switch (order) {
                case "FRA":
                    order = "testFra";
                    break;
                case "Shunt":
                    order = "testShunt";
                    break;
            }
            try {
                List<Test> collection;
                collection = await Mongo.Read(
                    collectionName,
                    Builders<Test>.Filter.Where(
                        t => !t.Deleted &&
                        t.Date >= since.Value && t.Date <= until.Value.AddDays(1)
                    ),
                    1000,
                    order
                );
                tests.AddRange(collection);
                DataTable table = new DataTable();
                table.Columns.Add("Id");
                table.Columns.Add("Ensaio");
                table.Columns.Add("Data");
                foreach (Test test in tests) {
                    DataRow row = table.NewRow();
                    row.BeginEdit();
                    row["Id"] = test.Id;
                    Transformer equipment = new Transformer();
                    equipment.Id = test.Equipment;
                    await equipment.Read();
                    row["Ensaio"] = equipment.SerialNumber + " - " + equipment.Description;
                    row["Data"] = test.Date.Subtract(new TimeSpan(3, 0, 0));
                    row.EndEdit();
                    table.Rows.Add(row);
                }
                return table;
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
