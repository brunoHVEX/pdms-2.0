﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model {
    class ConfigurationThreePhase {

        static private readonly string collectionName = "configurationThreePhase";

        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("ganho")]
        public List<double> Gain { get; set; }

        [BsonElement("gate")]
        public List<double> Gate { get; set; }

        [BsonElement("filtro")]
        public int Filter { get; set; }

        [BsonElement("ordem")]
        public double Order { get; set; }

        [BsonElement("frequenciaInferior")]
        public List<double> FrequencyUndercut { get; set; }

        [BsonElement("frequenciaSuperior")]
        public List<double> FrequencyTopCut { get; set; }

        [BsonElement("calibracao")]
        public List<double> Calibration { get; set; }

        [BsonElement("deleted")]
        public bool Deleted { get; set; }

        public ConfigurationThreePhase() { }

        public ConfigurationThreePhase(List<double> gain, List<double> gate, int filter, double order, List<double> frequencyUndercut, List<double> frequencyTopCut, List<double> calibration) {
            Id = new ObjectId();
            Gain = gain;
            Gate = gate;
            Filter = filter;
            Order = order;
            FrequencyUndercut = frequencyUndercut;
            FrequencyTopCut = frequencyTopCut;
            Calibration = calibration;
            Deleted = false;
        }

        public ConfigurationThreePhase(ObjectId id, List<double> gain, List<double> gate, int filter, double order, List<double> frequencyUndercut, List<double> frequencyTopCut, List<double> calibration) {
            Id = id;
            Gain = gain;
            Gate = gate;
            Filter = filter;
            Order = order;
            FrequencyUndercut = frequencyUndercut;
            FrequencyTopCut = frequencyTopCut;
            Calibration = calibration;
            Deleted = false;
        }

        public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"CONFIGURAÇÃO DE TESTE TRIFÁSICO REGISTRADA.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"CONFIGURAÇÃO DE TESTE TRIFÁSICO ALTERADA.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<ConfigurationThreePhase>.Filter.Eq(u => u.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<ConfigurationThreePhase>.Filter.Where(c => !c.Deleted)
                );
                var configuration = collection.FirstOrDefault();
                if (configuration != null) {
                    Id = configuration.Id;
                    Gain = configuration.Gain;
                    Gate = configuration.Gate;
                    Filter = configuration.Filter;
                    Order = configuration.Order;
                    FrequencyUndercut = configuration.FrequencyUndercut;
                    FrequencyTopCut = configuration.FrequencyTopCut;
                    Calibration = configuration.Calibration;
                }
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
