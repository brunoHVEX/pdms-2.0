﻿using HVEX.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model {
    public class WaveForm {
        public List<double> x { get; set; }
        public List<double> y { get; set; }

        //public WaveType type { get; set; }

        public WaveForm() {
            x = new List<double>();
            y = new List<double>();
        }

        public WaveForm(List<double> x, List<double> y) {
            this.x = x;
            this.y = y;
        }

        public void Clear() {
            this.x.Clear();
            this.y.Clear();
        }

    }
}
