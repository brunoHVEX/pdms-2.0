﻿using HVEX.Model.Enum;
using HVEX.Service;
using MathNet.Numerics.IntegralTransforms;
using MathNet.Numerics.Statistics;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using NationalInstruments.VisaNS;
//using RohdeSchwarz.RsInstrument;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;


namespace HVEX.Model {
    class OscilloscopePdms : Instruments {

        //public double desvioPadrao { get; set; }
        public bool calibrating;
        public double dp;
        public List<double> partialDischargesPhase1 = new List<double>();
        public List<double> partialDischargesPhase2 = new List<double>();
        public List<double> partialDischargesPhase3 = new List<double>();
        public double actualMaxPositiveValuePhase1 = 0.0;
        public double actualMaxPositiveValuePhase2 = 0.0;
        public double actualMaxPositiveValuePhase3 = 0.0;
        public double actualMaxNegativeValuePhase1 = 0.0;
        public double actualMaxNegativeValuePhase2 = 0.0;
        public double actualMaxNegativeValuePhase3 = 0.0;
        public double actualMaxValuePhase1 = 0.0;
        public double actualMaxValuePhase2 = 0.0;
        public double actualMaxValuePhase3 = 0.0;
        public int actualPartialDischargePhase1 = 0;
        public int actualPartialDischargePhase2 = 0;
        public int actualPartialDischargePhase3 = 0;
        public double desvioPadrao1 { get; set; }
        public double desvioPadrao2 { get; set; }
        public double desvioPadrao3 { get; set; }

        public double mediaF1 { get; set; }
        public int mediaF1Count { get; set; }
        public double mediaF2 { get; set; }
        public int mediaF2Count { get; set; }
        public double mediaF3 { get; set; }
        public int mediaF3Count { get; set; }
        //private double dp;
        private double target;
        private double sum;
        private double last;
        private double max = 0;
        private double min = 0;

        [BsonElement("channels")]
        public int Channels { get; set; }

        [BsonElement("instrument")]
        //public RsInstrument Instrument { get; set; }

        public MessageBasedSession Instrument { get; set; }

        public OscilloscopePdms() {
            Id = new ObjectId();
            Deleted = false;
        }

        public OscilloscopePdms(
            string serialNumber,
            string description,
            string brand,
            string model,
            string ip,
            int port,
            int channels) : base(
                serialNumber,
                description,
                brand,
                model,
                ip,
                port) {
            Id = new ObjectId();
            SerialNumber = serialNumber;
            Description = description;
            Brand = brand;
            Model = model;
            Ip = ip;
            Port = port;
            Channels = channels;
            Deleted = false;
        }

        override public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"OSCILOSCÓPIO {SerialNumber} REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<OscilloscopePdms>.Filter.Eq(u => u.Id, Id)
                );
                var osciloscope = collection.FirstOrDefault();
                if (osciloscope != null) {
                    SerialNumber = osciloscope.SerialNumber;
                    Description = osciloscope.Description;
                    Brand = osciloscope.Brand;
                    Model = osciloscope.Model;
                    Ip = osciloscope.Ip;
                    Port = osciloscope.Port;
                    Channels = osciloscope.Channels;
                } else {
                    throw new Exception("Nenhum osciloscópio, que atenda aos parâmetros especificados, foi encontrado.");
                }
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"OSCILOSCÓPIO {SerialNumber} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<OscilloscopePdms>.Filter.Eq(o => o.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"OSCILOSCÓPIO {SerialNumber} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<OscilloscopePdms>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task<DataTable> List(string order) {
            List<OscilloscopePdms> osciloscopes = new List<OscilloscopePdms>();
            switch (order) {
                case "Número de Série":
                    order = "serial_number";
                    break;
                case "Marca":
                    order = "brand";
                    break;
                case "Modelo":
                    order = "model";
                    break;
            }
            try {
                List<OscilloscopePdms> collection;
                if (!string.IsNullOrEmpty(SerialNumber) && !string.IsNullOrEmpty(Brand)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<OscilloscopePdms>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower()) &&
                            t.Brand.ToLower().Contains(Brand.ToLower())
                        ) &
                        Builders<OscilloscopePdms>.Filter.OfType<OscilloscopePdms>(),
                        1000,
                        order
                    );
                } else if (!string.IsNullOrEmpty(SerialNumber)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<OscilloscopePdms>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower())
                        ) &
                        Builders<OscilloscopePdms>.Filter.OfType<OscilloscopePdms>(),
                        1000,
                        order
                    );
                } else if (!string.IsNullOrEmpty(Brand)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<OscilloscopePdms>.Filter.Where(
                            t => !t.Deleted &&
                            t.Brand.ToLower().Contains(Brand.ToLower())
                        ) &
                        Builders<OscilloscopePdms>.Filter.OfType<OscilloscopePdms>(),
                        1000,
                        order
                    );
                } else {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<OscilloscopePdms>.Filter.Where(
                            t => !t.Deleted
                        ) &
                        Builders<OscilloscopePdms>.Filter.OfType<OscilloscopePdms>(),
                        1000,
                        order
                    );
                }
                if (collection.Count > 0) {
                    osciloscopes.AddRange(collection);
                    DataTable table = new DataTable();
                    table.Columns.Add(new DataColumn("Id"));
                    table.Columns.Add(new DataColumn("Tipo"));
                    table.Columns.Add(new DataColumn("Número de Série"));
                    table.Columns.Add(new DataColumn("Marca"));
                    table.Columns.Add(new DataColumn("Modelo"));
                    foreach (OscilloscopePdms osciloscope in osciloscopes) {
                        DataRow row = table.NewRow();
                        row.BeginEdit();
                        row["Id"] = osciloscope.Id;
                        row["Tipo"] = "Oscilloscope";
                        row["Número de Série"] = osciloscope.SerialNumber;
                        row["Marca"] = osciloscope.Brand;
                        row["Modelo"] = osciloscope.Model;
                        row.EndEdit();
                        table.Rows.Add(row);
                    }
                    return table;
                } else {
                    return null;
                }
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Connect() {
            try {
                //var resourceString = "TCPIP::" + Regex.Replace(Ip, "0*([0-9]+)", "${1}") + "::" + Port + "::SOCKET";
                string resourceString = "TCPIP::" + Regex.Replace(Ip, "0*([0-9]+)", "${1}") + "::INSTR";
                //Instrument = new RsInstrument(resourceString, true, false, "SelectVisa=SocketIo");
                //Instrument.VisaTimeout = 5000;
                //Instrument.WriteString("WGEN:OUTP:ENAB OFF");
                //Thread.Sleep(500);

                Instrument = (MessageBasedSession)ResourceManager.GetLocalManager().Open(resourceString);
                Instrument.Timeout = 3000;
                Instrument.DefaultBufferSize = 134217728;
                Instrument.Clear();
                Thread.Sleep(500);
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task Disconnect() {
            try {
                //Instrument.Dispose();

                Instrument.Dispose();
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task ConfigureInitialSetup(int channels) {
            try {
                int chan1 = 0;
                int chan2 = 0;
                int chan3 = 0;
                //Setando os canais
                switch (channels) {
                    case 1:
                        chan1 = 1;
                        break;
                    case 2:
                        chan1 = 2;
                        break;
                    case 3:
                        chan1 = 3;
                        break;
                    case 4:
                        chan1 = 4;
                        break;
                    case 6:
                        chan1 = 1;
                        chan2 = 2;
                        chan3 = 3;
                        break;
                    case 7:
                        chan1 = 1;
                        chan2 = 2;
                        chan3 = 4;
                        break;
                    case 8:
                        chan1 = 1;
                        chan2 = 3;
                        chan3 = 4;
                        break;
                    case 9:
                        chan1 = 2;
                        chan2 = 3;
                        chan3 = 4;
                        break;
                }
                CleanOscilloscopeSetupPdms(4);
                if (channels < 5) {
                    //Limpas as configurações do osciloscópio
                    //Instrument.Write("*RST");

                    //Ativa os canais de entrada e saída
                    Instrument.Write($"CHAN{chan1}:STAT ON");

                    Instrument.Write("TRIG:A:MODE AUTO");

                    Instrument.Write($"PROB{chan1}:SET:ATT:UNIT V");

                    Instrument.Write($"PROB{chan1}:SET:ATT:MAN 1");

                    Instrument.Write($"CHAN{chan1}:RANG 10.0");

                    Instrument.Write("TIM:SCAL 0.002");

                    Instrument.Write("TIM:POS 0.0");

                    Instrument.Write($"CHAN{chan1}:POS 0");

                    Instrument.Write("ACQ:HRES AUTO");

                    Instrument.Write($"CHAN{chan1}:BAND FULL");

                    Instrument.Write("WGEN:FUNC SIN");
                    Instrument.Write($"WGEN:VOLT 2Vpp");
                    Instrument.Write($"WGEN:FREQ 120Hz");
                    Instrument.Write($"WGEN:NOIS:REL 30");
                    Instrument.Write("WGEN:OUTP 1");

                    Thread.Sleep(500);
                } else {
                    //Limpas as configurações do osciloscópio
                    //Instrument.Write("*RST");

                    //Ativa os canais de entrada e saída
                    Instrument.Write($"CHAN{chan1}:STAT ON");
                    Instrument.Write($"CHAN{chan2}:STAT ON");
                    Instrument.Write($"CHAN{chan3}:STAT ON");

                    Instrument.Write("TRIG:A:MODE AUTO");

                    Instrument.Write($"PROB{chan1}:SET:ATT:UNIT V");
                    Instrument.Write($"PROB{chan2}:SET:ATT:UNIT V");
                    Instrument.Write($"PROB{chan3}:SET:ATT:UNIT V");

                    Instrument.Write($"PROB{chan1}:SET:ATT:MAN 1");
                    Instrument.Write($"PROB{chan2}:SET:ATT:MAN 1");
                    Instrument.Write($"PROB{chan3}:SET:ATT:MAN 1");

                    Instrument.Write($"CHAN{chan1}:RANG 10.0");
                    Instrument.Write($"CHAN{chan2}:RANG 10.0");
                    Instrument.Write($"CHAN{chan3}:RANG 10.0");

                    Instrument.Write("TIM:SCAL 0.002");

                    Instrument.Write("TIM:POS 0.0");

                    Instrument.Write($"CHAN{chan1}:POS 0");
                    Instrument.Write($"CHAN{chan2}:POS 0");
                    Instrument.Write($"CHAN{chan3}:POS 0");

                    Instrument.Write("ACQ:HRES AUTO");

                    Instrument.Write($"CHAN{chan1}:BAND FULL");
                    Instrument.Write($"CHAN{chan2}:BAND FULL");
                    Instrument.Write($"CHAN{chan3}:BAND FULL");

                    Instrument.Write("WGEN:FUNC SIN");
                    Instrument.Write($"WGEN:VOLT 2Vpp");
                    Instrument.Write($"WGEN:FREQ 120Hz");
                    Instrument.Write($"WGEN:NOIS:REL 30");
                    Instrument.Write("WGEN:OUTP 1");
                    Thread.Sleep(500);
                }
                //messageBasedSession.Write("AUT");
                //messageBasedSession.Write($"CHAN1:OFFS 0V");
                //messageBasedSession.Write($"CHAN2:OFFS 0V");
                //messageBasedSession.Write($"CHAN3:OFFS 0V");
                //messageBasedSession.Write("ACQ:HRES AUTO");

                ////Ativa os canais de medição que calcularão a amplitude e fase
                ////das ondas geradas e atribui os canais que servirão como fontes
                //messageBasedSession.Write("MEAS1:ENAB ON");
                //messageBasedSession.Write("MEAS2:ENAB ON");
                //messageBasedSession.Write("MEAS3:ENAB ON");
                //messageBasedSession.Write("MEAS1:MAIN PEAK");
                //messageBasedSession.Write("MEAS2:MAIN PEAK");
                //messageBasedSession.Write("MEAS3:MAIN PEAK");
                ////Instrument.WriteString($"MEAS1:SOUR CH{FirstChannel}");
                ////Instrument.WriteString($"MEAS2:SOUR CH{SecondChannel}");
                ////Instrument.WriteString($"MEAS3:SOUR CH{SecondChannel},CH{FirstChannel}");
                //messageBasedSession.Write($"MEAS1:SOUR CH1");
                //messageBasedSession.Write($"MEAS2:SOUR CH2");
                //messageBasedSession.Write($"MEAS3:SOUR CH3");
                //messageBasedSession.Write($"MEAS:STAT:ENAB ON");

                //Configura o Trigger para o sinal de Entrada
                //Instrument.WriteString($"TRIG:A:SOUR CH1");
                //Instrument.WriteString($"TRIG:A:MODE AUTO");
                //Instrument.WriteString($"TRIG:A:TYPE EDGE");
                //} catch (RsInstrumentException e) {
            } catch (Exception e) {
                throw e;
            }
        }

        public async Task CleanOscilloscopeSetupPdms(int channels) {
            try {
                int count = 1;
                string channel;
                //Instrument.WriteString("*RST");

                Instrument.Write("*RST");
                while (count <= channels) {
                    //Instrument.WriteString($"CHAN{count.ToString()}:STAT OFF");

                    Instrument.Write($"CHAN{count.ToString()}:STAT OFF");
                    count++;
                    Thread.Sleep(500);
                }
            //} catch (RsInstrumentException e) {
            } catch (Exception e) {
                throw e;
            }
        }

        public WaveForm ReadWave(int channel) {
            try {


                Instrument.Write($"CHAN{channel}:DATA?;*OPC?");
                byte[] curveArray = Instrument.ReadByteArray();

                Instrument.DefaultBufferSize = 134217728;

                Instrument.Write("*CLS");
                Instrument.Write("*CLE");
                Instrument.Write("FORM UINT,8");
                Instrument.Write($"CHAN{channel}:DATA:POIN DEF");

                List<double> resp = new List<double>();

                string response = null;
                //Retorne o tempo da primeira amostra da forma de onda indicada.
                double xOrigin = -3E-05;
                double xIncrement = 1.6E-09;
                response = Instrument.Query($"CHAN{channel}:DATA:YOR?");
                double yOrigin = double.Parse(response, CultureInfo.InvariantCulture);
                response = Instrument.Query($"CHAN{channel}:DATA:YINC?");
                double yIncrement = double.Parse(response, CultureInfo.InvariantCulture);

                int length = curveArray.Count();
                int tailSamples = (int)(length * 0.001);

                double[] timeArray = new double[length - 3 * tailSamples];
                double[] voltageArray = new double[length - 3 * tailSamples];

                int tempCounterMax = curveArray.Count() - 2 * tailSamples;
                //Gerando a onda 
                Parallel.For(0, length, counter => {
                    if (counter >= tailSamples && counter < tempCounterMax) {
                        timeArray[counter - tailSamples] = (xOrigin + (xIncrement * counter));
                        voltageArray[counter - tailSamples] = (yOrigin + (yIncrement * curveArray[counter]));
                    }
                });
                WaveForm signal = new WaveForm();
                signal.x = timeArray.ToList();
                signal.y = voltageArray.ToList();
                return signal;

            //} catch (RsInstrumentException e) {
            } catch (Exception e) {
                throw e;
            }
        }

        public WaveForm WaveFilter(WaveForm wave, string filter, double upperCutOff, double lowerCutOff, double order, double gain, int phase, int channels) {
            try {
                int numberPoint = wave.x.Count;
                int windowSize = 50;
                int average = 50;
                int range = 0;
                //Fazendo a mediana do sinal de 50 em 50 pontos
                List<double> median = new List<double>();
                List<double> auxMedian = new List<double>();
                for (int i = 0; i < numberPoint; i++) {
                    if ((i + windowSize) < numberPoint) range = windowSize;
                    else range = windowSize - ((i + windowSize) - numberPoint);
                    auxMedian = wave.y.GetRange(i, range);
                    median.Add(Statistics.Median(auxMedian));
                }
                //Pegando a média móvel do sinal
                List<double> movingAverage = Statistics.MovingAverage(median, average).ToList();
                //Subtraindo o sinal da média móvel
                List<double> waveFiltredAxisY = new List<double>();
                double subtractedPointWave = 0;
                for (int k = 0; k < numberPoint; k++) {
                    subtractedPointWave = wave.y[k] - movingAverage[k];
                    waveFiltredAxisY.Add(subtractedPointWave);
                }
                //Pegando o desvio padrão do sinal
                double standardDeviation = Statistics.StandardDeviation(waveFiltredAxisY.ToArray());
                //Filtrando o sinal de acordo com o desvio padrão
                for (int w = 0; w < numberPoint; w++) {
                    if (Math.Abs(waveFiltredAxisY[w]) < 3 * standardDeviation) waveFiltredAxisY[w] = 0;
                }
                WaveForm waveFiltered = new WaveForm();
                waveFiltered.x = wave.x;
                waveFiltered.y = waveFiltredAxisY;
                List<double> ordinate = filterType(waveFiltered, filter, upperCutOff, lowerCutOff, order);
                waveFiltered.y = ordinate;

                double soma = 0, media = 0, desvio = 0;

                //if (calibrating) {
                for (int i = 0; i < waveFiltered.x.Count(); i++) {
                    soma += ordinate[i] * gain;
                }
                media = soma / (waveFiltered.x.Count());
                //}

                WaveForm result = new WaveForm();
                for (int i = 0; i < waveFiltered.x.Count; i++) {
                    if (true) {
                        result.x.Add(waveFiltered.x[i]);
                        result.y.Add(waveFiltered.y[i] * gain);
                    } else {
                        result.x.Add(waveFiltered.x[i]);
                        result.y.Add(0);
                    }
                    //if (calibrating) {
                    desvio += (ordinate[i] * gain - media) * (ordinate[i] * gain - media);
                    //}
                }
                //if (calibrating) desvioPadrao = Math.Sqrt(desvio / ordinate.Count());
                //TestPdmsThreePhase modelTest.desvioPadrao = Math.Sqrt(desvio / ordinate.Count());
                //TestPdmsThreePhase testPdmsThreePhase = new TestPdmsThreePhase();*/

                //switch (channels) {
                //    case 1:
                //        chan1 = 1;
                //        break;
                //    case 2:
                //        chan1 = 2;
                //        break;
                //    case 3:
                //        chan1 = 3;
                //        break;
                //    case 4:
                //        chan1 = 4;
                //        break;
                //    case 6:
                //        chan1 = 1;
                //        chan2 = 2;
                //        chan3 = 3;
                //        break;
                //    case 7:
                //        chan1 = 1;
                //        chan2 = 2;
                //        chan3 = 4;
                //        break;
                //    case 8:
                //        chan1 = 1;
                //        chan2 = 3;
                //        chan3 = 4;
                //        break;
                //    case 9:
                //        chan1 = 2;
                //        chan2 = 3;
                //        chan3 = 4;
                //        break;
                //}

                if (phase == 1) {
                    desvioPadrao1 = Math.Sqrt(desvio / ordinate.Count());
                } else if (phase == 2 && channels == 6) {
                    desvioPadrao2 = Math.Sqrt(desvio / ordinate.Count());
                } else if (phase == 2 && channels == 7) {
                    desvioPadrao2 = Math.Sqrt(desvio / ordinate.Count());
                } else if (phase == 2 && channels == 9) {
                    desvioPadrao1 = Math.Sqrt(desvio / ordinate.Count());
                } else if (phase == 3 && channels == 6) {
                    desvioPadrao3 = Math.Sqrt(desvio / ordinate.Count());
                } else if (phase == 3 && channels == 7) {
                    desvioPadrao3 = Math.Sqrt(desvio / ordinate.Count());
                } else if (phase == 3 && channels == 8) {
                    desvioPadrao2 = Math.Sqrt(desvio / ordinate.Count());
                } else if (phase == 4) {
                    desvioPadrao3 = Math.Sqrt(desvio / ordinate.Count());
                }

                //switch (phase) {
                //    case 1:
                //        desvioPadrao1 = Math.Sqrt(desvio / ordinate.Count());
                //        break;
                //    case 2:
                //        desvioPadrao2 = Math.Sqrt(desvio / ordinate.Count());
                //        break;
                //    case 3:
                //        desvioPadrao3 = Math.Sqrt(desvio / ordinate.Count());
                //        break;
                //    case 4:
                //        desvioPadrao3 = Math.Sqrt(desvio / ordinate.Count());
                //        break;
                //}
                return result;
            //} catch (RsInstrumentException e) {
            } catch (Exception e) {
                throw e;
            }
        }

        public WaveForm AcquireFFt(WaveForm wave, string filter, double upperCutOff, double lowerCutOff, double order) {
            try {

                int numberPoint = wave.x.Count;
                int windowSize = 50;
                int average = 50;
                int range = 0;
                //Fazendo a mediana do sinal de 50 em 50 pontos
                List<double> median = new List<double>();
                List<double> auxMedian = new List<double>();
                for (int l = 0; l < numberPoint; l++) {
                    auxMedian.Clear();
                    if ((l + windowSize) < numberPoint) range = windowSize;
                    else range = windowSize - ((l + windowSize) - numberPoint);
                    auxMedian = wave.y.GetRange(l, range);
                    median.Add(Statistics.Median(auxMedian));
                }
                //Pegando a média móvel do sinal
                List<double> movingAverage = Statistics.MovingAverage(median, average).ToList();
                //Subtraindo o sinal da média móvel
                List<double> waveFiltredAxisY = new List<double>();
                double subtractedPointWave = 0;
                for (int k = 0; k < numberPoint; k++) {
                    subtractedPointWave = wave.y[k] - movingAverage[k];
                    waveFiltredAxisY.Add(subtractedPointWave);
                }
                //Pegando o desvio padrão do sinal
                double standardDeviation = Statistics.StandardDeviation(waveFiltredAxisY.ToArray());
                //Filtrando o sinal de acordo com o desvio padrão
                double sumWave = 0;
                List<double> waveFftAxisY = new List<double>();
                for (int w = 0; w < numberPoint; w++) {
                    if (Math.Abs(waveFiltredAxisY[w]) < 3 * standardDeviation) waveFiltredAxisY[w] = 0;
                    sumWave = movingAverage[w] + waveFiltredAxisY[w];
                    waveFftAxisY.Add(sumWave);
                }
                WaveForm waveFft = new WaveForm();
                waveFft.x = wave.x;
                waveFft.y = waveFftAxisY;
                ////Filtrando o sinal de acordo com o filtro selecionado
                //List<double> orderAxisY = filterType(waveFft, filter, upperCutOff, lowerCutOff, order);
                //waveFft.y = orderAxisY;

                int L = 0;
                double Et = 0, Fs = 0, T = 0;
                Complex[] complex = null;
                List<Complex[]> compF2 = new List<Complex[]>();

                numberPoint = waveFft.x.Count;
                Et = waveFft.x.ElementAt(numberPoint - 1) - waveFft.x.ElementAt(0);
                double compOffsetF2 = waveFft.x.ElementAt(0);
                Fs = numberPoint / Et;
                double Fs2 = Fs;
                double passoCF2 = waveFft.x.ElementAt(1) - waveFft.x.ElementAt(0);

                complex = waveFft.y.Select(ordinate => new Complex(ordinate, 0.0)).ToArray();
                Fourier.Forward(complex, FourierOptions.Matlab);

                if (compF2.Count > 5)
                    compF2.Clear();
                compF2.Add(complex.ToArray());
                L = numberPoint;
                T = L / Fs;

                int compLF2 = L;
                double compFSF2 = Fs;




                double[] frequency = new double[L / 2];
                double[] amplitude = new double[L / 2];

                double max = -100000;

                List<double> freq = new List<double>();
                List<double> amp = new List<double>();

                int i = 0;
                int j = 0;
                double tempFreq = 0;
                double tempAmp = 0;

                while (i < (L / 2)) {
                    tempFreq = i * Fs / L;
                    tempAmp = 2 * complex[i].Magnitude / L;
                    if (tempAmp > max)
                        max = tempAmp;

                    freq.Add(tempFreq);
                    amp.Add(tempAmp);

                    if (tempFreq > 500) i = i + 1 + (int)Math.Ceiling(tempFreq / 300.0);
                    else i++;
                }


                for (i = 0; i < (L / 2); i++) {
                    frequency[i] = i * Fs / L;
                    amplitude[i] = 2 * complex[i].Magnitude / L;
                    if (amplitude[i] > max)
                        max = amplitude[i];
                }

                WaveForm result = new WaveForm();
                result.x = frequency.ToList();
                result.y = amplitude./*Select(ordinate => ordinate / max).*/ToList();
                //fftReady1.Set();
                //inUse1.Set();
                return result;
            //} catch (RsInstrumentException e) {
            } catch (Exception e) {
                throw e;
            }
        }

        public List<double> filterType(WaveForm wave, string filterType, double upperCutOff, double lowerCutOff, double order) {
            try {
                double[] ordinate = null;
                int numberPoint = wave.x.Count;
                double et = Math.Abs(wave.x.ElementAt(numberPoint - 1) - wave.x.ElementAt(0));
                double fs = numberPoint / et;
                switch (filterType) {
                    case "Passa Baixa":
                        var lowpass = MathNet.Filtering.OnlineFilter.CreateBandpass(MathNet.Filtering.ImpulseResponse.Finite, fs, 0, upperCutOff, (int)order);
                        ordinate = lowpass.ProcessSamples(wave.y.ToArray());
                        break;
                    case "Passa Alta":
                        var highpass = MathNet.Filtering.OnlineFilter.CreateHighpass(MathNet.Filtering.ImpulseResponse.Finite, fs, lowerCutOff, (int)order);
                        ordinate = highpass.ProcessSamples(wave.y.ToArray());
                        break;
                    case "Passa Banda":
                        var bandpass = MathNet.Filtering.OnlineFilter.CreateBandpass(MathNet.Filtering.ImpulseResponse.Finite, fs, lowerCutOff, upperCutOff, (int)order);
                        ordinate = bandpass.ProcessSamples(wave.y.ToArray());
                        break;
                    case "Passa Faixa":
                        var bandstop = MathNet.Filtering.OnlineFilter.CreateBandstop(MathNet.Filtering.ImpulseResponse.Finite, fs, upperCutOff, (int)order);
                        ordinate = bandstop.ProcessSamples(wave.y.ToArray());
                        break;
                    default:
                        lowpass = MathNet.Filtering.OnlineFilter.CreateBandpass(MathNet.Filtering.ImpulseResponse.Finite, fs, 0, upperCutOff, (int)order);
                        ordinate = lowpass.ProcessSamples(wave.y.ToArray());
                        break;
                }
                return ordinate.ToList();
            //} catch (RsInstrumentException e) {
            } catch (Exception e) {
                throw e;
            }
        }

        public WaveForm PureWave(WaveForm wave, string filter, double upperCutOff, double lowerCutOff, double order) {
            try {
                int numberPoint = wave.x.Count;
                int windowSize = 50;
                int average = 50;
                int range = 0;
                //Fazendo a mediana do sinal de 50 em 50 pontos
                List<double> median = new List<double>();
                List<double> auxMedian = new List<double>();
                for (int i = 0; i < numberPoint; i++) {
                    if ((i + windowSize) < numberPoint) range = windowSize;
                    else range = windowSize - ((i + windowSize) - numberPoint);
                    auxMedian = wave.y.GetRange(i, range);
                    median.Add(Statistics.Median(auxMedian));
                }
                //Pegando a média móvel do sinal
                List<double> movingAverage = Statistics.MovingAverage(median, average).ToList();
                WaveForm result = new WaveForm();
                result.x = wave.x;
                result.y = movingAverage;
                List<double> ordinate = filterType(result, filter, upperCutOff, lowerCutOff, order);
                result.y = ordinate;
                return result;
            } catch (Exception e) {
                throw e;
            }
        }

        public List<double>[] Dispersion(WaveForm signal, double gate, WaveForm wave, int phase, int channels) {
            try {
                List<double> areas = new List<double>();
                int countgate = 0;
                int max = 0;
                int last = 0;
                int i = 0;
                double area = 0.0;
                double tempArea = 0.0;
                double sampleFactor = 0.0;
                int starting_point = 0;
                int countsamples = 0;
                bool first = true;
                double R = 1;
                double C = 1e-3;
                double x, y, xtemp;
                int numberPoint = signal.x.Count;
                //gate = 0.000000005;
                double negativeGate = gate * (-1);
                WaveForm filtered = new WaveForm();
                Graph pdPoints = null;
                List<double>[] result = new List<double>[3];
                result[0] = new List<double>();
                result[1] = new List<double>();
                result[2] = new List<double>();
                //Pegando o desvio padrão das tensões do sinal
                double stdDeviation = Statistics.StandardDeviation(signal.y);
                //Pegando a maior tensão absoluta do sinal
                double maxVoltage = Statistics.MaximumAbsolute(signal.y);
                //Proteção para quando o sinal tem muito ruído
                bool gateError = false;
                if (3 * stdDeviation < gate && gate <= maxVoltage) gateError = false;
                else gateError = true;

                if (gateError) {
                    // Algoritmo - Area do Envelope
                    // Pontos absolutos para cruzar o 0 somente quando o envelope acaba
                    filtered.y = signal.y.Select(v => Math.Abs(v)).ToList();
                    filtered.x = signal.x;
                    for (i = 10; i < filtered.x.Count; i++) {
                        // Quando chega no fim da onda verifica se teve alguma ocorrencia
                        if (i == (filtered.y.Count - 10)) {
                            if (countgate == 1 && filtered.y[max] >= gate) {
                                tempArea = C * area / R;
                                tempArea *= sampleFactor;
                                areas.Add(tempArea);
                                xtemp = signal.x[max];
                                result[0].Add(signal.x[i]);
                                result[1].Add(tempArea);
                                result[2].Add(wave.y[i]);
                                //pdPoints.addNode(xtemp, tempArea, max);
                            }
                            break;
                        }
                        // Reconhecendo os pontos em que o sinal cruza o gate
                        if (countgate == 0 && (filtered.y[i] > gate && filtered.y[i - 1] <= gate)) {
                            countgate = 1;
                            starting_point = i;
                            first = false;
                            max = i;
                        }
                        // Reconhecendo os pontos em que o sinal cruza o gate
                        if (countgate > 0 && (filtered.y[i] < gate && filtered.y[i - 1] >= gate)) {
                            countgate = 2;
                        }
                        if (countgate == 2 && Math.Abs(filtered.y[max]) >= gate) {
                            tempArea = C * area / R;
                            tempArea *= sampleFactor;
                            areas.Add(tempArea);
                            xtemp = signal.x[max];
                            result[0].Add(signal.x[i]);
                            result[1].Add(tempArea);
                            result[2].Add(wave.y[i]);
                            //pdPoints.addNode(xtemp, tempArea, max);
                            countsamples = 0;
                            countgate = 0;
                            area = 0;
                            first = true;
                        }
                        if (countgate == 1) {
                            countsamples++;
                            if (countsamples <= 1) sampleFactor = 1;
                            else if (countsamples > 10) sampleFactor = 1 / (10 * countsamples);
                            else sampleFactor = 1.0;

                            if (Math.Abs(filtered.y[i]) > Math.Abs(filtered.y[max]))
                                max = i;
                            // Integral Trapezoidal
                            x = 2 * filtered.y[i] + 2 * filtered.y[i - 1];
                            y = Math.Abs(filtered.x[i] - filtered.x[i - 1]);
                            double sumarea = x * y / 2;
                            area += sumarea;
                        }
                    }
                }

                if (areas.Count() > 0) {
                    double maxArea = 0;
                    foreach (double item in areas) {
                        if (Math.Abs(item) > Math.Abs(maxArea))
                            maxArea = item;
                    }
                    dp = maxArea * 1000000000000;
                }

                areas.Sort();

                if (phase == 1) {
                    mediaF1 = mediaF1 + getMaxMedia(areas.ToArray(), dp);
                    mediaF1Count++;
                    partialDischargesPhase1.Add(Statistics.MaximumAbsolute(result[1]));
                    actualMaxValuePhase1 = Statistics.MaximumAbsolute(result[1]);
                    actualPartialDischargePhase1 = result[1].Count;
                    MaxCharges(result[1], result[2], 1);
                } else if (phase == 2 && channels == 6) {
                    mediaF2 = mediaF2 + getMaxMedia(areas.ToArray(), dp);
                    mediaF2Count++;
                    partialDischargesPhase2.Add(Statistics.MaximumAbsolute(result[1]));
                    actualMaxValuePhase2 = Statistics.MaximumAbsolute(result[1]);
                    actualPartialDischargePhase2 = result[1].Count;
                    MaxCharges(result[1], result[2], 2);
                } else if (phase == 2 && channels == 7) {
                    mediaF2 = mediaF2 + getMaxMedia(areas.ToArray(), dp);
                    mediaF2Count++;
                    partialDischargesPhase2.Add(Statistics.MaximumAbsolute(result[1]));
                    actualMaxValuePhase2 = Statistics.MaximumAbsolute(result[1]);
                    actualPartialDischargePhase2 = result[1].Count;
                    MaxCharges(result[1], result[2], 2);
                } else if (phase == 2 && channels == 9) {
                    mediaF1 = mediaF1 + getMaxMedia(areas.ToArray(), dp);
                    mediaF1Count++;
                    partialDischargesPhase1.Add(Statistics.MaximumAbsolute(result[1]));
                    actualMaxValuePhase1 = Statistics.MaximumAbsolute(result[1]);
                    actualPartialDischargePhase1 = result[1].Count;
                    MaxCharges(result[1], result[2], 1);
                } else if (phase == 3 && channels == 6) {
                    mediaF3 = mediaF3 + getMaxMedia(areas.ToArray(), dp);
                    mediaF3Count++;
                    partialDischargesPhase3.Add(Statistics.MaximumAbsolute(result[1]));
                    actualMaxValuePhase3 = Statistics.MaximumAbsolute(result[1]);
                    actualPartialDischargePhase3 = result[1].Count;
                    MaxCharges(result[1], result[2], 3);
                } else if (phase == 3 && channels == 7) {
                    mediaF3 = mediaF3 + getMaxMedia(areas.ToArray(), dp);
                    mediaF3Count++;
                    partialDischargesPhase3.Add(Statistics.MaximumAbsolute(result[1]));
                    actualMaxValuePhase3 = Statistics.MaximumAbsolute(result[1]);
                    actualPartialDischargePhase3 = result[1].Count;
                    MaxCharges(result[1], result[2], 3);
                } else if (phase == 3 && channels == 8) {
                    mediaF2 = mediaF2 + getMaxMedia(areas.ToArray(), dp);
                    mediaF2Count++;
                    partialDischargesPhase2.Add(Statistics.MaximumAbsolute(result[1]));
                    actualMaxValuePhase2 = Statistics.MaximumAbsolute(result[1]);
                    actualPartialDischargePhase2 = result[1].Count;
                    MaxCharges(result[1], result[2], 2);
                } else if (phase == 4) {
                    mediaF3 = mediaF3 + getMaxMedia(areas.ToArray(), dp);
                    mediaF3Count++;
                    partialDischargesPhase3.Add(Statistics.MaximumAbsolute(result[1]));
                    actualMaxValuePhase3 = Statistics.MaximumAbsolute(result[1]);
                    actualPartialDischargePhase3 = result[1].Count;
                    MaxCharges(result[1], result[2], 3);
                }

                //switch (phase) {
                //    case 1:
                //        mediaF1 = mediaF1 + getMaxMedia(areas.ToArray(), dp);
                //        mediaF1Count++;
                //        partialDischargesPhase1.Add(Statistics.MaximumAbsolute(result[1]));
                //        actualMaxValuePhase1 = Statistics.MaximumAbsolute(result[1]);
                //        actualPartialDischargePhase1 = result[1].Count;
                //        MaxCharges(result[1], result[2],1);
                //        break;
                //    case 2:
                //        mediaF2 = mediaF2 + getMaxMedia(areas.ToArray(), dp);
                //        mediaF2Count++;
                //        partialDischargesPhase2.Add(Statistics.MaximumAbsolute(result[1]));
                //        actualMaxValuePhase2 = Statistics.MaximumAbsolute(result[1]);
                //        actualPartialDischargePhase2 = result[1].Count;
                //        MaxCharges(result[1], result[2],2);
                //        break;
                //    case 3:
                //        mediaF3 = mediaF3 + getMaxMedia(areas.ToArray(), dp);
                //        mediaF3Count++;
                //        partialDischargesPhase3.Add(Statistics.MaximumAbsolute(result[1]));
                //        actualMaxValuePhase3 = Statistics.MaximumAbsolute(result[1]);
                //        actualPartialDischargePhase3 = result[1].Count;
                //        MaxCharges(result[1], result[2],3);
                //        break;
                //}

                //partialDischargesPhase1.Add(Statistics.MaximumAbsolute(result[1]));
                //actualMaxValuePhase1 = Statistics.MaximumAbsolute(result[1]);
                //actualPartialDischargePhase1 = result[1].Count;
                //MaxCharges(result[1], result[2]);

                return result;
            //} catch (RsInstrumentException e) {
            } catch (Exception e) {
                throw e;
            }
        }

        public WaveForm DispersionDatailed(WaveForm wave, WaveForm waveDispered) {
            try {
                int numberPointWave = wave.x.Count;
                int numberPointDispersion = waveDispered.x.Count;
                WaveForm result = new WaveForm();
                for (int i = 0; i < numberPointDispersion; i++) {
                    for (int j = 0; j < numberPointWave; j++) {
                        if (waveDispered.x[i] == wave.x[j]) {
                            result.x.Add(waveDispered.x[i]);
                            result.y.Add(wave.y[j]);
                        }
                    }
                }
                return result;
            //} catch (RsInstrumentException e) {
            } catch (Exception e) {
                throw e;

            }
        }

        public List<double>[] PartialDischargeTrend(List<double> value) {
            try {
                int countValue = value.Count;
                double position = 0;
                List<double>[] result = new List<double>[2];
                result[0] = new List<double>();
                result[1] = new List<double>();
                for (int i =0; i<countValue; i++) {
                    result[0].Add(position);
                    result[1].Add(value[i]);
                    position++;
                }

                return result;
                //} catch (RsInstrumentException e) {
            } catch (Exception e) {
                throw e;

            }
        }

        public void MaxCharges(List<double> values, List<double> wave, int phase) {
            int count = values.Count;
            List<double> positiveValues = new List<double>();
            List<double> negativeValues = new List<double>();
            for (int i = 0; i < count; i++) {
                if (wave[i] > 0) {
                    positiveValues.Add(values[i]);
                }
                else{
                    negativeValues.Add(values[i]);
                }
            }
            switch (phase) {
                case 1:
                    actualMaxPositiveValuePhase1 = Statistics.MaximumAbsolute(positiveValues);
                    actualMaxNegativeValuePhase1 = Statistics.MaximumAbsolute(negativeValues);
                    break;
                case 2:
                    actualMaxPositiveValuePhase2 = Statistics.MaximumAbsolute(positiveValues);
                    actualMaxNegativeValuePhase2 = Statistics.MaximumAbsolute(negativeValues);
                    break;
                case 3:
                    actualMaxPositiveValuePhase3 = Statistics.MaximumAbsolute(positiveValues);
                    actualMaxNegativeValuePhase3 = Statistics.MaximumAbsolute(negativeValues);
                    break;
            }
            
        }

        public double UpdateGain(double oldC) {
            //oldC -> last gain
            //double oldDP = osciloscope.dp;
            double error = target - dp;

            //if (Math.Abs(error) < (target * 0.2) || !calibrating) {
            if (Math.Abs(error) < (target * 0.2)) {
                //calibrating = false;
                //stopCalibration = true;
                //return (int)oldC;
                return oldC;
            }

            if (max > 0) {
                // reached the maximum, works within the identified range
                if (error < 0) //gain is between min and c
                {
                    max = oldC;
                    oldC = (min + max) / 2;
                    dp *= oldC;
                } else // gain is between c and max
                  {
                    min = oldC;
                    oldC = (min + max) / 2;
                    dp *= oldC;
                }

                //return (int)oldC;
                return oldC;
            }
            // finds minimum and maximum
            if (target > dp) {
                min = oldC;
                oldC *= 2;
                dp *= oldC;

            } else {
                max = oldC;
                oldC = (min + max) / 2;
                dp *= oldC;
            }

            //return (int)oldC;
            return oldC;

        }

        public double UpdateGate(int phase) {
            double desvioPadrao = 0;
            switch (phase) {
                case 1:
                    desvioPadrao = this.desvioPadrao1;
                    break;
                case 2:
                    desvioPadrao = this.desvioPadrao2;
                    break;
                case 3:
                    desvioPadrao = this.desvioPadrao3;
                    break;
            }

            if (desvioPadrao == 0)
                return 0;

            decimal k = (decimal)(desvioPadrao * 0.75);

            if (k > 20)
                k = 20;

            else if (k < (decimal)0.00001)
                k = (decimal)0.00001;

            //return (int)(k * 100000);
            double result = Convert.ToDouble(k);
            result = result * 10;
            result = Math.Round(result, 3);
            return result;
        }

        public void startCalibration(double target) {
            sum = 0;
            this.target = target;
            last = 0;
            min = 0;
            max = 0;
            //calibrating = true;
        }

        public double getMaxMedia(double[] area, double max) {
            double sum = 0;
            int n = area.Length >= 5 ? 5 : area.Length;
            if (n == 0)
                return 0;

            if (max > 0) {
                for (int i = area.Length - 1; i > area.Length - n - 1; i--) {
                    sum += Math.Abs(area[i]);
                }
            } else {
                for (int i = 0; i < n; i++) {
                    sum += Math.Abs(area[i]);
                }
            }

            return sum / n;
        }
    }
}
