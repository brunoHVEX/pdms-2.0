﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model {
    class TestPdmsThreePhase: Test  {

        OscilloscopePdms osciloscope = new OscilloscopePdms();

        public TestPdmsThreePhase() {
            Id = new ObjectId();
            Deleted = false;
        }
        override public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"NOVO ENSAIO REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<TestPdmsThreePhase>.Filter.Eq(u => u.Id, Id)
                );
                var test = collection.FirstOrDefault();
                if (test != null) {
                    Instrument = test.Instrument;
                    User = test.User;
                    //Frequency = test.Frequency;
                    //Amplitude = test.Amplitude;
                    //Phase = test.Phase;
                    Date = test.Date;
                    Duration = test.Duration;
                    Deleted = test.Deleted;
                } else {
                    throw new Exception("Nenhum ensaio, que atenda aos parâmetros especificados, foi encontrado.");
                }
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"ENSAIO {Id} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<TestPdmsThreePhase>.Filter.Eq(t => t.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"ENSAIO {Id} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<TestPdmsThreePhase>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
