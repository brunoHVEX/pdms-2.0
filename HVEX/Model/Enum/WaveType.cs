﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model.Enum {
    enum WaveType {
        signal1 = 1,
        signal2 = 12,
        signal3 = 13,

        filtered1 = 2,
        filtered2 = 22,
        filtered3 = 23,

        fft1 = 3,
        fft2 = 32,
        fft3 = 33,

        dp1 = 4,
        dp2 = 42,
        dp3 = 43,

        riv = 5
    }
}
