﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model.Enum {
    enum Connection {
        Yy0,
        Dd0,
        Dz0,
        Zz0,
        Yd1,
        Dy1,
        Yz1,
        Yd5,
        Dy5,
        Yz5,
        Yy6,
        Dd6,
        Dz6,
        Yd11,
        Dy11,
        Yz11,
        Dd2,
        Dz2,
        Dd4,
        Dz4,
        Yd7,
        Dy7,
        Yz7,
        Dd8,
        Dz8,
        Dd10,
        Dz10
    }
}
