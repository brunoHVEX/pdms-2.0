﻿namespace HVEX.Model.Enum {
    enum AccessLevel {
        Master,
        Administrador,
        Engenheiro,
        Técnico
    }
}
