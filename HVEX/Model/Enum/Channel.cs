﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model.Enum {
    enum Channel {
        CH1 = 1,
        CH2 = 2,
        CH3 = 3,
        CH4 = 4,
        RIV = 5
    }
}
