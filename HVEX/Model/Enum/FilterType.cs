﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model.Enum {
    enum FilterType {
        LowPass = 0,
        BandPass = 1,
        HighPass = 2,
        BandReject = 3
    }
}
