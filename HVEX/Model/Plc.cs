﻿using HVEX.Service;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.Model {
    class Plc : Instruments {
        [BsonElement("acquisitionInterval")]
        public int AcquisitionInterval { get; set; }

        public Plc() {
            Id = new ObjectId();
            Deleted = false;
        }
        public Plc(
            string serialNumber,
            string description,
            string brand,
            string model,
            string ip,
            int port,
            int acquisitionInterval) : base(
                serialNumber,
                description,
                brand,
                model,
                ip,
                port) {
            Id = new ObjectId();
            SerialNumber = serialNumber;
            Description = description;
            Brand = brand;
            Model = model;
            Ip = ip;
            Port = port;
            AcquisitionInterval = acquisitionInterval;
            Deleted = false;
        }
        override public async Task Create(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"CLP {SerialNumber} REGISTRADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Create(collectionName, this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }
        override public async Task Read() {
            try {
                var collection = await Mongo.Read(
                    collectionName,
                    Builders<Plc>.Filter.Eq(u => u.Id, Id)
                );
                var osciloscope = collection.FirstOrDefault();
                if (osciloscope != null) {
                    SerialNumber = osciloscope.SerialNumber;
                    Description = osciloscope.Description;
                    Brand = osciloscope.Brand;
                    Model = osciloscope.Model;
                    Ip = osciloscope.Ip;
                    Port = osciloscope.Port;
                    AcquisitionInterval = osciloscope.AcquisitionInterval;
                } else {
                    throw new Exception("Nenhum CLP, que atenda aos parâmetros especificados, foi encontrado.");
                }
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Update(ObjectId userId) {
            try {
                Logger log = new Logger(
                    $"CLP {SerialNumber} ALTERADO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Update(collectionName, Builders<Plc>.Filter.Eq(o => o.Id, Id), this);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task Delete(ObjectId userId) {
            try {
                await Read();
                Logger log = new Logger(
                    $"CLP {SerialNumber} REMOVIDO.",
                    Environment.MachineName,
                    DateTime.Now,
                    userId
                );
                await Mongo.Delete<Plc>(collectionName, Id);
                await log.Create();
            } catch (Exception e) {
                throw e;
            }
        }

        override public async Task<DataTable> List(string order) {
            List<Plc> plcs = new List<Plc>();
            switch (order) {
                case "Número de Série":
                    order = "serial_number";
                    break;
                case "Marca":
                    order = "brand";
                    break;
                case "Modelo":
                    order = "model";
                    break;
            }
            try {
                List<Plc> collection;
                if (!string.IsNullOrEmpty(SerialNumber) && !string.IsNullOrEmpty(Brand)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Plc>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower()) &&
                            t.Brand.ToLower().Contains(Brand.ToLower())
                        ) &
                        Builders<Plc>.Filter.OfType<Plc>(),
                        1000,
                        order
                    );
                } else if (!string.IsNullOrEmpty(SerialNumber)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Plc>.Filter.Where(
                            t => !t.Deleted &&
                            t.SerialNumber.ToLower().Contains(SerialNumber.ToLower())
                        ) &
                        Builders<Plc>.Filter.OfType<Plc>(),
                        1000,
                        order
                    );
                } else if (!string.IsNullOrEmpty(Brand)) {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Plc>.Filter.Where(
                            t => !t.Deleted &&
                            t.Brand.ToLower().Contains(Brand.ToLower())
                        ) &
                        Builders<Plc>.Filter.OfType<Plc>(),
                        1000,
                        order
                    );
                } else {
                    collection = await Mongo.Read(
                        collectionName,
                        Builders<Plc>.Filter.Where(
                            t => !t.Deleted
                        ) &
                        Builders<Plc>.Filter.OfType<Plc>(),
                        1000,
                        order
                    );
                }
                if (collection.Count > 0) {
                    plcs.AddRange(collection);
                    DataTable table = new DataTable();
                    table.Columns.Add(new DataColumn("Id"));
                    table.Columns.Add(new DataColumn("Tipo"));
                    table.Columns.Add(new DataColumn("Número de Série"));
                    table.Columns.Add(new DataColumn("Marca"));
                    table.Columns.Add(new DataColumn("Modelo"));
                    foreach (Plc plc in plcs) {
                        DataRow row = table.NewRow();
                        row.BeginEdit();
                        row["Id"] = plc.Id;
                        row["Tipo"] = "Plc";
                        row["Número de Série"] = plc.SerialNumber;
                        row["Marca"] = plc.Brand;
                        row["Modelo"] = plc.Model;
                        row.EndEdit();
                        table.Rows.Add(row);
                    }
                    return table;
                } else {
                    return null;
                }
            } catch (Exception e) {
                throw e;
            }
        }
    }
}
