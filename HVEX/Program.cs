﻿using System;
using System.Windows.Forms;
using HVEX.View;

namespace HVEX {
    static class Program {
        /// <summary>
        /// Ponto de entrada principal para o aplicativo.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FrmSplashScreen splashScreen = new FrmSplashScreen();
            splashScreen.Show();
            Application.Run();
        }
    }
}
