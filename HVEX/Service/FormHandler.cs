﻿using System;
using System.Windows.Forms;

namespace HVEX.Service {
    class FormHandler {
        public static bool IsFormOpened(string frmName) {
            bool opened = false;
            foreach (Form frm in Application.OpenForms) {
                if (frm.Name == frmName) {
                    opened = true;
                    frm.Focus();
                }
            }
            return opened;
        }

        public static void CloseForm() {
            if (Application.OpenForms.Count == 0) {
                Environment.Exit(1);
            }
        }
    }
}
