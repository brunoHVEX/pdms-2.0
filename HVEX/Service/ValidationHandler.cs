﻿using System;
using System.Windows.Forms;

namespace HVEX.Service {
    class ValidationHandler {
        public static bool IsValidDateTime(MaskedTextBox textBox) {
            var aux = textBox.Text;
            textBox.TextMaskFormat = MaskFormat.ExcludePromptAndLiterals;
            if (textBox.Text != "") {
                textBox.TextMaskFormat = MaskFormat.IncludeLiterals;
                DateTime tempDate;
                return DateTime.TryParse(aux, out tempDate);
            } else {
                textBox.TextMaskFormat = MaskFormat.IncludeLiterals;
                return true;
            }
        }
        public static void IsNumber(KeyPressEventArgs e) {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != ',') {
                e.Handled = true;
            }
        }
        public static void IsDot(KeyPressEventArgs e) {
            if (e.KeyChar == ',') {
                e.KeyChar = '.';
            }
        }
        public static void IsInteger(KeyPressEventArgs e) {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar)) {
                e.Handled = true;
            }
        }
        public static void SupressEnter(KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
            }
        }
        public static bool IsTextBoxNull(TextBox textBox, string name) {
            if (textBox.Text.Trim() == "") {
                textBox.Focus();
                MessageBox.Show(
                    $"O campo {name} não pode estar vazio!",
                    "Atenção",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                );
                return true;
            } else {
                return false;
            }
        }
        public static bool IsComboBoxNull(ComboBox combobox, string name) {
            if (combobox.Text.Trim() == "") {
                combobox.Focus();
                MessageBox.Show(
                    $"O campo {name} não pode estar vazio.",
                    "Atenção!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                );
                return true;
            } else {
                return false;
            }
        }
        public static bool IsMaskedBoxNull(MaskedTextBox maskedBox, string name) {
            if (!maskedBox.MaskCompleted) {
                maskedBox.Focus();
                MessageBox.Show(
                    $"O campo {name} não pode estar vazio!",
                    "Atenção",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning
                );
                return true;
            } else {
                return false;
            }
        }
    }
}
