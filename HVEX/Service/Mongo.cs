﻿using HVEX.Model;
using HVEX.Model.Enum;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVEX.Service {
    class Mongo {
        private static IMongoClient client = new MongoClient("mongodb://127.0.0.1:27017");

        public static IMongoDatabase GetDatabase() {
            return client.GetDatabase("hvex");
        }

        public static async Task Create<BsonDocument>(string collectionName, BsonDocument document) {
            IMongoDatabase db = GetDatabase();
            var collection = db.GetCollection<BsonDocument>(collectionName);
            await collection.InsertOneAsync(document);
        }

        public static async Task<List<T>> Read<T>(
            string collectionName,
            FilterDefinition<T> filter,
            int limit = 100,
            string sort = "ObjectId",
            int orientation = 1
        ) {
            IMongoDatabase db = GetDatabase();
            var collection = db.GetCollection<T>(collectionName);
            List<T> result = await collection.Find(filter).Sort(
                "{" + sort + ":" + orientation + "}"
            ).Limit(limit).ToListAsync();
            return result;
        }

        public static async Task Update<BsonDocument>(
            string collectionName,
            FilterDefinition<BsonDocument> filter,
            BsonDocument document
        ) {
            IMongoDatabase db = GetDatabase();
            var collection = db.GetCollection<BsonDocument>(collectionName);
            await collection.ReplaceOneAsync(filter, document);
        }

        public static async Task Delete<T>(string collectionName, ObjectId id) {
            IMongoDatabase db = GetDatabase();
            var collection = db.GetCollection<T>(collectionName);
            var filter = Builders<T>.Filter.Eq("_id", id);
            var update = Builders<T>.Update.Set("deleted", true);
            await collection.UpdateOneAsync(filter, update);
        }

        public static void ListDatabases() {
            var databases = client.ListDatabaseNames().ToList();
            if (!databases.Contains("hvex")) {
                InitialConfig();
            }
        }

        public static bool InitialConfig() {
            try {
                User userMaster = new User(
                    "Master",
                    "master",
                    "Gb4CdP8g",
                    AccessLevel.Master.ToString(),
                    true,
                    ImageHandler.ImageToByteArray(Properties.Resources.user)
                );
                Task taskMaster = Task.Run(async () => await userMaster.CreateUserMaster(new ObjectId()));
                
                User userAdministrador = new User(
                    "Administrador",
                    "admin",
                    "admin",
                    AccessLevel.Administrador.ToString(),
                    ImageHandler.ImageToByteArray(Properties.Resources.user)
                );
                Task taskAdministrador = Task.Run(async () => await userAdministrador.Create(new ObjectId()));

                List<double> gain = new List<double>();
                gain.Add(100);
                gain.Add(100);
                gain.Add(100);
                List<double> gate = new List<double>();
                gate.Add(0.5);
                gate.Add(0.5);
                gate.Add(0.5);
                int filter = 2;
                double order = 500;
                List<double> frequencyUndercut = new List<double>();
                frequencyUndercut.Add(40);
                frequencyUndercut.Add(1);
                List<double> frequencyTopCut = new List<double>();
                frequencyTopCut.Add(1);
                frequencyTopCut.Add(2);
                List<double> calibration = new List<double>();
                calibration.Add(0);
                calibration.Add(0);
                calibration.Add(0);
                ConfigurationThreePhase configurationThreePhase = new ConfigurationThreePhase(
                                    gain,
                                    gate,
                                    filter,
                                    order,
                                    frequencyUndercut,
                                    frequencyTopCut,
                                    calibration);
                Task taskConfigurationThreePhase = Task.Run(async () => await configurationThreePhase.Create(new ObjectId()));

                double gainMono = 100;
                double gateMono = 0.5;
                int filterMono = 2;
                double orderMono = 500;
                List<double> frequencyUndercutMono = new List<double>();
                frequencyUndercutMono.Add(40);
                frequencyUndercutMono.Add(1);
                List<double> frequencyTopCutMono = new List<double>();
                frequencyTopCutMono.Add(1);
                frequencyTopCutMono.Add(2);
                double calibrationMono = 0;
                ConfigurationSinglePhase configurationSinglePhase = new ConfigurationSinglePhase(
                                    gainMono,
                                    gateMono,
                                    filterMono,
                                    orderMono,
                                    frequencyUndercutMono,
                                    frequencyTopCutMono,
                                    calibrationMono);
                Task taskConfigurationSinglePhase = Task.Run(async () => await configurationSinglePhase.Create(new ObjectId()));
                return true;
            } catch (Exception e) {
                new FormException($"Houve um erro ao tentar aplicar as configurações iniciais.\n{e.Message}");
                return false;
            }
        }
    }
}
