﻿using System;
using System.Windows.Forms;

namespace HVEX.Service {
    class DatabaseException : ApplicationException {
        public DatabaseException(string message) {
            MessageBox.Show(
                message,
                "Erro de conexão com o banco de dados!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning
            );
            return;
        }
    }

    class ImageException : ApplicationException {
        public ImageException(string message) {
            MessageBox.Show(
                message,
                "Erro ao tentar processar a imagem!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning
            );
            return;
        }
    }

    class FileException : ApplicationException {
        public FileException(string fileName, string errorMessage) {
            MessageBox.Show(
               $"Houve um erro ao tentar processar o arquivo {fileName}.\n{errorMessage}",
               "Erro ao tentar processar arquivo!",
               MessageBoxButtons.OK,
               MessageBoxIcon.Warning
            );
            return;
        }
    }

    class FormException : ApplicationException {
        public FormException(string message) {
            MessageBox.Show(
                message,
                "Erro!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information
            );
            return;
        }
    }

    class HardwareException : ApplicationException {
        public HardwareException(string message) {
            MessageBox.Show(
                message,
                "Erro de conexão com o hardware!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information
            );
            return;
        }
    }
}
