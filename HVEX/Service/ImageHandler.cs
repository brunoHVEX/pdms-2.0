﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace HVEX.Service {
    class ImageHandler {
        public static byte[] ImageToByteArray(Image image) {
            try {
                using (var ms = new MemoryStream()) {
                    image.Save(ms, ImageFormat.Png);
                    return ms.ToArray();
                }
            } catch (Exception e) {
                new FormException($"Houve um erro ao tentar converter a imagem para bytes.\n{e.Message}");
                return null;
            }
        }

        public static Image ByteArrayToImage(byte[] image) {
            try {
                var loadImage = (Bitmap)((new ImageConverter()).ConvertFrom(image));
                return loadImage;
            } catch (Exception e) {
                new FormException($"Houve um erro ao tentar recuperar a imagem do banco de dados.\n{e.Message}");
                return null;
            }
        }

        public static void SaveImage(byte[] imageData, ImageFormat imageFormat, string fileName) {
            Image image;
            try {
                MemoryStream memoryStream = new MemoryStream(imageData, 0, imageData.Length);
                image = Image.FromStream(memoryStream, true);
                image.Save(fileName, imageFormat);
            } catch (Exception e) {
                new FormException($"Houve um erro ao tentar recuperar a imagem do banco de dados.\n{e.Message}");
            }
        }
    }
}
