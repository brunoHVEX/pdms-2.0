﻿
namespace HVEX.Service {
    static class MeasurementHandler {
        public static double ConvertToV(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "V":
                    newVal = value;
                    break;
                case "kV":
                    newVal = value * 1000;
                    break;
                case "MV":
                    newVal = value * 1000000;
                    break;
            }
            return newVal;
        }

        public static double ConvertToKV(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "V":
                    newVal = value / 1000;
                    break;
                case "kV":
                    newVal = value;
                    break;
                case "MV":
                    newVal = value * 1000;
                    break;
            }
            return newVal;
        }

        public static double ConvertToMV(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "V":
                    newVal = value / 1000000;
                    break;
                case "kV":
                    newVal = value / 1000;
                    break;
                case "MV":
                    newVal = value;
                    break;
            }
            return newVal;
        }

        public static double ConvertToA(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "A":
                    newVal = value;
                    break;
                case "kA":
                    newVal = value * 1000;
                    break;
            }
            return newVal;
        }

        public static double ConvertToKA(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "A":
                    newVal = value / 1000;
                    break;
                case "kA":
                    newVal = value;
                    break;
            }
            return newVal;
        }

        public static double ConvertToVA(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "VA":
                    newVal = value;
                    break;
                case "kVA":
                    newVal = value * 1000;
                    break;
                case "MVA":
                    newVal = value * 1000000;
                    break;
            }
            return newVal;
        }

        public static double ConvertTokVA(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "VA":
                    newVal = value / 1000;
                    break;
                case "kVA":
                    newVal = value;
                    break;
                case "MVA":
                    newVal = value * 1000;
                    break;
            }
            return newVal;
        }

        public static double ConvertToMVA(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "VA":
                    newVal = value / 1000000;
                    break;
                case "kVA":
                    newVal = value / 1000;
                    break;
                case "MVA":
                    newVal = value;
                    break;
            }
            return newVal;
        }

        public static double ConvertToHz(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "Hz":
                    newVal = value;
                    break;
                case "kHz":
                    newVal = value * 1000;
                    break;
                case "MHz":
                    newVal = value * 1000000;
                    break;
            }
            return newVal;
        }

        public static double ConvertTokHz(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "Hz":
                    newVal = value / 1000;
                    break;
                case "kHz":
                    newVal = value;
                    break;
                case "MHz":
                    newVal = value * 1000;
                    break;
            }
            return newVal;
        }

        public static double ConvertToMHz(double value, string unit) {
            double newVal = new double();
            switch (unit) {
                case "Hz":
                    newVal = value / 1000000;
                    break;
                case "kHz":
                    newVal = value / 1000;
                    break;
                case "MHz":
                    newVal = value;
                    break;
            }
            return newVal;
        }
    }
}
