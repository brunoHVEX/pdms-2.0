﻿
namespace HVEX.View {
    partial class FrmManufacturer {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmManufacturer));
            this.tabManufacturer = new System.Windows.Forms.TabControl();
            this.tabRegister = new System.Windows.Forms.TabPage();
            this.btClean = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.pbManufacturer = new System.Windows.Forms.PictureBox();
            this.tbPhone = new System.Windows.Forms.MaskedTextBox();
            this.tbCountry = new System.Windows.Forms.TextBox();
            this.tbState = new System.Windows.Forms.TextBox();
            this.tbCity = new System.Windows.Forms.TextBox();
            this.tbDistrict = new System.Windows.Forms.TextBox();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.tbEmail = new System.Windows.Forms.TextBox();
            this.tbContact = new System.Windows.Forms.TextBox();
            this.tbFantasyName = new System.Windows.Forms.TextBox();
            this.tbCompanyName = new System.Windows.Forms.TextBox();
            this.lbCountry = new System.Windows.Forms.Label();
            this.lbState = new System.Windows.Forms.Label();
            this.lbCity = new System.Windows.Forms.Label();
            this.lbDistrict = new System.Windows.Forms.Label();
            this.lbAddress = new System.Windows.Forms.Label();
            this.lbEmail = new System.Windows.Forms.Label();
            this.lbContact = new System.Windows.Forms.Label();
            this.lbPhone = new System.Windows.Forms.Label();
            this.lbFantasyName = new System.Windows.Forms.Label();
            this.lbCompanyName = new System.Windows.Forms.Label();
            this.tabSearch = new System.Windows.Forms.TabPage();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btSearch = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.dgManufacturer = new System.Windows.Forms.DataGridView();
            this.cbOrder = new System.Windows.Forms.ComboBox();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lbOrder = new System.Windows.Forms.Label();
            this.lbSearch = new System.Windows.Forms.Label();
            this.tabManufacturer.SuspendLayout();
            this.tabRegister.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbManufacturer)).BeginInit();
            this.tabSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgManufacturer)).BeginInit();
            this.SuspendLayout();
            // 
            // tabManufacturer
            // 
            this.tabManufacturer.Controls.Add(this.tabRegister);
            this.tabManufacturer.Controls.Add(this.tabSearch);
            this.tabManufacturer.ItemSize = new System.Drawing.Size(54, 18);
            this.tabManufacturer.Location = new System.Drawing.Point(12, 12);
            this.tabManufacturer.Name = "tabManufacturer";
            this.tabManufacturer.SelectedIndex = 0;
            this.tabManufacturer.Size = new System.Drawing.Size(760, 417);
            this.tabManufacturer.TabIndex = 1;
            this.tabManufacturer.SelectedIndexChanged += new System.EventHandler(this.tabManufacturer_SelectedIndexChanged);
            // 
            // tabRegister
            // 
            this.tabRegister.Controls.Add(this.btClean);
            this.tabRegister.Controls.Add(this.btSave);
            this.tabRegister.Controls.Add(this.pbManufacturer);
            this.tabRegister.Controls.Add(this.tbPhone);
            this.tabRegister.Controls.Add(this.tbCountry);
            this.tabRegister.Controls.Add(this.tbState);
            this.tabRegister.Controls.Add(this.tbCity);
            this.tabRegister.Controls.Add(this.tbDistrict);
            this.tabRegister.Controls.Add(this.tbAddress);
            this.tabRegister.Controls.Add(this.tbEmail);
            this.tabRegister.Controls.Add(this.tbContact);
            this.tabRegister.Controls.Add(this.tbFantasyName);
            this.tabRegister.Controls.Add(this.tbCompanyName);
            this.tabRegister.Controls.Add(this.lbCountry);
            this.tabRegister.Controls.Add(this.lbState);
            this.tabRegister.Controls.Add(this.lbCity);
            this.tabRegister.Controls.Add(this.lbDistrict);
            this.tabRegister.Controls.Add(this.lbAddress);
            this.tabRegister.Controls.Add(this.lbEmail);
            this.tabRegister.Controls.Add(this.lbContact);
            this.tabRegister.Controls.Add(this.lbPhone);
            this.tabRegister.Controls.Add(this.lbFantasyName);
            this.tabRegister.Controls.Add(this.lbCompanyName);
            this.tabRegister.Location = new System.Drawing.Point(4, 22);
            this.tabRegister.Name = "tabRegister";
            this.tabRegister.Padding = new System.Windows.Forms.Padding(3);
            this.tabRegister.Size = new System.Drawing.Size(752, 391);
            this.tabRegister.TabIndex = 0;
            this.tabRegister.Text = "Registrar";
            this.tabRegister.UseVisualStyleBackColor = true;
            // 
            // btClean
            // 
            this.btClean.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btClean.FlatAppearance.BorderSize = 0;
            this.btClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btClean.ForeColor = System.Drawing.Color.White;
            this.btClean.Image = global::HVEX.Properties.Resources.white_clean;
            this.btClean.Location = new System.Drawing.Point(669, 357);
            this.btClean.Name = "btClean";
            this.btClean.Size = new System.Drawing.Size(75, 23);
            this.btClean.TabIndex = 12;
            this.btClean.Text = "Limpar";
            this.btClean.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btClean.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btClean.UseVisualStyleBackColor = false;
            this.btClean.Click += new System.EventHandler(this.btClean_Click);
            // 
            // btSave
            // 
            this.btSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btSave.FlatAppearance.BorderSize = 0;
            this.btSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSave.ForeColor = System.Drawing.Color.White;
            this.btSave.Image = global::HVEX.Properties.Resources.white_save;
            this.btSave.Location = new System.Drawing.Point(588, 357);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(75, 23);
            this.btSave.TabIndex = 11;
            this.btSave.Text = "Salvar";
            this.btSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.UseVisualStyleBackColor = false;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // pbManufacturer
            // 
            this.pbManufacturer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbManufacturer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbManufacturer.Image = global::HVEX.Properties.Resources.company_image;
            this.pbManufacturer.Location = new System.Drawing.Point(474, 26);
            this.pbManufacturer.Name = "pbManufacturer";
            this.pbManufacturer.Size = new System.Drawing.Size(270, 270);
            this.pbManufacturer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbManufacturer.TabIndex = 33;
            this.pbManufacturer.TabStop = false;
            this.pbManufacturer.Click += new System.EventHandler(this.pbManufacturer_Click);
            // 
            // tbPhone
            // 
            this.tbPhone.Location = new System.Drawing.Point(12, 125);
            this.tbPhone.Mask = "(00)0000-0000";
            this.tbPhone.Name = "tbPhone";
            this.tbPhone.Size = new System.Drawing.Size(205, 20);
            this.tbPhone.TabIndex = 3;
            // 
            // tbCountry
            // 
            this.tbCountry.Location = new System.Drawing.Point(529, 326);
            this.tbCountry.Name = "tbCountry";
            this.tbCountry.Size = new System.Drawing.Size(215, 20);
            this.tbCountry.TabIndex = 10;
            // 
            // tbState
            // 
            this.tbState.Location = new System.Drawing.Point(268, 326);
            this.tbState.Name = "tbState";
            this.tbState.Size = new System.Drawing.Size(255, 20);
            this.tbState.TabIndex = 9;
            // 
            // tbCity
            // 
            this.tbCity.Location = new System.Drawing.Point(12, 326);
            this.tbCity.Name = "tbCity";
            this.tbCity.Size = new System.Drawing.Size(250, 20);
            this.tbCity.TabIndex = 8;
            // 
            // tbDistrict
            // 
            this.tbDistrict.Location = new System.Drawing.Point(12, 276);
            this.tbDistrict.Name = "tbDistrict";
            this.tbDistrict.Size = new System.Drawing.Size(440, 20);
            this.tbDistrict.TabIndex = 7;
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(12, 226);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(440, 20);
            this.tbAddress.TabIndex = 6;
            // 
            // tbEmail
            // 
            this.tbEmail.Location = new System.Drawing.Point(12, 176);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Size = new System.Drawing.Size(440, 20);
            this.tbEmail.TabIndex = 5;
            // 
            // tbContact
            // 
            this.tbContact.Location = new System.Drawing.Point(227, 125);
            this.tbContact.Name = "tbContact";
            this.tbContact.Size = new System.Drawing.Size(225, 20);
            this.tbContact.TabIndex = 4;
            // 
            // tbFantasyName
            // 
            this.tbFantasyName.Location = new System.Drawing.Point(12, 76);
            this.tbFantasyName.Name = "tbFantasyName";
            this.tbFantasyName.Size = new System.Drawing.Size(440, 20);
            this.tbFantasyName.TabIndex = 2;
            // 
            // tbCompanyName
            // 
            this.tbCompanyName.Location = new System.Drawing.Point(12, 27);
            this.tbCompanyName.Name = "tbCompanyName";
            this.tbCompanyName.Size = new System.Drawing.Size(440, 20);
            this.tbCompanyName.TabIndex = 1;
            // 
            // lbCountry
            // 
            this.lbCountry.AutoSize = true;
            this.lbCountry.Location = new System.Drawing.Point(526, 310);
            this.lbCountry.Name = "lbCountry";
            this.lbCountry.Size = new System.Drawing.Size(29, 13);
            this.lbCountry.TabIndex = 32;
            this.lbCountry.Text = "País";
            // 
            // lbState
            // 
            this.lbState.AutoSize = true;
            this.lbState.Location = new System.Drawing.Point(265, 310);
            this.lbState.Name = "lbState";
            this.lbState.Size = new System.Drawing.Size(40, 13);
            this.lbState.TabIndex = 31;
            this.lbState.Text = "Estado";
            // 
            // lbCity
            // 
            this.lbCity.AutoSize = true;
            this.lbCity.Location = new System.Drawing.Point(9, 310);
            this.lbCity.Name = "lbCity";
            this.lbCity.Size = new System.Drawing.Size(40, 13);
            this.lbCity.TabIndex = 30;
            this.lbCity.Text = "Cidade";
            // 
            // lbDistrict
            // 
            this.lbDistrict.AutoSize = true;
            this.lbDistrict.Location = new System.Drawing.Point(9, 260);
            this.lbDistrict.Name = "lbDistrict";
            this.lbDistrict.Size = new System.Drawing.Size(34, 13);
            this.lbDistrict.TabIndex = 6;
            this.lbDistrict.Text = "Bairro";
            // 
            // lbAddress
            // 
            this.lbAddress.AutoSize = true;
            this.lbAddress.Location = new System.Drawing.Point(9, 210);
            this.lbAddress.Name = "lbAddress";
            this.lbAddress.Size = new System.Drawing.Size(53, 13);
            this.lbAddress.TabIndex = 28;
            this.lbAddress.Text = "Endereço";
            // 
            // lbEmail
            // 
            this.lbEmail.AutoSize = true;
            this.lbEmail.Location = new System.Drawing.Point(9, 160);
            this.lbEmail.Name = "lbEmail";
            this.lbEmail.Size = new System.Drawing.Size(32, 13);
            this.lbEmail.TabIndex = 27;
            this.lbEmail.Text = "Email";
            // 
            // lbContact
            // 
            this.lbContact.AutoSize = true;
            this.lbContact.Location = new System.Drawing.Point(224, 110);
            this.lbContact.Name = "lbContact";
            this.lbContact.Size = new System.Drawing.Size(44, 13);
            this.lbContact.TabIndex = 26;
            this.lbContact.Text = "Contato";
            // 
            // lbPhone
            // 
            this.lbPhone.AutoSize = true;
            this.lbPhone.Location = new System.Drawing.Point(9, 110);
            this.lbPhone.Name = "lbPhone";
            this.lbPhone.Size = new System.Drawing.Size(49, 13);
            this.lbPhone.TabIndex = 25;
            this.lbPhone.Text = "Telefone";
            // 
            // lbFantasyName
            // 
            this.lbFantasyName.AutoSize = true;
            this.lbFantasyName.Location = new System.Drawing.Point(9, 60);
            this.lbFantasyName.Name = "lbFantasyName";
            this.lbFantasyName.Size = new System.Drawing.Size(78, 13);
            this.lbFantasyName.TabIndex = 24;
            this.lbFantasyName.Text = "Nome Fantasia";
            // 
            // lbCompanyName
            // 
            this.lbCompanyName.AutoSize = true;
            this.lbCompanyName.Location = new System.Drawing.Point(9, 10);
            this.lbCompanyName.Name = "lbCompanyName";
            this.lbCompanyName.Size = new System.Drawing.Size(70, 13);
            this.lbCompanyName.TabIndex = 23;
            this.lbCompanyName.Text = "Razão Social";
            // 
            // tabSearch
            // 
            this.tabSearch.Controls.Add(this.btUpdate);
            this.tabSearch.Controls.Add(this.btSearch);
            this.tabSearch.Controls.Add(this.btDelete);
            this.tabSearch.Controls.Add(this.dgManufacturer);
            this.tabSearch.Controls.Add(this.cbOrder);
            this.tabSearch.Controls.Add(this.tbSearch);
            this.tabSearch.Controls.Add(this.lbOrder);
            this.tabSearch.Controls.Add(this.lbSearch);
            this.tabSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabSearch.Size = new System.Drawing.Size(752, 391);
            this.tabSearch.TabIndex = 1;
            this.tabSearch.Text = "Pesquisar";
            this.tabSearch.UseVisualStyleBackColor = true;
            // 
            // btUpdate
            // 
            this.btUpdate.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btUpdate.FlatAppearance.BorderSize = 0;
            this.btUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUpdate.ForeColor = System.Drawing.Color.White;
            this.btUpdate.Image = global::HVEX.Properties.Resources.white_update;
            this.btUpdate.Location = new System.Drawing.Point(667, 362);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(75, 23);
            this.btUpdate.TabIndex = 5;
            this.btUpdate.Text = "Editar";
            this.btUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btUpdate.UseVisualStyleBackColor = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btSearch
            // 
            this.btSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btSearch.FlatAppearance.BorderSize = 0;
            this.btSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSearch.ForeColor = System.Drawing.Color.White;
            this.btSearch.Image = global::HVEX.Properties.Resources.white_search;
            this.btSearch.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btSearch.Location = new System.Drawing.Point(662, 26);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(80, 21);
            this.btSearch.TabIndex = 3;
            this.btSearch.Text = "Pesquisar";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.UseVisualStyleBackColor = false;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // btDelete
            // 
            this.btDelete.BackColor = System.Drawing.Color.DarkRed;
            this.btDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDelete.FlatAppearance.BorderSize = 0;
            this.btDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btDelete.ForeColor = System.Drawing.Color.White;
            this.btDelete.Image = ((System.Drawing.Image)(resources.GetObject("btDelete.Image")));
            this.btDelete.Location = new System.Drawing.Point(586, 362);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(75, 23);
            this.btDelete.TabIndex = 4;
            this.btDelete.Text = "Excluir";
            this.btDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.UseVisualStyleBackColor = false;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // dgManufacturer
            // 
            this.dgManufacturer.AllowUserToAddRows = false;
            this.dgManufacturer.AllowUserToDeleteRows = false;
            this.dgManufacturer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgManufacturer.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgManufacturer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgManufacturer.Location = new System.Drawing.Point(13, 53);
            this.dgManufacturer.Name = "dgManufacturer";
            this.dgManufacturer.ReadOnly = true;
            this.dgManufacturer.Size = new System.Drawing.Size(729, 303);
            this.dgManufacturer.TabIndex = 9;
            this.dgManufacturer.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgManufacturer_MouseDoubleClick);
            // 
            // cbOrder
            // 
            this.cbOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrder.DropDownWidth = 181;
            this.cbOrder.FormattingEnabled = true;
            this.cbOrder.Items.AddRange(new object[] {
            "Razão Social",
            "Nome Fantasia",
            "Telefone",
            "Contato"});
            this.cbOrder.Location = new System.Drawing.Point(475, 26);
            this.cbOrder.Name = "cbOrder";
            this.cbOrder.Size = new System.Drawing.Size(181, 21);
            this.cbOrder.TabIndex = 2;
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(13, 26);
            this.tbSearch.Multiline = true;
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(456, 21);
            this.tbSearch.TabIndex = 1;
            this.tbSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSearch_KeyDown);
            // 
            // lbOrder
            // 
            this.lbOrder.AutoSize = true;
            this.lbOrder.Location = new System.Drawing.Point(472, 10);
            this.lbOrder.Name = "lbOrder";
            this.lbOrder.Size = new System.Drawing.Size(38, 13);
            this.lbOrder.TabIndex = 10;
            this.lbOrder.Text = "Ordem";
            // 
            // lbSearch
            // 
            this.lbSearch.AutoSize = true;
            this.lbSearch.Location = new System.Drawing.Point(10, 10);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(70, 13);
            this.lbSearch.TabIndex = 5;
            this.lbSearch.Text = "Razão Social";
            // 
            // FrmManufacturer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(784, 441);
            this.Controls.Add(this.tabManufacturer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmManufacturer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fabricantes";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmManufacturer_FormClosed);
            this.tabManufacturer.ResumeLayout(false);
            this.tabRegister.ResumeLayout(false);
            this.tabRegister.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbManufacturer)).EndInit();
            this.tabSearch.ResumeLayout(false);
            this.tabSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgManufacturer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabManufacturer;
        private System.Windows.Forms.TabPage tabRegister;
        private System.Windows.Forms.Button btClean;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.PictureBox pbManufacturer;
        private System.Windows.Forms.MaskedTextBox tbPhone;
        private System.Windows.Forms.TextBox tbCountry;
        private System.Windows.Forms.TextBox tbState;
        private System.Windows.Forms.TextBox tbCity;
        private System.Windows.Forms.TextBox tbDistrict;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.TextBox tbEmail;
        private System.Windows.Forms.TextBox tbContact;
        private System.Windows.Forms.TextBox tbFantasyName;
        private System.Windows.Forms.TextBox tbCompanyName;
        private System.Windows.Forms.Label lbCountry;
        private System.Windows.Forms.Label lbState;
        private System.Windows.Forms.Label lbCity;
        private System.Windows.Forms.Label lbDistrict;
        private System.Windows.Forms.Label lbAddress;
        private System.Windows.Forms.Label lbEmail;
        private System.Windows.Forms.Label lbContact;
        private System.Windows.Forms.Label lbPhone;
        private System.Windows.Forms.Label lbFantasyName;
        private System.Windows.Forms.Label lbCompanyName;
        private System.Windows.Forms.TabPage tabSearch;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.DataGridView dgManufacturer;
        private System.Windows.Forms.ComboBox cbOrder;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lbOrder;
        private System.Windows.Forms.Label lbSearch;
    }
}