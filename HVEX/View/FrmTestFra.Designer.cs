﻿
namespace HVEX.View {
    partial class FrmTestFra {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTestFra));
            this.btGetEquipment = new System.Windows.Forms.Button();
            this.tbTransformer = new System.Windows.Forms.TextBox();
            this.lbTransformer = new System.Windows.Forms.Label();
            this.pnTest = new System.Windows.Forms.Panel();
            this.lbScale = new System.Windows.Forms.Label();
            this.rbHz = new System.Windows.Forms.RadioButton();
            this.rbLog = new System.Windows.Forms.RadioButton();
            this.tabGraphs = new System.Windows.Forms.TabControl();
            this.tabResults = new System.Windows.Forms.TabPage();
            this.tableSecondGraphs = new System.Windows.Forms.TableLayoutPanel();
            this.graphAmplitude = new ZedGraph.ZedGraphControl();
            this.graphPhase = new ZedGraph.ZedGraphControl();
            this.tabInputOutput = new System.Windows.Forms.TabPage();
            this.tableFirstGraphs = new System.Windows.Forms.TableLayoutPanel();
            this.graphInput = new ZedGraph.ZedGraphControl();
            this.graphOutput = new ZedGraph.ZedGraphControl();
            this.gbTestController = new System.Windows.Forms.GroupBox();
            this.lbCurrentStatus = new System.Windows.Forms.Label();
            this.lbStatus = new System.Windows.Forms.Label();
            this.gbAcquisition = new System.Windows.Forms.GroupBox();
            this.pbSignalAnalysis = new System.Windows.Forms.ProgressBar();
            this.tableAcquisition = new System.Windows.Forms.TableLayoutPanel();
            this.lbAcquisitionInput = new System.Windows.Forms.Label();
            this.lbAcquisitionOutput = new System.Windows.Forms.Label();
            this.lbAcquisitionResult = new System.Windows.Forms.Label();
            this.lbAcquisitionPhase = new System.Windows.Forms.Label();
            this.lbAmplitudeSample = new System.Windows.Forms.Label();
            this.lbCurrentInput = new System.Windows.Forms.Label();
            this.lbCurrentOutput = new System.Windows.Forms.Label();
            this.lbCurrentResult = new System.Windows.Forms.Label();
            this.lbCurrentPhase = new System.Windows.Forms.Label();
            this.lbCurrentSample = new System.Windows.Forms.Label();
            this.lbVoltageInjection = new System.Windows.Forms.Label();
            this.lbInjectVoltage = new System.Windows.Forms.Label();
            this.lbSignalFrequency = new System.Windows.Forms.Label();
            this.lbCurrentFrequency = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lbSamples = new System.Windows.Forms.Label();
            this.tbSamples = new System.Windows.Forms.TextBox();
            this.lbTime = new System.Windows.Forms.Label();
            this.lbTestTime = new System.Windows.Forms.Label();
            this.btStop = new System.Windows.Forms.Button();
            this.cbOsciloscope = new System.Windows.Forms.ComboBox();
            this.lbParamters = new System.Windows.Forms.Label();
            this.tableController = new System.Windows.Forms.TableLayoutPanel();
            this.lbMinFrequency = new System.Windows.Forms.Label();
            this.tbMaxAmplitude = new System.Windows.Forms.TextBox();
            this.lbMaxAmplitude = new System.Windows.Forms.Label();
            this.lbMaxFrequency = new System.Windows.Forms.Label();
            this.tbMinAmplitude = new System.Windows.Forms.TextBox();
            this.tbMinFrequency = new System.Windows.Forms.TextBox();
            this.tbMaxFrequency = new System.Windows.Forms.TextBox();
            this.cbMinFrequencyMeasurement = new System.Windows.Forms.ComboBox();
            this.cbMaxFrequencyMeasurement = new System.Windows.Forms.ComboBox();
            this.btStart = new System.Windows.Forms.Button();
            this.lbOsciloscope = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btConnection = new System.Windows.Forms.Button();
            this.lbConnection = new System.Windows.Forms.Label();
            this.lbOsciloscopePort = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.lbBrand = new System.Windows.Forms.Label();
            this.tbIp = new System.Windows.Forms.MaskedTextBox();
            this.lbOsciloscopeIp = new System.Windows.Forms.Label();
            this.timerForm = new System.Windows.Forms.Timer(this.components);
            this.pnTest.SuspendLayout();
            this.tabGraphs.SuspendLayout();
            this.tabResults.SuspendLayout();
            this.tableSecondGraphs.SuspendLayout();
            this.tabInputOutput.SuspendLayout();
            this.tableFirstGraphs.SuspendLayout();
            this.gbTestController.SuspendLayout();
            this.gbAcquisition.SuspendLayout();
            this.tableAcquisition.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableController.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btGetEquipment
            // 
            this.btGetEquipment.Location = new System.Drawing.Point(527, 11);
            this.btGetEquipment.Name = "btGetEquipment";
            this.btGetEquipment.Size = new System.Drawing.Size(30, 25);
            this.btGetEquipment.TabIndex = 10;
            this.btGetEquipment.Text = "...";
            this.btGetEquipment.UseVisualStyleBackColor = true;
            this.btGetEquipment.Click += new System.EventHandler(this.btGetEquipment_Click);
            // 
            // tbTransformer
            // 
            this.tbTransformer.BackColor = System.Drawing.Color.LightSalmon;
            this.tbTransformer.Enabled = false;
            this.tbTransformer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTransformer.Location = new System.Drawing.Point(177, 12);
            this.tbTransformer.Name = "tbTransformer";
            this.tbTransformer.ReadOnly = true;
            this.tbTransformer.Size = new System.Drawing.Size(350, 23);
            this.tbTransformer.TabIndex = 13;
            // 
            // lbTransformer
            // 
            this.lbTransformer.AutoSize = true;
            this.lbTransformer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTransformer.ForeColor = System.Drawing.Color.White;
            this.lbTransformer.Location = new System.Drawing.Point(29, 12);
            this.lbTransformer.Name = "lbTransformer";
            this.lbTransformer.Size = new System.Drawing.Size(142, 20);
            this.lbTransformer.TabIndex = 12;
            this.lbTransformer.Text = "EQUIPAMENTO:";
            // 
            // pnTest
            // 
            this.pnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnTest.BackColor = System.Drawing.Color.White;
            this.pnTest.Controls.Add(this.lbScale);
            this.pnTest.Controls.Add(this.rbHz);
            this.pnTest.Controls.Add(this.rbLog);
            this.pnTest.Controls.Add(this.tabGraphs);
            this.pnTest.Controls.Add(this.gbTestController);
            this.pnTest.Location = new System.Drawing.Point(14, 40);
            this.pnTest.Name = "pnTest";
            this.pnTest.Size = new System.Drawing.Size(1257, 659);
            this.pnTest.TabIndex = 11;
            // 
            // lbScale
            // 
            this.lbScale.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbScale.AutoSize = true;
            this.lbScale.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbScale.Location = new System.Drawing.Point(978, 18);
            this.lbScale.Name = "lbScale";
            this.lbScale.Size = new System.Drawing.Size(134, 13);
            this.lbScale.TabIndex = 5;
            this.lbScale.Text = "Escala de Frequência:";
            // 
            // rbHz
            // 
            this.rbHz.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbHz.AutoSize = true;
            this.rbHz.Location = new System.Drawing.Point(1184, 16);
            this.rbHz.Name = "rbHz";
            this.rbHz.Size = new System.Drawing.Size(54, 17);
            this.rbHz.TabIndex = 4;
            this.rbHz.Text = "Linear";
            this.rbHz.UseVisualStyleBackColor = true;
            this.rbHz.CheckedChanged += new System.EventHandler(this.rbHz_CheckedChanged);
            // 
            // rbLog
            // 
            this.rbLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rbLog.AutoSize = true;
            this.rbLog.Checked = true;
            this.rbLog.Location = new System.Drawing.Point(1120, 16);
            this.rbLog.Name = "rbLog";
            this.rbLog.Size = new System.Drawing.Size(58, 17);
            this.rbLog.TabIndex = 3;
            this.rbLog.TabStop = true;
            this.rbLog.Text = "Log 10";
            this.rbLog.UseVisualStyleBackColor = true;
            this.rbLog.CheckedChanged += new System.EventHandler(this.rbLog_CheckedChanged);
            // 
            // tabGraphs
            // 
            this.tabGraphs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabGraphs.Controls.Add(this.tabResults);
            this.tabGraphs.Controls.Add(this.tabInputOutput);
            this.tabGraphs.Location = new System.Drawing.Point(236, 16);
            this.tabGraphs.Name = "tabGraphs";
            this.tabGraphs.SelectedIndex = 0;
            this.tabGraphs.Size = new System.Drawing.Size(1010, 634);
            this.tabGraphs.TabIndex = 1;
            // 
            // tabResults
            // 
            this.tabResults.Controls.Add(this.tableSecondGraphs);
            this.tabResults.Location = new System.Drawing.Point(4, 22);
            this.tabResults.Name = "tabResults";
            this.tabResults.Padding = new System.Windows.Forms.Padding(3);
            this.tabResults.Size = new System.Drawing.Size(1002, 608);
            this.tabResults.TabIndex = 1;
            this.tabResults.Text = "Resultados";
            this.tabResults.UseVisualStyleBackColor = true;
            // 
            // tableSecondGraphs
            // 
            this.tableSecondGraphs.ColumnCount = 1;
            this.tableSecondGraphs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableSecondGraphs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableSecondGraphs.Controls.Add(this.graphAmplitude, 0, 0);
            this.tableSecondGraphs.Controls.Add(this.graphPhase, 0, 1);
            this.tableSecondGraphs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableSecondGraphs.Location = new System.Drawing.Point(3, 3);
            this.tableSecondGraphs.Name = "tableSecondGraphs";
            this.tableSecondGraphs.RowCount = 2;
            this.tableSecondGraphs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableSecondGraphs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableSecondGraphs.Size = new System.Drawing.Size(996, 602);
            this.tableSecondGraphs.TabIndex = 0;
            // 
            // graphAmplitude
            // 
            this.graphAmplitude.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphAmplitude.Location = new System.Drawing.Point(4, 4);
            this.graphAmplitude.Margin = new System.Windows.Forms.Padding(4);
            this.graphAmplitude.Name = "graphAmplitude";
            this.graphAmplitude.ScrollGrace = 0D;
            this.graphAmplitude.ScrollMaxX = 0D;
            this.graphAmplitude.ScrollMaxY = 0D;
            this.graphAmplitude.ScrollMaxY2 = 0D;
            this.graphAmplitude.ScrollMinX = 0D;
            this.graphAmplitude.ScrollMinY = 0D;
            this.graphAmplitude.ScrollMinY2 = 0D;
            this.graphAmplitude.Size = new System.Drawing.Size(988, 293);
            this.graphAmplitude.TabIndex = 0;
            this.graphAmplitude.UseExtendedPrintDialog = true;
            // 
            // graphPhase
            // 
            this.graphPhase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphPhase.Location = new System.Drawing.Point(4, 305);
            this.graphPhase.Margin = new System.Windows.Forms.Padding(4);
            this.graphPhase.Name = "graphPhase";
            this.graphPhase.ScrollGrace = 0D;
            this.graphPhase.ScrollMaxX = 0D;
            this.graphPhase.ScrollMaxY = 0D;
            this.graphPhase.ScrollMaxY2 = 0D;
            this.graphPhase.ScrollMinX = 0D;
            this.graphPhase.ScrollMinY = 0D;
            this.graphPhase.ScrollMinY2 = 0D;
            this.graphPhase.Size = new System.Drawing.Size(988, 293);
            this.graphPhase.TabIndex = 1;
            this.graphPhase.UseExtendedPrintDialog = true;
            // 
            // tabInputOutput
            // 
            this.tabInputOutput.Controls.Add(this.tableFirstGraphs);
            this.tabInputOutput.Location = new System.Drawing.Point(4, 22);
            this.tabInputOutput.Name = "tabInputOutput";
            this.tabInputOutput.Padding = new System.Windows.Forms.Padding(3);
            this.tabInputOutput.Size = new System.Drawing.Size(1002, 608);
            this.tabInputOutput.TabIndex = 0;
            this.tabInputOutput.Text = "Entrada e Saída";
            this.tabInputOutput.UseVisualStyleBackColor = true;
            // 
            // tableFirstGraphs
            // 
            this.tableFirstGraphs.ColumnCount = 1;
            this.tableFirstGraphs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableFirstGraphs.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableFirstGraphs.Controls.Add(this.graphInput, 0, 1);
            this.tableFirstGraphs.Controls.Add(this.graphOutput, 0, 0);
            this.tableFirstGraphs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableFirstGraphs.Location = new System.Drawing.Point(3, 3);
            this.tableFirstGraphs.Name = "tableFirstGraphs";
            this.tableFirstGraphs.RowCount = 2;
            this.tableFirstGraphs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableFirstGraphs.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableFirstGraphs.Size = new System.Drawing.Size(996, 602);
            this.tableFirstGraphs.TabIndex = 0;
            // 
            // graphInput
            // 
            this.graphInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphInput.Location = new System.Drawing.Point(4, 305);
            this.graphInput.Margin = new System.Windows.Forms.Padding(4);
            this.graphInput.Name = "graphInput";
            this.graphInput.ScrollGrace = 0D;
            this.graphInput.ScrollMaxX = 0D;
            this.graphInput.ScrollMaxY = 0D;
            this.graphInput.ScrollMaxY2 = 0D;
            this.graphInput.ScrollMinX = 0D;
            this.graphInput.ScrollMinY = 0D;
            this.graphInput.ScrollMinY2 = 0D;
            this.graphInput.Size = new System.Drawing.Size(988, 293);
            this.graphInput.TabIndex = 0;
            this.graphInput.UseExtendedPrintDialog = true;
            // 
            // graphOutput
            // 
            this.graphOutput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.graphOutput.Location = new System.Drawing.Point(4, 4);
            this.graphOutput.Margin = new System.Windows.Forms.Padding(4);
            this.graphOutput.Name = "graphOutput";
            this.graphOutput.ScrollGrace = 0D;
            this.graphOutput.ScrollMaxX = 0D;
            this.graphOutput.ScrollMaxY = 0D;
            this.graphOutput.ScrollMaxY2 = 0D;
            this.graphOutput.ScrollMinX = 0D;
            this.graphOutput.ScrollMinY = 0D;
            this.graphOutput.ScrollMinY2 = 0D;
            this.graphOutput.Size = new System.Drawing.Size(988, 293);
            this.graphOutput.TabIndex = 1;
            this.graphOutput.UseExtendedPrintDialog = true;
            // 
            // gbTestController
            // 
            this.gbTestController.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbTestController.Controls.Add(this.lbCurrentStatus);
            this.gbTestController.Controls.Add(this.lbStatus);
            this.gbTestController.Controls.Add(this.gbAcquisition);
            this.gbTestController.Controls.Add(this.tableLayoutPanel2);
            this.gbTestController.Controls.Add(this.lbTime);
            this.gbTestController.Controls.Add(this.lbTestTime);
            this.gbTestController.Controls.Add(this.btStop);
            this.gbTestController.Controls.Add(this.cbOsciloscope);
            this.gbTestController.Controls.Add(this.lbParamters);
            this.gbTestController.Controls.Add(this.tableController);
            this.gbTestController.Controls.Add(this.btStart);
            this.gbTestController.Controls.Add(this.lbOsciloscope);
            this.gbTestController.Controls.Add(this.tableLayoutPanel1);
            this.gbTestController.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbTestController.Location = new System.Drawing.Point(10, 10);
            this.gbTestController.Name = "gbTestController";
            this.gbTestController.Size = new System.Drawing.Size(220, 640);
            this.gbTestController.TabIndex = 0;
            this.gbTestController.TabStop = false;
            this.gbTestController.Text = "Controle de Ensaio";
            // 
            // lbCurrentStatus
            // 
            this.lbCurrentStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentStatus.BackColor = System.Drawing.Color.White;
            this.lbCurrentStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentStatus.Location = new System.Drawing.Point(50, 606);
            this.lbCurrentStatus.Margin = new System.Windows.Forms.Padding(0);
            this.lbCurrentStatus.Name = "lbCurrentStatus";
            this.lbCurrentStatus.Size = new System.Drawing.Size(158, 20);
            this.lbCurrentStatus.TabIndex = 16;
            this.lbCurrentStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbStatus
            // 
            this.lbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbStatus.AutoSize = true;
            this.lbStatus.BackColor = System.Drawing.Color.Transparent;
            this.lbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatus.Location = new System.Drawing.Point(11, 609);
            this.lbStatus.Margin = new System.Windows.Forms.Padding(0);
            this.lbStatus.Name = "lbStatus";
            this.lbStatus.Size = new System.Drawing.Size(40, 13);
            this.lbStatus.TabIndex = 15;
            this.lbStatus.Text = "Status:";
            this.lbStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbAcquisition
            // 
            this.gbAcquisition.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.gbAcquisition.Controls.Add(this.pbSignalAnalysis);
            this.gbAcquisition.Controls.Add(this.tableAcquisition);
            this.gbAcquisition.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAcquisition.Location = new System.Drawing.Point(5, 360);
            this.gbAcquisition.Name = "gbAcquisition";
            this.gbAcquisition.Size = new System.Drawing.Size(209, 243);
            this.gbAcquisition.TabIndex = 14;
            this.gbAcquisition.TabStop = false;
            this.gbAcquisition.Text = "Análise do Sinal";
            // 
            // pbSignalAnalysis
            // 
            this.pbSignalAnalysis.Location = new System.Drawing.Point(5, 17);
            this.pbSignalAnalysis.Name = "pbSignalAnalysis";
            this.pbSignalAnalysis.Size = new System.Drawing.Size(199, 23);
            this.pbSignalAnalysis.TabIndex = 2;
            // 
            // tableAcquisition
            // 
            this.tableAcquisition.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableAcquisition.ColumnCount = 2;
            this.tableAcquisition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.58586F));
            this.tableAcquisition.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.41414F));
            this.tableAcquisition.Controls.Add(this.lbAcquisitionInput, 0, 2);
            this.tableAcquisition.Controls.Add(this.lbAcquisitionOutput, 0, 3);
            this.tableAcquisition.Controls.Add(this.lbAcquisitionResult, 0, 4);
            this.tableAcquisition.Controls.Add(this.lbAcquisitionPhase, 0, 5);
            this.tableAcquisition.Controls.Add(this.lbAmplitudeSample, 0, 6);
            this.tableAcquisition.Controls.Add(this.lbCurrentInput, 1, 2);
            this.tableAcquisition.Controls.Add(this.lbCurrentOutput, 1, 3);
            this.tableAcquisition.Controls.Add(this.lbCurrentResult, 1, 4);
            this.tableAcquisition.Controls.Add(this.lbCurrentPhase, 1, 5);
            this.tableAcquisition.Controls.Add(this.lbCurrentSample, 1, 6);
            this.tableAcquisition.Controls.Add(this.lbVoltageInjection, 0, 1);
            this.tableAcquisition.Controls.Add(this.lbInjectVoltage, 1, 1);
            this.tableAcquisition.Controls.Add(this.lbSignalFrequency, 0, 0);
            this.tableAcquisition.Controls.Add(this.lbCurrentFrequency, 1, 0);
            this.tableAcquisition.Location = new System.Drawing.Point(5, 46);
            this.tableAcquisition.Name = "tableAcquisition";
            this.tableAcquisition.RowCount = 7;
            this.tableAcquisition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableAcquisition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableAcquisition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableAcquisition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableAcquisition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableAcquisition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableAcquisition.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableAcquisition.Size = new System.Drawing.Size(199, 190);
            this.tableAcquisition.TabIndex = 0;
            // 
            // lbAcquisitionInput
            // 
            this.lbAcquisitionInput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAcquisitionInput.AutoSize = true;
            this.lbAcquisitionInput.BackColor = System.Drawing.Color.Transparent;
            this.lbAcquisitionInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAcquisitionInput.Location = new System.Drawing.Point(1, 55);
            this.lbAcquisitionInput.Margin = new System.Windows.Forms.Padding(0);
            this.lbAcquisitionInput.Name = "lbAcquisitionInput";
            this.lbAcquisitionInput.Size = new System.Drawing.Size(114, 26);
            this.lbAcquisitionInput.TabIndex = 0;
            this.lbAcquisitionInput.Text = "Amplitude da  Entrada:";
            this.lbAcquisitionInput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbAcquisitionOutput
            // 
            this.lbAcquisitionOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAcquisitionOutput.AutoSize = true;
            this.lbAcquisitionOutput.BackColor = System.Drawing.Color.Transparent;
            this.lbAcquisitionOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAcquisitionOutput.Location = new System.Drawing.Point(1, 82);
            this.lbAcquisitionOutput.Margin = new System.Windows.Forms.Padding(0);
            this.lbAcquisitionOutput.Name = "lbAcquisitionOutput";
            this.lbAcquisitionOutput.Size = new System.Drawing.Size(114, 26);
            this.lbAcquisitionOutput.TabIndex = 1;
            this.lbAcquisitionOutput.Text = "Amplitude da Saída:";
            this.lbAcquisitionOutput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbAcquisitionResult
            // 
            this.lbAcquisitionResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAcquisitionResult.AutoSize = true;
            this.lbAcquisitionResult.BackColor = System.Drawing.Color.Transparent;
            this.lbAcquisitionResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAcquisitionResult.Location = new System.Drawing.Point(1, 109);
            this.lbAcquisitionResult.Margin = new System.Windows.Forms.Padding(0);
            this.lbAcquisitionResult.Name = "lbAcquisitionResult";
            this.lbAcquisitionResult.Size = new System.Drawing.Size(114, 26);
            this.lbAcquisitionResult.TabIndex = 2;
            this.lbAcquisitionResult.Text = "Ganho:";
            this.lbAcquisitionResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbAcquisitionPhase
            // 
            this.lbAcquisitionPhase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAcquisitionPhase.AutoSize = true;
            this.lbAcquisitionPhase.BackColor = System.Drawing.Color.Transparent;
            this.lbAcquisitionPhase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAcquisitionPhase.Location = new System.Drawing.Point(1, 136);
            this.lbAcquisitionPhase.Margin = new System.Windows.Forms.Padding(0);
            this.lbAcquisitionPhase.Name = "lbAcquisitionPhase";
            this.lbAcquisitionPhase.Size = new System.Drawing.Size(114, 26);
            this.lbAcquisitionPhase.TabIndex = 3;
            this.lbAcquisitionPhase.Text = "Fase:";
            this.lbAcquisitionPhase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbAmplitudeSample
            // 
            this.lbAmplitudeSample.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbAmplitudeSample.AutoSize = true;
            this.lbAmplitudeSample.BackColor = System.Drawing.Color.Transparent;
            this.lbAmplitudeSample.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAmplitudeSample.Location = new System.Drawing.Point(1, 163);
            this.lbAmplitudeSample.Margin = new System.Windows.Forms.Padding(0);
            this.lbAmplitudeSample.Name = "lbAmplitudeSample";
            this.lbAmplitudeSample.Size = new System.Drawing.Size(114, 26);
            this.lbAmplitudeSample.TabIndex = 4;
            this.lbAmplitudeSample.Text = "Amostra Nº:";
            this.lbAmplitudeSample.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCurrentInput
            // 
            this.lbCurrentInput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentInput.AutoSize = true;
            this.lbCurrentInput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentInput.Location = new System.Drawing.Point(116, 55);
            this.lbCurrentInput.Margin = new System.Windows.Forms.Padding(0);
            this.lbCurrentInput.Name = "lbCurrentInput";
            this.lbCurrentInput.Size = new System.Drawing.Size(82, 26);
            this.lbCurrentInput.TabIndex = 5;
            this.lbCurrentInput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCurrentOutput
            // 
            this.lbCurrentOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentOutput.AutoSize = true;
            this.lbCurrentOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentOutput.Location = new System.Drawing.Point(116, 82);
            this.lbCurrentOutput.Margin = new System.Windows.Forms.Padding(0);
            this.lbCurrentOutput.Name = "lbCurrentOutput";
            this.lbCurrentOutput.Size = new System.Drawing.Size(82, 26);
            this.lbCurrentOutput.TabIndex = 6;
            this.lbCurrentOutput.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCurrentResult
            // 
            this.lbCurrentResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentResult.AutoSize = true;
            this.lbCurrentResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentResult.Location = new System.Drawing.Point(116, 109);
            this.lbCurrentResult.Margin = new System.Windows.Forms.Padding(0);
            this.lbCurrentResult.Name = "lbCurrentResult";
            this.lbCurrentResult.Size = new System.Drawing.Size(82, 26);
            this.lbCurrentResult.TabIndex = 7;
            this.lbCurrentResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCurrentPhase
            // 
            this.lbCurrentPhase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentPhase.AutoSize = true;
            this.lbCurrentPhase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentPhase.Location = new System.Drawing.Point(116, 136);
            this.lbCurrentPhase.Margin = new System.Windows.Forms.Padding(0);
            this.lbCurrentPhase.Name = "lbCurrentPhase";
            this.lbCurrentPhase.Size = new System.Drawing.Size(82, 26);
            this.lbCurrentPhase.TabIndex = 8;
            this.lbCurrentPhase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCurrentSample
            // 
            this.lbCurrentSample.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentSample.AutoSize = true;
            this.lbCurrentSample.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentSample.Location = new System.Drawing.Point(116, 163);
            this.lbCurrentSample.Margin = new System.Windows.Forms.Padding(0);
            this.lbCurrentSample.Name = "lbCurrentSample";
            this.lbCurrentSample.Size = new System.Drawing.Size(82, 26);
            this.lbCurrentSample.TabIndex = 9;
            this.lbCurrentSample.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbVoltageInjection
            // 
            this.lbVoltageInjection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbVoltageInjection.AutoSize = true;
            this.lbVoltageInjection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVoltageInjection.Location = new System.Drawing.Point(1, 28);
            this.lbVoltageInjection.Margin = new System.Windows.Forms.Padding(0);
            this.lbVoltageInjection.Name = "lbVoltageInjection";
            this.lbVoltageInjection.Size = new System.Drawing.Size(114, 26);
            this.lbVoltageInjection.TabIndex = 10;
            this.lbVoltageInjection.Text = "Tensão Injetada:";
            this.lbVoltageInjection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbInjectVoltage
            // 
            this.lbInjectVoltage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInjectVoltage.AutoSize = true;
            this.lbInjectVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbInjectVoltage.Location = new System.Drawing.Point(116, 28);
            this.lbInjectVoltage.Margin = new System.Windows.Forms.Padding(0);
            this.lbInjectVoltage.Name = "lbInjectVoltage";
            this.lbInjectVoltage.Size = new System.Drawing.Size(82, 26);
            this.lbInjectVoltage.TabIndex = 11;
            this.lbInjectVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSignalFrequency
            // 
            this.lbSignalFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSignalFrequency.AutoSize = true;
            this.lbSignalFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSignalFrequency.Location = new System.Drawing.Point(4, 1);
            this.lbSignalFrequency.Name = "lbSignalFrequency";
            this.lbSignalFrequency.Size = new System.Drawing.Size(108, 26);
            this.lbSignalFrequency.TabIndex = 12;
            this.lbSignalFrequency.Text = "Frequência do Sinal:";
            this.lbSignalFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCurrentFrequency
            // 
            this.lbCurrentFrequency.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCurrentFrequency.AutoSize = true;
            this.lbCurrentFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentFrequency.Location = new System.Drawing.Point(119, 1);
            this.lbCurrentFrequency.Name = "lbCurrentFrequency";
            this.lbCurrentFrequency.Size = new System.Drawing.Size(76, 26);
            this.lbCurrentFrequency.TabIndex = 13;
            this.lbCurrentFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.30769F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.69231F));
            this.tableLayoutPanel2.Controls.Add(this.lbSamples, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbSamples, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 322);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(208, 30);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // lbSamples
            // 
            this.lbSamples.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbSamples.AutoSize = true;
            this.lbSamples.BackColor = System.Drawing.Color.Transparent;
            this.lbSamples.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSamples.Location = new System.Drawing.Point(1, 1);
            this.lbSamples.Margin = new System.Windows.Forms.Padding(0);
            this.lbSamples.Name = "lbSamples";
            this.lbSamples.Size = new System.Drawing.Size(86, 28);
            this.lbSamples.TabIndex = 2;
            this.lbSamples.Text = "Nº de Amostras:";
            this.lbSamples.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbSamples
            // 
            this.tbSamples.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbSamples.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSamples.Location = new System.Drawing.Point(91, 4);
            this.tbSamples.Multiline = true;
            this.tbSamples.Name = "tbSamples";
            this.tbSamples.Size = new System.Drawing.Size(113, 22);
            this.tbSamples.TabIndex = 3;
            this.tbSamples.Text = "256";
            this.tbSamples.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSamples_KeyDown);
            this.tbSamples.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSamples_KeyPress);
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTime.Location = new System.Drawing.Point(127, 45);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(43, 15);
            this.lbTime.TabIndex = 13;
            this.lbTime.Text = "00:00";
            // 
            // lbTestTime
            // 
            this.lbTestTime.AutoSize = true;
            this.lbTestTime.BackColor = System.Drawing.Color.Transparent;
            this.lbTestTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTestTime.Location = new System.Drawing.Point(92, 27);
            this.lbTestTime.Name = "lbTestTime";
            this.lbTestTime.Size = new System.Drawing.Size(119, 15);
            this.lbTestTime.TabIndex = 12;
            this.lbTestTime.Text = "Tempo de Ensaio";
            this.lbTestTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btStop
            // 
            this.btStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStop.Location = new System.Drawing.Point(5, 45);
            this.btStop.Name = "btStop";
            this.btStop.Size = new System.Drawing.Size(80, 25);
            this.btStop.TabIndex = 2;
            this.btStop.Text = "Parar Ensaio";
            this.btStop.UseVisualStyleBackColor = true;
            this.btStop.Click += new System.EventHandler(this.btStop_Click);
            // 
            // cbOsciloscope
            // 
            this.cbOsciloscope.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbOsciloscope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOsciloscope.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOsciloscope.FormattingEnabled = true;
            this.cbOsciloscope.Location = new System.Drawing.Point(6, 94);
            this.cbOsciloscope.Name = "cbOsciloscope";
            this.cbOsciloscope.Size = new System.Drawing.Size(208, 21);
            this.cbOsciloscope.TabIndex = 5;
            this.cbOsciloscope.SelectedIndexChanged += new System.EventHandler(this.cbOsciloscope_SelectedIndexChanged);
            // 
            // lbParamters
            // 
            this.lbParamters.AutoSize = true;
            this.lbParamters.Location = new System.Drawing.Point(6, 217);
            this.lbParamters.Name = "lbParamters";
            this.lbParamters.Size = new System.Drawing.Size(70, 13);
            this.lbParamters.TabIndex = 9;
            this.lbParamters.Text = "Parâmetros";
            // 
            // tableController
            // 
            this.tableController.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableController.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableController.ColumnCount = 3;
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.30769F));
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.36539F));
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.84615F));
            this.tableController.Controls.Add(this.lbMinFrequency, 0, 0);
            this.tableController.Controls.Add(this.tbMaxAmplitude, 2, 2);
            this.tableController.Controls.Add(this.lbMaxAmplitude, 0, 2);
            this.tableController.Controls.Add(this.lbMaxFrequency, 0, 1);
            this.tableController.Controls.Add(this.tbMinAmplitude, 1, 2);
            this.tableController.Controls.Add(this.tbMinFrequency, 1, 0);
            this.tableController.Controls.Add(this.tbMaxFrequency, 1, 1);
            this.tableController.Controls.Add(this.cbMinFrequencyMeasurement, 2, 0);
            this.tableController.Controls.Add(this.cbMaxFrequencyMeasurement, 2, 1);
            this.tableController.Location = new System.Drawing.Point(6, 233);
            this.tableController.Name = "tableController";
            this.tableController.RowCount = 5;
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.Size = new System.Drawing.Size(208, 95);
            this.tableController.TabIndex = 3;
            // 
            // lbMinFrequency
            // 
            this.lbMinFrequency.AutoSize = true;
            this.lbMinFrequency.BackColor = System.Drawing.Color.Transparent;
            this.lbMinFrequency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMinFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMinFrequency.Location = new System.Drawing.Point(1, 1);
            this.lbMinFrequency.Margin = new System.Windows.Forms.Padding(0);
            this.lbMinFrequency.Name = "lbMinFrequency";
            this.lbMinFrequency.Size = new System.Drawing.Size(86, 28);
            this.lbMinFrequency.TabIndex = 0;
            this.lbMinFrequency.Text = "Frequência Mín:";
            this.lbMinFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbMaxAmplitude
            // 
            this.tbMaxAmplitude.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMaxAmplitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMaxAmplitude.Location = new System.Drawing.Point(150, 62);
            this.tbMaxAmplitude.Multiline = true;
            this.tbMaxAmplitude.Name = "tbMaxAmplitude";
            this.tbMaxAmplitude.Size = new System.Drawing.Size(54, 22);
            this.tbMaxAmplitude.TabIndex = 2;
            this.tbMaxAmplitude.Text = "5";
            this.tbMaxAmplitude.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbMaxAmplitude_KeyDown);
            this.tbMaxAmplitude.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMaxAmplitude_KeyPress);
            // 
            // lbMaxAmplitude
            // 
            this.lbMaxAmplitude.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMaxAmplitude.AutoSize = true;
            this.lbMaxAmplitude.BackColor = System.Drawing.Color.Transparent;
            this.lbMaxAmplitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaxAmplitude.Location = new System.Drawing.Point(1, 59);
            this.lbMaxAmplitude.Margin = new System.Windows.Forms.Padding(0);
            this.lbMaxAmplitude.Name = "lbMaxAmplitude";
            this.lbMaxAmplitude.Size = new System.Drawing.Size(86, 28);
            this.lbMaxAmplitude.TabIndex = 1;
            this.lbMaxAmplitude.Text = "Faixa de tensão do ensaio (Vpp):";
            this.lbMaxAmplitude.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbMaxFrequency
            // 
            this.lbMaxFrequency.AutoSize = true;
            this.lbMaxFrequency.BackColor = System.Drawing.Color.Transparent;
            this.lbMaxFrequency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbMaxFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMaxFrequency.Location = new System.Drawing.Point(1, 30);
            this.lbMaxFrequency.Margin = new System.Windows.Forms.Padding(0);
            this.lbMaxFrequency.Name = "lbMaxFrequency";
            this.lbMaxFrequency.Size = new System.Drawing.Size(86, 28);
            this.lbMaxFrequency.TabIndex = 1;
            this.lbMaxFrequency.Text = "Frequência Máx:";
            this.lbMaxFrequency.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbMinAmplitude
            // 
            this.tbMinAmplitude.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMinAmplitude.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMinAmplitude.Location = new System.Drawing.Point(91, 62);
            this.tbMinAmplitude.Multiline = true;
            this.tbMinAmplitude.Name = "tbMinAmplitude";
            this.tbMinAmplitude.Size = new System.Drawing.Size(52, 22);
            this.tbMinAmplitude.TabIndex = 1;
            this.tbMinAmplitude.Text = "0,1";
            this.tbMinAmplitude.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbMinAmplitude_KeyDown);
            this.tbMinAmplitude.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMinAmplitude_KeyPress);
            // 
            // tbMinFrequency
            // 
            this.tbMinFrequency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMinFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMinFrequency.Location = new System.Drawing.Point(91, 4);
            this.tbMinFrequency.Multiline = true;
            this.tbMinFrequency.Name = "tbMinFrequency";
            this.tbMinFrequency.Size = new System.Drawing.Size(52, 22);
            this.tbMinFrequency.TabIndex = 1;
            this.tbMinFrequency.Text = "20";
            this.tbMinFrequency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbMinFrequency_KeyDown);
            this.tbMinFrequency.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMinFrequency_KeyPress);
            // 
            // tbMaxFrequency
            // 
            this.tbMaxFrequency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMaxFrequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMaxFrequency.Location = new System.Drawing.Point(91, 33);
            this.tbMaxFrequency.Multiline = true;
            this.tbMaxFrequency.Name = "tbMaxFrequency";
            this.tbMaxFrequency.Size = new System.Drawing.Size(52, 22);
            this.tbMaxFrequency.TabIndex = 3;
            this.tbMaxFrequency.Text = "1";
            this.tbMaxFrequency.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbMaxFrequency_KeyDown);
            this.tbMaxFrequency.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbMaxFrequency_KeyPress);
            // 
            // cbMinFrequencyMeasurement
            // 
            this.cbMinFrequencyMeasurement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMinFrequencyMeasurement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMinFrequencyMeasurement.FormattingEnabled = true;
            this.cbMinFrequencyMeasurement.Items.AddRange(new object[] {
            "Hz",
            "kHz",
            "MHz"});
            this.cbMinFrequencyMeasurement.Location = new System.Drawing.Point(150, 4);
            this.cbMinFrequencyMeasurement.Name = "cbMinFrequencyMeasurement";
            this.cbMinFrequencyMeasurement.Size = new System.Drawing.Size(54, 23);
            this.cbMinFrequencyMeasurement.TabIndex = 2;
            // 
            // cbMaxFrequencyMeasurement
            // 
            this.cbMaxFrequencyMeasurement.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMaxFrequencyMeasurement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMaxFrequencyMeasurement.FormattingEnabled = true;
            this.cbMaxFrequencyMeasurement.Items.AddRange(new object[] {
            "Hz",
            "kHz",
            "MHz"});
            this.cbMaxFrequencyMeasurement.Location = new System.Drawing.Point(150, 33);
            this.cbMaxFrequencyMeasurement.Name = "cbMaxFrequencyMeasurement";
            this.cbMaxFrequencyMeasurement.Size = new System.Drawing.Size(54, 23);
            this.cbMaxFrequencyMeasurement.TabIndex = 4;
            // 
            // btStart
            // 
            this.btStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStart.Location = new System.Drawing.Point(5, 19);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(80, 25);
            this.btStart.TabIndex = 1;
            this.btStart.Text = "Iniciar Ensaio";
            this.btStart.UseVisualStyleBackColor = true;
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // lbOsciloscope
            // 
            this.lbOsciloscope.AutoSize = true;
            this.lbOsciloscope.Location = new System.Drawing.Point(6, 78);
            this.lbOsciloscope.Name = "lbOsciloscope";
            this.lbOsciloscope.Size = new System.Drawing.Size(79, 13);
            this.lbOsciloscope.TabIndex = 6;
            this.lbOsciloscope.Text = "Osciloscópio";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.71844F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.28156F));
            this.tableLayoutPanel1.Controls.Add(this.btConnection, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbConnection, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbOsciloscopePort, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbPort, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbBrand, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.tbIp, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbOsciloscopeIp, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 121);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(208, 88);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // btConnection
            // 
            this.btConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btConnection.Location = new System.Drawing.Point(4, 4);
            this.btConnection.Name = "btConnection";
            this.btConnection.Size = new System.Drawing.Size(81, 22);
            this.btConnection.TabIndex = 1;
            this.btConnection.Text = "Conectar";
            this.btConnection.UseVisualStyleBackColor = true;
            this.btConnection.Click += new System.EventHandler(this.btConnection_Click);
            // 
            // lbConnection
            // 
            this.lbConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbConnection.AutoSize = true;
            this.lbConnection.BackColor = System.Drawing.Color.LightSalmon;
            this.lbConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConnection.Location = new System.Drawing.Point(89, 1);
            this.lbConnection.Margin = new System.Windows.Forms.Padding(0);
            this.lbConnection.Name = "lbConnection";
            this.lbConnection.Size = new System.Drawing.Size(118, 28);
            this.lbConnection.TabIndex = 13;
            this.lbConnection.Text = "Desconectado";
            this.lbConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbOsciloscopePort
            // 
            this.lbOsciloscopePort.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbOsciloscopePort.AutoSize = true;
            this.lbOsciloscopePort.BackColor = System.Drawing.Color.Transparent;
            this.lbOsciloscopePort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOsciloscopePort.Location = new System.Drawing.Point(1, 59);
            this.lbOsciloscopePort.Margin = new System.Windows.Forms.Padding(0);
            this.lbOsciloscopePort.Name = "lbOsciloscopePort";
            this.lbOsciloscopePort.Size = new System.Drawing.Size(87, 28);
            this.lbOsciloscopePort.TabIndex = 8;
            this.lbOsciloscopePort.Text = "Porta:";
            this.lbOsciloscopePort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbPort
            // 
            this.tbPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPort.Location = new System.Drawing.Point(92, 62);
            this.tbPort.Multiline = true;
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(111, 22);
            this.tbPort.TabIndex = 3;
            this.tbPort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbPort_KeyDown);
            this.tbPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPort_KeyPress);
            // 
            // lbBrand
            // 
            this.lbBrand.AutoSize = true;
            this.lbBrand.BackColor = System.Drawing.Color.Transparent;
            this.lbBrand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBrand.Location = new System.Drawing.Point(89, 88);
            this.lbBrand.Margin = new System.Windows.Forms.Padding(0);
            this.lbBrand.Name = "lbBrand";
            this.lbBrand.Size = new System.Drawing.Size(118, 28);
            this.lbBrand.TabIndex = 10;
            this.lbBrand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbIp
            // 
            this.tbIp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbIp.Culture = new System.Globalization.CultureInfo("");
            this.tbIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIp.Location = new System.Drawing.Point(92, 33);
            this.tbIp.Mask = "000.000.000.000";
            this.tbIp.Name = "tbIp";
            this.tbIp.Size = new System.Drawing.Size(111, 22);
            this.tbIp.TabIndex = 2;
            // 
            // lbOsciloscopeIp
            // 
            this.lbOsciloscopeIp.AutoSize = true;
            this.lbOsciloscopeIp.BackColor = System.Drawing.Color.Transparent;
            this.lbOsciloscopeIp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbOsciloscopeIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOsciloscopeIp.Location = new System.Drawing.Point(1, 30);
            this.lbOsciloscopeIp.Margin = new System.Windows.Forms.Padding(0);
            this.lbOsciloscopeIp.Name = "lbOsciloscopeIp";
            this.lbOsciloscopeIp.Size = new System.Drawing.Size(87, 28);
            this.lbOsciloscopeIp.TabIndex = 2;
            this.lbOsciloscopeIp.Text = "IP:";
            this.lbOsciloscopeIp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timerForm
            // 
            this.timerForm.Tick += new System.EventHandler(this.timerForm_Tick);
            // 
            // FrmTestFra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(1284, 711);
            this.Controls.Add(this.btGetEquipment);
            this.Controls.Add(this.tbTransformer);
            this.Controls.Add(this.lbTransformer);
            this.Controls.Add(this.pnTest);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmTestFra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Controle de Ensaio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTestFra_FormClosed);
            this.Load += new System.EventHandler(this.FrmTestFra_Load);
            this.pnTest.ResumeLayout(false);
            this.pnTest.PerformLayout();
            this.tabGraphs.ResumeLayout(false);
            this.tabResults.ResumeLayout(false);
            this.tableSecondGraphs.ResumeLayout(false);
            this.tabInputOutput.ResumeLayout(false);
            this.tableFirstGraphs.ResumeLayout(false);
            this.gbTestController.ResumeLayout(false);
            this.gbTestController.PerformLayout();
            this.gbAcquisition.ResumeLayout(false);
            this.tableAcquisition.ResumeLayout(false);
            this.tableAcquisition.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableController.ResumeLayout(false);
            this.tableController.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btGetEquipment;
        private System.Windows.Forms.TextBox tbTransformer;
        private System.Windows.Forms.Label lbTransformer;
        private System.Windows.Forms.Panel pnTest;
        private System.Windows.Forms.Label lbScale;
        private System.Windows.Forms.RadioButton rbHz;
        private System.Windows.Forms.RadioButton rbLog;
        private System.Windows.Forms.TabControl tabGraphs;
        private System.Windows.Forms.TabPage tabResults;
        private System.Windows.Forms.TableLayoutPanel tableSecondGraphs;
        private ZedGraph.ZedGraphControl graphAmplitude;
        private ZedGraph.ZedGraphControl graphPhase;
        private System.Windows.Forms.TabPage tabInputOutput;
        private System.Windows.Forms.TableLayoutPanel tableFirstGraphs;
        private ZedGraph.ZedGraphControl graphInput;
        private ZedGraph.ZedGraphControl graphOutput;
        private System.Windows.Forms.GroupBox gbTestController;
        private System.Windows.Forms.Label lbCurrentStatus;
        private System.Windows.Forms.Label lbStatus;
        private System.Windows.Forms.GroupBox gbAcquisition;
        private System.Windows.Forms.ProgressBar pbSignalAnalysis;
        private System.Windows.Forms.TableLayoutPanel tableAcquisition;
        private System.Windows.Forms.Label lbAcquisitionInput;
        private System.Windows.Forms.Label lbAcquisitionOutput;
        private System.Windows.Forms.Label lbAcquisitionResult;
        private System.Windows.Forms.Label lbAcquisitionPhase;
        private System.Windows.Forms.Label lbAmplitudeSample;
        private System.Windows.Forms.Label lbCurrentInput;
        private System.Windows.Forms.Label lbCurrentOutput;
        private System.Windows.Forms.Label lbCurrentResult;
        private System.Windows.Forms.Label lbCurrentPhase;
        private System.Windows.Forms.Label lbCurrentSample;
        private System.Windows.Forms.Label lbVoltageInjection;
        private System.Windows.Forms.Label lbInjectVoltage;
        private System.Windows.Forms.Label lbSignalFrequency;
        private System.Windows.Forms.Label lbCurrentFrequency;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lbSamples;
        private System.Windows.Forms.TextBox tbSamples;
        private System.Windows.Forms.Label lbTime;
        private System.Windows.Forms.Label lbTestTime;
        private System.Windows.Forms.Button btStop;
        private System.Windows.Forms.ComboBox cbOsciloscope;
        private System.Windows.Forms.Label lbParamters;
        private System.Windows.Forms.TableLayoutPanel tableController;
        private System.Windows.Forms.Label lbMinFrequency;
        private System.Windows.Forms.TextBox tbMaxAmplitude;
        private System.Windows.Forms.Label lbMaxAmplitude;
        private System.Windows.Forms.Label lbMaxFrequency;
        private System.Windows.Forms.TextBox tbMinAmplitude;
        private System.Windows.Forms.TextBox tbMinFrequency;
        private System.Windows.Forms.TextBox tbMaxFrequency;
        private System.Windows.Forms.ComboBox cbMinFrequencyMeasurement;
        private System.Windows.Forms.ComboBox cbMaxFrequencyMeasurement;
        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.Label lbOsciloscope;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btConnection;
        private System.Windows.Forms.Label lbConnection;
        private System.Windows.Forms.Label lbOsciloscopePort;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label lbBrand;
        private System.Windows.Forms.MaskedTextBox tbIp;
        private System.Windows.Forms.Label lbOsciloscopeIp;
        private System.Windows.Forms.Timer timerForm;
    }
}