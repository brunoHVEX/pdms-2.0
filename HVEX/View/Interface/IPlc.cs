﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.View.Interface {
    public interface IPlc:IInstrument  {
        int AcquisitionInterval { get; set; }
    }
}
