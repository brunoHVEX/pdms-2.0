﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVEX.View.Interface {
    public interface ITestFra {
        string Id { get; set; }
        List<double>[] InputSignal { set; }
        List<double>[] OutputSignal { set; }
        List<double>[] AmplitudeSignal { set; }
        List<double>[] PhaseSignal { set; }
        double MinAmplitude { get; set; }
        double MaxAmplitude { get; set; }
        double MinFrequency { get; set; }
        string MinFrequencyMeasurement { get; set; }
        double MaxFrequency { get; set; }
        string MaxFrequencyMeasurement { get; set; }
        string CurrentStatus { get; set; }
        int Samples { get; set; }
        int AnalysisTotal { set; }
        int AnalysisProgress { set; }
        double CurrentFrequency { get; set; }
        double InjectedVoltage { get; set; }
        double CurrentInput { get; set; }
        double CurrentOutput { get; set; }
        double CurrentResult { get; set; }
        double CurrentPhase { get; set; }
        int CurrentSample { get; set; }
        string EquipmentId { get; set; }
        string EquipmentDescription { get; set; }
        string InstrumentConnection { get; set; }
        string InstrumentId { get; set; }
        string InstrumentIp { get; set; }
        int InstrumentPort { get; set; }
        DateTime Date { get; set; }
        DataTable List { set; }
        DataTable OsciloscopeList { set; }
    }
}
