﻿using System.Data;

namespace HVEX.View.Interface {
    public interface IEquipment {
        string Id { get; set; }
        string SerialNumber { get; set; }
        string Model { get; set; }
        DataTable List { set; }
    }
}
