﻿using System.Data;

namespace HVEX.View.Interface {
    public interface IInstrument {
        string Id { get; set; }
        string SerialNumber { get; set; }
        string Description { get; set; }
        string Brand { get; set; }
        string Model { get; set; }
        string Ip { get; set; }
        int Port { get; set; }
        string InstrumentType { get; set; }
        DataTable List { set; }
    }
}
