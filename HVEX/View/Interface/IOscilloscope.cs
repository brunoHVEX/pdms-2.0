﻿using RohdeSchwarz.RsInstrument;

namespace HVEX.View.Interface {
    public interface IOscilloscope:IInstrument  {
        int Channels { get; set; }
        string Instrument { get; set; }
    }
}
