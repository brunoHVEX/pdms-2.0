﻿using HVEX.Model;
using System.Collections.Generic;
using System.Data;

namespace HVEX.View.Interface {
    public interface ITestPdmsThreePhase {
        string EquipmentId { get; set; }
        string IdConfiguration { get; set; }
        string EquipmentDescription { get; set; }
        string InstrumentConnection { get; set; }
        string InstrumentId { get; set; }
        string InstrumentIp { get; set; }
        int InstrumentPort { get; set; }
        string Channels { get; set; }
        WaveForm SignalPhase1 { set; }
        WaveForm SignalPhase2 { set; }
        WaveForm SignalPhase3 { set; }
        WaveForm SignalFiltredPhase1 { set; }
        WaveForm SignalFiltredPhase2 { set; }
        WaveForm SignalFiltredPhase3 { set; }
        WaveForm FftPhase1 { set; }
        WaveForm FftPhase2 { set; }
        WaveForm FftPhase3 { set; }
        List<double>[] DispersionPhase1 { set; }
        List<double>[] DispersionPhase2 { set; }
        List<double>[] DispersionPhase3 { set; }
        List<double>[] DispersionDetailedPhase1 { set; }
        List<double>[] DispersionDetailedPhase2 { set; }
        List<double>[] DispersionDetailedPhase3 { set; }
        List<double>[] PartialDischargeTrendPhase1 { set; }
        List<double>[] PartialDischargeTrendPhase2 { set; }
        List<double>[] PartialDischargeTrendPhase3 { set; }
        double GainPhase1 { get; set; }
        double GainPhase2 { get; set; }
        double GainPhase3 { get; set; }
        double GatePhase1 { get; set; }
        double GatePhase2 { get; set; }
        double GatePhase3 { get; set; }
        string Filter { get; set; }
        double Order { get; set; }
        double FrequencyUndercut { get; set; }
        string UnitFrequencyUndercut { get; set; }
        double FrequencyTopCut { get; set; }
        string UnitFrequencyTopCut { get; set; }
        double ChargePhase1 { get; set; }
        double OccurrencesPhase1 { get; set; }
        double ChargePhase2 { get; set; }
        double OccurrencesPhase2 { get; set; }
        double ChargePhase3 { get; set; }
        double OccurrencesPhase3 { get; set; }
        double TotalPositiveDischarges { get; set; }
        double TotalNegativeDischarges { get; set; }
        double TotalOccurrences { get; set; }
        double CalibrationPhase1 { get; set; }
        double CalibrationPhase2 { get; set; }
        double CalibrationPhase3 { get; set; }
        DataTable OsciloscopeList { set; }
        bool Osciloscope { get; set; }
    }
}
