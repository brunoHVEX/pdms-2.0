﻿using System;
using System.Data;
using System.Drawing;

namespace HVEX.View.Interface {
    interface IUser {
        string Id { get; set; }
        string Name { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string AccessLevel { get; set; }
        Image Image { get; set; }
        DataTable List { set; }
    }
}
