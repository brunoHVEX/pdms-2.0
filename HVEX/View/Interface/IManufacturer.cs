﻿
using System.Data;
using System.Drawing;

namespace HVEX.View.Interface {
    interface IManufacturer {
        string Id { get; set; }
        string CompanyName { get; set; }
        string FantasyName { get; set; }
        string Email { get; set; }
        string Phone { get; set; }
        string Contact { get; set; }
        string Address { get; set; }
        string District { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Country { get; set; }
        Image Image { get; set; }
        DataTable List { set; }
    }
}
