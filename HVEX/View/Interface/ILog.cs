﻿using System;
using System.Data;

namespace HVEX.View.Interface {
    interface ILog {
        string Id { get; set; }
        string Description { get; set; }
        string Error { get; set; }
        string Source { get; set; }
        DateTime Date { get; set; }
        string User { get; set; }
        DataTable List { set; }
    }
}
