﻿using HVEX.Model;
using System.Collections.Generic;
using System.Data;

namespace HVEX.View {
    public interface ITestPdmsSinglePhase {
        string EquipmentId { get; set; }

        string IdConfiguration { get; set; }
        string EquipmentDescription { get; set; }
        string InstrumentConnection { get; set; }
        string InstrumentId { get; set; }
        string InstrumentIp { get; set; }
        int InstrumentPort { get; set; }
        string Channels { get; set; }
        DataTable OsciloscopeList { set; }
        WaveForm SignalPhase1 { set; }
        WaveForm SignalFiltredPhase1 { set; }
        WaveForm FftPhase1 { set; }
        List<double>[] DispersionPhase1 { set; }
        List<double>[] DispersionDetailedPhase1 { set; }
        List<double>[] PartialDischargeTrendPhase1 { set; }
        double GainPhase1 { get; set; }
        double GatePhase1 { get; set; }
        string Filter { get; set; }
        double Order { get; set; }
        double FrequencyUndercut { get; set; }
        string UnitFrequencyUndercut { get; set; }
        double FrequencyTopCut { get; set; }
        string UnitFrequencyTopCut { get; set; }
        double ChargePhase1 { get; set; }
        double OccurrencesPhase1 { get; set; }
        double TotalPositiveDischarges { get; set; }
        double TotalNegativeDischarges { get; set; }
        double TotalOccurrences { get; set; }
        double CalibrationPhase1 { get; set; }
        bool Osciloscope { get; set; }
    }
}
