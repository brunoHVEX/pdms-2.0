﻿using MongoDB.Bson;
using System;
using System.Data;

namespace HVEX.View.Interface {
    public interface ITest {
        ObjectId Id { get; set; }
        ObjectId Equipment { get; set; }
        ObjectId Instrument { get; set; }
        ObjectId User { get; set; }
        DateTime Date { get; set; }
        DateTime Duration { get; set; }
        string TestType { get; set; }
        DataTable List { set; }
    }
}
