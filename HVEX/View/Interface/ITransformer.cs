﻿using System;
using System.Collections.Generic;
using System.Data;

namespace HVEX.View.Interface {
    public interface ITransformer : IEquipment{
        string InternalNumber { get; set; }
        string Description { get; set; }
        string Manufacturer { get; set; }
        DateTime ManufactureDate { get; set; }
        string Customer { get; set; }
        string SpecimenType { get; set; }
        string TrafoType { get; set; }
        string TrafoEfficiencyLevel { get; set; }
        double TrafoRegulationRange { get; set; }
        double TrafoWeight { get; set; }
        string TrafoPolarity { get; set; }
        int TrafoPhases { get; set; }
        int TrafoFrequency { get; set; }
        double TrafoTemperatureLimit { get; set; }
        double TrafoResistanceTemperature { get; set; }
        string TrafoInsulatingOil { get; set; }
        double TrafoVoltageClass { get; set; }
        string TrafoState { get; set; }
        string TrafoConnection { get; set; }
        double[] TrafoPower { get; set; }
        string[] TrafoPowerMeasurement { get; set; }
        double[] TrafoVoltage { get; set; }
        string[] TrafoVoltageMeasurement { get; set; }
        double[] TrafoCurrent { get; set; }
        string[] TrafoCurrentMeasurement { get; set; }
        string[] TrafoWindingMaterial { get; set; }
        double[] TrafoResistance { get; set; }
        string[] TrafoResistanceMeasurement { get; set; }
        List<double> TrafoTaps { get; set; }
        DataTable ManufacturerList { set; }
        DataTable CustomerList { set; }
    }
}
