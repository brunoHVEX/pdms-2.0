﻿
namespace HVEX.View {
    partial class FrmEquipment {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEquipment));
            this.tabTestBodies = new System.Windows.Forms.TabControl();
            this.tabRegister = new System.Windows.Forms.TabPage();
            this.btClean = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.rbTransformer = new System.Windows.Forms.RadioButton();
            this.rbGeneric = new System.Windows.Forms.RadioButton();
            this.lbGeneral = new System.Windows.Forms.Label();
            this.pnGeneral = new System.Windows.Forms.Panel();
            this.tbManufactureDate = new System.Windows.Forms.MaskedTextBox();
            this.cbManufacturer = new System.Windows.Forms.ComboBox();
            this.cbCustomer = new System.Windows.Forms.ComboBox();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.tbInternalNumber = new System.Windows.Forms.TextBox();
            this.tbSerialNumber = new System.Windows.Forms.TextBox();
            this.lbManufactureDate = new System.Windows.Forms.Label();
            this.lbModel = new System.Windows.Forms.Label();
            this.lbDescription = new System.Windows.Forms.Label();
            this.lbInternalNumber = new System.Windows.Forms.Label();
            this.lbManufacturer = new System.Windows.Forms.Label();
            this.lbCustomer = new System.Windows.Forms.Label();
            this.lbSerialNumber = new System.Windows.Forms.Label();
            this.tabTestBodyType = new System.Windows.Forms.TabControl();
            this.tabGeneric = new System.Windows.Forms.TabPage();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.tabTransformer = new System.Windows.Forms.TabPage();
            this.lbTaps = new System.Windows.Forms.Label();
            this.pnTaps = new System.Windows.Forms.Panel();
            this.btDelete = new System.Windows.Forms.Button();
            this.btAdd = new System.Windows.Forms.Button();
            this.dgTaps = new System.Windows.Forms.DataGridView();
            this.TapVoltage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lbSpecifications = new System.Windows.Forms.Label();
            this.pnSpecifications = new System.Windows.Forms.Panel();
            this.btConnection = new System.Windows.Forms.Button();
            this.tbConnection = new System.Windows.Forms.TextBox();
            this.cbState = new System.Windows.Forms.ComboBox();
            this.cbVoltageClass = new System.Windows.Forms.ComboBox();
            this.cbInsulatingOil = new System.Windows.Forms.ComboBox();
            this.cbPolarity = new System.Windows.Forms.ComboBox();
            this.cbFrequency = new System.Windows.Forms.ComboBox();
            this.cbPhases = new System.Windows.Forms.ComboBox();
            this.tbResistanceTemperature = new System.Windows.Forms.TextBox();
            this.tbRegulationRange = new System.Windows.Forms.TextBox();
            this.tbType = new System.Windows.Forms.TextBox();
            this.tbWeight = new System.Windows.Forms.TextBox();
            this.tbTemperatureLimit = new System.Windows.Forms.TextBox();
            this.tbEfficiencyLevel = new System.Windows.Forms.TextBox();
            this.lbConnection = new System.Windows.Forms.Label();
            this.lbResistenceTemperature = new System.Windows.Forms.Label();
            this.lbRegulationRange = new System.Windows.Forms.Label();
            this.lbType = new System.Windows.Forms.Label();
            this.lbWeight = new System.Windows.Forms.Label();
            this.lbTemperatureLimit = new System.Windows.Forms.Label();
            this.lbEfficiencyLevel = new System.Windows.Forms.Label();
            this.lbState = new System.Windows.Forms.Label();
            this.lbVoltageClass = new System.Windows.Forms.Label();
            this.lbInsulatingOil = new System.Windows.Forms.Label();
            this.lbPolarity = new System.Windows.Forms.Label();
            this.lbFrequency = new System.Windows.Forms.Label();
            this.lbPhases = new System.Windows.Forms.Label();
            this.lbWindings = new System.Windows.Forms.Label();
            this.pnWindings = new System.Windows.Forms.Panel();
            this.pnSecondWinding = new System.Windows.Forms.Panel();
            this.cbSecondResistance = new System.Windows.Forms.ComboBox();
            this.cbSecondWindingMaterial = new System.Windows.Forms.ComboBox();
            this.cbSecondCurrent = new System.Windows.Forms.ComboBox();
            this.cbSecondVoltage = new System.Windows.Forms.ComboBox();
            this.cbSecondPower = new System.Windows.Forms.ComboBox();
            this.tbSecondResistance = new System.Windows.Forms.TextBox();
            this.tbSecondCurrent = new System.Windows.Forms.TextBox();
            this.tbSecondVoltage = new System.Windows.Forms.TextBox();
            this.tbSecondPower = new System.Windows.Forms.TextBox();
            this.pnThirdWinding = new System.Windows.Forms.Panel();
            this.cbThirdResistance = new System.Windows.Forms.ComboBox();
            this.cbThirdWindingMaterial = new System.Windows.Forms.ComboBox();
            this.cbThirdCurrent = new System.Windows.Forms.ComboBox();
            this.cbThirdVoltage = new System.Windows.Forms.ComboBox();
            this.cbThirdPower = new System.Windows.Forms.ComboBox();
            this.tbThirdResistance = new System.Windows.Forms.TextBox();
            this.tbThirdCurrent = new System.Windows.Forms.TextBox();
            this.tbThirdVoltage = new System.Windows.Forms.TextBox();
            this.tbThirdPower = new System.Windows.Forms.TextBox();
            this.pnFirstWinding = new System.Windows.Forms.Panel();
            this.cbFirstResistance = new System.Windows.Forms.ComboBox();
            this.cbFirstWindingMaterial = new System.Windows.Forms.ComboBox();
            this.cbFirstCurrent = new System.Windows.Forms.ComboBox();
            this.cbFirstVoltage = new System.Windows.Forms.ComboBox();
            this.cbFirstPower = new System.Windows.Forms.ComboBox();
            this.tbFirstResistance = new System.Windows.Forms.TextBox();
            this.tbFirstCurrent = new System.Windows.Forms.TextBox();
            this.tbFirstVoltage = new System.Windows.Forms.TextBox();
            this.tbFirstPower = new System.Windows.Forms.TextBox();
            this.cbTerciary = new System.Windows.Forms.CheckBox();
            this.cbSecondary = new System.Windows.Forms.CheckBox();
            this.cbPrimary = new System.Windows.Forms.CheckBox();
            this.lbResistence = new System.Windows.Forms.Label();
            this.lbWindingMaterial = new System.Windows.Forms.Label();
            this.lbCurrent = new System.Windows.Forms.Label();
            this.lbVoltage = new System.Windows.Forms.Label();
            this.lbPower = new System.Windows.Forms.Label();
            this.tabSearch = new System.Windows.Forms.TabPage();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btRemove = new System.Windows.Forms.Button();
            this.dgTestBodies = new System.Windows.Forms.DataGridView();
            this.btSearch = new System.Windows.Forms.Button();
            this.cbOrder = new System.Windows.Forms.ComboBox();
            this.tbSearchDescription = new System.Windows.Forms.TextBox();
            this.tbSearchSerialNumber = new System.Windows.Forms.TextBox();
            this.lbOrder = new System.Windows.Forms.Label();
            this.lbSearchDescription = new System.Windows.Forms.Label();
            this.lbSearchSerialNumber = new System.Windows.Forms.Label();
            this.tabTestBodies.SuspendLayout();
            this.tabRegister.SuspendLayout();
            this.pnGeneral.SuspendLayout();
            this.tabTestBodyType.SuspendLayout();
            this.tabGeneric.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            this.tabTransformer.SuspendLayout();
            this.pnTaps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTaps)).BeginInit();
            this.pnSpecifications.SuspendLayout();
            this.pnWindings.SuspendLayout();
            this.pnSecondWinding.SuspendLayout();
            this.pnThirdWinding.SuspendLayout();
            this.pnFirstWinding.SuspendLayout();
            this.tabSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTestBodies)).BeginInit();
            this.SuspendLayout();
            // 
            // tabTestBodies
            // 
            this.tabTestBodies.Controls.Add(this.tabRegister);
            this.tabTestBodies.Controls.Add(this.tabSearch);
            this.tabTestBodies.Location = new System.Drawing.Point(11, 12);
            this.tabTestBodies.Name = "tabTestBodies";
            this.tabTestBodies.SelectedIndex = 0;
            this.tabTestBodies.Size = new System.Drawing.Size(848, 577);
            this.tabTestBodies.TabIndex = 1;
            this.tabTestBodies.SelectedIndexChanged += new System.EventHandler(this.tabTestBodies_SelectedIndexChanged);
            // 
            // tabRegister
            // 
            this.tabRegister.Controls.Add(this.btClean);
            this.tabRegister.Controls.Add(this.btSave);
            this.tabRegister.Controls.Add(this.rbTransformer);
            this.tabRegister.Controls.Add(this.rbGeneric);
            this.tabRegister.Controls.Add(this.lbGeneral);
            this.tabRegister.Controls.Add(this.pnGeneral);
            this.tabRegister.Controls.Add(this.tabTestBodyType);
            this.tabRegister.Location = new System.Drawing.Point(4, 22);
            this.tabRegister.Name = "tabRegister";
            this.tabRegister.Padding = new System.Windows.Forms.Padding(3);
            this.tabRegister.Size = new System.Drawing.Size(840, 551);
            this.tabRegister.TabIndex = 0;
            this.tabRegister.Text = "Registrar";
            this.tabRegister.UseVisualStyleBackColor = true;
            // 
            // btClean
            // 
            this.btClean.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btClean.FlatAppearance.BorderSize = 0;
            this.btClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btClean.ForeColor = System.Drawing.Color.White;
            this.btClean.Image = global::HVEX.Properties.Resources.white_clean;
            this.btClean.Location = new System.Drawing.Point(759, 523);
            this.btClean.Name = "btClean";
            this.btClean.Size = new System.Drawing.Size(75, 23);
            this.btClean.TabIndex = 8;
            this.btClean.Text = "Limpar";
            this.btClean.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btClean.UseVisualStyleBackColor = false;
            this.btClean.Click += new System.EventHandler(this.btClean_Click);
            // 
            // btSave
            // 
            this.btSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btSave.FlatAppearance.BorderSize = 0;
            this.btSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSave.ForeColor = System.Drawing.Color.White;
            this.btSave.Image = global::HVEX.Properties.Resources.white_save;
            this.btSave.Location = new System.Drawing.Point(678, 523);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(75, 23);
            this.btSave.TabIndex = 7;
            this.btSave.Text = "Salvar";
            this.btSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.UseVisualStyleBackColor = false;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // rbTransformer
            // 
            this.rbTransformer.AutoSize = true;
            this.rbTransformer.Location = new System.Drawing.Point(735, 131);
            this.rbTransformer.Name = "rbTransformer";
            this.rbTransformer.Size = new System.Drawing.Size(93, 17);
            this.rbTransformer.TabIndex = 9;
            this.rbTransformer.Text = "Transformador";
            this.rbTransformer.UseVisualStyleBackColor = true;
            this.rbTransformer.CheckedChanged += new System.EventHandler(this.rbTransformer_CheckedChanged);
            // 
            // rbGeneric
            // 
            this.rbGeneric.AutoSize = true;
            this.rbGeneric.Checked = true;
            this.rbGeneric.Location = new System.Drawing.Point(662, 131);
            this.rbGeneric.Name = "rbGeneric";
            this.rbGeneric.Size = new System.Drawing.Size(68, 17);
            this.rbGeneric.TabIndex = 14;
            this.rbGeneric.TabStop = true;
            this.rbGeneric.Text = "Genérico";
            this.rbGeneric.UseVisualStyleBackColor = true;
            this.rbGeneric.CheckedChanged += new System.EventHandler(this.rbGeneric_CheckedChanged);
            // 
            // lbGeneral
            // 
            this.lbGeneral.AutoSize = true;
            this.lbGeneral.Location = new System.Drawing.Point(18, 8);
            this.lbGeneral.Name = "lbGeneral";
            this.lbGeneral.Size = new System.Drawing.Size(32, 13);
            this.lbGeneral.TabIndex = 0;
            this.lbGeneral.Text = "Geral";
            // 
            // pnGeneral
            // 
            this.pnGeneral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnGeneral.Controls.Add(this.tbManufactureDate);
            this.pnGeneral.Controls.Add(this.cbManufacturer);
            this.pnGeneral.Controls.Add(this.cbCustomer);
            this.pnGeneral.Controls.Add(this.tbModel);
            this.pnGeneral.Controls.Add(this.tbDescription);
            this.pnGeneral.Controls.Add(this.tbInternalNumber);
            this.pnGeneral.Controls.Add(this.tbSerialNumber);
            this.pnGeneral.Controls.Add(this.lbManufactureDate);
            this.pnGeneral.Controls.Add(this.lbModel);
            this.pnGeneral.Controls.Add(this.lbDescription);
            this.pnGeneral.Controls.Add(this.lbInternalNumber);
            this.pnGeneral.Controls.Add(this.lbManufacturer);
            this.pnGeneral.Controls.Add(this.lbCustomer);
            this.pnGeneral.Controls.Add(this.lbSerialNumber);
            this.pnGeneral.Location = new System.Drawing.Point(6, 12);
            this.pnGeneral.Name = "pnGeneral";
            this.pnGeneral.Size = new System.Drawing.Size(828, 110);
            this.pnGeneral.TabIndex = 1;
            // 
            // tbManufactureDate
            // 
            this.tbManufactureDate.Location = new System.Drawing.Point(715, 78);
            this.tbManufactureDate.Mask = "00/00/0000";
            this.tbManufactureDate.Name = "tbManufactureDate";
            this.tbManufactureDate.Size = new System.Drawing.Size(102, 20);
            this.tbManufactureDate.TabIndex = 22;
            this.tbManufactureDate.ValidatingType = typeof(System.DateTime);
            // 
            // cbManufacturer
            // 
            this.cbManufacturer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbManufacturer.DropDownWidth = 320;
            this.cbManufacturer.FormattingEnabled = true;
            this.cbManufacturer.Location = new System.Drawing.Point(497, 29);
            this.cbManufacturer.Name = "cbManufacturer";
            this.cbManufacturer.Size = new System.Drawing.Size(320, 21);
            this.cbManufacturer.TabIndex = 3;
            // 
            // cbCustomer
            // 
            this.cbCustomer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCustomer.DropDownWidth = 315;
            this.cbCustomer.FormattingEnabled = true;
            this.cbCustomer.Location = new System.Drawing.Point(176, 29);
            this.cbCustomer.Name = "cbCustomer";
            this.cbCustomer.Size = new System.Drawing.Size(315, 21);
            this.cbCustomer.TabIndex = 2;
            // 
            // tbModel
            // 
            this.tbModel.Location = new System.Drawing.Point(497, 78);
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(212, 20);
            this.tbModel.TabIndex = 6;
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(177, 78);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(314, 20);
            this.tbDescription.TabIndex = 5;
            // 
            // tbInternalNumber
            // 
            this.tbInternalNumber.Location = new System.Drawing.Point(8, 78);
            this.tbInternalNumber.Name = "tbInternalNumber";
            this.tbInternalNumber.Size = new System.Drawing.Size(163, 20);
            this.tbInternalNumber.TabIndex = 4;
            // 
            // tbSerialNumber
            // 
            this.tbSerialNumber.Location = new System.Drawing.Point(8, 29);
            this.tbSerialNumber.Name = "tbSerialNumber";
            this.tbSerialNumber.Size = new System.Drawing.Size(163, 20);
            this.tbSerialNumber.TabIndex = 1;
            // 
            // lbManufactureDate
            // 
            this.lbManufactureDate.AutoSize = true;
            this.lbManufactureDate.Location = new System.Drawing.Point(712, 62);
            this.lbManufactureDate.Name = "lbManufactureDate";
            this.lbManufactureDate.Size = new System.Drawing.Size(101, 13);
            this.lbManufactureDate.TabIndex = 21;
            this.lbManufactureDate.Text = "Data de Fabricação";
            // 
            // lbModel
            // 
            this.lbModel.AutoSize = true;
            this.lbModel.Location = new System.Drawing.Point(494, 62);
            this.lbModel.Name = "lbModel";
            this.lbModel.Size = new System.Drawing.Size(42, 13);
            this.lbModel.TabIndex = 19;
            this.lbModel.Text = "Modelo";
            // 
            // lbDescription
            // 
            this.lbDescription.AutoSize = true;
            this.lbDescription.Location = new System.Drawing.Point(173, 62);
            this.lbDescription.Name = "lbDescription";
            this.lbDescription.Size = new System.Drawing.Size(55, 13);
            this.lbDescription.TabIndex = 20;
            this.lbDescription.Text = "Descrição";
            // 
            // lbInternalNumber
            // 
            this.lbInternalNumber.AutoSize = true;
            this.lbInternalNumber.Location = new System.Drawing.Point(8, 62);
            this.lbInternalNumber.Name = "lbInternalNumber";
            this.lbInternalNumber.Size = new System.Drawing.Size(80, 13);
            this.lbInternalNumber.TabIndex = 3;
            this.lbInternalNumber.Text = "Número Interno";
            // 
            // lbManufacturer
            // 
            this.lbManufacturer.AutoSize = true;
            this.lbManufacturer.Location = new System.Drawing.Point(494, 13);
            this.lbManufacturer.Name = "lbManufacturer";
            this.lbManufacturer.Size = new System.Drawing.Size(57, 13);
            this.lbManufacturer.TabIndex = 2;
            this.lbManufacturer.Text = "Fabricante";
            // 
            // lbCustomer
            // 
            this.lbCustomer.AutoSize = true;
            this.lbCustomer.Location = new System.Drawing.Point(173, 13);
            this.lbCustomer.Name = "lbCustomer";
            this.lbCustomer.Size = new System.Drawing.Size(48, 13);
            this.lbCustomer.TabIndex = 1;
            this.lbCustomer.Text = "Empresa";
            // 
            // lbSerialNumber
            // 
            this.lbSerialNumber.AutoSize = true;
            this.lbSerialNumber.Location = new System.Drawing.Point(8, 13);
            this.lbSerialNumber.Name = "lbSerialNumber";
            this.lbSerialNumber.Size = new System.Drawing.Size(84, 13);
            this.lbSerialNumber.TabIndex = 0;
            this.lbSerialNumber.Text = "Número de série";
            // 
            // tabTestBodyType
            // 
            this.tabTestBodyType.Controls.Add(this.tabGeneric);
            this.tabTestBodyType.Controls.Add(this.tabTransformer);
            this.tabTestBodyType.ItemSize = new System.Drawing.Size(80, 18);
            this.tabTestBodyType.Location = new System.Drawing.Point(6, 132);
            this.tabTestBodyType.Name = "tabTestBodyType";
            this.tabTestBodyType.SelectedIndex = 0;
            this.tabTestBodyType.Size = new System.Drawing.Size(828, 384);
            this.tabTestBodyType.TabIndex = 2;
            // 
            // tabGeneric
            // 
            this.tabGeneric.Controls.Add(this.pbLogo);
            this.tabGeneric.Location = new System.Drawing.Point(4, 22);
            this.tabGeneric.Name = "tabGeneric";
            this.tabGeneric.Padding = new System.Windows.Forms.Padding(3);
            this.tabGeneric.Size = new System.Drawing.Size(820, 358);
            this.tabGeneric.TabIndex = 0;
            this.tabGeneric.Text = "Genérico";
            this.tabGeneric.UseVisualStyleBackColor = true;
            // 
            // pbLogo
            // 
            this.pbLogo.Image = global::HVEX.Properties.Resources.logo;
            this.pbLogo.Location = new System.Drawing.Point(210, 79);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(400, 200);
            this.pbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbLogo.TabIndex = 0;
            this.pbLogo.TabStop = false;
            // 
            // tabTransformer
            // 
            this.tabTransformer.Controls.Add(this.lbTaps);
            this.tabTransformer.Controls.Add(this.pnTaps);
            this.tabTransformer.Controls.Add(this.lbSpecifications);
            this.tabTransformer.Controls.Add(this.pnSpecifications);
            this.tabTransformer.Controls.Add(this.lbWindings);
            this.tabTransformer.Controls.Add(this.pnWindings);
            this.tabTransformer.Location = new System.Drawing.Point(4, 22);
            this.tabTransformer.Name = "tabTransformer";
            this.tabTransformer.Padding = new System.Windows.Forms.Padding(3);
            this.tabTransformer.Size = new System.Drawing.Size(820, 358);
            this.tabTransformer.TabIndex = 1;
            this.tabTransformer.Text = "Transformador";
            this.tabTransformer.UseVisualStyleBackColor = true;
            // 
            // lbTaps
            // 
            this.lbTaps.AutoSize = true;
            this.lbTaps.Location = new System.Drawing.Point(19, 207);
            this.lbTaps.Name = "lbTaps";
            this.lbTaps.Size = new System.Drawing.Size(31, 13);
            this.lbTaps.TabIndex = 26;
            this.lbTaps.Text = "Taps";
            // 
            // pnTaps
            // 
            this.pnTaps.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnTaps.Controls.Add(this.btDelete);
            this.pnTaps.Controls.Add(this.btAdd);
            this.pnTaps.Controls.Add(this.dgTaps);
            this.pnTaps.Location = new System.Drawing.Point(9, 216);
            this.pnTaps.Name = "pnTaps";
            this.pnTaps.Size = new System.Drawing.Size(469, 135);
            this.pnTaps.TabIndex = 4;
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(399, 73);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(61, 23);
            this.btDelete.TabIndex = 2;
            this.btDelete.Text = "Remover";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(399, 44);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(61, 23);
            this.btAdd.TabIndex = 1;
            this.btAdd.Text = "Adicionar";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // dgTaps
            // 
            this.dgTaps.AllowUserToAddRows = false;
            this.dgTaps.AllowUserToDeleteRows = false;
            this.dgTaps.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgTaps.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgTaps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTaps.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TapVoltage});
            this.dgTaps.Location = new System.Drawing.Point(6, 14);
            this.dgTaps.Name = "dgTaps";
            this.dgTaps.Size = new System.Drawing.Size(387, 113);
            this.dgTaps.TabIndex = 0;
            // 
            // TapVoltage
            // 
            this.TapVoltage.HeaderText = "Tensão ( V )";
            this.TapVoltage.Name = "TapVoltage";
            // 
            // lbSpecifications
            // 
            this.lbSpecifications.AutoSize = true;
            this.lbSpecifications.Location = new System.Drawing.Point(505, 9);
            this.lbSpecifications.Name = "lbSpecifications";
            this.lbSpecifications.Size = new System.Drawing.Size(79, 13);
            this.lbSpecifications.TabIndex = 24;
            this.lbSpecifications.Text = "Especificações";
            // 
            // pnSpecifications
            // 
            this.pnSpecifications.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnSpecifications.Controls.Add(this.btConnection);
            this.pnSpecifications.Controls.Add(this.tbConnection);
            this.pnSpecifications.Controls.Add(this.cbState);
            this.pnSpecifications.Controls.Add(this.cbVoltageClass);
            this.pnSpecifications.Controls.Add(this.cbInsulatingOil);
            this.pnSpecifications.Controls.Add(this.cbPolarity);
            this.pnSpecifications.Controls.Add(this.cbFrequency);
            this.pnSpecifications.Controls.Add(this.cbPhases);
            this.pnSpecifications.Controls.Add(this.tbResistanceTemperature);
            this.pnSpecifications.Controls.Add(this.tbRegulationRange);
            this.pnSpecifications.Controls.Add(this.tbType);
            this.pnSpecifications.Controls.Add(this.tbWeight);
            this.pnSpecifications.Controls.Add(this.tbTemperatureLimit);
            this.pnSpecifications.Controls.Add(this.tbEfficiencyLevel);
            this.pnSpecifications.Controls.Add(this.lbConnection);
            this.pnSpecifications.Controls.Add(this.lbResistenceTemperature);
            this.pnSpecifications.Controls.Add(this.lbRegulationRange);
            this.pnSpecifications.Controls.Add(this.lbType);
            this.pnSpecifications.Controls.Add(this.lbWeight);
            this.pnSpecifications.Controls.Add(this.lbTemperatureLimit);
            this.pnSpecifications.Controls.Add(this.lbEfficiencyLevel);
            this.pnSpecifications.Controls.Add(this.lbState);
            this.pnSpecifications.Controls.Add(this.lbVoltageClass);
            this.pnSpecifications.Controls.Add(this.lbInsulatingOil);
            this.pnSpecifications.Controls.Add(this.lbPolarity);
            this.pnSpecifications.Controls.Add(this.lbFrequency);
            this.pnSpecifications.Controls.Add(this.lbPhases);
            this.pnSpecifications.Location = new System.Drawing.Point(493, 17);
            this.pnSpecifications.Name = "pnSpecifications";
            this.pnSpecifications.Size = new System.Drawing.Size(317, 334);
            this.pnSpecifications.TabIndex = 2;
            // 
            // btConnection
            // 
            this.btConnection.BackColor = System.Drawing.Color.Gainsboro;
            this.btConnection.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btConnection.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btConnection.Location = new System.Drawing.Point(284, 300);
            this.btConnection.Name = "btConnection";
            this.btConnection.Size = new System.Drawing.Size(26, 21);
            this.btConnection.TabIndex = 13;
            this.btConnection.Text = "...";
            this.btConnection.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btConnection.UseVisualStyleBackColor = false;
            this.btConnection.Click += new System.EventHandler(this.btConnection_Click);
            // 
            // tbConnection
            // 
            this.tbConnection.BackColor = System.Drawing.SystemColors.Control;
            this.tbConnection.Enabled = false;
            this.tbConnection.Location = new System.Drawing.Point(160, 300);
            this.tbConnection.Multiline = true;
            this.tbConnection.Name = "tbConnection";
            this.tbConnection.ReadOnly = true;
            this.tbConnection.Size = new System.Drawing.Size(123, 21);
            this.tbConnection.TabIndex = 0;
            // 
            // cbState
            // 
            this.cbState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbState.DropDownWidth = 150;
            this.cbState.FormattingEnabled = true;
            this.cbState.Items.AddRange(new object[] {
            "Novo",
            "Reformado"});
            this.cbState.Location = new System.Drawing.Point(5, 255);
            this.cbState.Name = "cbState";
            this.cbState.Size = new System.Drawing.Size(150, 21);
            this.cbState.TabIndex = 11;
            // 
            // cbVoltageClass
            // 
            this.cbVoltageClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVoltageClass.DropDownWidth = 150;
            this.cbVoltageClass.FormattingEnabled = true;
            this.cbVoltageClass.Items.AddRange(new object[] {
            "0,6",
            "1,2",
            "3,6",
            "7,2",
            "12,0",
            "15,0",
            "17,5",
            "24,0",
            "36,0"});
            this.cbVoltageClass.Location = new System.Drawing.Point(5, 210);
            this.cbVoltageClass.Name = "cbVoltageClass";
            this.cbVoltageClass.Size = new System.Drawing.Size(150, 21);
            this.cbVoltageClass.TabIndex = 9;
            // 
            // cbInsulatingOil
            // 
            this.cbInsulatingOil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInsulatingOil.DropDownWidth = 150;
            this.cbInsulatingOil.FormattingEnabled = true;
            this.cbInsulatingOil.Items.AddRange(new object[] {
            "Mineral",
            "Vegetal"});
            this.cbInsulatingOil.Location = new System.Drawing.Point(5, 165);
            this.cbInsulatingOil.Name = "cbInsulatingOil";
            this.cbInsulatingOil.Size = new System.Drawing.Size(150, 21);
            this.cbInsulatingOil.TabIndex = 7;
            // 
            // cbPolarity
            // 
            this.cbPolarity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPolarity.DropDownWidth = 150;
            this.cbPolarity.FormattingEnabled = true;
            this.cbPolarity.Items.AddRange(new object[] {
            "Neutra",
            "Positiva",
            "Negativa"});
            this.cbPolarity.Location = new System.Drawing.Point(5, 120);
            this.cbPolarity.Name = "cbPolarity";
            this.cbPolarity.Size = new System.Drawing.Size(150, 21);
            this.cbPolarity.TabIndex = 5;
            // 
            // cbFrequency
            // 
            this.cbFrequency.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrequency.DropDownWidth = 150;
            this.cbFrequency.FormattingEnabled = true;
            this.cbFrequency.Items.AddRange(new object[] {
            "50",
            "60"});
            this.cbFrequency.Location = new System.Drawing.Point(5, 75);
            this.cbFrequency.Name = "cbFrequency";
            this.cbFrequency.Size = new System.Drawing.Size(150, 21);
            this.cbFrequency.TabIndex = 3;
            // 
            // cbPhases
            // 
            this.cbPhases.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPhases.DropDownWidth = 150;
            this.cbPhases.FormattingEnabled = true;
            this.cbPhases.Items.AddRange(new object[] {
            "1 - Monofásico",
            "2 - Bifásico",
            "3 - Trifásico"});
            this.cbPhases.Location = new System.Drawing.Point(5, 30);
            this.cbPhases.Name = "cbPhases";
            this.cbPhases.Size = new System.Drawing.Size(150, 21);
            this.cbPhases.TabIndex = 1;
            // 
            // tbResistanceTemperature
            // 
            this.tbResistanceTemperature.Location = new System.Drawing.Point(160, 255);
            this.tbResistanceTemperature.Multiline = true;
            this.tbResistanceTemperature.Name = "tbResistanceTemperature";
            this.tbResistanceTemperature.Size = new System.Drawing.Size(150, 21);
            this.tbResistanceTemperature.TabIndex = 12;
            this.tbResistanceTemperature.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbResistanceTemperature_KeyDown);
            this.tbResistanceTemperature.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbResistanceTemperature_KeyPress);
            // 
            // tbRegulationRange
            // 
            this.tbRegulationRange.Location = new System.Drawing.Point(160, 210);
            this.tbRegulationRange.Multiline = true;
            this.tbRegulationRange.Name = "tbRegulationRange";
            this.tbRegulationRange.Size = new System.Drawing.Size(150, 21);
            this.tbRegulationRange.TabIndex = 10;
            this.tbRegulationRange.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbRegulationRange_KeyDown);
            this.tbRegulationRange.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRegulationRange_KeyPress);
            // 
            // tbType
            // 
            this.tbType.Location = new System.Drawing.Point(160, 165);
            this.tbType.Multiline = true;
            this.tbType.Name = "tbType";
            this.tbType.Size = new System.Drawing.Size(150, 21);
            this.tbType.TabIndex = 8;
            this.tbType.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbType_KeyDown);
            // 
            // tbWeight
            // 
            this.tbWeight.Location = new System.Drawing.Point(160, 120);
            this.tbWeight.Multiline = true;
            this.tbWeight.Name = "tbWeight";
            this.tbWeight.Size = new System.Drawing.Size(150, 21);
            this.tbWeight.TabIndex = 6;
            this.tbWeight.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbWeight_KeyDown);
            this.tbWeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbWeight_KeyPress);
            // 
            // tbTemperatureLimit
            // 
            this.tbTemperatureLimit.Location = new System.Drawing.Point(160, 75);
            this.tbTemperatureLimit.Multiline = true;
            this.tbTemperatureLimit.Name = "tbTemperatureLimit";
            this.tbTemperatureLimit.Size = new System.Drawing.Size(150, 21);
            this.tbTemperatureLimit.TabIndex = 4;
            this.tbTemperatureLimit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbTemperatureLimit_KeyDown);
            this.tbTemperatureLimit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTemperatureLimit_KeyPress);
            // 
            // tbEfficiencyLevel
            // 
            this.tbEfficiencyLevel.Location = new System.Drawing.Point(160, 30);
            this.tbEfficiencyLevel.Multiline = true;
            this.tbEfficiencyLevel.Name = "tbEfficiencyLevel";
            this.tbEfficiencyLevel.Size = new System.Drawing.Size(150, 21);
            this.tbEfficiencyLevel.TabIndex = 2;
            this.tbEfficiencyLevel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbEfficiencyLevel_KeyDown);
            this.tbEfficiencyLevel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbEfficiencyLevel_KeyPress);
            // 
            // lbConnection
            // 
            this.lbConnection.AutoSize = true;
            this.lbConnection.Location = new System.Drawing.Point(157, 284);
            this.lbConnection.Name = "lbConnection";
            this.lbConnection.Size = new System.Drawing.Size(49, 13);
            this.lbConnection.TabIndex = 23;
            this.lbConnection.Text = "Conexão";
            // 
            // lbResistenceTemperature
            // 
            this.lbResistenceTemperature.AutoSize = true;
            this.lbResistenceTemperature.Location = new System.Drawing.Point(157, 239);
            this.lbResistenceTemperature.Name = "lbResistenceTemperature";
            this.lbResistenceTemperature.Size = new System.Drawing.Size(140, 13);
            this.lbResistenceTemperature.TabIndex = 21;
            this.lbResistenceTemperature.Text = "Temperatura de Resistência";
            // 
            // lbRegulationRange
            // 
            this.lbRegulationRange.AutoSize = true;
            this.lbRegulationRange.Location = new System.Drawing.Point(157, 194);
            this.lbRegulationRange.Name = "lbRegulationRange";
            this.lbRegulationRange.Size = new System.Drawing.Size(125, 13);
            this.lbRegulationRange.TabIndex = 27;
            this.lbRegulationRange.Text = "Faixa de Regulação ( % )";
            // 
            // lbType
            // 
            this.lbType.AutoSize = true;
            this.lbType.Location = new System.Drawing.Point(157, 149);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(28, 13);
            this.lbType.TabIndex = 19;
            this.lbType.Text = "Tipo";
            // 
            // lbWeight
            // 
            this.lbWeight.AutoSize = true;
            this.lbWeight.Location = new System.Drawing.Point(157, 104);
            this.lbWeight.Name = "lbWeight";
            this.lbWeight.Size = new System.Drawing.Size(65, 13);
            this.lbWeight.TabIndex = 17;
            this.lbWeight.Text = "Massa ( kg )";
            // 
            // lbTemperatureLimit
            // 
            this.lbTemperatureLimit.AutoSize = true;
            this.lbTemperatureLimit.Location = new System.Drawing.Point(157, 59);
            this.lbTemperatureLimit.Name = "lbTemperatureLimit";
            this.lbTemperatureLimit.Size = new System.Drawing.Size(138, 13);
            this.lbTemperatureLimit.TabIndex = 15;
            this.lbTemperatureLimit.Text = "Limite de Temperatura ( C° )";
            // 
            // lbEfficiencyLevel
            // 
            this.lbEfficiencyLevel.AutoSize = true;
            this.lbEfficiencyLevel.Location = new System.Drawing.Point(157, 14);
            this.lbEfficiencyLevel.Name = "lbEfficiencyLevel";
            this.lbEfficiencyLevel.Size = new System.Drawing.Size(97, 13);
            this.lbEfficiencyLevel.TabIndex = 13;
            this.lbEfficiencyLevel.Text = "Nível de Eficiência";
            // 
            // lbState
            // 
            this.lbState.AutoSize = true;
            this.lbState.Location = new System.Drawing.Point(2, 239);
            this.lbState.Name = "lbState";
            this.lbState.Size = new System.Drawing.Size(40, 13);
            this.lbState.TabIndex = 20;
            this.lbState.Text = "Estado";
            // 
            // lbVoltageClass
            // 
            this.lbVoltageClass.AutoSize = true;
            this.lbVoltageClass.Location = new System.Drawing.Point(2, 194);
            this.lbVoltageClass.Name = "lbVoltageClass";
            this.lbVoltageClass.Size = new System.Drawing.Size(120, 13);
            this.lbVoltageClass.TabIndex = 26;
            this.lbVoltageClass.Text = "Classe de Tensão ( kV )";
            // 
            // lbInsulatingOil
            // 
            this.lbInsulatingOil.AutoSize = true;
            this.lbInsulatingOil.Location = new System.Drawing.Point(2, 149);
            this.lbInsulatingOil.Name = "lbInsulatingOil";
            this.lbInsulatingOil.Size = new System.Drawing.Size(69, 13);
            this.lbInsulatingOil.TabIndex = 18;
            this.lbInsulatingOil.Text = "Óleo Isolante";
            // 
            // lbPolarity
            // 
            this.lbPolarity.AutoSize = true;
            this.lbPolarity.Location = new System.Drawing.Point(2, 104);
            this.lbPolarity.Name = "lbPolarity";
            this.lbPolarity.Size = new System.Drawing.Size(57, 13);
            this.lbPolarity.TabIndex = 16;
            this.lbPolarity.Text = "Polaridade";
            // 
            // lbFrequency
            // 
            this.lbFrequency.AutoSize = true;
            this.lbFrequency.Location = new System.Drawing.Point(2, 59);
            this.lbFrequency.Name = "lbFrequency";
            this.lbFrequency.Size = new System.Drawing.Size(88, 13);
            this.lbFrequency.TabIndex = 14;
            this.lbFrequency.Text = "Frequência ( Hz )";
            // 
            // lbPhases
            // 
            this.lbPhases.AutoSize = true;
            this.lbPhases.Location = new System.Drawing.Point(2, 14);
            this.lbPhases.Name = "lbPhases";
            this.lbPhases.Size = new System.Drawing.Size(35, 13);
            this.lbPhases.TabIndex = 12;
            this.lbPhases.Text = "Fases";
            // 
            // lbWindings
            // 
            this.lbWindings.AutoSize = true;
            this.lbWindings.Location = new System.Drawing.Point(19, 9);
            this.lbWindings.Name = "lbWindings";
            this.lbWindings.Size = new System.Drawing.Size(71, 13);
            this.lbWindings.TabIndex = 23;
            this.lbWindings.Text = "Enrolamentos";
            // 
            // pnWindings
            // 
            this.pnWindings.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnWindings.Controls.Add(this.pnSecondWinding);
            this.pnWindings.Controls.Add(this.pnThirdWinding);
            this.pnWindings.Controls.Add(this.pnFirstWinding);
            this.pnWindings.Controls.Add(this.cbTerciary);
            this.pnWindings.Controls.Add(this.cbSecondary);
            this.pnWindings.Controls.Add(this.cbPrimary);
            this.pnWindings.Controls.Add(this.lbResistence);
            this.pnWindings.Controls.Add(this.lbWindingMaterial);
            this.pnWindings.Controls.Add(this.lbCurrent);
            this.pnWindings.Controls.Add(this.lbVoltage);
            this.pnWindings.Controls.Add(this.lbPower);
            this.pnWindings.Location = new System.Drawing.Point(9, 17);
            this.pnWindings.Name = "pnWindings";
            this.pnWindings.Size = new System.Drawing.Size(470, 180);
            this.pnWindings.TabIndex = 1;
            // 
            // pnSecondWinding
            // 
            this.pnSecondWinding.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnSecondWinding.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnSecondWinding.Controls.Add(this.cbSecondResistance);
            this.pnSecondWinding.Controls.Add(this.cbSecondWindingMaterial);
            this.pnSecondWinding.Controls.Add(this.cbSecondCurrent);
            this.pnSecondWinding.Controls.Add(this.cbSecondVoltage);
            this.pnSecondWinding.Controls.Add(this.cbSecondPower);
            this.pnSecondWinding.Controls.Add(this.tbSecondResistance);
            this.pnSecondWinding.Controls.Add(this.tbSecondCurrent);
            this.pnSecondWinding.Controls.Add(this.tbSecondVoltage);
            this.pnSecondWinding.Controls.Add(this.tbSecondPower);
            this.pnSecondWinding.Location = new System.Drawing.Point(212, 30);
            this.pnSecondWinding.Name = "pnSecondWinding";
            this.pnSecondWinding.Size = new System.Drawing.Size(121, 140);
            this.pnSecondWinding.TabIndex = 4;
            // 
            // cbSecondResistance
            // 
            this.cbSecondResistance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSecondResistance.DropDownWidth = 55;
            this.cbSecondResistance.FormattingEnabled = true;
            this.cbSecondResistance.Items.AddRange(new object[] {
            "kOhm",
            "Ohm",
            "mOhm",
            "uOhm"});
            this.cbSecondResistance.Location = new System.Drawing.Point(60, 111);
            this.cbSecondResistance.Name = "cbSecondResistance";
            this.cbSecondResistance.Size = new System.Drawing.Size(55, 21);
            this.cbSecondResistance.TabIndex = 9;
            // 
            // cbSecondWindingMaterial
            // 
            this.cbSecondWindingMaterial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSecondWindingMaterial.DropDownWidth = 113;
            this.cbSecondWindingMaterial.FormattingEnabled = true;
            this.cbSecondWindingMaterial.Items.AddRange(new object[] {
            "Alumínio",
            "Cobre"});
            this.cbSecondWindingMaterial.Location = new System.Drawing.Point(3, 85);
            this.cbSecondWindingMaterial.Name = "cbSecondWindingMaterial";
            this.cbSecondWindingMaterial.Size = new System.Drawing.Size(113, 21);
            this.cbSecondWindingMaterial.TabIndex = 7;
            // 
            // cbSecondCurrent
            // 
            this.cbSecondCurrent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSecondCurrent.DropDownWidth = 55;
            this.cbSecondCurrent.FormattingEnabled = true;
            this.cbSecondCurrent.Items.AddRange(new object[] {
            "A",
            "kA"});
            this.cbSecondCurrent.Location = new System.Drawing.Point(60, 59);
            this.cbSecondCurrent.Name = "cbSecondCurrent";
            this.cbSecondCurrent.Size = new System.Drawing.Size(55, 21);
            this.cbSecondCurrent.TabIndex = 6;
            // 
            // cbSecondVoltage
            // 
            this.cbSecondVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSecondVoltage.DropDownWidth = 55;
            this.cbSecondVoltage.FormattingEnabled = true;
            this.cbSecondVoltage.Items.AddRange(new object[] {
            "V",
            "kV",
            "MV"});
            this.cbSecondVoltage.Location = new System.Drawing.Point(60, 31);
            this.cbSecondVoltage.Name = "cbSecondVoltage";
            this.cbSecondVoltage.Size = new System.Drawing.Size(55, 21);
            this.cbSecondVoltage.TabIndex = 4;
            // 
            // cbSecondPower
            // 
            this.cbSecondPower.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSecondPower.DropDownWidth = 55;
            this.cbSecondPower.FormattingEnabled = true;
            this.cbSecondPower.Items.AddRange(new object[] {
            "VA",
            "kVA",
            "MVA"});
            this.cbSecondPower.Location = new System.Drawing.Point(60, 4);
            this.cbSecondPower.Name = "cbSecondPower";
            this.cbSecondPower.Size = new System.Drawing.Size(55, 21);
            this.cbSecondPower.TabIndex = 2;
            // 
            // tbSecondResistance
            // 
            this.tbSecondResistance.Location = new System.Drawing.Point(3, 111);
            this.tbSecondResistance.Multiline = true;
            this.tbSecondResistance.Name = "tbSecondResistance";
            this.tbSecondResistance.Size = new System.Drawing.Size(55, 21);
            this.tbSecondResistance.TabIndex = 8;
            this.tbSecondResistance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSecondResistance_KeyDown);
            this.tbSecondResistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSecondResistance_KeyPress);
            // 
            // tbSecondCurrent
            // 
            this.tbSecondCurrent.Location = new System.Drawing.Point(3, 59);
            this.tbSecondCurrent.Multiline = true;
            this.tbSecondCurrent.Name = "tbSecondCurrent";
            this.tbSecondCurrent.Size = new System.Drawing.Size(55, 21);
            this.tbSecondCurrent.TabIndex = 5;
            this.tbSecondCurrent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSecondCurrent_KeyDown);
            this.tbSecondCurrent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSecondCurrent_KeyPress);
            // 
            // tbSecondVoltage
            // 
            this.tbSecondVoltage.Location = new System.Drawing.Point(3, 31);
            this.tbSecondVoltage.Multiline = true;
            this.tbSecondVoltage.Name = "tbSecondVoltage";
            this.tbSecondVoltage.Size = new System.Drawing.Size(55, 21);
            this.tbSecondVoltage.TabIndex = 3;
            this.tbSecondVoltage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSecondVoltage_KeyDown);
            this.tbSecondVoltage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSecondVoltage_KeyPress);
            // 
            // tbSecondPower
            // 
            this.tbSecondPower.Location = new System.Drawing.Point(3, 4);
            this.tbSecondPower.Multiline = true;
            this.tbSecondPower.Name = "tbSecondPower";
            this.tbSecondPower.Size = new System.Drawing.Size(55, 21);
            this.tbSecondPower.TabIndex = 1;
            this.tbSecondPower.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSecondPower_KeyDown);
            this.tbSecondPower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbSecondPower_KeyPress);
            // 
            // pnThirdWinding
            // 
            this.pnThirdWinding.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnThirdWinding.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnThirdWinding.Controls.Add(this.cbThirdResistance);
            this.pnThirdWinding.Controls.Add(this.cbThirdWindingMaterial);
            this.pnThirdWinding.Controls.Add(this.cbThirdCurrent);
            this.pnThirdWinding.Controls.Add(this.cbThirdVoltage);
            this.pnThirdWinding.Controls.Add(this.cbThirdPower);
            this.pnThirdWinding.Controls.Add(this.tbThirdResistance);
            this.pnThirdWinding.Controls.Add(this.tbThirdCurrent);
            this.pnThirdWinding.Controls.Add(this.tbThirdVoltage);
            this.pnThirdWinding.Controls.Add(this.tbThirdPower);
            this.pnThirdWinding.Location = new System.Drawing.Point(339, 30);
            this.pnThirdWinding.Name = "pnThirdWinding";
            this.pnThirdWinding.Size = new System.Drawing.Size(121, 140);
            this.pnThirdWinding.TabIndex = 6;
            // 
            // cbThirdResistance
            // 
            this.cbThirdResistance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbThirdResistance.DropDownWidth = 55;
            this.cbThirdResistance.FormattingEnabled = true;
            this.cbThirdResistance.Items.AddRange(new object[] {
            "kOhm",
            "Ohm",
            "mOhm",
            "uOhm"});
            this.cbThirdResistance.Location = new System.Drawing.Point(60, 111);
            this.cbThirdResistance.Name = "cbThirdResistance";
            this.cbThirdResistance.Size = new System.Drawing.Size(55, 21);
            this.cbThirdResistance.TabIndex = 9;
            // 
            // cbThirdWindingMaterial
            // 
            this.cbThirdWindingMaterial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbThirdWindingMaterial.DropDownWidth = 112;
            this.cbThirdWindingMaterial.FormattingEnabled = true;
            this.cbThirdWindingMaterial.Items.AddRange(new object[] {
            "Alumínio",
            "Cobre"});
            this.cbThirdWindingMaterial.Location = new System.Drawing.Point(3, 85);
            this.cbThirdWindingMaterial.Name = "cbThirdWindingMaterial";
            this.cbThirdWindingMaterial.Size = new System.Drawing.Size(112, 21);
            this.cbThirdWindingMaterial.TabIndex = 7;
            // 
            // cbThirdCurrent
            // 
            this.cbThirdCurrent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbThirdCurrent.DropDownWidth = 55;
            this.cbThirdCurrent.FormattingEnabled = true;
            this.cbThirdCurrent.Items.AddRange(new object[] {
            "A",
            "kA"});
            this.cbThirdCurrent.Location = new System.Drawing.Point(60, 59);
            this.cbThirdCurrent.Name = "cbThirdCurrent";
            this.cbThirdCurrent.Size = new System.Drawing.Size(55, 21);
            this.cbThirdCurrent.TabIndex = 6;
            // 
            // cbThirdVoltage
            // 
            this.cbThirdVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbThirdVoltage.DropDownWidth = 55;
            this.cbThirdVoltage.FormattingEnabled = true;
            this.cbThirdVoltage.Items.AddRange(new object[] {
            "V",
            "kV",
            "MV"});
            this.cbThirdVoltage.Location = new System.Drawing.Point(60, 31);
            this.cbThirdVoltage.Name = "cbThirdVoltage";
            this.cbThirdVoltage.Size = new System.Drawing.Size(55, 21);
            this.cbThirdVoltage.TabIndex = 4;
            // 
            // cbThirdPower
            // 
            this.cbThirdPower.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbThirdPower.DropDownWidth = 55;
            this.cbThirdPower.FormattingEnabled = true;
            this.cbThirdPower.Items.AddRange(new object[] {
            "VA",
            "kVA",
            "MVA"});
            this.cbThirdPower.Location = new System.Drawing.Point(60, 4);
            this.cbThirdPower.Name = "cbThirdPower";
            this.cbThirdPower.Size = new System.Drawing.Size(55, 21);
            this.cbThirdPower.TabIndex = 2;
            // 
            // tbThirdResistance
            // 
            this.tbThirdResistance.Location = new System.Drawing.Point(3, 111);
            this.tbThirdResistance.Multiline = true;
            this.tbThirdResistance.Name = "tbThirdResistance";
            this.tbThirdResistance.Size = new System.Drawing.Size(55, 21);
            this.tbThirdResistance.TabIndex = 8;
            this.tbThirdResistance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbThirdResistance_KeyDown);
            this.tbThirdResistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbThirdResistance_KeyPress);
            // 
            // tbThirdCurrent
            // 
            this.tbThirdCurrent.Location = new System.Drawing.Point(3, 59);
            this.tbThirdCurrent.Multiline = true;
            this.tbThirdCurrent.Name = "tbThirdCurrent";
            this.tbThirdCurrent.Size = new System.Drawing.Size(55, 21);
            this.tbThirdCurrent.TabIndex = 5;
            this.tbThirdCurrent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbThirdCurrent_KeyDown);
            this.tbThirdCurrent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbThirdCurrent_KeyPress);
            // 
            // tbThirdVoltage
            // 
            this.tbThirdVoltage.Location = new System.Drawing.Point(3, 31);
            this.tbThirdVoltage.Multiline = true;
            this.tbThirdVoltage.Name = "tbThirdVoltage";
            this.tbThirdVoltage.Size = new System.Drawing.Size(55, 21);
            this.tbThirdVoltage.TabIndex = 3;
            this.tbThirdVoltage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbThirdVoltage_KeyDown);
            this.tbThirdVoltage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbThirdVoltage_KeyPress);
            // 
            // tbThirdPower
            // 
            this.tbThirdPower.Location = new System.Drawing.Point(3, 4);
            this.tbThirdPower.Multiline = true;
            this.tbThirdPower.Name = "tbThirdPower";
            this.tbThirdPower.Size = new System.Drawing.Size(55, 21);
            this.tbThirdPower.TabIndex = 1;
            this.tbThirdPower.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbThirdPower_KeyDown);
            this.tbThirdPower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbThirdPower_KeyPress);
            // 
            // pnFirstWinding
            // 
            this.pnFirstWinding.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pnFirstWinding.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnFirstWinding.Controls.Add(this.cbFirstResistance);
            this.pnFirstWinding.Controls.Add(this.cbFirstWindingMaterial);
            this.pnFirstWinding.Controls.Add(this.cbFirstCurrent);
            this.pnFirstWinding.Controls.Add(this.cbFirstVoltage);
            this.pnFirstWinding.Controls.Add(this.cbFirstPower);
            this.pnFirstWinding.Controls.Add(this.tbFirstResistance);
            this.pnFirstWinding.Controls.Add(this.tbFirstCurrent);
            this.pnFirstWinding.Controls.Add(this.tbFirstVoltage);
            this.pnFirstWinding.Controls.Add(this.tbFirstPower);
            this.pnFirstWinding.Location = new System.Drawing.Point(84, 30);
            this.pnFirstWinding.Name = "pnFirstWinding";
            this.pnFirstWinding.Size = new System.Drawing.Size(121, 140);
            this.pnFirstWinding.TabIndex = 2;
            // 
            // cbFirstResistance
            // 
            this.cbFirstResistance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFirstResistance.DropDownWidth = 55;
            this.cbFirstResistance.FormattingEnabled = true;
            this.cbFirstResistance.Items.AddRange(new object[] {
            "kOhm",
            "Ohm",
            "mOhm",
            "uOhm"});
            this.cbFirstResistance.Location = new System.Drawing.Point(60, 111);
            this.cbFirstResistance.Name = "cbFirstResistance";
            this.cbFirstResistance.Size = new System.Drawing.Size(55, 21);
            this.cbFirstResistance.TabIndex = 9;
            // 
            // cbFirstWindingMaterial
            // 
            this.cbFirstWindingMaterial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFirstWindingMaterial.DropDownWidth = 112;
            this.cbFirstWindingMaterial.FormattingEnabled = true;
            this.cbFirstWindingMaterial.Items.AddRange(new object[] {
            "Alumínio",
            "Cobre"});
            this.cbFirstWindingMaterial.Location = new System.Drawing.Point(3, 85);
            this.cbFirstWindingMaterial.Name = "cbFirstWindingMaterial";
            this.cbFirstWindingMaterial.Size = new System.Drawing.Size(111, 21);
            this.cbFirstWindingMaterial.TabIndex = 7;
            // 
            // cbFirstCurrent
            // 
            this.cbFirstCurrent.AutoCompleteCustomSource.AddRange(new string[] {
            "A",
            "kA"});
            this.cbFirstCurrent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFirstCurrent.DropDownWidth = 55;
            this.cbFirstCurrent.FormattingEnabled = true;
            this.cbFirstCurrent.Items.AddRange(new object[] {
            "A",
            "kA"});
            this.cbFirstCurrent.Location = new System.Drawing.Point(60, 59);
            this.cbFirstCurrent.Name = "cbFirstCurrent";
            this.cbFirstCurrent.Size = new System.Drawing.Size(55, 21);
            this.cbFirstCurrent.TabIndex = 6;
            // 
            // cbFirstVoltage
            // 
            this.cbFirstVoltage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFirstVoltage.DropDownWidth = 55;
            this.cbFirstVoltage.FormattingEnabled = true;
            this.cbFirstVoltage.Items.AddRange(new object[] {
            "V",
            "kV",
            "MV"});
            this.cbFirstVoltage.Location = new System.Drawing.Point(60, 31);
            this.cbFirstVoltage.Name = "cbFirstVoltage";
            this.cbFirstVoltage.Size = new System.Drawing.Size(55, 21);
            this.cbFirstVoltage.TabIndex = 4;
            // 
            // cbFirstPower
            // 
            this.cbFirstPower.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFirstPower.DropDownWidth = 55;
            this.cbFirstPower.FormattingEnabled = true;
            this.cbFirstPower.Items.AddRange(new object[] {
            "VA",
            "kVA",
            "MVA"});
            this.cbFirstPower.Location = new System.Drawing.Point(60, 4);
            this.cbFirstPower.Name = "cbFirstPower";
            this.cbFirstPower.Size = new System.Drawing.Size(55, 21);
            this.cbFirstPower.TabIndex = 2;
            // 
            // tbFirstResistance
            // 
            this.tbFirstResistance.Location = new System.Drawing.Point(3, 111);
            this.tbFirstResistance.Multiline = true;
            this.tbFirstResistance.Name = "tbFirstResistance";
            this.tbFirstResistance.Size = new System.Drawing.Size(55, 21);
            this.tbFirstResistance.TabIndex = 8;
            this.tbFirstResistance.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbFirstResistance_KeyDown);
            this.tbFirstResistance.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFirstResistance_KeyPress);
            // 
            // tbFirstCurrent
            // 
            this.tbFirstCurrent.Location = new System.Drawing.Point(3, 59);
            this.tbFirstCurrent.Multiline = true;
            this.tbFirstCurrent.Name = "tbFirstCurrent";
            this.tbFirstCurrent.Size = new System.Drawing.Size(55, 21);
            this.tbFirstCurrent.TabIndex = 5;
            this.tbFirstCurrent.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbFirstCurrent_KeyDown);
            this.tbFirstCurrent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFirstCurrent_KeyPress);
            // 
            // tbFirstVoltage
            // 
            this.tbFirstVoltage.Location = new System.Drawing.Point(3, 31);
            this.tbFirstVoltage.Multiline = true;
            this.tbFirstVoltage.Name = "tbFirstVoltage";
            this.tbFirstVoltage.Size = new System.Drawing.Size(55, 21);
            this.tbFirstVoltage.TabIndex = 3;
            this.tbFirstVoltage.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbFirstVoltage_KeyDown);
            this.tbFirstVoltage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFirstVoltage_KeyPress);
            // 
            // tbFirstPower
            // 
            this.tbFirstPower.Location = new System.Drawing.Point(3, 4);
            this.tbFirstPower.Multiline = true;
            this.tbFirstPower.Name = "tbFirstPower";
            this.tbFirstPower.Size = new System.Drawing.Size(55, 21);
            this.tbFirstPower.TabIndex = 1;
            this.tbFirstPower.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbFirstPower_KeyDown);
            this.tbFirstPower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbFirstPower_KeyPress);
            // 
            // cbTerciary
            // 
            this.cbTerciary.AutoSize = true;
            this.cbTerciary.Location = new System.Drawing.Point(365, 7);
            this.cbTerciary.Name = "cbTerciary";
            this.cbTerciary.Size = new System.Drawing.Size(67, 17);
            this.cbTerciary.TabIndex = 5;
            this.cbTerciary.Text = "Terciário";
            this.cbTerciary.UseVisualStyleBackColor = true;
            this.cbTerciary.CheckedChanged += new System.EventHandler(this.cbTerciary_CheckedChanged);
            // 
            // cbSecondary
            // 
            this.cbSecondary.AutoSize = true;
            this.cbSecondary.Checked = true;
            this.cbSecondary.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSecondary.Location = new System.Drawing.Point(233, 7);
            this.cbSecondary.Name = "cbSecondary";
            this.cbSecondary.Size = new System.Drawing.Size(80, 17);
            this.cbSecondary.TabIndex = 3;
            this.cbSecondary.Text = "Secundário";
            this.cbSecondary.UseVisualStyleBackColor = true;
            this.cbSecondary.CheckedChanged += new System.EventHandler(this.cbSecondary_CheckedChanged);
            // 
            // cbPrimary
            // 
            this.cbPrimary.AutoSize = true;
            this.cbPrimary.Checked = true;
            this.cbPrimary.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbPrimary.Enabled = false;
            this.cbPrimary.Location = new System.Drawing.Point(115, 7);
            this.cbPrimary.Name = "cbPrimary";
            this.cbPrimary.Size = new System.Drawing.Size(63, 17);
            this.cbPrimary.TabIndex = 1;
            this.cbPrimary.Text = "Primário";
            this.cbPrimary.UseVisualStyleBackColor = true;
            // 
            // lbResistence
            // 
            this.lbResistence.AutoSize = true;
            this.lbResistence.Location = new System.Drawing.Point(6, 145);
            this.lbResistence.Name = "lbResistence";
            this.lbResistence.Size = new System.Drawing.Size(62, 13);
            this.lbResistence.TabIndex = 9;
            this.lbResistence.Text = "Resistência";
            // 
            // lbWindingMaterial
            // 
            this.lbWindingMaterial.AutoSize = true;
            this.lbWindingMaterial.Location = new System.Drawing.Point(6, 114);
            this.lbWindingMaterial.Name = "lbWindingMaterial";
            this.lbWindingMaterial.Size = new System.Drawing.Size(66, 26);
            this.lbWindingMaterial.TabIndex = 8;
            this.lbWindingMaterial.Text = "Material de \r\nEnrolamento";
            // 
            // lbCurrent
            // 
            this.lbCurrent.AutoSize = true;
            this.lbCurrent.Location = new System.Drawing.Point(6, 93);
            this.lbCurrent.Name = "lbCurrent";
            this.lbCurrent.Size = new System.Drawing.Size(47, 13);
            this.lbCurrent.TabIndex = 7;
            this.lbCurrent.Text = "Corrente";
            // 
            // lbVoltage
            // 
            this.lbVoltage.AutoSize = true;
            this.lbVoltage.Location = new System.Drawing.Point(6, 64);
            this.lbVoltage.Name = "lbVoltage";
            this.lbVoltage.Size = new System.Drawing.Size(43, 13);
            this.lbVoltage.TabIndex = 6;
            this.lbVoltage.Text = "Tensão";
            // 
            // lbPower
            // 
            this.lbPower.AutoSize = true;
            this.lbPower.Location = new System.Drawing.Point(6, 38);
            this.lbPower.Name = "lbPower";
            this.lbPower.Size = new System.Drawing.Size(49, 13);
            this.lbPower.TabIndex = 5;
            this.lbPower.Text = "Potência";
            // 
            // tabSearch
            // 
            this.tabSearch.Controls.Add(this.btUpdate);
            this.tabSearch.Controls.Add(this.btRemove);
            this.tabSearch.Controls.Add(this.dgTestBodies);
            this.tabSearch.Controls.Add(this.btSearch);
            this.tabSearch.Controls.Add(this.cbOrder);
            this.tabSearch.Controls.Add(this.tbSearchDescription);
            this.tabSearch.Controls.Add(this.tbSearchSerialNumber);
            this.tabSearch.Controls.Add(this.lbOrder);
            this.tabSearch.Controls.Add(this.lbSearchDescription);
            this.tabSearch.Controls.Add(this.lbSearchSerialNumber);
            this.tabSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabSearch.Size = new System.Drawing.Size(840, 551);
            this.tabSearch.TabIndex = 1;
            this.tabSearch.Text = "Pesquisar";
            this.tabSearch.UseVisualStyleBackColor = true;
            // 
            // btUpdate
            // 
            this.btUpdate.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btUpdate.FlatAppearance.BorderSize = 0;
            this.btUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUpdate.ForeColor = System.Drawing.Color.White;
            this.btUpdate.Image = global::HVEX.Properties.Resources.white_update;
            this.btUpdate.Location = new System.Drawing.Point(752, 517);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(75, 23);
            this.btUpdate.TabIndex = 12;
            this.btUpdate.Text = "Editar";
            this.btUpdate.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btUpdate.UseVisualStyleBackColor = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btRemove
            // 
            this.btRemove.BackColor = System.Drawing.Color.DarkRed;
            this.btRemove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btRemove.FlatAppearance.BorderSize = 0;
            this.btRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btRemove.ForeColor = System.Drawing.Color.White;
            this.btRemove.Image = ((System.Drawing.Image)(resources.GetObject("btRemove.Image")));
            this.btRemove.Location = new System.Drawing.Point(671, 517);
            this.btRemove.Name = "btRemove";
            this.btRemove.Size = new System.Drawing.Size(75, 23);
            this.btRemove.TabIndex = 11;
            this.btRemove.Text = "Excluir";
            this.btRemove.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btRemove.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btRemove.UseVisualStyleBackColor = false;
            this.btRemove.Click += new System.EventHandler(this.btRemove_Click);
            // 
            // dgTestBodies
            // 
            this.dgTestBodies.AllowUserToAddRows = false;
            this.dgTestBodies.AllowUserToDeleteRows = false;
            this.dgTestBodies.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgTestBodies.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgTestBodies.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTestBodies.Location = new System.Drawing.Point(13, 53);
            this.dgTestBodies.Name = "dgTestBodies";
            this.dgTestBodies.ReadOnly = true;
            this.dgTestBodies.Size = new System.Drawing.Size(814, 458);
            this.dgTestBodies.TabIndex = 10;
            this.dgTestBodies.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgTestBodies_MouseDoubleClick);
            // 
            // btSearch
            // 
            this.btSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btSearch.FlatAppearance.BorderSize = 0;
            this.btSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSearch.ForeColor = System.Drawing.Color.White;
            this.btSearch.Image = global::HVEX.Properties.Resources.white_search;
            this.btSearch.Location = new System.Drawing.Point(747, 26);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(80, 21);
            this.btSearch.TabIndex = 4;
            this.btSearch.Text = "Pesquisar";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.UseVisualStyleBackColor = false;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // cbOrder
            // 
            this.cbOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrder.DropDownWidth = 139;
            this.cbOrder.FormattingEnabled = true;
            this.cbOrder.Items.AddRange(new object[] {
            "Número de Série",
            "Descrição"});
            this.cbOrder.Location = new System.Drawing.Point(602, 27);
            this.cbOrder.Name = "cbOrder";
            this.cbOrder.Size = new System.Drawing.Size(139, 21);
            this.cbOrder.TabIndex = 3;
            // 
            // tbSearchDescription
            // 
            this.tbSearchDescription.Location = new System.Drawing.Point(146, 26);
            this.tbSearchDescription.Multiline = true;
            this.tbSearchDescription.Name = "tbSearchDescription";
            this.tbSearchDescription.Size = new System.Drawing.Size(450, 21);
            this.tbSearchDescription.TabIndex = 2;
            this.tbSearchDescription.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSearchDescription_KeyDown);
            // 
            // tbSearchSerialNumber
            // 
            this.tbSearchSerialNumber.Location = new System.Drawing.Point(13, 26);
            this.tbSearchSerialNumber.Multiline = true;
            this.tbSearchSerialNumber.Name = "tbSearchSerialNumber";
            this.tbSearchSerialNumber.Size = new System.Drawing.Size(127, 21);
            this.tbSearchSerialNumber.TabIndex = 1;
            this.tbSearchSerialNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSearchSerialNumber_KeyDown);
            // 
            // lbOrder
            // 
            this.lbOrder.AutoSize = true;
            this.lbOrder.Location = new System.Drawing.Point(599, 10);
            this.lbOrder.Name = "lbOrder";
            this.lbOrder.Size = new System.Drawing.Size(38, 13);
            this.lbOrder.TabIndex = 9;
            this.lbOrder.Text = "Ordem";
            // 
            // lbSearchDescription
            // 
            this.lbSearchDescription.AutoSize = true;
            this.lbSearchDescription.Location = new System.Drawing.Point(143, 10);
            this.lbSearchDescription.Name = "lbSearchDescription";
            this.lbSearchDescription.Size = new System.Drawing.Size(55, 13);
            this.lbSearchDescription.TabIndex = 8;
            this.lbSearchDescription.Text = "Descrição";
            // 
            // lbSearchSerialNumber
            // 
            this.lbSearchSerialNumber.AutoSize = true;
            this.lbSearchSerialNumber.Location = new System.Drawing.Point(10, 10);
            this.lbSearchSerialNumber.Name = "lbSearchSerialNumber";
            this.lbSearchSerialNumber.Size = new System.Drawing.Size(86, 13);
            this.lbSearchSerialNumber.TabIndex = 0;
            this.lbSearchSerialNumber.Text = "Número de Série";
            // 
            // FrmEquipment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(871, 601);
            this.Controls.Add(this.tabTestBodies);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmEquipment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Corpo de Prova";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTestBody_FormClosed);
            this.Load += new System.EventHandler(this.FrmTestBody_Load);
            this.tabTestBodies.ResumeLayout(false);
            this.tabRegister.ResumeLayout(false);
            this.tabRegister.PerformLayout();
            this.pnGeneral.ResumeLayout(false);
            this.pnGeneral.PerformLayout();
            this.tabTestBodyType.ResumeLayout(false);
            this.tabGeneric.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            this.tabTransformer.ResumeLayout(false);
            this.tabTransformer.PerformLayout();
            this.pnTaps.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTaps)).EndInit();
            this.pnSpecifications.ResumeLayout(false);
            this.pnSpecifications.PerformLayout();
            this.pnWindings.ResumeLayout(false);
            this.pnWindings.PerformLayout();
            this.pnSecondWinding.ResumeLayout(false);
            this.pnSecondWinding.PerformLayout();
            this.pnThirdWinding.ResumeLayout(false);
            this.pnThirdWinding.PerformLayout();
            this.pnFirstWinding.ResumeLayout(false);
            this.pnFirstWinding.PerformLayout();
            this.tabSearch.ResumeLayout(false);
            this.tabSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTestBodies)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabTestBodies;
        private System.Windows.Forms.TabPage tabRegister;
        private System.Windows.Forms.Button btClean;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.RadioButton rbTransformer;
        private System.Windows.Forms.RadioButton rbGeneric;
        private System.Windows.Forms.Label lbGeneral;
        private System.Windows.Forms.Panel pnGeneral;
        private System.Windows.Forms.MaskedTextBox tbManufactureDate;
        private System.Windows.Forms.ComboBox cbManufacturer;
        private System.Windows.Forms.ComboBox cbCustomer;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.TextBox tbInternalNumber;
        private System.Windows.Forms.TextBox tbSerialNumber;
        private System.Windows.Forms.Label lbManufactureDate;
        private System.Windows.Forms.Label lbModel;
        private System.Windows.Forms.Label lbDescription;
        private System.Windows.Forms.Label lbInternalNumber;
        private System.Windows.Forms.Label lbManufacturer;
        private System.Windows.Forms.Label lbCustomer;
        private System.Windows.Forms.Label lbSerialNumber;
        private System.Windows.Forms.TabControl tabTestBodyType;
        private System.Windows.Forms.TabPage tabGeneric;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.TabPage tabTransformer;
        private System.Windows.Forms.Label lbTaps;
        private System.Windows.Forms.Panel pnTaps;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.DataGridView dgTaps;
        private System.Windows.Forms.DataGridViewTextBoxColumn TapVoltage;
        private System.Windows.Forms.Label lbSpecifications;
        private System.Windows.Forms.Panel pnSpecifications;
        private System.Windows.Forms.Button btConnection;
        private System.Windows.Forms.TextBox tbConnection;
        private System.Windows.Forms.ComboBox cbState;
        private System.Windows.Forms.ComboBox cbVoltageClass;
        private System.Windows.Forms.ComboBox cbInsulatingOil;
        private System.Windows.Forms.ComboBox cbPolarity;
        private System.Windows.Forms.ComboBox cbFrequency;
        private System.Windows.Forms.ComboBox cbPhases;
        private System.Windows.Forms.TextBox tbResistanceTemperature;
        private System.Windows.Forms.TextBox tbRegulationRange;
        private System.Windows.Forms.TextBox tbType;
        private System.Windows.Forms.TextBox tbWeight;
        private System.Windows.Forms.TextBox tbTemperatureLimit;
        private System.Windows.Forms.TextBox tbEfficiencyLevel;
        private System.Windows.Forms.Label lbConnection;
        private System.Windows.Forms.Label lbResistenceTemperature;
        private System.Windows.Forms.Label lbRegulationRange;
        private System.Windows.Forms.Label lbType;
        private System.Windows.Forms.Label lbWeight;
        private System.Windows.Forms.Label lbTemperatureLimit;
        private System.Windows.Forms.Label lbEfficiencyLevel;
        private System.Windows.Forms.Label lbState;
        private System.Windows.Forms.Label lbVoltageClass;
        private System.Windows.Forms.Label lbInsulatingOil;
        private System.Windows.Forms.Label lbPolarity;
        private System.Windows.Forms.Label lbFrequency;
        private System.Windows.Forms.Label lbPhases;
        private System.Windows.Forms.Label lbWindings;
        private System.Windows.Forms.Panel pnWindings;
        private System.Windows.Forms.Panel pnSecondWinding;
        private System.Windows.Forms.ComboBox cbSecondResistance;
        private System.Windows.Forms.ComboBox cbSecondWindingMaterial;
        private System.Windows.Forms.ComboBox cbSecondCurrent;
        private System.Windows.Forms.ComboBox cbSecondVoltage;
        private System.Windows.Forms.ComboBox cbSecondPower;
        private System.Windows.Forms.TextBox tbSecondResistance;
        private System.Windows.Forms.TextBox tbSecondCurrent;
        private System.Windows.Forms.TextBox tbSecondVoltage;
        private System.Windows.Forms.TextBox tbSecondPower;
        private System.Windows.Forms.Panel pnThirdWinding;
        private System.Windows.Forms.ComboBox cbThirdResistance;
        private System.Windows.Forms.ComboBox cbThirdWindingMaterial;
        private System.Windows.Forms.ComboBox cbThirdCurrent;
        private System.Windows.Forms.ComboBox cbThirdVoltage;
        private System.Windows.Forms.ComboBox cbThirdPower;
        private System.Windows.Forms.TextBox tbThirdResistance;
        private System.Windows.Forms.TextBox tbThirdCurrent;
        private System.Windows.Forms.TextBox tbThirdVoltage;
        private System.Windows.Forms.TextBox tbThirdPower;
        private System.Windows.Forms.Panel pnFirstWinding;
        private System.Windows.Forms.ComboBox cbFirstResistance;
        private System.Windows.Forms.ComboBox cbFirstWindingMaterial;
        private System.Windows.Forms.ComboBox cbFirstCurrent;
        private System.Windows.Forms.ComboBox cbFirstVoltage;
        private System.Windows.Forms.ComboBox cbFirstPower;
        private System.Windows.Forms.TextBox tbFirstResistance;
        private System.Windows.Forms.TextBox tbFirstCurrent;
        private System.Windows.Forms.TextBox tbFirstVoltage;
        private System.Windows.Forms.TextBox tbFirstPower;
        private System.Windows.Forms.CheckBox cbTerciary;
        private System.Windows.Forms.CheckBox cbSecondary;
        private System.Windows.Forms.CheckBox cbPrimary;
        private System.Windows.Forms.Label lbResistence;
        private System.Windows.Forms.Label lbWindingMaterial;
        private System.Windows.Forms.Label lbCurrent;
        private System.Windows.Forms.Label lbVoltage;
        private System.Windows.Forms.Label lbPower;
        private System.Windows.Forms.TabPage tabSearch;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.Button btRemove;
        private System.Windows.Forms.DataGridView dgTestBodies;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.ComboBox cbOrder;
        private System.Windows.Forms.TextBox tbSearchDescription;
        private System.Windows.Forms.TextBox tbSearchSerialNumber;
        private System.Windows.Forms.Label lbOrder;
        private System.Windows.Forms.Label lbSearchDescription;
        private System.Windows.Forms.Label lbSearchSerialNumber;
    }
}