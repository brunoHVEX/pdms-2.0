﻿
namespace HVEX.View {
    partial class FrmTestPdmsThreePhase {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTestPdmsThreePhase));
            this.btGetEquipment = new System.Windows.Forms.Button();
            this.tbTransformer = new System.Windows.Forms.TextBox();
            this.lbTransformer = new System.Windows.Forms.Label();
            this.pnTest = new System.Windows.Forms.Panel();
            this.pnGraphics = new System.Windows.Forms.Panel();
            this.tbGraphs = new System.Windows.Forms.TabControl();
            this.tpSignals = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.gcSignalPhase1 = new ZedGraph.ZedGraphControl();
            this.gcSignalFiltredPhase1 = new ZedGraph.ZedGraphControl();
            this.gcSignalPhase2 = new ZedGraph.ZedGraphControl();
            this.gcSignalFiltredPhase2 = new ZedGraph.ZedGraphControl();
            this.gcSignalPhase3 = new ZedGraph.ZedGraphControl();
            this.gcSignalFiltredPhase3 = new ZedGraph.ZedGraphControl();
            this.tpDispersion = new System.Windows.Forms.TabPage();
            this.tlpDispersions = new System.Windows.Forms.TableLayoutPanel();
            this.gcDispersionPhase1 = new ZedGraph.ZedGraphControl();
            this.gcDispersionPhase2 = new ZedGraph.ZedGraphControl();
            this.gcDispersionPhase3 = new ZedGraph.ZedGraphControl();
            this.tpDispersionDetailed = new System.Windows.Forms.TabPage();
            this.tlpDispersionDetailed = new System.Windows.Forms.TableLayoutPanel();
            this.gcDispersionDetailedPhase1 = new ZedGraph.ZedGraphControl();
            this.gcDispersionDetailedPhase2 = new ZedGraph.ZedGraphControl();
            this.gcDispersionDetailedPhase3 = new ZedGraph.ZedGraphControl();
            this.tpPartialDischargeTrend = new System.Windows.Forms.TabPage();
            this.tlpPartialDischargeTrend = new System.Windows.Forms.TableLayoutPanel();
            this.gcPartialDischargeTrendPhase1 = new ZedGraph.ZedGraphControl();
            this.gcPartialDischargeTrendPhase2 = new ZedGraph.ZedGraphControl();
            this.gcPartialDischargeTrendPhase3 = new ZedGraph.ZedGraphControl();
            this.tpFft = new System.Windows.Forms.TabPage();
            this.tlpFft = new System.Windows.Forms.TableLayoutPanel();
            this.gcFftPhase1 = new ZedGraph.ZedGraphControl();
            this.gcFftPhase2 = new ZedGraph.ZedGraphControl();
            this.gcFftPhase3 = new ZedGraph.ZedGraphControl();
            this.gbTest = new System.Windows.Forms.GroupBox();
            this.nudTestTime = new System.Windows.Forms.NumericUpDown();
            this.btStopTest = new System.Windows.Forms.Button();
            this.btStartTest = new System.Windows.Forms.Button();
            this.lbTimer = new System.Windows.Forms.Label();
            this.gbTotalDischarges = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lbTotalOccurrences = new System.Windows.Forms.Label();
            this.lbTotalPositiveDischarges = new System.Windows.Forms.Label();
            this.tbTotalPositiveDischarges = new System.Windows.Forms.TextBox();
            this.lbTotalNegativeDischarges = new System.Windows.Forms.Label();
            this.tbTotalNegativeDischarges = new System.Windows.Forms.TextBox();
            this.tbTotalOccurrences = new System.Windows.Forms.TextBox();
            this.gbPartialDischargePhase3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tbOccurrencesPhase3 = new System.Windows.Forms.TextBox();
            this.lbOccurrencesPhase3 = new System.Windows.Forms.Label();
            this.lbChargePhase3 = new System.Windows.Forms.Label();
            this.tbChargePhase3 = new System.Windows.Forms.TextBox();
            this.gbPartialDischargePhase2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbOccurrencesPhase2 = new System.Windows.Forms.TextBox();
            this.lbOccurrencesPhase2 = new System.Windows.Forms.Label();
            this.lbChargePhase2 = new System.Windows.Forms.Label();
            this.tbChargePhase2 = new System.Windows.Forms.TextBox();
            this.gbPartialDischargePhase1 = new System.Windows.Forms.GroupBox();
            this.tpPartialDischargePhase1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbOccurrencesPhase1 = new System.Windows.Forms.TextBox();
            this.lbOccurrencesPhase1 = new System.Windows.Forms.Label();
            this.lbChargePhase1 = new System.Windows.Forms.Label();
            this.tbChargePhase1 = new System.Windows.Forms.TextBox();
            this.gbTestController = new System.Windows.Forms.GroupBox();
            this.btResetConfigurations = new System.Windows.Forms.Button();
            this.btSaveCalibration = new System.Windows.Forms.Button();
            this.btCalibration = new System.Windows.Forms.Button();
            this.gbCalibration = new System.Windows.Forms.GroupBox();
            this.tpCalibration = new System.Windows.Forms.TableLayoutPanel();
            this.lbCalibrationPhase2 = new System.Windows.Forms.Label();
            this.tbCalibrationPhase3 = new System.Windows.Forms.NumericUpDown();
            this.tbCalibrationPhase2 = new System.Windows.Forms.NumericUpDown();
            this.tbCalibrationPhase1 = new System.Windows.Forms.NumericUpDown();
            this.lbCalibrationPhase1 = new System.Windows.Forms.Label();
            this.lbDp = new System.Windows.Forms.Label();
            this.lbPhase = new System.Windows.Forms.Label();
            this.lbCalibrationPhase3 = new System.Windows.Forms.Label();
            this.tableCute = new System.Windows.Forms.TableLayoutPanel();
            this.cbFrequencyTopCut = new System.Windows.Forms.ComboBox();
            this.tbFrequencyTopCut = new System.Windows.Forms.NumericUpDown();
            this.lbTopCut = new System.Windows.Forms.Label();
            this.cbFrequencyUndercut = new System.Windows.Forms.ComboBox();
            this.lbUndercut = new System.Windows.Forms.Label();
            this.tbFrequencyUndercut = new System.Windows.Forms.NumericUpDown();
            this.tableFilter = new System.Windows.Forms.TableLayoutPanel();
            this.cbFilter = new System.Windows.Forms.ComboBox();
            this.lbFilter = new System.Windows.Forms.Label();
            this.lbOrder = new System.Windows.Forms.Label();
            this.tbOrder = new System.Windows.Forms.NumericUpDown();
            this.btStop = new System.Windows.Forms.Button();
            this.cbOsciloscope = new System.Windows.Forms.ComboBox();
            this.lbParamters = new System.Windows.Forms.Label();
            this.tableController = new System.Windows.Forms.TableLayoutPanel();
            this.lbPhase3 = new System.Windows.Forms.Label();
            this.lbPhase2 = new System.Windows.Forms.Label();
            this.lbPhase1 = new System.Windows.Forms.Label();
            this.lbGain = new System.Windows.Forms.Label();
            this.lbGate = new System.Windows.Forms.Label();
            this.tbGatePhase3 = new System.Windows.Forms.NumericUpDown();
            this.tbGainPhase1 = new System.Windows.Forms.NumericUpDown();
            this.tbGatePhase2 = new System.Windows.Forms.NumericUpDown();
            this.tbGainPhase2 = new System.Windows.Forms.NumericUpDown();
            this.tbGainPhase3 = new System.Windows.Forms.NumericUpDown();
            this.tbGatePhase1 = new System.Windows.Forms.NumericUpDown();
            this.btStart = new System.Windows.Forms.Button();
            this.lbOsciloscope = new System.Windows.Forms.Label();
            this.tableOscilloscope = new System.Windows.Forms.TableLayoutPanel();
            this.label29 = new System.Windows.Forms.Label();
            this.btConnection = new System.Windows.Forms.Button();
            this.lbConnection = new System.Windows.Forms.Label();
            this.lbOsciloscopePort = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.tbIp = new System.Windows.Forms.MaskedTextBox();
            this.lbOsciloscopeIp = new System.Windows.Forms.Label();
            this.lbBrand = new System.Windows.Forms.Label();
            this.cbChannels = new System.Windows.Forms.ComboBox();
            this.timerForm = new System.Windows.Forms.Timer(this.components);
            this.pnTest.SuspendLayout();
            this.pnGraphics.SuspendLayout();
            this.tbGraphs.SuspendLayout();
            this.tpSignals.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tpDispersion.SuspendLayout();
            this.tlpDispersions.SuspendLayout();
            this.tpDispersionDetailed.SuspendLayout();
            this.tlpDispersionDetailed.SuspendLayout();
            this.tpPartialDischargeTrend.SuspendLayout();
            this.tlpPartialDischargeTrend.SuspendLayout();
            this.tpFft.SuspendLayout();
            this.tlpFft.SuspendLayout();
            this.gbTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTestTime)).BeginInit();
            this.gbTotalDischarges.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.gbPartialDischargePhase3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.gbPartialDischargePhase2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.gbPartialDischargePhase1.SuspendLayout();
            this.tpPartialDischargePhase1.SuspendLayout();
            this.gbTestController.SuspendLayout();
            this.gbCalibration.SuspendLayout();
            this.tpCalibration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalibrationPhase3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalibrationPhase2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalibrationPhase1)).BeginInit();
            this.tableCute.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrequencyTopCut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrequencyUndercut)).BeginInit();
            this.tableFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOrder)).BeginInit();
            this.tableController.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbGatePhase3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGainPhase1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGatePhase2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGainPhase2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGainPhase3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGatePhase1)).BeginInit();
            this.tableOscilloscope.SuspendLayout();
            this.SuspendLayout();
            // 
            // btGetEquipment
            // 
            this.btGetEquipment.Location = new System.Drawing.Point(527, 11);
            this.btGetEquipment.Name = "btGetEquipment";
            this.btGetEquipment.Size = new System.Drawing.Size(30, 25);
            this.btGetEquipment.TabIndex = 1;
            this.btGetEquipment.Text = "...";
            this.btGetEquipment.UseVisualStyleBackColor = true;
            this.btGetEquipment.Click += new System.EventHandler(this.btGetEquipment_Click);
            // 
            // tbTransformer
            // 
            this.tbTransformer.BackColor = System.Drawing.SystemColors.Window;
            this.tbTransformer.Enabled = false;
            this.tbTransformer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTransformer.Location = new System.Drawing.Point(177, 12);
            this.tbTransformer.Name = "tbTransformer";
            this.tbTransformer.ReadOnly = true;
            this.tbTransformer.Size = new System.Drawing.Size(350, 23);
            this.tbTransformer.TabIndex = 17;
            // 
            // lbTransformer
            // 
            this.lbTransformer.AutoSize = true;
            this.lbTransformer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTransformer.ForeColor = System.Drawing.Color.White;
            this.lbTransformer.Location = new System.Drawing.Point(29, 12);
            this.lbTransformer.Name = "lbTransformer";
            this.lbTransformer.Size = new System.Drawing.Size(142, 20);
            this.lbTransformer.TabIndex = 16;
            this.lbTransformer.Text = "EQUIPAMENTO:";
            // 
            // pnTest
            // 
            this.pnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnTest.BackColor = System.Drawing.Color.White;
            this.pnTest.Controls.Add(this.pnGraphics);
            this.pnTest.Controls.Add(this.gbTest);
            this.pnTest.Controls.Add(this.gbTestController);
            this.pnTest.Location = new System.Drawing.Point(14, 40);
            this.pnTest.Name = "pnTest";
            this.pnTest.Size = new System.Drawing.Size(1257, 659);
            this.pnTest.TabIndex = 15;
            // 
            // pnGraphics
            // 
            this.pnGraphics.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnGraphics.Controls.Add(this.tbGraphs);
            this.pnGraphics.Location = new System.Drawing.Point(236, 10);
            this.pnGraphics.Name = "pnGraphics";
            this.pnGraphics.Size = new System.Drawing.Size(784, 640);
            this.pnGraphics.TabIndex = 18;
            // 
            // tbGraphs
            // 
            this.tbGraphs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGraphs.Controls.Add(this.tpSignals);
            this.tbGraphs.Controls.Add(this.tpDispersion);
            this.tbGraphs.Controls.Add(this.tpDispersionDetailed);
            this.tbGraphs.Controls.Add(this.tpPartialDischargeTrend);
            this.tbGraphs.Controls.Add(this.tpFft);
            this.tbGraphs.Location = new System.Drawing.Point(3, 6);
            this.tbGraphs.Name = "tbGraphs";
            this.tbGraphs.SelectedIndex = 0;
            this.tbGraphs.Size = new System.Drawing.Size(779, 634);
            this.tbGraphs.TabIndex = 0;
            // 
            // tpSignals
            // 
            this.tpSignals.Controls.Add(this.tableLayoutPanel3);
            this.tpSignals.Location = new System.Drawing.Point(4, 22);
            this.tpSignals.Name = "tpSignals";
            this.tpSignals.Padding = new System.Windows.Forms.Padding(3);
            this.tpSignals.Size = new System.Drawing.Size(771, 608);
            this.tpSignals.TabIndex = 0;
            this.tpSignals.Text = "Sinal";
            this.tpSignals.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.gcSignalPhase1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.gcSignalFiltredPhase1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.gcSignalPhase2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.gcSignalFiltredPhase2, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.gcSignalPhase3, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.gcSignalFiltredPhase3, 1, 2);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(765, 602);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // gcSignalPhase1
            // 
            this.gcSignalPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSignalPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcSignalPhase1.Name = "gcSignalPhase1";
            this.gcSignalPhase1.ScrollGrace = 0D;
            this.gcSignalPhase1.ScrollMaxX = 0D;
            this.gcSignalPhase1.ScrollMaxY = 0D;
            this.gcSignalPhase1.ScrollMaxY2 = 0D;
            this.gcSignalPhase1.ScrollMinX = 0D;
            this.gcSignalPhase1.ScrollMinY = 0D;
            this.gcSignalPhase1.ScrollMinY2 = 0D;
            this.gcSignalPhase1.Size = new System.Drawing.Size(376, 194);
            this.gcSignalPhase1.TabIndex = 0;
            this.gcSignalPhase1.UseExtendedPrintDialog = true;
            // 
            // gcSignalFiltredPhase1
            // 
            this.gcSignalFiltredPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSignalFiltredPhase1.Location = new System.Drawing.Point(385, 3);
            this.gcSignalFiltredPhase1.Name = "gcSignalFiltredPhase1";
            this.gcSignalFiltredPhase1.ScrollGrace = 0D;
            this.gcSignalFiltredPhase1.ScrollMaxX = 0D;
            this.gcSignalFiltredPhase1.ScrollMaxY = 0D;
            this.gcSignalFiltredPhase1.ScrollMaxY2 = 0D;
            this.gcSignalFiltredPhase1.ScrollMinX = 0D;
            this.gcSignalFiltredPhase1.ScrollMinY = 0D;
            this.gcSignalFiltredPhase1.ScrollMinY2 = 0D;
            this.gcSignalFiltredPhase1.Size = new System.Drawing.Size(377, 194);
            this.gcSignalFiltredPhase1.TabIndex = 1;
            this.gcSignalFiltredPhase1.UseExtendedPrintDialog = true;
            // 
            // gcSignalPhase2
            // 
            this.gcSignalPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSignalPhase2.Location = new System.Drawing.Point(3, 203);
            this.gcSignalPhase2.Name = "gcSignalPhase2";
            this.gcSignalPhase2.ScrollGrace = 0D;
            this.gcSignalPhase2.ScrollMaxX = 0D;
            this.gcSignalPhase2.ScrollMaxY = 0D;
            this.gcSignalPhase2.ScrollMaxY2 = 0D;
            this.gcSignalPhase2.ScrollMinX = 0D;
            this.gcSignalPhase2.ScrollMinY = 0D;
            this.gcSignalPhase2.ScrollMinY2 = 0D;
            this.gcSignalPhase2.Size = new System.Drawing.Size(376, 194);
            this.gcSignalPhase2.TabIndex = 2;
            this.gcSignalPhase2.UseExtendedPrintDialog = true;
            // 
            // gcSignalFiltredPhase2
            // 
            this.gcSignalFiltredPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSignalFiltredPhase2.Location = new System.Drawing.Point(385, 203);
            this.gcSignalFiltredPhase2.Name = "gcSignalFiltredPhase2";
            this.gcSignalFiltredPhase2.ScrollGrace = 0D;
            this.gcSignalFiltredPhase2.ScrollMaxX = 0D;
            this.gcSignalFiltredPhase2.ScrollMaxY = 0D;
            this.gcSignalFiltredPhase2.ScrollMaxY2 = 0D;
            this.gcSignalFiltredPhase2.ScrollMinX = 0D;
            this.gcSignalFiltredPhase2.ScrollMinY = 0D;
            this.gcSignalFiltredPhase2.ScrollMinY2 = 0D;
            this.gcSignalFiltredPhase2.Size = new System.Drawing.Size(377, 194);
            this.gcSignalFiltredPhase2.TabIndex = 3;
            this.gcSignalFiltredPhase2.UseExtendedPrintDialog = true;
            // 
            // gcSignalPhase3
            // 
            this.gcSignalPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSignalPhase3.Location = new System.Drawing.Point(3, 403);
            this.gcSignalPhase3.Name = "gcSignalPhase3";
            this.gcSignalPhase3.ScrollGrace = 0D;
            this.gcSignalPhase3.ScrollMaxX = 0D;
            this.gcSignalPhase3.ScrollMaxY = 0D;
            this.gcSignalPhase3.ScrollMaxY2 = 0D;
            this.gcSignalPhase3.ScrollMinX = 0D;
            this.gcSignalPhase3.ScrollMinY = 0D;
            this.gcSignalPhase3.ScrollMinY2 = 0D;
            this.gcSignalPhase3.Size = new System.Drawing.Size(376, 196);
            this.gcSignalPhase3.TabIndex = 4;
            this.gcSignalPhase3.UseExtendedPrintDialog = true;
            // 
            // gcSignalFiltredPhase3
            // 
            this.gcSignalFiltredPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSignalFiltredPhase3.Location = new System.Drawing.Point(385, 403);
            this.gcSignalFiltredPhase3.Name = "gcSignalFiltredPhase3";
            this.gcSignalFiltredPhase3.ScrollGrace = 0D;
            this.gcSignalFiltredPhase3.ScrollMaxX = 0D;
            this.gcSignalFiltredPhase3.ScrollMaxY = 0D;
            this.gcSignalFiltredPhase3.ScrollMaxY2 = 0D;
            this.gcSignalFiltredPhase3.ScrollMinX = 0D;
            this.gcSignalFiltredPhase3.ScrollMinY = 0D;
            this.gcSignalFiltredPhase3.ScrollMinY2 = 0D;
            this.gcSignalFiltredPhase3.Size = new System.Drawing.Size(377, 196);
            this.gcSignalFiltredPhase3.TabIndex = 5;
            this.gcSignalFiltredPhase3.UseExtendedPrintDialog = true;
            // 
            // tpDispersion
            // 
            this.tpDispersion.Controls.Add(this.tlpDispersions);
            this.tpDispersion.Location = new System.Drawing.Point(4, 22);
            this.tpDispersion.Name = "tpDispersion";
            this.tpDispersion.Padding = new System.Windows.Forms.Padding(3);
            this.tpDispersion.Size = new System.Drawing.Size(771, 608);
            this.tpDispersion.TabIndex = 1;
            this.tpDispersion.Text = "Dispersão";
            this.tpDispersion.UseVisualStyleBackColor = true;
            // 
            // tlpDispersions
            // 
            this.tlpDispersions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpDispersions.ColumnCount = 1;
            this.tlpDispersions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDispersions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpDispersions.Controls.Add(this.gcDispersionPhase1, 0, 0);
            this.tlpDispersions.Controls.Add(this.gcDispersionPhase2, 0, 1);
            this.tlpDispersions.Controls.Add(this.gcDispersionPhase3, 0, 2);
            this.tlpDispersions.Location = new System.Drawing.Point(3, 3);
            this.tlpDispersions.Name = "tlpDispersions";
            this.tlpDispersions.RowCount = 3;
            this.tlpDispersions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpDispersions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpDispersions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpDispersions.Size = new System.Drawing.Size(760, 602);
            this.tlpDispersions.TabIndex = 0;
            // 
            // gcDispersionPhase1
            // 
            this.gcDispersionPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDispersionPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcDispersionPhase1.Name = "gcDispersionPhase1";
            this.gcDispersionPhase1.ScrollGrace = 0D;
            this.gcDispersionPhase1.ScrollMaxX = 0D;
            this.gcDispersionPhase1.ScrollMaxY = 0D;
            this.gcDispersionPhase1.ScrollMaxY2 = 0D;
            this.gcDispersionPhase1.ScrollMinX = 0D;
            this.gcDispersionPhase1.ScrollMinY = 0D;
            this.gcDispersionPhase1.ScrollMinY2 = 0D;
            this.gcDispersionPhase1.Size = new System.Drawing.Size(754, 194);
            this.gcDispersionPhase1.TabIndex = 0;
            this.gcDispersionPhase1.UseExtendedPrintDialog = true;
            // 
            // gcDispersionPhase2
            // 
            this.gcDispersionPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDispersionPhase2.Location = new System.Drawing.Point(3, 203);
            this.gcDispersionPhase2.Name = "gcDispersionPhase2";
            this.gcDispersionPhase2.ScrollGrace = 0D;
            this.gcDispersionPhase2.ScrollMaxX = 0D;
            this.gcDispersionPhase2.ScrollMaxY = 0D;
            this.gcDispersionPhase2.ScrollMaxY2 = 0D;
            this.gcDispersionPhase2.ScrollMinX = 0D;
            this.gcDispersionPhase2.ScrollMinY = 0D;
            this.gcDispersionPhase2.ScrollMinY2 = 0D;
            this.gcDispersionPhase2.Size = new System.Drawing.Size(754, 194);
            this.gcDispersionPhase2.TabIndex = 1;
            this.gcDispersionPhase2.UseExtendedPrintDialog = true;
            // 
            // gcDispersionPhase3
            // 
            this.gcDispersionPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDispersionPhase3.Location = new System.Drawing.Point(3, 403);
            this.gcDispersionPhase3.Name = "gcDispersionPhase3";
            this.gcDispersionPhase3.ScrollGrace = 0D;
            this.gcDispersionPhase3.ScrollMaxX = 0D;
            this.gcDispersionPhase3.ScrollMaxY = 0D;
            this.gcDispersionPhase3.ScrollMaxY2 = 0D;
            this.gcDispersionPhase3.ScrollMinX = 0D;
            this.gcDispersionPhase3.ScrollMinY = 0D;
            this.gcDispersionPhase3.ScrollMinY2 = 0D;
            this.gcDispersionPhase3.Size = new System.Drawing.Size(754, 196);
            this.gcDispersionPhase3.TabIndex = 2;
            this.gcDispersionPhase3.UseExtendedPrintDialog = true;
            // 
            // tpDispersionDetailed
            // 
            this.tpDispersionDetailed.Controls.Add(this.tlpDispersionDetailed);
            this.tpDispersionDetailed.Location = new System.Drawing.Point(4, 22);
            this.tpDispersionDetailed.Name = "tpDispersionDetailed";
            this.tpDispersionDetailed.Padding = new System.Windows.Forms.Padding(3);
            this.tpDispersionDetailed.Size = new System.Drawing.Size(771, 608);
            this.tpDispersionDetailed.TabIndex = 2;
            this.tpDispersionDetailed.Text = "Dispersão Detalhada";
            this.tpDispersionDetailed.UseVisualStyleBackColor = true;
            // 
            // tlpDispersionDetailed
            // 
            this.tlpDispersionDetailed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpDispersionDetailed.ColumnCount = 1;
            this.tlpDispersionDetailed.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpDispersionDetailed.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpDispersionDetailed.Controls.Add(this.gcDispersionDetailedPhase1, 0, 0);
            this.tlpDispersionDetailed.Controls.Add(this.gcDispersionDetailedPhase2, 0, 1);
            this.tlpDispersionDetailed.Controls.Add(this.gcDispersionDetailedPhase3, 0, 2);
            this.tlpDispersionDetailed.Location = new System.Drawing.Point(3, 3);
            this.tlpDispersionDetailed.Name = "tlpDispersionDetailed";
            this.tlpDispersionDetailed.RowCount = 3;
            this.tlpDispersionDetailed.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpDispersionDetailed.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpDispersionDetailed.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpDispersionDetailed.Size = new System.Drawing.Size(763, 601);
            this.tlpDispersionDetailed.TabIndex = 0;
            // 
            // gcDispersionDetailedPhase1
            // 
            this.gcDispersionDetailedPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDispersionDetailedPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcDispersionDetailedPhase1.Name = "gcDispersionDetailedPhase1";
            this.gcDispersionDetailedPhase1.ScrollGrace = 0D;
            this.gcDispersionDetailedPhase1.ScrollMaxX = 0D;
            this.gcDispersionDetailedPhase1.ScrollMaxY = 0D;
            this.gcDispersionDetailedPhase1.ScrollMaxY2 = 0D;
            this.gcDispersionDetailedPhase1.ScrollMinX = 0D;
            this.gcDispersionDetailedPhase1.ScrollMinY = 0D;
            this.gcDispersionDetailedPhase1.ScrollMinY2 = 0D;
            this.gcDispersionDetailedPhase1.Size = new System.Drawing.Size(757, 194);
            this.gcDispersionDetailedPhase1.TabIndex = 0;
            this.gcDispersionDetailedPhase1.UseExtendedPrintDialog = true;
            // 
            // gcDispersionDetailedPhase2
            // 
            this.gcDispersionDetailedPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDispersionDetailedPhase2.Location = new System.Drawing.Point(3, 203);
            this.gcDispersionDetailedPhase2.Name = "gcDispersionDetailedPhase2";
            this.gcDispersionDetailedPhase2.ScrollGrace = 0D;
            this.gcDispersionDetailedPhase2.ScrollMaxX = 0D;
            this.gcDispersionDetailedPhase2.ScrollMaxY = 0D;
            this.gcDispersionDetailedPhase2.ScrollMaxY2 = 0D;
            this.gcDispersionDetailedPhase2.ScrollMinX = 0D;
            this.gcDispersionDetailedPhase2.ScrollMinY = 0D;
            this.gcDispersionDetailedPhase2.ScrollMinY2 = 0D;
            this.gcDispersionDetailedPhase2.Size = new System.Drawing.Size(757, 194);
            this.gcDispersionDetailedPhase2.TabIndex = 1;
            this.gcDispersionDetailedPhase2.UseExtendedPrintDialog = true;
            // 
            // gcDispersionDetailedPhase3
            // 
            this.gcDispersionDetailedPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDispersionDetailedPhase3.Location = new System.Drawing.Point(3, 403);
            this.gcDispersionDetailedPhase3.Name = "gcDispersionDetailedPhase3";
            this.gcDispersionDetailedPhase3.ScrollGrace = 0D;
            this.gcDispersionDetailedPhase3.ScrollMaxX = 0D;
            this.gcDispersionDetailedPhase3.ScrollMaxY = 0D;
            this.gcDispersionDetailedPhase3.ScrollMaxY2 = 0D;
            this.gcDispersionDetailedPhase3.ScrollMinX = 0D;
            this.gcDispersionDetailedPhase3.ScrollMinY = 0D;
            this.gcDispersionDetailedPhase3.ScrollMinY2 = 0D;
            this.gcDispersionDetailedPhase3.Size = new System.Drawing.Size(757, 195);
            this.gcDispersionDetailedPhase3.TabIndex = 2;
            this.gcDispersionDetailedPhase3.UseExtendedPrintDialog = true;
            // 
            // tpPartialDischargeTrend
            // 
            this.tpPartialDischargeTrend.Controls.Add(this.tlpPartialDischargeTrend);
            this.tpPartialDischargeTrend.Location = new System.Drawing.Point(4, 22);
            this.tpPartialDischargeTrend.Name = "tpPartialDischargeTrend";
            this.tpPartialDischargeTrend.Padding = new System.Windows.Forms.Padding(3);
            this.tpPartialDischargeTrend.Size = new System.Drawing.Size(771, 608);
            this.tpPartialDischargeTrend.TabIndex = 3;
            this.tpPartialDischargeTrend.Text = "Tendência DP";
            this.tpPartialDischargeTrend.UseVisualStyleBackColor = true;
            // 
            // tlpPartialDischargeTrend
            // 
            this.tlpPartialDischargeTrend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpPartialDischargeTrend.ColumnCount = 1;
            this.tlpPartialDischargeTrend.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPartialDischargeTrend.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpPartialDischargeTrend.Controls.Add(this.gcPartialDischargeTrendPhase1, 0, 0);
            this.tlpPartialDischargeTrend.Controls.Add(this.gcPartialDischargeTrendPhase2, 0, 1);
            this.tlpPartialDischargeTrend.Controls.Add(this.gcPartialDischargeTrendPhase3, 0, 2);
            this.tlpPartialDischargeTrend.Location = new System.Drawing.Point(3, 3);
            this.tlpPartialDischargeTrend.Name = "tlpPartialDischargeTrend";
            this.tlpPartialDischargeTrend.RowCount = 3;
            this.tlpPartialDischargeTrend.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpPartialDischargeTrend.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpPartialDischargeTrend.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpPartialDischargeTrend.Size = new System.Drawing.Size(765, 602);
            this.tlpPartialDischargeTrend.TabIndex = 0;
            // 
            // gcPartialDischargeTrendPhase1
            // 
            this.gcPartialDischargeTrendPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcPartialDischargeTrendPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcPartialDischargeTrendPhase1.Name = "gcPartialDischargeTrendPhase1";
            this.gcPartialDischargeTrendPhase1.ScrollGrace = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMaxX = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMaxY = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMaxY2 = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMinX = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMinY = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMinY2 = 0D;
            this.gcPartialDischargeTrendPhase1.Size = new System.Drawing.Size(759, 194);
            this.gcPartialDischargeTrendPhase1.TabIndex = 0;
            this.gcPartialDischargeTrendPhase1.UseExtendedPrintDialog = true;
            // 
            // gcPartialDischargeTrendPhase2
            // 
            this.gcPartialDischargeTrendPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcPartialDischargeTrendPhase2.Location = new System.Drawing.Point(3, 203);
            this.gcPartialDischargeTrendPhase2.Name = "gcPartialDischargeTrendPhase2";
            this.gcPartialDischargeTrendPhase2.ScrollGrace = 0D;
            this.gcPartialDischargeTrendPhase2.ScrollMaxX = 0D;
            this.gcPartialDischargeTrendPhase2.ScrollMaxY = 0D;
            this.gcPartialDischargeTrendPhase2.ScrollMaxY2 = 0D;
            this.gcPartialDischargeTrendPhase2.ScrollMinX = 0D;
            this.gcPartialDischargeTrendPhase2.ScrollMinY = 0D;
            this.gcPartialDischargeTrendPhase2.ScrollMinY2 = 0D;
            this.gcPartialDischargeTrendPhase2.Size = new System.Drawing.Size(759, 194);
            this.gcPartialDischargeTrendPhase2.TabIndex = 1;
            this.gcPartialDischargeTrendPhase2.UseExtendedPrintDialog = true;
            // 
            // gcPartialDischargeTrendPhase3
            // 
            this.gcPartialDischargeTrendPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcPartialDischargeTrendPhase3.Location = new System.Drawing.Point(3, 403);
            this.gcPartialDischargeTrendPhase3.Name = "gcPartialDischargeTrendPhase3";
            this.gcPartialDischargeTrendPhase3.ScrollGrace = 0D;
            this.gcPartialDischargeTrendPhase3.ScrollMaxX = 0D;
            this.gcPartialDischargeTrendPhase3.ScrollMaxY = 0D;
            this.gcPartialDischargeTrendPhase3.ScrollMaxY2 = 0D;
            this.gcPartialDischargeTrendPhase3.ScrollMinX = 0D;
            this.gcPartialDischargeTrendPhase3.ScrollMinY = 0D;
            this.gcPartialDischargeTrendPhase3.ScrollMinY2 = 0D;
            this.gcPartialDischargeTrendPhase3.Size = new System.Drawing.Size(759, 196);
            this.gcPartialDischargeTrendPhase3.TabIndex = 2;
            this.gcPartialDischargeTrendPhase3.UseExtendedPrintDialog = true;
            // 
            // tpFft
            // 
            this.tpFft.Controls.Add(this.tlpFft);
            this.tpFft.Location = new System.Drawing.Point(4, 22);
            this.tpFft.Name = "tpFft";
            this.tpFft.Padding = new System.Windows.Forms.Padding(3);
            this.tpFft.Size = new System.Drawing.Size(771, 608);
            this.tpFft.TabIndex = 4;
            this.tpFft.Text = "FFT";
            this.tpFft.UseVisualStyleBackColor = true;
            // 
            // tlpFft
            // 
            this.tlpFft.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpFft.ColumnCount = 1;
            this.tlpFft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpFft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpFft.Controls.Add(this.gcFftPhase1, 0, 0);
            this.tlpFft.Controls.Add(this.gcFftPhase2, 0, 1);
            this.tlpFft.Controls.Add(this.gcFftPhase3, 0, 2);
            this.tlpFft.Location = new System.Drawing.Point(3, 3);
            this.tlpFft.Name = "tlpFft";
            this.tlpFft.RowCount = 3;
            this.tlpFft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpFft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpFft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpFft.Size = new System.Drawing.Size(765, 602);
            this.tlpFft.TabIndex = 0;
            // 
            // gcFftPhase1
            // 
            this.gcFftPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcFftPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcFftPhase1.Name = "gcFftPhase1";
            this.gcFftPhase1.ScrollGrace = 0D;
            this.gcFftPhase1.ScrollMaxX = 0D;
            this.gcFftPhase1.ScrollMaxY = 0D;
            this.gcFftPhase1.ScrollMaxY2 = 0D;
            this.gcFftPhase1.ScrollMinX = 0D;
            this.gcFftPhase1.ScrollMinY = 0D;
            this.gcFftPhase1.ScrollMinY2 = 0D;
            this.gcFftPhase1.Size = new System.Drawing.Size(759, 194);
            this.gcFftPhase1.TabIndex = 0;
            this.gcFftPhase1.UseExtendedPrintDialog = true;
            // 
            // gcFftPhase2
            // 
            this.gcFftPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcFftPhase2.Location = new System.Drawing.Point(3, 203);
            this.gcFftPhase2.Name = "gcFftPhase2";
            this.gcFftPhase2.ScrollGrace = 0D;
            this.gcFftPhase2.ScrollMaxX = 0D;
            this.gcFftPhase2.ScrollMaxY = 0D;
            this.gcFftPhase2.ScrollMaxY2 = 0D;
            this.gcFftPhase2.ScrollMinX = 0D;
            this.gcFftPhase2.ScrollMinY = 0D;
            this.gcFftPhase2.ScrollMinY2 = 0D;
            this.gcFftPhase2.Size = new System.Drawing.Size(759, 194);
            this.gcFftPhase2.TabIndex = 1;
            this.gcFftPhase2.UseExtendedPrintDialog = true;
            // 
            // gcFftPhase3
            // 
            this.gcFftPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcFftPhase3.Location = new System.Drawing.Point(3, 403);
            this.gcFftPhase3.Name = "gcFftPhase3";
            this.gcFftPhase3.ScrollGrace = 0D;
            this.gcFftPhase3.ScrollMaxX = 0D;
            this.gcFftPhase3.ScrollMaxY = 0D;
            this.gcFftPhase3.ScrollMaxY2 = 0D;
            this.gcFftPhase3.ScrollMinX = 0D;
            this.gcFftPhase3.ScrollMinY = 0D;
            this.gcFftPhase3.ScrollMinY2 = 0D;
            this.gcFftPhase3.Size = new System.Drawing.Size(759, 196);
            this.gcFftPhase3.TabIndex = 2;
            this.gcFftPhase3.UseExtendedPrintDialog = true;
            // 
            // gbTest
            // 
            this.gbTest.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTest.Controls.Add(this.nudTestTime);
            this.gbTest.Controls.Add(this.btStopTest);
            this.gbTest.Controls.Add(this.btStartTest);
            this.gbTest.Controls.Add(this.lbTimer);
            this.gbTest.Controls.Add(this.gbTotalDischarges);
            this.gbTest.Controls.Add(this.gbPartialDischargePhase3);
            this.gbTest.Controls.Add(this.gbPartialDischargePhase2);
            this.gbTest.Controls.Add(this.gbPartialDischargePhase1);
            this.gbTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbTest.Location = new System.Drawing.Point(1025, 10);
            this.gbTest.Name = "gbTest";
            this.gbTest.Size = new System.Drawing.Size(220, 640);
            this.gbTest.TabIndex = 17;
            this.gbTest.TabStop = false;
            this.gbTest.Text = "Ensaio";
            // 
            // nudTestTime
            // 
            this.nudTestTime.Location = new System.Drawing.Point(128, 49);
            this.nudTestTime.Name = "nudTestTime";
            this.nudTestTime.Size = new System.Drawing.Size(79, 20);
            this.nudTestTime.TabIndex = 22;
            this.nudTestTime.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // btStopTest
            // 
            this.btStopTest.Enabled = false;
            this.btStopTest.Location = new System.Drawing.Point(70, 47);
            this.btStopTest.Name = "btStopTest";
            this.btStopTest.Size = new System.Drawing.Size(52, 23);
            this.btStopTest.TabIndex = 21;
            this.btStopTest.Text = "Parar";
            this.btStopTest.UseVisualStyleBackColor = true;
            // 
            // btStartTest
            // 
            this.btStartTest.Location = new System.Drawing.Point(12, 47);
            this.btStartTest.Name = "btStartTest";
            this.btStartTest.Size = new System.Drawing.Size(52, 23);
            this.btStartTest.TabIndex = 20;
            this.btStartTest.Text = "Iniciar";
            this.btStartTest.UseVisualStyleBackColor = true;
            this.btStartTest.Click += new System.EventHandler(this.btStartTest_Click);
            // 
            // lbTimer
            // 
            this.lbTimer.AutoSize = true;
            this.lbTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTimer.Location = new System.Drawing.Point(12, 20);
            this.lbTimer.Name = "lbTimer";
            this.lbTimer.Size = new System.Drawing.Size(88, 24);
            this.lbTimer.TabIndex = 19;
            this.lbTimer.Text = "00:00:00";
            // 
            // gbTotalDischarges
            // 
            this.gbTotalDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTotalDischarges.Controls.Add(this.tableLayoutPanel4);
            this.gbTotalDischarges.Location = new System.Drawing.Point(6, 345);
            this.gbTotalDischarges.Name = "gbTotalDischarges";
            this.gbTotalDischarges.Size = new System.Drawing.Size(208, 114);
            this.gbTotalDischarges.TabIndex = 3;
            this.gbTotalDischarges.TabStop = false;
            this.gbTotalDischarges.Text = "Descargas (Total)";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel4.Controls.Add(this.lbTotalOccurrences, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lbTotalPositiveDischarges, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tbTotalPositiveDischarges, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.lbTotalNegativeDischarges, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbTotalNegativeDischarges, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbTotalOccurrences, 1, 2);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(196, 89);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // lbTotalOccurrences
            // 
            this.lbTotalOccurrences.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTotalOccurrences.AutoSize = true;
            this.lbTotalOccurrences.BackColor = System.Drawing.Color.Transparent;
            this.lbTotalOccurrences.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalOccurrences.Location = new System.Drawing.Point(1, 59);
            this.lbTotalOccurrences.Margin = new System.Windows.Forms.Padding(0);
            this.lbTotalOccurrences.Name = "lbTotalOccurrences";
            this.lbTotalOccurrences.Size = new System.Drawing.Size(67, 29);
            this.lbTotalOccurrences.TabIndex = 4;
            this.lbTotalOccurrences.Text = "Ocorrências:";
            this.lbTotalOccurrences.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTotalPositiveDischarges
            // 
            this.lbTotalPositiveDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTotalPositiveDischarges.AutoSize = true;
            this.lbTotalPositiveDischarges.BackColor = System.Drawing.Color.Transparent;
            this.lbTotalPositiveDischarges.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalPositiveDischarges.Location = new System.Drawing.Point(1, 1);
            this.lbTotalPositiveDischarges.Margin = new System.Windows.Forms.Padding(0);
            this.lbTotalPositiveDischarges.Name = "lbTotalPositiveDischarges";
            this.lbTotalPositiveDischarges.Size = new System.Drawing.Size(67, 28);
            this.lbTotalPositiveDischarges.TabIndex = 0;
            this.lbTotalPositiveDischarges.Text = "Positiva:";
            this.lbTotalPositiveDischarges.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbTotalPositiveDischarges
            // 
            this.tbTotalPositiveDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTotalPositiveDischarges.Location = new System.Drawing.Point(72, 4);
            this.tbTotalPositiveDischarges.Name = "tbTotalPositiveDischarges";
            this.tbTotalPositiveDischarges.Size = new System.Drawing.Size(120, 20);
            this.tbTotalPositiveDischarges.TabIndex = 2;
            // 
            // lbTotalNegativeDischarges
            // 
            this.lbTotalNegativeDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTotalNegativeDischarges.AutoSize = true;
            this.lbTotalNegativeDischarges.BackColor = System.Drawing.Color.Transparent;
            this.lbTotalNegativeDischarges.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalNegativeDischarges.Location = new System.Drawing.Point(1, 30);
            this.lbTotalNegativeDischarges.Margin = new System.Windows.Forms.Padding(0);
            this.lbTotalNegativeDischarges.Name = "lbTotalNegativeDischarges";
            this.lbTotalNegativeDischarges.Size = new System.Drawing.Size(67, 28);
            this.lbTotalNegativeDischarges.TabIndex = 1;
            this.lbTotalNegativeDischarges.Text = "Negativa:";
            this.lbTotalNegativeDischarges.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbTotalNegativeDischarges
            // 
            this.tbTotalNegativeDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTotalNegativeDischarges.Location = new System.Drawing.Point(72, 33);
            this.tbTotalNegativeDischarges.Name = "tbTotalNegativeDischarges";
            this.tbTotalNegativeDischarges.Size = new System.Drawing.Size(120, 20);
            this.tbTotalNegativeDischarges.TabIndex = 3;
            // 
            // tbTotalOccurrences
            // 
            this.tbTotalOccurrences.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTotalOccurrences.Location = new System.Drawing.Point(72, 62);
            this.tbTotalOccurrences.Name = "tbTotalOccurrences";
            this.tbTotalOccurrences.Size = new System.Drawing.Size(120, 20);
            this.tbTotalOccurrences.TabIndex = 5;
            // 
            // gbPartialDischargePhase3
            // 
            this.gbPartialDischargePhase3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPartialDischargePhase3.Controls.Add(this.tableLayoutPanel2);
            this.gbPartialDischargePhase3.Location = new System.Drawing.Point(6, 254);
            this.gbPartialDischargePhase3.Name = "gbPartialDischargePhase3";
            this.gbPartialDischargePhase3.Size = new System.Drawing.Size(208, 83);
            this.gbPartialDischargePhase3.TabIndex = 2;
            this.gbPartialDischargePhase3.TabStop = false;
            this.gbPartialDischargePhase3.Text = "Descarga Parcial (Fase C)";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel2.Controls.Add(this.tbOccurrencesPhase3, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lbOccurrencesPhase3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lbChargePhase3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tbChargePhase3, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(196, 58);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tbOccurrencesPhase3
            // 
            this.tbOccurrencesPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOccurrencesPhase3.Location = new System.Drawing.Point(72, 32);
            this.tbOccurrencesPhase3.Name = "tbOccurrencesPhase3";
            this.tbOccurrencesPhase3.Size = new System.Drawing.Size(120, 20);
            this.tbOccurrencesPhase3.TabIndex = 3;
            // 
            // lbOccurrencesPhase3
            // 
            this.lbOccurrencesPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbOccurrencesPhase3.AutoSize = true;
            this.lbOccurrencesPhase3.BackColor = System.Drawing.Color.Transparent;
            this.lbOccurrencesPhase3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOccurrencesPhase3.Location = new System.Drawing.Point(1, 29);
            this.lbOccurrencesPhase3.Margin = new System.Windows.Forms.Padding(0);
            this.lbOccurrencesPhase3.Name = "lbOccurrencesPhase3";
            this.lbOccurrencesPhase3.Size = new System.Drawing.Size(67, 28);
            this.lbOccurrencesPhase3.TabIndex = 1;
            this.lbOccurrencesPhase3.Text = "Ocorrências:";
            this.lbOccurrencesPhase3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbChargePhase3
            // 
            this.lbChargePhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbChargePhase3.AutoSize = true;
            this.lbChargePhase3.BackColor = System.Drawing.Color.Transparent;
            this.lbChargePhase3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChargePhase3.Location = new System.Drawing.Point(1, 1);
            this.lbChargePhase3.Margin = new System.Windows.Forms.Padding(0);
            this.lbChargePhase3.Name = "lbChargePhase3";
            this.lbChargePhase3.Size = new System.Drawing.Size(67, 27);
            this.lbChargePhase3.TabIndex = 0;
            this.lbChargePhase3.Text = "Carga (pC):";
            this.lbChargePhase3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbChargePhase3
            // 
            this.tbChargePhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbChargePhase3.Location = new System.Drawing.Point(72, 4);
            this.tbChargePhase3.Name = "tbChargePhase3";
            this.tbChargePhase3.Size = new System.Drawing.Size(120, 20);
            this.tbChargePhase3.TabIndex = 2;
            // 
            // gbPartialDischargePhase2
            // 
            this.gbPartialDischargePhase2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPartialDischargePhase2.Controls.Add(this.tableLayoutPanel1);
            this.gbPartialDischargePhase2.Location = new System.Drawing.Point(6, 165);
            this.gbPartialDischargePhase2.Name = "gbPartialDischargePhase2";
            this.gbPartialDischargePhase2.Size = new System.Drawing.Size(208, 83);
            this.gbPartialDischargePhase2.TabIndex = 1;
            this.gbPartialDischargePhase2.TabStop = false;
            this.gbPartialDischargePhase2.Text = "Descarga Parcial (Fase B)";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel1.Controls.Add(this.tbOccurrencesPhase2, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbOccurrencesPhase2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbChargePhase2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbChargePhase2, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(196, 58);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tbOccurrencesPhase2
            // 
            this.tbOccurrencesPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOccurrencesPhase2.Location = new System.Drawing.Point(72, 32);
            this.tbOccurrencesPhase2.Name = "tbOccurrencesPhase2";
            this.tbOccurrencesPhase2.Size = new System.Drawing.Size(120, 20);
            this.tbOccurrencesPhase2.TabIndex = 3;
            // 
            // lbOccurrencesPhase2
            // 
            this.lbOccurrencesPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbOccurrencesPhase2.AutoSize = true;
            this.lbOccurrencesPhase2.BackColor = System.Drawing.Color.Transparent;
            this.lbOccurrencesPhase2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOccurrencesPhase2.Location = new System.Drawing.Point(1, 29);
            this.lbOccurrencesPhase2.Margin = new System.Windows.Forms.Padding(0);
            this.lbOccurrencesPhase2.Name = "lbOccurrencesPhase2";
            this.lbOccurrencesPhase2.Size = new System.Drawing.Size(67, 28);
            this.lbOccurrencesPhase2.TabIndex = 1;
            this.lbOccurrencesPhase2.Text = "Ocorrências:";
            this.lbOccurrencesPhase2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbChargePhase2
            // 
            this.lbChargePhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbChargePhase2.AutoSize = true;
            this.lbChargePhase2.BackColor = System.Drawing.Color.Transparent;
            this.lbChargePhase2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChargePhase2.Location = new System.Drawing.Point(1, 1);
            this.lbChargePhase2.Margin = new System.Windows.Forms.Padding(0);
            this.lbChargePhase2.Name = "lbChargePhase2";
            this.lbChargePhase2.Size = new System.Drawing.Size(67, 27);
            this.lbChargePhase2.TabIndex = 0;
            this.lbChargePhase2.Text = "Carga (pC):";
            this.lbChargePhase2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbChargePhase2
            // 
            this.tbChargePhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbChargePhase2.Location = new System.Drawing.Point(72, 4);
            this.tbChargePhase2.Name = "tbChargePhase2";
            this.tbChargePhase2.Size = new System.Drawing.Size(120, 20);
            this.tbChargePhase2.TabIndex = 2;
            // 
            // gbPartialDischargePhase1
            // 
            this.gbPartialDischargePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPartialDischargePhase1.Controls.Add(this.tpPartialDischargePhase1);
            this.gbPartialDischargePhase1.Location = new System.Drawing.Point(6, 76);
            this.gbPartialDischargePhase1.Name = "gbPartialDischargePhase1";
            this.gbPartialDischargePhase1.Size = new System.Drawing.Size(208, 83);
            this.gbPartialDischargePhase1.TabIndex = 0;
            this.gbPartialDischargePhase1.TabStop = false;
            this.gbPartialDischargePhase1.Text = "Descarga Parcial (Fase A)";
            // 
            // tpPartialDischargePhase1
            // 
            this.tpPartialDischargePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tpPartialDischargePhase1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tpPartialDischargePhase1.ColumnCount = 2;
            this.tpPartialDischargePhase1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tpPartialDischargePhase1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tpPartialDischargePhase1.Controls.Add(this.tbOccurrencesPhase1, 1, 1);
            this.tpPartialDischargePhase1.Controls.Add(this.lbOccurrencesPhase1, 0, 1);
            this.tpPartialDischargePhase1.Controls.Add(this.lbChargePhase1, 0, 0);
            this.tpPartialDischargePhase1.Controls.Add(this.tbChargePhase1, 1, 0);
            this.tpPartialDischargePhase1.Location = new System.Drawing.Point(6, 19);
            this.tpPartialDischargePhase1.Name = "tpPartialDischargePhase1";
            this.tpPartialDischargePhase1.RowCount = 2;
            this.tpPartialDischargePhase1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpPartialDischargePhase1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpPartialDischargePhase1.Size = new System.Drawing.Size(196, 58);
            this.tpPartialDischargePhase1.TabIndex = 0;
            // 
            // tbOccurrencesPhase1
            // 
            this.tbOccurrencesPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOccurrencesPhase1.Location = new System.Drawing.Point(72, 32);
            this.tbOccurrencesPhase1.Name = "tbOccurrencesPhase1";
            this.tbOccurrencesPhase1.Size = new System.Drawing.Size(120, 20);
            this.tbOccurrencesPhase1.TabIndex = 3;
            // 
            // lbOccurrencesPhase1
            // 
            this.lbOccurrencesPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbOccurrencesPhase1.AutoSize = true;
            this.lbOccurrencesPhase1.BackColor = System.Drawing.Color.Transparent;
            this.lbOccurrencesPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOccurrencesPhase1.Location = new System.Drawing.Point(1, 29);
            this.lbOccurrencesPhase1.Margin = new System.Windows.Forms.Padding(0);
            this.lbOccurrencesPhase1.Name = "lbOccurrencesPhase1";
            this.lbOccurrencesPhase1.Size = new System.Drawing.Size(67, 28);
            this.lbOccurrencesPhase1.TabIndex = 1;
            this.lbOccurrencesPhase1.Text = "Ocorrências:";
            this.lbOccurrencesPhase1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbChargePhase1
            // 
            this.lbChargePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbChargePhase1.AutoSize = true;
            this.lbChargePhase1.BackColor = System.Drawing.Color.Transparent;
            this.lbChargePhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChargePhase1.Location = new System.Drawing.Point(1, 1);
            this.lbChargePhase1.Margin = new System.Windows.Forms.Padding(0);
            this.lbChargePhase1.Name = "lbChargePhase1";
            this.lbChargePhase1.Size = new System.Drawing.Size(67, 27);
            this.lbChargePhase1.TabIndex = 0;
            this.lbChargePhase1.Text = "Carga (pC):";
            this.lbChargePhase1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbChargePhase1
            // 
            this.tbChargePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbChargePhase1.Location = new System.Drawing.Point(72, 4);
            this.tbChargePhase1.Name = "tbChargePhase1";
            this.tbChargePhase1.Size = new System.Drawing.Size(120, 20);
            this.tbChargePhase1.TabIndex = 2;
            // 
            // gbTestController
            // 
            this.gbTestController.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbTestController.Controls.Add(this.btResetConfigurations);
            this.gbTestController.Controls.Add(this.btSaveCalibration);
            this.gbTestController.Controls.Add(this.btCalibration);
            this.gbTestController.Controls.Add(this.gbCalibration);
            this.gbTestController.Controls.Add(this.tableCute);
            this.gbTestController.Controls.Add(this.tableFilter);
            this.gbTestController.Controls.Add(this.btStop);
            this.gbTestController.Controls.Add(this.cbOsciloscope);
            this.gbTestController.Controls.Add(this.lbParamters);
            this.gbTestController.Controls.Add(this.tableController);
            this.gbTestController.Controls.Add(this.btStart);
            this.gbTestController.Controls.Add(this.lbOsciloscope);
            this.gbTestController.Controls.Add(this.tableOscilloscope);
            this.gbTestController.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbTestController.Location = new System.Drawing.Point(10, 10);
            this.gbTestController.Name = "gbTestController";
            this.gbTestController.Size = new System.Drawing.Size(220, 640);
            this.gbTestController.TabIndex = 0;
            this.gbTestController.TabStop = false;
            this.gbTestController.Text = "Controle de Ensaio";
            // 
            // btResetConfigurations
            // 
            this.btResetConfigurations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btResetConfigurations.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btResetConfigurations.Location = new System.Drawing.Point(146, 614);
            this.btResetConfigurations.Name = "btResetConfigurations";
            this.btResetConfigurations.Size = new System.Drawing.Size(75, 23);
            this.btResetConfigurations.TabIndex = 25;
            this.btResetConfigurations.Text = "Resetar";
            this.btResetConfigurations.UseVisualStyleBackColor = true;
            this.btResetConfigurations.Click += new System.EventHandler(this.btResetConfigurations_Click);
            // 
            // btSaveCalibration
            // 
            this.btSaveCalibration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btSaveCalibration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSaveCalibration.Location = new System.Drawing.Point(73, 614);
            this.btSaveCalibration.Name = "btSaveCalibration";
            this.btSaveCalibration.Size = new System.Drawing.Size(75, 23);
            this.btSaveCalibration.TabIndex = 24;
            this.btSaveCalibration.Text = "Salvar";
            this.btSaveCalibration.UseVisualStyleBackColor = true;
            this.btSaveCalibration.Click += new System.EventHandler(this.btSaveCalibration_Click);
            // 
            // btCalibration
            // 
            this.btCalibration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btCalibration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCalibration.Location = new System.Drawing.Point(-1, 614);
            this.btCalibration.Name = "btCalibration";
            this.btCalibration.Size = new System.Drawing.Size(75, 23);
            this.btCalibration.TabIndex = 23;
            this.btCalibration.Text = "Calibrar";
            this.btCalibration.UseVisualStyleBackColor = true;
            this.btCalibration.Click += new System.EventHandler(this.btCalibration_Click);
            // 
            // gbCalibration
            // 
            this.gbCalibration.Controls.Add(this.tpCalibration);
            this.gbCalibration.Location = new System.Drawing.Point(5, 466);
            this.gbCalibration.Name = "gbCalibration";
            this.gbCalibration.Size = new System.Drawing.Size(210, 134);
            this.gbCalibration.TabIndex = 18;
            this.gbCalibration.TabStop = false;
            this.gbCalibration.Text = "Calibração";
            // 
            // tpCalibration
            // 
            this.tpCalibration.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tpCalibration.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tpCalibration.ColumnCount = 2;
            this.tpCalibration.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpCalibration.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpCalibration.Controls.Add(this.lbCalibrationPhase2, 0, 2);
            this.tpCalibration.Controls.Add(this.tbCalibrationPhase3, 1, 3);
            this.tpCalibration.Controls.Add(this.tbCalibrationPhase2, 1, 2);
            this.tpCalibration.Controls.Add(this.tbCalibrationPhase1, 1, 1);
            this.tpCalibration.Controls.Add(this.lbCalibrationPhase1, 0, 1);
            this.tpCalibration.Controls.Add(this.lbDp, 1, 0);
            this.tpCalibration.Controls.Add(this.lbPhase, 0, 0);
            this.tpCalibration.Controls.Add(this.lbCalibrationPhase3, 0, 3);
            this.tpCalibration.Location = new System.Drawing.Point(4, 19);
            this.tpCalibration.Name = "tpCalibration";
            this.tpCalibration.RowCount = 4;
            this.tpCalibration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tpCalibration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tpCalibration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tpCalibration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tpCalibration.Size = new System.Drawing.Size(202, 109);
            this.tpCalibration.TabIndex = 18;
            // 
            // lbCalibrationPhase2
            // 
            this.lbCalibrationPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCalibrationPhase2.AutoSize = true;
            this.lbCalibrationPhase2.BackColor = System.Drawing.Color.Transparent;
            this.lbCalibrationPhase2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCalibrationPhase2.Location = new System.Drawing.Point(4, 55);
            this.lbCalibrationPhase2.Name = "lbCalibrationPhase2";
            this.lbCalibrationPhase2.Size = new System.Drawing.Size(93, 26);
            this.lbCalibrationPhase2.TabIndex = 8;
            this.lbCalibrationPhase2.Text = "Fase 2";
            this.lbCalibrationPhase2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbCalibrationPhase3
            // 
            this.tbCalibrationPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCalibrationPhase3.DecimalPlaces = 2;
            this.tbCalibrationPhase3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCalibrationPhase3.Location = new System.Drawing.Point(104, 85);
            this.tbCalibrationPhase3.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbCalibrationPhase3.Name = "tbCalibrationPhase3";
            this.tbCalibrationPhase3.Size = new System.Drawing.Size(94, 20);
            this.tbCalibrationPhase3.TabIndex = 25;
            // 
            // tbCalibrationPhase2
            // 
            this.tbCalibrationPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCalibrationPhase2.DecimalPlaces = 2;
            this.tbCalibrationPhase2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCalibrationPhase2.Location = new System.Drawing.Point(104, 58);
            this.tbCalibrationPhase2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbCalibrationPhase2.Name = "tbCalibrationPhase2";
            this.tbCalibrationPhase2.Size = new System.Drawing.Size(94, 20);
            this.tbCalibrationPhase2.TabIndex = 24;
            // 
            // tbCalibrationPhase1
            // 
            this.tbCalibrationPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCalibrationPhase1.DecimalPlaces = 2;
            this.tbCalibrationPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCalibrationPhase1.Location = new System.Drawing.Point(104, 31);
            this.tbCalibrationPhase1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbCalibrationPhase1.Name = "tbCalibrationPhase1";
            this.tbCalibrationPhase1.Size = new System.Drawing.Size(94, 20);
            this.tbCalibrationPhase1.TabIndex = 23;
            // 
            // lbCalibrationPhase1
            // 
            this.lbCalibrationPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCalibrationPhase1.AutoSize = true;
            this.lbCalibrationPhase1.BackColor = System.Drawing.Color.Transparent;
            this.lbCalibrationPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCalibrationPhase1.Location = new System.Drawing.Point(4, 28);
            this.lbCalibrationPhase1.Name = "lbCalibrationPhase1";
            this.lbCalibrationPhase1.Size = new System.Drawing.Size(93, 26);
            this.lbCalibrationPhase1.TabIndex = 6;
            this.lbCalibrationPhase1.Text = "Fase 1";
            this.lbCalibrationPhase1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDp
            // 
            this.lbDp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDp.AutoSize = true;
            this.lbDp.BackColor = System.Drawing.Color.Transparent;
            this.lbDp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDp.Location = new System.Drawing.Point(104, 1);
            this.lbDp.Name = "lbDp";
            this.lbDp.Size = new System.Drawing.Size(94, 26);
            this.lbDp.TabIndex = 5;
            this.lbDp.Text = "DP (pC)";
            this.lbDp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPhase
            // 
            this.lbPhase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPhase.AutoSize = true;
            this.lbPhase.BackColor = System.Drawing.Color.Transparent;
            this.lbPhase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhase.Location = new System.Drawing.Point(4, 1);
            this.lbPhase.Name = "lbPhase";
            this.lbPhase.Size = new System.Drawing.Size(93, 26);
            this.lbPhase.TabIndex = 4;
            this.lbPhase.Text = "Fase";
            this.lbPhase.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCalibrationPhase3
            // 
            this.lbCalibrationPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCalibrationPhase3.AutoSize = true;
            this.lbCalibrationPhase3.BackColor = System.Drawing.Color.Transparent;
            this.lbCalibrationPhase3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCalibrationPhase3.Location = new System.Drawing.Point(4, 82);
            this.lbCalibrationPhase3.Name = "lbCalibrationPhase3";
            this.lbCalibrationPhase3.Size = new System.Drawing.Size(93, 26);
            this.lbCalibrationPhase3.TabIndex = 7;
            this.lbCalibrationPhase3.Text = "Fase 3";
            this.lbCalibrationPhase3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableCute
            // 
            this.tableCute.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableCute.ColumnCount = 3;
            this.tableCute.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44F));
            this.tableCute.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableCute.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableCute.Controls.Add(this.cbFrequencyTopCut, 2, 1);
            this.tableCute.Controls.Add(this.tbFrequencyTopCut, 1, 1);
            this.tableCute.Controls.Add(this.lbTopCut, 0, 1);
            this.tableCute.Controls.Add(this.cbFrequencyUndercut, 2, 0);
            this.tableCute.Controls.Add(this.lbUndercut, 0, 0);
            this.tableCute.Controls.Add(this.tbFrequencyUndercut, 1, 0);
            this.tableCute.Location = new System.Drawing.Point(6, 406);
            this.tableCute.Name = "tableCute";
            this.tableCute.RowCount = 2;
            this.tableCute.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableCute.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableCute.Size = new System.Drawing.Size(209, 52);
            this.tableCute.TabIndex = 14;
            // 
            // cbFrequencyTopCut
            // 
            this.cbFrequencyTopCut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFrequencyTopCut.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrequencyTopCut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFrequencyTopCut.FormattingEnabled = true;
            this.cbFrequencyTopCut.Items.AddRange(new object[] {
            "Hz",
            "kHz",
            "MHz",
            "GHz"});
            this.cbFrequencyTopCut.Location = new System.Drawing.Point(153, 29);
            this.cbFrequencyTopCut.Name = "cbFrequencyTopCut";
            this.cbFrequencyTopCut.Size = new System.Drawing.Size(52, 21);
            this.cbFrequencyTopCut.TabIndex = 15;
            // 
            // tbFrequencyTopCut
            // 
            this.tbFrequencyTopCut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFrequencyTopCut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFrequencyTopCut.Location = new System.Drawing.Point(95, 29);
            this.tbFrequencyTopCut.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbFrequencyTopCut.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbFrequencyTopCut.Name = "tbFrequencyTopCut";
            this.tbFrequencyTopCut.Size = new System.Drawing.Size(51, 20);
            this.tbFrequencyTopCut.TabIndex = 31;
            this.tbFrequencyTopCut.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbTopCut
            // 
            this.lbTopCut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTopCut.AutoSize = true;
            this.lbTopCut.BackColor = System.Drawing.Color.Transparent;
            this.lbTopCut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTopCut.Location = new System.Drawing.Point(1, 26);
            this.lbTopCut.Margin = new System.Windows.Forms.Padding(0);
            this.lbTopCut.Name = "lbTopCut";
            this.lbTopCut.Size = new System.Drawing.Size(90, 25);
            this.lbTopCut.TabIndex = 28;
            this.lbTopCut.Text = "Corte superior:";
            this.lbTopCut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbFrequencyUndercut
            // 
            this.cbFrequencyUndercut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFrequencyUndercut.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrequencyUndercut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFrequencyUndercut.FormattingEnabled = true;
            this.cbFrequencyUndercut.Items.AddRange(new object[] {
            "Hz",
            "kHz",
            "MHz",
            "GHz"});
            this.cbFrequencyUndercut.Location = new System.Drawing.Point(153, 4);
            this.cbFrequencyUndercut.Name = "cbFrequencyUndercut";
            this.cbFrequencyUndercut.Size = new System.Drawing.Size(52, 21);
            this.cbFrequencyUndercut.TabIndex = 13;
            // 
            // lbUndercut
            // 
            this.lbUndercut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbUndercut.AutoSize = true;
            this.lbUndercut.BackColor = System.Drawing.Color.Transparent;
            this.lbUndercut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUndercut.Location = new System.Drawing.Point(1, 1);
            this.lbUndercut.Margin = new System.Windows.Forms.Padding(0);
            this.lbUndercut.Name = "lbUndercut";
            this.lbUndercut.Size = new System.Drawing.Size(90, 24);
            this.lbUndercut.TabIndex = 27;
            this.lbUndercut.Text = "Corte inferior:";
            this.lbUndercut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbFrequencyUndercut
            // 
            this.tbFrequencyUndercut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFrequencyUndercut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFrequencyUndercut.Location = new System.Drawing.Point(95, 4);
            this.tbFrequencyUndercut.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbFrequencyUndercut.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbFrequencyUndercut.Name = "tbFrequencyUndercut";
            this.tbFrequencyUndercut.Size = new System.Drawing.Size(51, 20);
            this.tbFrequencyUndercut.TabIndex = 30;
            this.tbFrequencyUndercut.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // tableFilter
            // 
            this.tableFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableFilter.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableFilter.ColumnCount = 2;
            this.tableFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableFilter.Controls.Add(this.cbFilter, 1, 0);
            this.tableFilter.Controls.Add(this.lbFilter, 0, 0);
            this.tableFilter.Controls.Add(this.lbOrder, 0, 1);
            this.tableFilter.Controls.Add(this.tbOrder, 1, 1);
            this.tableFilter.Location = new System.Drawing.Point(6, 351);
            this.tableFilter.Name = "tableFilter";
            this.tableFilter.RowCount = 2;
            this.tableFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.21429F));
            this.tableFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.78571F));
            this.tableFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableFilter.Size = new System.Drawing.Size(209, 51);
            this.tableFilter.TabIndex = 4;
            // 
            // cbFilter
            // 
            this.cbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFilter.FormattingEnabled = true;
            this.cbFilter.Items.AddRange(new object[] {
            "Passa Baixa",
            "Passa Alta",
            "Passa Banda",
            "Passa Faixa"});
            this.cbFilter.Location = new System.Drawing.Point(108, 4);
            this.cbFilter.Name = "cbFilter";
            this.cbFilter.Size = new System.Drawing.Size(97, 21);
            this.cbFilter.TabIndex = 10;
            // 
            // lbFilter
            // 
            this.lbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbFilter.AutoSize = true;
            this.lbFilter.BackColor = System.Drawing.Color.Transparent;
            this.lbFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFilter.Location = new System.Drawing.Point(1, 1);
            this.lbFilter.Margin = new System.Windows.Forms.Padding(0);
            this.lbFilter.Name = "lbFilter";
            this.lbFilter.Size = new System.Drawing.Size(103, 23);
            this.lbFilter.TabIndex = 16;
            this.lbFilter.Text = "Filtro:";
            this.lbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbOrder
            // 
            this.lbOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbOrder.AutoSize = true;
            this.lbOrder.BackColor = System.Drawing.Color.Transparent;
            this.lbOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrder.Location = new System.Drawing.Point(4, 25);
            this.lbOrder.Name = "lbOrder";
            this.lbOrder.Size = new System.Drawing.Size(97, 25);
            this.lbOrder.TabIndex = 14;
            this.lbOrder.Text = "Ordem:";
            this.lbOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbOrder
            // 
            this.tbOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOrder.Location = new System.Drawing.Point(108, 28);
            this.tbOrder.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbOrder.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbOrder.Name = "tbOrder";
            this.tbOrder.Size = new System.Drawing.Size(97, 20);
            this.tbOrder.TabIndex = 29;
            this.tbOrder.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // btStop
            // 
            this.btStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStop.Location = new System.Drawing.Point(112, 19);
            this.btStop.Name = "btStop";
            this.btStop.Size = new System.Drawing.Size(101, 25);
            this.btStop.TabIndex = 2;
            this.btStop.Text = "Parar Aquisição";
            this.btStop.UseVisualStyleBackColor = true;
            this.btStop.Click += new System.EventHandler(this.btStop_Click);
            // 
            // cbOsciloscope
            // 
            this.cbOsciloscope.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbOsciloscope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOsciloscope.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOsciloscope.FormattingEnabled = true;
            this.cbOsciloscope.Location = new System.Drawing.Point(6, 66);
            this.cbOsciloscope.Name = "cbOsciloscope";
            this.cbOsciloscope.Size = new System.Drawing.Size(208, 21);
            this.cbOsciloscope.TabIndex = 2;
            this.cbOsciloscope.SelectedIndexChanged += new System.EventHandler(this.cbOsciloscope_SelectedIndexChanged);
            // 
            // lbParamters
            // 
            this.lbParamters.AutoSize = true;
            this.lbParamters.Location = new System.Drawing.Point(6, 216);
            this.lbParamters.Name = "lbParamters";
            this.lbParamters.Size = new System.Drawing.Size(70, 13);
            this.lbParamters.TabIndex = 9;
            this.lbParamters.Text = "Parâmetros";
            // 
            // tableController
            // 
            this.tableController.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableController.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableController.ColumnCount = 4;
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableController.Controls.Add(this.lbPhase3, 0, 3);
            this.tableController.Controls.Add(this.lbPhase2, 0, 2);
            this.tableController.Controls.Add(this.lbPhase1, 0, 1);
            this.tableController.Controls.Add(this.lbGain, 1, 0);
            this.tableController.Controls.Add(this.lbGate, 2, 0);
            this.tableController.Controls.Add(this.tbGatePhase3, 2, 3);
            this.tableController.Controls.Add(this.tbGainPhase1, 1, 1);
            this.tableController.Controls.Add(this.tbGatePhase2, 2, 2);
            this.tableController.Controls.Add(this.tbGainPhase2, 1, 2);
            this.tableController.Controls.Add(this.tbGainPhase3, 1, 3);
            this.tableController.Controls.Add(this.tbGatePhase1, 2, 1);
            this.tableController.Location = new System.Drawing.Point(6, 232);
            this.tableController.Name = "tableController";
            this.tableController.RowCount = 5;
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.Size = new System.Drawing.Size(208, 114);
            this.tableController.TabIndex = 3;
            // 
            // lbPhase3
            // 
            this.lbPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPhase3.AutoSize = true;
            this.lbPhase3.BackColor = System.Drawing.Color.Transparent;
            this.lbPhase3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhase3.Location = new System.Drawing.Point(1, 88);
            this.lbPhase3.Margin = new System.Windows.Forms.Padding(0);
            this.lbPhase3.Name = "lbPhase3";
            this.lbPhase3.Size = new System.Drawing.Size(20, 28);
            this.lbPhase3.TabIndex = 1;
            this.lbPhase3.Text = "F3";
            this.lbPhase3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPhase2
            // 
            this.lbPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPhase2.AutoSize = true;
            this.lbPhase2.BackColor = System.Drawing.Color.Transparent;
            this.lbPhase2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhase2.Location = new System.Drawing.Point(1, 59);
            this.lbPhase2.Margin = new System.Windows.Forms.Padding(0);
            this.lbPhase2.Name = "lbPhase2";
            this.lbPhase2.Size = new System.Drawing.Size(20, 28);
            this.lbPhase2.TabIndex = 1;
            this.lbPhase2.Text = "F2";
            this.lbPhase2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPhase1
            // 
            this.lbPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPhase1.AutoSize = true;
            this.lbPhase1.BackColor = System.Drawing.Color.Transparent;
            this.lbPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhase1.Location = new System.Drawing.Point(1, 30);
            this.lbPhase1.Margin = new System.Windows.Forms.Padding(0);
            this.lbPhase1.Name = "lbPhase1";
            this.lbPhase1.Size = new System.Drawing.Size(20, 28);
            this.lbPhase1.TabIndex = 0;
            this.lbPhase1.Text = "F1";
            this.lbPhase1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbGain
            // 
            this.lbGain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGain.AutoSize = true;
            this.lbGain.BackColor = System.Drawing.Color.Transparent;
            this.lbGain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGain.Location = new System.Drawing.Point(22, 1);
            this.lbGain.Margin = new System.Windows.Forms.Padding(0);
            this.lbGain.Name = "lbGain";
            this.lbGain.Size = new System.Drawing.Size(81, 28);
            this.lbGain.TabIndex = 5;
            this.lbGain.Text = "Ganho";
            this.lbGain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbGate
            // 
            this.lbGate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGate.AutoSize = true;
            this.lbGate.BackColor = System.Drawing.Color.Transparent;
            this.lbGate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGate.Location = new System.Drawing.Point(104, 1);
            this.lbGate.Margin = new System.Windows.Forms.Padding(0);
            this.lbGate.Name = "lbGate";
            this.lbGate.Size = new System.Drawing.Size(81, 28);
            this.lbGate.TabIndex = 6;
            this.lbGate.Text = "Gate";
            this.lbGate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbGatePhase3
            // 
            this.tbGatePhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGatePhase3.DecimalPlaces = 3;
            this.tbGatePhase3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGatePhase3.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.tbGatePhase3.Location = new System.Drawing.Point(107, 91);
            this.tbGatePhase3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.tbGatePhase3.Name = "tbGatePhase3";
            this.tbGatePhase3.Size = new System.Drawing.Size(75, 20);
            this.tbGatePhase3.TabIndex = 28;
            this.tbGatePhase3.Value = new decimal(new int[] {
            500,
            0,
            0,
            196608});
            // 
            // tbGainPhase1
            // 
            this.tbGainPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGainPhase1.DecimalPlaces = 2;
            this.tbGainPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGainPhase1.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tbGainPhase1.Location = new System.Drawing.Point(25, 33);
            this.tbGainPhase1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbGainPhase1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.tbGainPhase1.Name = "tbGainPhase1";
            this.tbGainPhase1.Size = new System.Drawing.Size(75, 20);
            this.tbGainPhase1.TabIndex = 23;
            this.tbGainPhase1.Value = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            // 
            // tbGatePhase2
            // 
            this.tbGatePhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGatePhase2.DecimalPlaces = 3;
            this.tbGatePhase2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGatePhase2.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.tbGatePhase2.Location = new System.Drawing.Point(107, 62);
            this.tbGatePhase2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.tbGatePhase2.Name = "tbGatePhase2";
            this.tbGatePhase2.Size = new System.Drawing.Size(75, 20);
            this.tbGatePhase2.TabIndex = 27;
            this.tbGatePhase2.Value = new decimal(new int[] {
            500,
            0,
            0,
            196608});
            // 
            // tbGainPhase2
            // 
            this.tbGainPhase2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGainPhase2.DecimalPlaces = 2;
            this.tbGainPhase2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGainPhase2.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tbGainPhase2.Location = new System.Drawing.Point(25, 62);
            this.tbGainPhase2.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbGainPhase2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.tbGainPhase2.Name = "tbGainPhase2";
            this.tbGainPhase2.Size = new System.Drawing.Size(75, 20);
            this.tbGainPhase2.TabIndex = 24;
            this.tbGainPhase2.Value = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            // 
            // tbGainPhase3
            // 
            this.tbGainPhase3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGainPhase3.DecimalPlaces = 2;
            this.tbGainPhase3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGainPhase3.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tbGainPhase3.Location = new System.Drawing.Point(25, 91);
            this.tbGainPhase3.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbGainPhase3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.tbGainPhase3.Name = "tbGainPhase3";
            this.tbGainPhase3.Size = new System.Drawing.Size(75, 20);
            this.tbGainPhase3.TabIndex = 25;
            this.tbGainPhase3.Value = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            // 
            // tbGatePhase1
            // 
            this.tbGatePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGatePhase1.DecimalPlaces = 3;
            this.tbGatePhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGatePhase1.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.tbGatePhase1.Location = new System.Drawing.Point(107, 33);
            this.tbGatePhase1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.tbGatePhase1.Name = "tbGatePhase1";
            this.tbGatePhase1.Size = new System.Drawing.Size(75, 20);
            this.tbGatePhase1.TabIndex = 26;
            this.tbGatePhase1.Value = new decimal(new int[] {
            500,
            0,
            0,
            196608});
            // 
            // btStart
            // 
            this.btStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStart.Location = new System.Drawing.Point(5, 19);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(102, 25);
            this.btStart.TabIndex = 1;
            this.btStart.Text = "Iniciar Aquisição";
            this.btStart.UseVisualStyleBackColor = true;
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // lbOsciloscope
            // 
            this.lbOsciloscope.AutoSize = true;
            this.lbOsciloscope.Location = new System.Drawing.Point(6, 50);
            this.lbOsciloscope.Name = "lbOsciloscope";
            this.lbOsciloscope.Size = new System.Drawing.Size(79, 13);
            this.lbOsciloscope.TabIndex = 6;
            this.lbOsciloscope.Text = "Osciloscópio";
            // 
            // tableOscilloscope
            // 
            this.tableOscilloscope.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableOscilloscope.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableOscilloscope.ColumnCount = 2;
            this.tableOscilloscope.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.71844F));
            this.tableOscilloscope.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.28156F));
            this.tableOscilloscope.Controls.Add(this.label29, 0, 3);
            this.tableOscilloscope.Controls.Add(this.btConnection, 0, 0);
            this.tableOscilloscope.Controls.Add(this.lbConnection, 1, 0);
            this.tableOscilloscope.Controls.Add(this.lbOsciloscopePort, 0, 2);
            this.tableOscilloscope.Controls.Add(this.tbPort, 1, 2);
            this.tableOscilloscope.Controls.Add(this.tbIp, 1, 1);
            this.tableOscilloscope.Controls.Add(this.lbOsciloscopeIp, 0, 1);
            this.tableOscilloscope.Controls.Add(this.lbBrand, 0, 4);
            this.tableOscilloscope.Controls.Add(this.cbChannels, 1, 3);
            this.tableOscilloscope.Location = new System.Drawing.Point(6, 93);
            this.tableOscilloscope.Name = "tableOscilloscope";
            this.tableOscilloscope.RowCount = 5;
            this.tableOscilloscope.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableOscilloscope.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableOscilloscope.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableOscilloscope.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableOscilloscope.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableOscilloscope.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableOscilloscope.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableOscilloscope.Size = new System.Drawing.Size(208, 116);
            this.tableOscilloscope.TabIndex = 6;
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(1, 88);
            this.label29.Margin = new System.Windows.Forms.Padding(0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(87, 28);
            this.label29.TabIndex = 14;
            this.label29.Text = "Canais:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btConnection
            // 
            this.btConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btConnection.Location = new System.Drawing.Point(4, 4);
            this.btConnection.Name = "btConnection";
            this.btConnection.Size = new System.Drawing.Size(81, 22);
            this.btConnection.TabIndex = 3;
            this.btConnection.Text = "Conectar";
            this.btConnection.UseVisualStyleBackColor = true;
            this.btConnection.Click += new System.EventHandler(this.btConnection_Click);
            // 
            // lbConnection
            // 
            this.lbConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbConnection.AutoSize = true;
            this.lbConnection.BackColor = System.Drawing.Color.LightSalmon;
            this.lbConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConnection.Location = new System.Drawing.Point(89, 1);
            this.lbConnection.Margin = new System.Windows.Forms.Padding(0);
            this.lbConnection.Name = "lbConnection";
            this.lbConnection.Size = new System.Drawing.Size(118, 28);
            this.lbConnection.TabIndex = 13;
            this.lbConnection.Text = "Desconectado";
            this.lbConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbOsciloscopePort
            // 
            this.lbOsciloscopePort.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbOsciloscopePort.AutoSize = true;
            this.lbOsciloscopePort.BackColor = System.Drawing.Color.Transparent;
            this.lbOsciloscopePort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOsciloscopePort.Location = new System.Drawing.Point(1, 59);
            this.lbOsciloscopePort.Margin = new System.Windows.Forms.Padding(0);
            this.lbOsciloscopePort.Name = "lbOsciloscopePort";
            this.lbOsciloscopePort.Size = new System.Drawing.Size(87, 28);
            this.lbOsciloscopePort.TabIndex = 8;
            this.lbOsciloscopePort.Text = "Porta:";
            this.lbOsciloscopePort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbPort
            // 
            this.tbPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPort.Location = new System.Drawing.Point(92, 62);
            this.tbPort.Multiline = true;
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(111, 22);
            this.tbPort.TabIndex = 3;
            this.tbPort.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbPort_KeyDown);
            this.tbPort.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPort_KeyPress);
            // 
            // tbIp
            // 
            this.tbIp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbIp.Culture = new System.Globalization.CultureInfo("");
            this.tbIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIp.Location = new System.Drawing.Point(92, 33);
            this.tbIp.Mask = "000.000.000.000";
            this.tbIp.Name = "tbIp";
            this.tbIp.Size = new System.Drawing.Size(111, 22);
            this.tbIp.TabIndex = 2;
            // 
            // lbOsciloscopeIp
            // 
            this.lbOsciloscopeIp.AutoSize = true;
            this.lbOsciloscopeIp.BackColor = System.Drawing.Color.Transparent;
            this.lbOsciloscopeIp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbOsciloscopeIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOsciloscopeIp.Location = new System.Drawing.Point(1, 30);
            this.lbOsciloscopeIp.Margin = new System.Windows.Forms.Padding(0);
            this.lbOsciloscopeIp.Name = "lbOsciloscopeIp";
            this.lbOsciloscopeIp.Size = new System.Drawing.Size(87, 28);
            this.lbOsciloscopeIp.TabIndex = 2;
            this.lbOsciloscopeIp.Text = "IP:";
            this.lbOsciloscopeIp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbBrand
            // 
            this.lbBrand.AutoSize = true;
            this.lbBrand.BackColor = System.Drawing.Color.Transparent;
            this.lbBrand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBrand.Location = new System.Drawing.Point(1, 117);
            this.lbBrand.Margin = new System.Windows.Forms.Padding(0);
            this.lbBrand.Name = "lbBrand";
            this.lbBrand.Size = new System.Drawing.Size(87, 20);
            this.lbBrand.TabIndex = 10;
            this.lbBrand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbChannels
            // 
            this.cbChannels.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChannels.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChannels.FormattingEnabled = true;
            this.cbChannels.Items.AddRange(new object[] {
            "Canais 1,2,3",
            "Canais 1,2,4",
            "Canais 1,3,4",
            "Canais 2,3,4"});
            this.cbChannels.Location = new System.Drawing.Point(92, 91);
            this.cbChannels.Name = "cbChannels";
            this.cbChannels.Size = new System.Drawing.Size(112, 23);
            this.cbChannels.TabIndex = 4;
            // 
            // timerForm
            // 
            this.timerForm.Interval = 1000;
            this.timerForm.Tick += new System.EventHandler(this.timerForm_Tick);
            // 
            // FrmTestPdmsThreePhase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(1284, 711);
            this.Controls.Add(this.btGetEquipment);
            this.Controls.Add(this.tbTransformer);
            this.Controls.Add(this.lbTransformer);
            this.Controls.Add(this.pnTest);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmTestPdmsThreePhase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Controle de Ensaio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTestPdmsThreePhase_FormClosed);
            this.Load += new System.EventHandler(this.FrmTestPdmsThreePhase_Load);
            this.pnTest.ResumeLayout(false);
            this.pnGraphics.ResumeLayout(false);
            this.tbGraphs.ResumeLayout(false);
            this.tpSignals.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tpDispersion.ResumeLayout(false);
            this.tlpDispersions.ResumeLayout(false);
            this.tpDispersionDetailed.ResumeLayout(false);
            this.tlpDispersionDetailed.ResumeLayout(false);
            this.tpPartialDischargeTrend.ResumeLayout(false);
            this.tlpPartialDischargeTrend.ResumeLayout(false);
            this.tpFft.ResumeLayout(false);
            this.tlpFft.ResumeLayout(false);
            this.gbTest.ResumeLayout(false);
            this.gbTest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTestTime)).EndInit();
            this.gbTotalDischarges.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.gbPartialDischargePhase3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.gbPartialDischargePhase2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.gbPartialDischargePhase1.ResumeLayout(false);
            this.tpPartialDischargePhase1.ResumeLayout(false);
            this.tpPartialDischargePhase1.PerformLayout();
            this.gbTestController.ResumeLayout(false);
            this.gbTestController.PerformLayout();
            this.gbCalibration.ResumeLayout(false);
            this.tpCalibration.ResumeLayout(false);
            this.tpCalibration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalibrationPhase3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalibrationPhase2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalibrationPhase1)).EndInit();
            this.tableCute.ResumeLayout(false);
            this.tableCute.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrequencyTopCut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrequencyUndercut)).EndInit();
            this.tableFilter.ResumeLayout(false);
            this.tableFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOrder)).EndInit();
            this.tableController.ResumeLayout(false);
            this.tableController.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbGatePhase3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGainPhase1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGatePhase2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGainPhase2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGainPhase3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGatePhase1)).EndInit();
            this.tableOscilloscope.ResumeLayout(false);
            this.tableOscilloscope.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btGetEquipment;
        private System.Windows.Forms.TextBox tbTransformer;
        private System.Windows.Forms.Label lbTransformer;
        private System.Windows.Forms.Panel pnTest;
        private System.Windows.Forms.GroupBox gbTestController;
        private System.Windows.Forms.Button btStop;
        private System.Windows.Forms.ComboBox cbOsciloscope;
        private System.Windows.Forms.Label lbParamters;
        private System.Windows.Forms.TableLayoutPanel tableController;
        private System.Windows.Forms.Label lbPhase1;
        private System.Windows.Forms.Label lbPhase3;
        private System.Windows.Forms.Label lbPhase2;
        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.Label lbOsciloscope;
        private System.Windows.Forms.TableLayoutPanel tableOscilloscope;
        private System.Windows.Forms.Button btConnection;
        private System.Windows.Forms.Label lbConnection;
        private System.Windows.Forms.Label lbOsciloscopePort;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label lbBrand;
        private System.Windows.Forms.MaskedTextBox tbIp;
        private System.Windows.Forms.Label lbOsciloscopeIp;
        private System.Windows.Forms.Panel pnGraphics;
        private System.Windows.Forms.Timer timerForm;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cbChannels;
        private System.Windows.Forms.GroupBox gbTest;
        private System.Windows.Forms.TabControl tbGraphs;
        private System.Windows.Forms.TabPage tpSignals;
        private System.Windows.Forms.TabPage tpDispersion;
        private System.Windows.Forms.TabPage tpDispersionDetailed;
        private System.Windows.Forms.TabPage tpPartialDischargeTrend;
        private System.Windows.Forms.TabPage tpFft;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private ZedGraph.ZedGraphControl gcSignalPhase1;
        private ZedGraph.ZedGraphControl gcSignalFiltredPhase1;
        private ZedGraph.ZedGraphControl gcSignalPhase2;
        private ZedGraph.ZedGraphControl gcSignalFiltredPhase2;
        private ZedGraph.ZedGraphControl gcSignalPhase3;
        private ZedGraph.ZedGraphControl gcSignalFiltredPhase3;
        private System.Windows.Forms.TableLayoutPanel tlpDispersions;
        private ZedGraph.ZedGraphControl gcDispersionPhase1;
        private ZedGraph.ZedGraphControl gcDispersionPhase2;
        private ZedGraph.ZedGraphControl gcDispersionPhase3;
        private System.Windows.Forms.TableLayoutPanel tlpDispersionDetailed;
        private ZedGraph.ZedGraphControl gcDispersionDetailedPhase1;
        private ZedGraph.ZedGraphControl gcDispersionDetailedPhase2;
        private ZedGraph.ZedGraphControl gcDispersionDetailedPhase3;
        private System.Windows.Forms.TableLayoutPanel tlpPartialDischargeTrend;
        private ZedGraph.ZedGraphControl gcPartialDischargeTrendPhase1;
        private ZedGraph.ZedGraphControl gcPartialDischargeTrendPhase2;
        private ZedGraph.ZedGraphControl gcPartialDischargeTrendPhase3;
        private System.Windows.Forms.TableLayoutPanel tlpFft;
        private ZedGraph.ZedGraphControl gcFftPhase1;
        private ZedGraph.ZedGraphControl gcFftPhase2;
        private ZedGraph.ZedGraphControl gcFftPhase3;
        private System.Windows.Forms.Label lbGain;
        private System.Windows.Forms.Label lbGate;
        private System.Windows.Forms.TableLayoutPanel tableCute;
        private System.Windows.Forms.TableLayoutPanel tableFilter;
        private System.Windows.Forms.ComboBox cbFilter;
        private System.Windows.Forms.Label lbFilter;
        private System.Windows.Forms.Label lbOrder;
        private System.Windows.Forms.ComboBox cbFrequencyTopCut;
        private System.Windows.Forms.Label lbTopCut;
        private System.Windows.Forms.ComboBox cbFrequencyUndercut;
        private System.Windows.Forms.Label lbUndercut;
        private System.Windows.Forms.GroupBox gbPartialDischargePhase1;
        private System.Windows.Forms.TableLayoutPanel tpPartialDischargePhase1;
        private System.Windows.Forms.Label lbChargePhase1;
        private System.Windows.Forms.Label lbOccurrencesPhase1;
        private System.Windows.Forms.TextBox tbOccurrencesPhase1;
        private System.Windows.Forms.TextBox tbChargePhase1;
        private System.Windows.Forms.GroupBox gbTotalDischarges;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TextBox tbTotalNegativeDischarges;
        private System.Windows.Forms.Label lbTotalNegativeDischarges;
        private System.Windows.Forms.TextBox tbTotalPositiveDischarges;
        private System.Windows.Forms.GroupBox gbPartialDischargePhase3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox tbOccurrencesPhase3;
        private System.Windows.Forms.Label lbOccurrencesPhase3;
        private System.Windows.Forms.Label lbChargePhase3;
        private System.Windows.Forms.TextBox tbChargePhase3;
        private System.Windows.Forms.GroupBox gbPartialDischargePhase2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox tbOccurrencesPhase2;
        private System.Windows.Forms.Label lbOccurrencesPhase2;
        private System.Windows.Forms.Label lbChargePhase2;
        private System.Windows.Forms.TextBox tbChargePhase2;
        private System.Windows.Forms.Label lbTotalOccurrences;
        private System.Windows.Forms.Label lbTotalPositiveDischarges;
        private System.Windows.Forms.TextBox tbTotalOccurrences;
        private System.Windows.Forms.GroupBox gbCalibration;
        private System.Windows.Forms.TableLayoutPanel tpCalibration;
        private System.Windows.Forms.Label lbCalibrationPhase2;
        private System.Windows.Forms.Label lbCalibrationPhase1;
        private System.Windows.Forms.Label lbDp;
        private System.Windows.Forms.Label lbPhase;
        private System.Windows.Forms.Label lbCalibrationPhase3;
        private System.Windows.Forms.NumericUpDown nudTestTime;
        private System.Windows.Forms.Button btStopTest;
        private System.Windows.Forms.Button btStartTest;
        private System.Windows.Forms.Label lbTimer;
        private System.Windows.Forms.Button btCalibration;
        private System.Windows.Forms.NumericUpDown tbGainPhase1;
        private System.Windows.Forms.NumericUpDown tbGatePhase3;
        private System.Windows.Forms.NumericUpDown tbGatePhase2;
        private System.Windows.Forms.NumericUpDown tbGatePhase1;
        private System.Windows.Forms.NumericUpDown tbGainPhase3;
        private System.Windows.Forms.NumericUpDown tbGainPhase2;
        private System.Windows.Forms.NumericUpDown tbFrequencyUndercut;
        private System.Windows.Forms.NumericUpDown tbOrder;
        private System.Windows.Forms.NumericUpDown tbFrequencyTopCut;
        private System.Windows.Forms.Button btSaveCalibration;
        private System.Windows.Forms.Button btResetConfigurations;
        private System.Windows.Forms.NumericUpDown tbCalibrationPhase1;
        private System.Windows.Forms.NumericUpDown tbCalibrationPhase3;
        private System.Windows.Forms.NumericUpDown tbCalibrationPhase2;
    }
}