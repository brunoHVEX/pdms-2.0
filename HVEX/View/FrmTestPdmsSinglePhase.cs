﻿using HVEX.Controller;
using HVEX.Model;
using HVEX.Service;
using HVEX.View.Interface;
using HVEX.View.SubForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace HVEX.View {
    public partial class FrmTestPdmsSinglePhase : Form, ITestPdmsSinglePhase {

        private string loggedUser;
        TestPdmsSinglePhaseController controller;
        OscilloscopePdms oscilloscope = new OscilloscopePdms();

        #region Interface Attributes
        public string Id { get; set; }
        public string IdConfiguration { get; set; }
        public string EquipmentId { get; set; }
        public string EquipmentDescription {
            get { return (string)tbTransformer.Invoke(new Func<string>(() => tbTransformer.Text)); }
            set {
                tbTransformer.Invoke((MethodInvoker)delegate {
                    tbTransformer.Text = value;
                });
            }
        }
        public string InstrumentId {
            get { return (string)cbOsciloscope.Invoke(new Func<string>(() => cbOsciloscope.SelectedValue.ToString())); }
            set {
                cbOsciloscope.Invoke((MethodInvoker)delegate {
                    cbOsciloscope.Text = value.ToString();
                });
            }
        }
        public string InstrumentConnection {
            get { return (string)lbConnection.Invoke(new Func<string>(() => lbConnection.Text)); }
            set {
                try {
                    if (value == "Conectado") {
                        lbConnection.Invoke((MethodInvoker)delegate {
                            lbConnection.BackColor = System.Drawing.Color.LightGreen;
                            lbConnection.Text = value;
                        });
                        btConnection.Invoke((MethodInvoker)delegate {
                            btConnection.Text = "Desconectar";
                        });
                    } else {
                        lbConnection.Invoke((MethodInvoker)delegate {
                            lbConnection.BackColor = System.Drawing.Color.Salmon;
                            lbConnection.Text = value;
                        });
                        btConnection.Invoke((MethodInvoker)delegate {
                            btConnection.Text = "Conectar";
                        });
                    }
                } catch {

                }
            }
        }
        public string InstrumentIp {
            get { return tbIp.Text; }
            set { tbIp.Text = value; }
        }

        public int InstrumentPort {
            get { return int.Parse(tbPort.Text); }
            set { tbPort.Text = value.ToString(); }
        }
        public DataTable OsciloscopeList {
            set {
                cbOsciloscope.DisplayMember = "Descrição";
                cbOsciloscope.ValueMember = "Id";
                cbOsciloscope.DataSource = value;
            }
        }

        public string Channels {
            get { return (string)cbChannel.Invoke(new Func<string>(() => cbChannel.SelectedItem.ToString())); }
            set { cbChannel.Invoke((MethodInvoker)delegate { cbChannel.SelectedItem = value; }); }
        }

        public WaveForm SignalPhase1 {
            set { SetGraph(gcSignalPhase1, value); }
        }

        public WaveForm SignalFiltredPhase1 {
            set { SetGraph(gcSignalFiltredPhase1, value); }
        }
        
        public List<double>[] DispersionPhase1 {
            set { SetPointsGraph(gcDispersionPhase1, value); }
        }

        public List<double>[] DispersionDetailedPhase1 {
            set { SetPointsGraph(gcDispersionDetailedPhase1, value); }
        }

        public List<double>[] PartialDischargeTrendPhase1 {
            set { SetBarGraph(gcPartialDischargeTrendPhase1, value); }
        }

        public WaveForm FftPhase1 {
            set { SetGraph(gcFftPhase1, value); }
        }

        public double GainPhase1 {
            get { return (double)tbGainPhase1.Invoke(new Func<double>(() => double.Parse(tbGainPhase1.Text))); }
            set {
                tbGainPhase1.Invoke((MethodInvoker)delegate {
                    tbGainPhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }

        public double GatePhase1 {
            get { return (double)tbGatePhase1.Invoke(new Func<double>(() => double.Parse(tbGatePhase1.Text))); }
            set {
                tbGatePhase1.Invoke((MethodInvoker)delegate {
                    tbGatePhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }

        public string Filter {
            get {
                return (string)cbFilter.Invoke(new Func<string>(() => cbFilter.SelectedItem.ToString()));
            }
            set { cbFilter.Invoke((MethodInvoker)delegate { cbFilter.SelectedItem = value; }); }
        }
        public double Order {
            get { return (double)tbOrder.Invoke(new Func<double>(() => double.Parse(tbOrder.Text))); }
            set {
                tbOrder.Invoke((MethodInvoker)delegate {
                    tbOrder.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double FrequencyUndercut {
            get { return (double)tbFrequencyUndercut.Invoke(new Func<double>(() => double.Parse(tbFrequencyUndercut.Text))); }
            set {
                tbFrequencyUndercut.Invoke((MethodInvoker)delegate {
                    tbFrequencyUndercut.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public string UnitFrequencyUndercut {
            get { return (string)cbFrequencyUndercut.Invoke(new Func<string>(() => cbFrequencyUndercut.SelectedItem.ToString())); }
            set { cbFrequencyUndercut.Invoke((MethodInvoker)delegate { cbFrequencyUndercut.SelectedItem = value; }); }
        }
        public double FrequencyTopCut {
            get { return (double)tbFrequencyTopCut.Invoke(new Func<double>(() => double.Parse(tbFrequencyTopCut.Text))); }
            set {
                tbFrequencyTopCut.Invoke((MethodInvoker)delegate {
                    tbFrequencyTopCut.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public string UnitFrequencyTopCut {
            get { return (string)cbFrequencyTopCut.Invoke(new Func<string>(() => cbFrequencyTopCut.SelectedItem.ToString())); }
            set { cbFrequencyTopCut.Invoke((MethodInvoker)delegate { cbFrequencyTopCut.SelectedItem = value; }); }
        }
        public double ChargePhase1 {
            get { return (double)tbChargePhase1.Invoke(new Func<double>(() => double.Parse(tbChargePhase1.Text))); }
            set {
                tbChargePhase1.Invoke((MethodInvoker)delegate {
                    tbChargePhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double OccurrencesPhase1 {
            get { return (double)tbOccurrencesPhase1.Invoke(new Func<double>(() => double.Parse(tbOccurrencesPhase1.Text))); }
            set {
                tbOccurrencesPhase1.Invoke((MethodInvoker)delegate {
                    tbOccurrencesPhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }

        public double TotalPositiveDischarges {
            get { return (double)tbTotalPositiveDischarges.Invoke(new Func<double>(() => double.Parse(tbTotalPositiveDischarges.Text))); }
            set {
                tbTotalPositiveDischarges.Invoke((MethodInvoker)delegate {
                    tbTotalPositiveDischarges.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double TotalNegativeDischarges {
            get { return (double)tbTotalNegativeDischarges.Invoke(new Func<double>(() => double.Parse(tbTotalNegativeDischarges.Text))); }
            set {
                tbTotalNegativeDischarges.Invoke((MethodInvoker)delegate {
                    tbTotalNegativeDischarges.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double TotalOccurrences {
            get { return (double)tbTotalOccurrences.Invoke(new Func<double>(() => double.Parse(tbTotalOccurrences.Text))); }
            set {
                tbTotalOccurrences.Invoke((MethodInvoker)delegate {
                    tbTotalOccurrences.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double CalibrationPhase1 {
            get { return (double)tbCalibrationPhase1.Invoke(new Func<double>(() => double.Parse(tbCalibrationPhase1.Text))); }
            set {
                tbCalibrationPhase1.Invoke((MethodInvoker)delegate {
                    tbCalibrationPhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }

        public bool Osciloscope { get; set; }
        #endregion

        public FrmTestPdmsSinglePhase(string userId) {
            loggedUser = userId;
            controller = new TestPdmsSinglePhaseController(this);
            InitializeComponent();
        }

        #region Events
        private async void FrmTestPdmsSinglePhase_Load(object sender, EventArgs e) {
            await controller.ListOsciloscopes(loggedUser);
            if (Osciloscope) {
                cbOsciloscope.SelectedIndex = 0;
                cbChannel.SelectedIndex = 0;
            } else {
                tbIp.Enabled = false;
                tbPort.Enabled = false;
                cbChannel.Enabled = false;
                btConnection.Enabled = false;
            }
            await ReadConfigurationAsync();
            timerForm.Start();
        }
        private void btGetEquipment_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmChoseEquipment")) {
                FrmChoseEquipment frm = new FrmChoseEquipment(loggedUser, this);
                frm.Show();
            }
        }
        private async void cbOsciloscope_SelectedIndexChanged(object sender, EventArgs e) {
            CleanInstrument();
            if (cbOsciloscope.SelectedIndex != -1) {
                await controller.Disconnect(loggedUser);
                await controller.GetOsciloscope(loggedUser, cbOsciloscope.SelectedValue.ToString());
            }
        }
        private async void FrmTestPdmsSinglePhase_FormClosed(object sender, FormClosedEventArgs e) {
            try {
                await controller.Disconnect(loggedUser);
                FormHandler.CloseForm();
            } catch (Exception ex) {
                Console.WriteLine(ex);
            }
        }
        private async void btConnection_Click(object sender, EventArgs e) {
            Cursor.Current = Cursors.WaitCursor;
            if (btConnection.Text == "Conectar") {
                await controller.Connect(loggedUser);
            } else {
                await controller.Disconnect(loggedUser);
            }
            Cursor.Current = Cursors.Default;
        }
        private void timerForm_Tick(object sender, EventArgs e) {
            if (!string.IsNullOrEmpty(tbTransformer.Text)) {
                tbTransformer.BackColor = System.Drawing.SystemColors.Window;
            }
        }
        #endregion

        #region Functions
        private void CleanInstrument() {
            tbIp.Text = "";
            tbPort.Text = "";
        }

        public void SetGraph(ZedGraph.ZedGraphControl graph, WaveForm value) {
            graph.GraphPane.CurveList.Clear();
            graph.GraphPane.AddCurve(
                null,
                value.x.ToArray(),
                value.y.ToArray(),
                System.Drawing.Color.Black,
                ZedGraph.SymbolType.None
            );
            graph.AxisChange();
            graph.Invalidate();
        }

        public void SetPointsGraph(ZedGraph.ZedGraphControl graph, List<double>[] value) {
            graph.GraphPane.CurveList.Clear();
            LineItem curve = graph.GraphPane.AddCurve(
                                                null,
                                                value[0].ToArray(),
                                                value[1].ToArray(),
                                                System.Drawing.Color.Black,
                                                ZedGraph.SymbolType.Circle
                                                );
            curve.Line.IsVisible = false;
            graph.GraphPane.AddCurve(
                                    null,
                                    value[2].ToArray(),
                                    value[3].ToArray(),
                                    System.Drawing.Color.Gray,
                                    ZedGraph.SymbolType.None);
            graph.AxisChange();
            graph.Invalidate();
        }

        public void SetBarGraph(ZedGraph.ZedGraphControl graph, List<double>[] value) {
            graph.GraphPane.CurveList.Clear();
            graph.GraphPane.AddBar(
                                    null,
                                    value[0].ToArray(),
                                    value[1].ToArray(),
                                    System.Drawing.Color.Black);
            graph.AxisChange();
            graph.Invalidate();
        }
        #endregion

        private async void btStart_Click(object sender, EventArgs e) {
            StartTest();
            bool result = await Task.Run(() => controller.RunTest(loggedUser));
        }

        private void btStop_Click(object sender, EventArgs e) {
            StopTest();
        }

        private void StartTest() {
            controller.TestRunning = true;
            //testTimer.Reset();
            //testTimer.Start();
        }
        private async void StopTest() {
            controller.TestRunning = false;
            await controller.StopTest(loggedUser);
            //testTimer.Stop();
        }

        private void btCalibration_Click(object sender, EventArgs e) {
            oscilloscope.calibrating = true;
            if (!string.IsNullOrEmpty(tbCalibrationPhase1.Text)) {
                controller.UpGain(Convert.ToDouble(tbCalibrationPhase1.Text), 1);
            }
            oscilloscope.calibrating = false;
        }

        private async void btSaveCalibration_Click(object sender, EventArgs e) {
            if (await controller.SaveConfigurations(loggedUser)) {
                MessageBox.Show("As configurações foram registradas com sucesso!");
            }
        }

        private async Task ReadConfigurationAsync() {
            ConfigurationSinglePhase configurationSinglePhase = new ConfigurationSinglePhase();
            await configurationSinglePhase.Read();
            IdConfiguration = configurationSinglePhase.Id.ToString();
            tbGainPhase1.Value = Convert.ToDecimal(configurationSinglePhase.Gain);
            tbGatePhase1.Value = Convert.ToDecimal(configurationSinglePhase.Gate);
            cbFilter.SelectedIndex = configurationSinglePhase.Filter;
            tbOrder.Value = Convert.ToDecimal(configurationSinglePhase.Order);
            tbFrequencyUndercut.Value = Convert.ToDecimal(configurationSinglePhase.FrequencyUndercut[0]);
            cbFrequencyUndercut.SelectedIndex = Convert.ToInt32(configurationSinglePhase.FrequencyUndercut[1]);
            tbFrequencyTopCut.Value = Convert.ToDecimal(configurationSinglePhase.FrequencyTopCut[0]);
            cbFrequencyTopCut.SelectedIndex = Convert.ToInt32(configurationSinglePhase.FrequencyTopCut[1]);
            tbCalibrationPhase1.Value = Convert.ToDecimal(configurationSinglePhase.Calibration);
        }

        private async void btResetConfigurations_Click(object sender, EventArgs e) {
            if (await controller.ResetConfigurations(loggedUser)) {
                MessageBox.Show("As configurações foram resetadas com sucesso!");
            }
        }
    }
}
