﻿using HVEX.Controller;
using HVEX.Service;
using HVEX.View.Interface;
using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmLogger : Form, ILog {

        private LoggerController controller;

        #region Interface Attributes
        public string Id { get; set; }

        public string Description { get; set; }

        public string Error { get; set; }

        public string Source { get; set; }

        public DateTime Date { get; set; }

        public string User { get; set; }

        public DataTable List {
            set {
                BindingSource binding = new BindingSource();
                binding.DataSource = value;
                dgLog.DataSource = binding;
                dgLog.Columns[0].Visible = false;
            }
        }
        #endregion 

        public FrmLogger() {
            InitializeComponent();
            controller = new LoggerController(this);
        }

        #region Events
        private async void FrmLogger_Load(object sender, EventArgs e) {
            await LoadLogs();
        }
        private async void tbSearch_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadLogs();
            }
        }
        private async void tbSince_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadLogs();
            }
        }
        private async void tbUntil_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadLogs();
            }
        }
        private async void dgLog_MouseDoubleClick(object sender, MouseEventArgs e) {
            Id = dgLog.SelectedRows[0].Cells[0].Value.ToString();
            await controller.Read();
        }
        private void dgLog_DataSourceChanged(object sender, EventArgs e) {
            HighlightErrors();
        }
        private void dgLog_Sorted(object sender, EventArgs e) {
            HighlightErrors();
        }
        private async void btSearch_Click(object sender, EventArgs e) {
            await LoadLogs();
        }
        #endregion

        #region Functions
        public async Task LoadLogs() {
            CleanDataGrid();
            if (
                ValidationHandler.IsValidDateTime(tbSince) &&
                ValidationHandler.IsValidDateTime(tbUntil)
            ) {
                DateTime? since = null;
                DateTime? until = null;
                string order = "Data";
                if (tbSince.MaskCompleted) {
                    since = DateTime.Parse(tbSince.Text, CultureInfo.CurrentCulture);
                }
                if (tbUntil.MaskCompleted) {
                    until = DateTime.Parse(
                        tbUntil.Text, CultureInfo.CurrentCulture
                    ).Add(new TimeSpan(23, 59, 59));
                }
                if (cbOrder.SelectedIndex != -1) {
                    order = cbOrder.SelectedItem.ToString();
                }
                await controller.List(tbSearch.Text, since, until, order);
            } else {
                new FormException("Entre com uma data válida.");
            }
        }
        private void CleanDataGrid() {
            dgLog.DataSource = null;
        }
        private void HighlightErrors() {
            foreach (DataGridViewRow row in dgLog.Rows) {
                if (row.Cells[1].Value.ToString().Contains("ERRO AO TENTAR")) {
                    row.DefaultCellStyle.ForeColor = Color.Red;
                }
            }
        }

        #endregion
    }
}
