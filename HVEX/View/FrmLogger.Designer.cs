﻿
namespace HVEX.View {
    partial class FrmLogger {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogger));
            this.pnLogger = new System.Windows.Forms.Panel();
            this.dgLog = new System.Windows.Forms.DataGridView();
            this.btSearch = new System.Windows.Forms.Button();
            this.cbOrder = new System.Windows.Forms.ComboBox();
            this.tbUntil = new System.Windows.Forms.MaskedTextBox();
            this.tbSince = new System.Windows.Forms.MaskedTextBox();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.lbOrder = new System.Windows.Forms.Label();
            this.lbUntil = new System.Windows.Forms.Label();
            this.lbSince = new System.Windows.Forms.Label();
            this.lbSearche = new System.Windows.Forms.Label();
            this.pnLogger.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).BeginInit();
            this.SuspendLayout();
            // 
            // pnLogger
            // 
            this.pnLogger.BackColor = System.Drawing.Color.White;
            this.pnLogger.Controls.Add(this.dgLog);
            this.pnLogger.Controls.Add(this.btSearch);
            this.pnLogger.Controls.Add(this.cbOrder);
            this.pnLogger.Controls.Add(this.tbUntil);
            this.pnLogger.Controls.Add(this.tbSince);
            this.pnLogger.Controls.Add(this.tbSearch);
            this.pnLogger.Controls.Add(this.lbOrder);
            this.pnLogger.Controls.Add(this.lbUntil);
            this.pnLogger.Controls.Add(this.lbSince);
            this.pnLogger.Controls.Add(this.lbSearche);
            this.pnLogger.Location = new System.Drawing.Point(12, 12);
            this.pnLogger.Name = "pnLogger";
            this.pnLogger.Size = new System.Drawing.Size(760, 417);
            this.pnLogger.TabIndex = 1;
            // 
            // dgLog
            // 
            this.dgLog.AllowUserToAddRows = false;
            this.dgLog.AllowUserToDeleteRows = false;
            this.dgLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgLog.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgLog.Location = new System.Drawing.Point(13, 52);
            this.dgLog.Name = "dgLog";
            this.dgLog.ReadOnly = true;
            this.dgLog.Size = new System.Drawing.Size(734, 352);
            this.dgLog.TabIndex = 0;
            this.dgLog.DataSourceChanged += new System.EventHandler(this.dgLog_DataSourceChanged);
            this.dgLog.Sorted += new System.EventHandler(this.dgLog_Sorted);
            this.dgLog.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dgLog_MouseDoubleClick);
            // 
            // btSearch
            // 
            this.btSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btSearch.FlatAppearance.BorderSize = 0;
            this.btSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSearch.ForeColor = System.Drawing.Color.White;
            this.btSearch.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btSearch.Location = new System.Drawing.Point(667, 26);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(80, 21);
            this.btSearch.TabIndex = 5;
            this.btSearch.Text = "Pesquisar";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.UseVisualStyleBackColor = false;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // cbOrder
            // 
            this.cbOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrder.DropDownWidth = 136;
            this.cbOrder.FormattingEnabled = true;
            this.cbOrder.Location = new System.Drawing.Point(525, 26);
            this.cbOrder.Name = "cbOrder";
            this.cbOrder.Size = new System.Drawing.Size(136, 21);
            this.cbOrder.TabIndex = 4;
            // 
            // tbUntil
            // 
            this.tbUntil.Location = new System.Drawing.Point(419, 26);
            this.tbUntil.Mask = "00/00/0000";
            this.tbUntil.Name = "tbUntil";
            this.tbUntil.Size = new System.Drawing.Size(100, 20);
            this.tbUntil.TabIndex = 3;
            this.tbUntil.ValidatingType = typeof(System.DateTime);
            this.tbUntil.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbUntil_KeyDown);
            // 
            // tbSince
            // 
            this.tbSince.Location = new System.Drawing.Point(313, 26);
            this.tbSince.Mask = "00/00/0000";
            this.tbSince.Name = "tbSince";
            this.tbSince.Size = new System.Drawing.Size(100, 20);
            this.tbSince.TabIndex = 2;
            this.tbSince.ValidatingType = typeof(System.DateTime);
            this.tbSince.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSince_KeyDown);
            // 
            // tbSearch
            // 
            this.tbSearch.Location = new System.Drawing.Point(13, 26);
            this.tbSearch.Multiline = true;
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(294, 21);
            this.tbSearch.TabIndex = 1;
            this.tbSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSearch_KeyDown);
            // 
            // lbOrder
            // 
            this.lbOrder.AutoSize = true;
            this.lbOrder.Location = new System.Drawing.Point(522, 10);
            this.lbOrder.Name = "lbOrder";
            this.lbOrder.Size = new System.Drawing.Size(38, 13);
            this.lbOrder.TabIndex = 5;
            this.lbOrder.Text = "Ordem";
            // 
            // lbUntil
            // 
            this.lbUntil.AutoSize = true;
            this.lbUntil.Location = new System.Drawing.Point(416, 10);
            this.lbUntil.Name = "lbUntil";
            this.lbUntil.Size = new System.Drawing.Size(23, 13);
            this.lbUntil.TabIndex = 9;
            this.lbUntil.Text = "Até";
            // 
            // lbSince
            // 
            this.lbSince.AutoSize = true;
            this.lbSince.Location = new System.Drawing.Point(310, 10);
            this.lbSince.Name = "lbSince";
            this.lbSince.Size = new System.Drawing.Size(38, 13);
            this.lbSince.TabIndex = 8;
            this.lbSince.Text = "Desde";
            // 
            // lbSearche
            // 
            this.lbSearche.AutoSize = true;
            this.lbSearche.Location = new System.Drawing.Point(10, 10);
            this.lbSearche.Name = "lbSearche";
            this.lbSearche.Size = new System.Drawing.Size(55, 13);
            this.lbSearche.TabIndex = 1;
            this.lbSearche.Text = "Descrição";
            // 
            // FrmLogger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(784, 441);
            this.Controls.Add(this.pnLogger);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmLogger";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Logs do Sistema";
            this.Load += new System.EventHandler(this.FrmLogger_Load);
            this.pnLogger.ResumeLayout(false);
            this.pnLogger.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnLogger;
        private System.Windows.Forms.DataGridView dgLog;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.ComboBox cbOrder;
        private System.Windows.Forms.MaskedTextBox tbUntil;
        private System.Windows.Forms.MaskedTextBox tbSince;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label lbOrder;
        private System.Windows.Forms.Label lbUntil;
        private System.Windows.Forms.Label lbSince;
        private System.Windows.Forms.Label lbSearche;
    }
}