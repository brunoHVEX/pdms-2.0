﻿using HVEX.Controller;
using HVEX.Service;
using HVEX.View.Interface;
using HVEX.View.SubForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmTestFra : Form, ITestFra {

        private string loggedUser;
        private bool testing;
        TestFraController controller;
        Stopwatch testTimer = new Stopwatch();

        #region Interface Attributes
        public string Id { get; set; }
        public List<double>[] InputSignal {
            set { SetGraph(graphInput, value); }
        }
        public List<double>[] OutputSignal {
            set { SetGraph(graphOutput, value); }
        }
        public List<double>[] AmplitudeSignal {
            set { SetGraph(graphAmplitude, value); }
        }
        public List<double>[] PhaseSignal {
            set { SetGraph(graphPhase, value); }
        }
        public double MinAmplitude {
            get {
                //ValidarCampo(tbMinAmplitude);
                return (double)tbMinAmplitude.Invoke(new Func<double>(() => double.Parse(tbMinAmplitude.Text, CultureInfo.InstalledUICulture)));
            }
            set {
                tbMinAmplitude.Invoke((MethodInvoker)delegate {
                    //ValidarCampo(tbMinAmplitude);
                });
            }
        }
        public double MaxAmplitude {
            get {
                //ValidarCampo(tbMaxAmplitude);
                return (double)tbMaxAmplitude.Invoke(new Func<double>(() => double.Parse(tbMaxAmplitude.Text, CultureInfo.InvariantCulture)));
            }
            set {
                tbMaxAmplitude.Invoke((MethodInvoker)delegate {
                    //ValidarCampo(tbMaxAmplitude);
                });
            }
        }
        public double MinFrequency {
            get {
                return (double)tbMinFrequency.Invoke(new Func<double>(() => double.Parse(tbMinFrequency.Text)));
            }
            set {
                tbMinFrequency.Invoke((MethodInvoker)delegate {
                    tbMinFrequency.Text = value.ToString();
                });
            }
        }

        public string MinFrequencyMeasurement {
            get {
                return (string)cbMinFrequencyMeasurement.Invoke(
                    new Func<string>(() => cbMinFrequencyMeasurement.SelectedItem.ToString())
                );
            }
            set {
                cbMinFrequencyMeasurement.Invoke((MethodInvoker)delegate {
                    cbMinFrequencyMeasurement.SelectedItem = value;
                });
            }
        }

        public double MaxFrequency {
            get { return (double)tbMaxFrequency.Invoke(new Func<double>(() => double.Parse(tbMaxFrequency.Text))); }
            set {
                tbMaxFrequency.Invoke((MethodInvoker)delegate {
                    tbMaxFrequency.Text = value.ToString();
                });
            }
        }

        public string MaxFrequencyMeasurement {
            get {
                return (string)cbMaxFrequencyMeasurement.Invoke(
                  new Func<string>(() => cbMaxFrequencyMeasurement.SelectedItem.ToString())
                );
            }
            set {
                cbMaxFrequencyMeasurement.Invoke((MethodInvoker)delegate {
                    cbMaxFrequencyMeasurement.SelectedItem = value;
                });
            }
        }

        public string CurrentStatus {
            get {
                return (string)lbCurrentStatus.Invoke(
                  new Func<string>(() => lbCurrentStatus.Text.ToString())
                );
            }
            set {
                lbCurrentStatus.Invoke((MethodInvoker)delegate {
                    lbCurrentStatus.Text = value;
                });
            }
        }

        public int Samples {
            get { return (int)tbSamples.Invoke(new Func<int>(() => int.Parse(tbSamples.Text))); }
            set {
                tbSamples.Invoke((MethodInvoker)delegate {
                    tbSamples.Text = value.ToString();
                });
            }
        }

        public int AnalysisTotal {
            set {
                pbSignalAnalysis.Invoke((MethodInvoker)delegate {
                    pbSignalAnalysis.Maximum = value;
                });
            }
        }
        public int AnalysisProgress {
            set {
                pbSignalAnalysis.Invoke((MethodInvoker)delegate {
                    pbSignalAnalysis.Value = value;
                });
            }
        }

        public double CurrentFrequency {
            get { return (double)lbCurrentFrequency.Invoke(new Func<double>(() => double.Parse(lbCurrentFrequency.Text))); }
            set {
                lbCurrentFrequency.Invoke((MethodInvoker)delegate {
                    lbCurrentFrequency.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR")) + " kHz";
                });
            }
        }

        public double InjectedVoltage {
            get { return (double)lbInjectVoltage.Invoke(new Func<double>(() => double.Parse(lbInjectVoltage.Text))); }
            set {
                lbInjectVoltage.Invoke((MethodInvoker)delegate {
                    lbInjectVoltage.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR")) + " Vpp";
                });
            }
        }

        public double CurrentInput {
            get { return (double)lbCurrentInput.Invoke(new Func<double>(() => double.Parse(lbCurrentInput.Text))); }
            set {
                lbCurrentInput.Invoke((MethodInvoker)delegate {
                    lbCurrentInput.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR")) + " Vpp";
                });
            }
        }

        public double CurrentOutput {
            get { return (double)lbCurrentOutput.Invoke(new Func<double>(() => double.Parse(lbCurrentOutput.Text))); }
            set {
                lbCurrentOutput.Invoke((MethodInvoker)delegate {
                    lbCurrentOutput.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR")) + " Vpp";
                });
            }
        }

        public double CurrentResult {
            get { return (double)lbCurrentResult.Invoke(new Func<double>(() => double.Parse(lbCurrentResult.Text))); }
            set {
                lbCurrentResult.Invoke((MethodInvoker)delegate {
                    lbCurrentResult.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR")) + " dB";
                });
            }
        }

        public double CurrentPhase {
            get { return (double)lbCurrentPhase.Invoke(new Func<double>(() => double.Parse(lbCurrentPhase.Text))); }
            set {
                lbCurrentPhase.Invoke((MethodInvoker)delegate {
                    lbCurrentPhase.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR")) + " °";
                });
            }
        }

        public int CurrentSample {
            get { return (int)lbCurrentSample.Invoke(new Func<int>(() => int.Parse(lbCurrentSample.Text))); }
            set {
                lbCurrentSample.Invoke((MethodInvoker)delegate {
                    lbCurrentSample.Text = value.ToString();

                });
            }
        }

        public string EquipmentId { get; set; }

        public string EquipmentDescription {
            get { return (string)tbTransformer.Invoke(new Func<string>(() => tbTransformer.Text)); }
            set {
                tbTransformer.Invoke((MethodInvoker)delegate {
                    tbTransformer.Text = value;
                });
            }
        }

        public string InstrumentConnection {
            get { return (string)lbConnection.Invoke(new Func<string>(() => lbConnection.Text)); }
            set {
                try {
                    if (value == "Conectado") {
                        lbConnection.Invoke((MethodInvoker)delegate {
                            lbConnection.BackColor = System.Drawing.Color.LightGreen;
                            lbConnection.Text = value;
                        });
                        btConnection.Invoke((MethodInvoker)delegate {
                            btConnection.Text = "Desconectar";
                        });
                    } else {
                        lbConnection.Invoke((MethodInvoker)delegate {
                            lbConnection.BackColor = System.Drawing.Color.Salmon;
                            lbConnection.Text = value;
                        });
                        btConnection.Invoke((MethodInvoker)delegate {
                            btConnection.Text = "Conectar";
                        });
                    }
                } catch {

                }
            }
        }

        public string InstrumentId {
            get { return (string)cbOsciloscope.Invoke(new Func<string>(() => cbOsciloscope.SelectedValue.ToString())); }
            set {
                cbOsciloscope.Invoke((MethodInvoker)delegate {
                    cbOsciloscope.Text = value.ToString();
                });
            }
        }

        public string InstrumentIp {
            get { return tbIp.Text; }
            set { tbIp.Text = value; }
        }

        public int InstrumentPort {
            get { return int.Parse(tbPort.Text); }
            set { tbPort.Text = value.ToString(); }
        }

        public DateTime Date { get; set; }

        public DataTable List {
            set {

            }
        }

        public DataTable OsciloscopeList {
            set {
                cbOsciloscope.DisplayMember = "Descrição";
                cbOsciloscope.ValueMember = "Id";
                cbOsciloscope.DataSource = value;
            }
        }
        #endregion

        public FrmTestFra(string userId) {
            loggedUser = userId;
            controller = new TestFraController(this);
            InitializeComponent();
        }

        #region Events
        private async void FrmTestFra_Load(object sender, EventArgs e) {
            pbSignalAnalysis.Minimum = 0;
            graphInput.GraphPane.Title.Text = "Entrada";
            graphInput.GraphPane.XAxis.Title.Text = "Tempo (s)";
            graphInput.GraphPane.YAxis.Title.Text = "Tensão (V)";
            graphOutput.GraphPane.Title.Text = "Saída";
            graphOutput.GraphPane.XAxis.Title.Text = "Tempo (s)";
            graphOutput.GraphPane.YAxis.Title.Text = "Tensão (V)";
            graphAmplitude.GraphPane.Title.Text = "Ganho";
            graphAmplitude.GraphPane.XAxis.Title.Text = "Frequência (Hz)";
            graphAmplitude.GraphPane.XAxis.Type = ZedGraph.AxisType.Log;
            graphAmplitude.GraphPane.YAxis.Title.Text = "Ganho (dB)";
            graphPhase.GraphPane.Title.Text = "Fase";
            graphPhase.GraphPane.XAxis.Title.Text = "Frequência (Hz)";
            graphPhase.GraphPane.XAxis.Type = ZedGraph.AxisType.Log;
            graphPhase.GraphPane.YAxis.Title.Text = "Fase (°)";
            cbMinFrequencyMeasurement.SelectedItem = "Hz";
            cbMaxFrequencyMeasurement.SelectedItem = "MHz";
            await controller.ListOsciloscopes(loggedUser);
            cbOsciloscope.SelectedIndex = 0;
            timerForm.Start();
        }
        private async void FrmTestFra_FormClosed(object sender, FormClosedEventArgs e) {
            try {
                await controller.Disconnect(loggedUser);
                FormHandler.CloseForm();
            } catch (Exception ex) {
                Console.WriteLine(ex);
            }
        }
        private async void cbOsciloscope_SelectedIndexChanged(object sender, EventArgs e) {
            CleanInstrument();
            if (cbOsciloscope.SelectedIndex != -1) {
                await controller.Disconnect(loggedUser);
                await controller.GetOsciloscope(loggedUser, cbOsciloscope.SelectedValue.ToString());
            }
        }
        private void timerForm_Tick(object sender, EventArgs e) {
            if (!string.IsNullOrEmpty(tbTransformer.Text)) {
                tbTransformer.BackColor = System.Drawing.SystemColors.Window;
            }
            if (InstrumentConnection == "Conectado" && !string.IsNullOrEmpty(EquipmentId)) {
                if (!testing) {
                    btStart.Enabled = true;
                    btStop.Enabled = false;
                    tbIp.Enabled = true;
                    tbPort.Enabled = true;
                    tbMinFrequency.Enabled = true;
                    tbMaxFrequency.Enabled = true;
                    btGetEquipment.Enabled = true;
                    tbSamples.Enabled = true;
                    btConnection.Enabled = true;
                    cbOsciloscope.Enabled = true;
                    cbMinFrequencyMeasurement.Enabled = true;
                    cbMaxFrequencyMeasurement.Enabled = true;
                    tbMinAmplitude.Enabled = true;
                    tbMaxAmplitude.Enabled = true;
                } else {
                    btStart.Enabled = false;
                    btStop.Enabled = true;
                    tbIp.Enabled = false;
                    tbPort.Enabled = false;
                    tbMinFrequency.Enabled = false;
                    tbMaxFrequency.Enabled = false;
                    btGetEquipment.Enabled = false;
                    tbSamples.Enabled = false;
                    btConnection.Enabled = false;
                    cbOsciloscope.Enabled = false;
                    cbMinFrequencyMeasurement.Enabled = false;
                    cbMaxFrequencyMeasurement.Enabled = false;
                    tbMinAmplitude.Enabled = false;
                    tbMaxAmplitude.Enabled = false;
                }
            } else {
                btStart.Enabled = true;
                btStop.Enabled = false;
                tbIp.Enabled = true;
                tbPort.Enabled = true;
                tbMinFrequency.Enabled = true;
                tbMaxFrequency.Enabled = true;
                btGetEquipment.Enabled = true;
                tbSamples.Enabled = true;
                btConnection.Enabled = true;
                cbOsciloscope.Enabled = true;
                cbMinFrequencyMeasurement.Enabled = true;
                cbMaxFrequencyMeasurement.Enabled = true;
                tbMinAmplitude.Enabled = true;
                tbMaxAmplitude.Enabled = true;
            }
            if (testTimer.IsRunning) {
                lbTime.Text = testTimer.Elapsed.ToString("mm\\:ss");
            }
        }
        private void tbPort_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbPort_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbMinFrequency_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbMinFrequency_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbMaxFrequency_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbMaxFrequency_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbMinAmplitude_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbMinAmplitude_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbMaxAmplitude_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbMaxAmplitude_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbSamples_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbSamples_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsInteger(e);
        }
        private async void btStart_Click(object sender, EventArgs e) {
            CurrentStatus = "Ensaio Iniciado";
            StopTest();
            StartTest();
            bool result = await Task.Run(() => controller.RunTest(loggedUser));
            testing = false;
            testTimer.Stop();
            if (result) {
                MessageBox.Show("O ensaio foi finalizado e registrado com sucesso!");
            } else {
                MessageBox.Show("O ensaio foi interrompido e não pôde ser registrado.");
            }
        }
        private void btStop_Click(object sender, EventArgs e) {
            CurrentStatus = "Ensaio Finalizado";
            StopTest();
        }
        private async void btConnection_Click(object sender, EventArgs e) {
            Cursor.Current = Cursors.WaitCursor;
            if (btConnection.Text == "Conectar") {
                await controller.Connect(loggedUser);
            } else {
                await controller.Disconnect(loggedUser);
            }
            Cursor.Current = Cursors.Default;
        }
        private void rbLog_CheckedChanged(object sender, EventArgs e) {
            graphAmplitude.GraphPane.XAxis.Type = ZedGraph.AxisType.Log;
            graphAmplitude.AxisChange();
            graphAmplitude.Invalidate();
            graphPhase.GraphPane.XAxis.Type = ZedGraph.AxisType.Log;
            graphPhase.AxisChange();
            graphPhase.Invalidate();
        }
        private void rbHz_CheckedChanged(object sender, EventArgs e) {
            graphAmplitude.GraphPane.XAxis.Type = ZedGraph.AxisType.Linear;
            graphAmplitude.AxisChange();
            graphAmplitude.Invalidate();
            graphPhase.GraphPane.XAxis.Type = ZedGraph.AxisType.Linear;
            graphPhase.AxisChange();
            graphPhase.Invalidate();
        }
        private void btGetEquipment_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmChoseTrafo")) {
                FrmChoseEquipment frm = new FrmChoseEquipment(loggedUser, this);
                frm.Show();
            }
        }
        #endregion

        #region Functions
        private void CleanInstrument() {
            tbIp.Text = "";
            tbPort.Text = "";
        }
        public void SetGraph(ZedGraph.ZedGraphControl graph, List<double>[] value) {
            System.Drawing.Color color = new System.Drawing.Color();
            bool graphSituation = true;
            if (graph.Name == "graphInput") {
                color = System.Drawing.Color.Orange;
            } else if (graph.Name == "graphOutput") {
                color = System.Drawing.Color.Green;
            } else if (graph.Name == "graphAmplitude") {
                color = System.Drawing.Color.Red;
            } else if (graph.Name == "graphInputOutputRms") {
                graphSituation = false;
            } else {
                color = System.Drawing.Color.Blue;
            }
            graph.Invoke((MethodInvoker)delegate {
                graph.GraphPane.CurveList.Clear();
                if (graphSituation) {
                    for (int i = 0; i < value.Length - 1; i++) {
                        graph.GraphPane.AddCurve(
                            null,
                            value[0].ToArray(),
                            value[1].ToArray(),
                            color,
                            ZedGraph.SymbolType.None
                        );
                        graph.AxisChange();
                        graph.Invalidate();
                    }
                } else {
                    graph.GraphPane.AddCurve(
                        "Tensão de entrada (Vrms)",
                        value[0].ToArray(),
                        value[1].ToArray(),
                        System.Drawing.Color.Orange,
                        ZedGraph.SymbolType.None
                    );
                    graph.GraphPane.AddCurve(
                        "Corrente de saída (Arms)",
                        value[0].ToArray(),
                        value[2].ToArray(),
                        System.Drawing.Color.Green,
                        ZedGraph.SymbolType.None
                    );
                    graph.AxisChange();
                    graph.Invalidate();
                }
            });
        }
        private void StartTest() {
            testing = true;
            controller.TestRunning = true;
            testTimer.Reset();
            testTimer.Start();
        }
        private async void StopTest() {
            testing = false;
            await controller.StopTest();
            testTimer.Stop();
        }
        public void ValidarCampo(TextBox txt) {
            string temp;
            double value = 0;
            temp = txt.Text;

            if (temp.Length > 0) {
                temp = temp.Replace(',', '.');
                txt.Text = temp;
                value = Convert.ToDouble(temp, CultureInfo.InvariantCulture);
            } else {
                //txt.Text = "0";
            }
        }
        #endregion 

    }
}
