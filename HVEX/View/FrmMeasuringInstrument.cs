﻿using HVEX.Controller;
using HVEX.Service;
using HVEX.View.Interface;
using RohdeSchwarz.RsInstrument;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmMeasuringInstrument : Form,IOscilloscope, IPlc {

        private string loggedUser;
        private InstrumentController controller;

        #region Interface Attributes
        public string Id { get; set; }

        public string SerialNumber {
            get { return tbSerialNumber.Text; }
            set { tbSerialNumber.Text = value; }
        }
        public string Description {
            get { return tbDescription.Text; }
            set { tbDescription.Text = value; }
        }
        public string Brand {
            get { return tbBrand.Text; }
            set { tbBrand.Text = value; }
        }
        public string Model {
            get { return tbModel.Text; }
            set { tbModel.Text = value; }
        }
        public string Ip {
            get { return tbIp.Text; }
            set { tbIp.Text = value; }
        }
        public int Port {
            get { return Convert.ToInt32(tbPort.Text); }
            set { tbPort.Text = value.ToString(); }
        }
        public string InstrumentType {
            get {
                if (rbOscilloscope.Checked) {
                    return "Oscilloscope";
                } else if (rbPlc.Checked) {
                    return "Plc";
                } else {
                    return "Oscilloscope";
                }
            }
            set {
                switch (value) {
                    case "Oscilloscope":
                        rbOscilloscope.Checked = true;
                        break;
                    case "Plc":
                        rbPlc.Checked = true;
                        break;
                }
            }
        }
        public int Channels {
            get {
                return Convert.ToInt32(tbChannels.Text); ;
            }
            set { tbChannels.Text = value.ToString(); }
        }
        public string Instrument {
            get { return tbInstrument.Text; }
            set { tbInstrument.Text = value.ToString(); } }
        public DataTable List {
            set {
                BindingSource binding = new BindingSource();
                binding.DataSource = value;
                dgMeasuringInstruments.DataSource = binding;
                dgMeasuringInstruments.Columns[0].Visible = false;
            }
        }
        public int AcquisitionInterval {
            get { return Convert.ToInt32(tbAcquisitioninterval.Text); }
            set { tbAcquisitioninterval.Text = value.ToString(); } }
        #endregion

        public FrmMeasuringInstrument(string userId) {
            loggedUser = userId;
            controller = new InstrumentController(this);
            InitializeComponent();
        }
        #region Events
        private void FrmMeasuringInstrument_Load(object sender, EventArgs e) {
            tabInstrumentsType.TabPages.Remove(tabPlc);
        }
        private void FrmMeasuringInstrument_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }
        private void rbOscilloscope_CheckedChanged(object sender, EventArgs e) {
            if (rbOscilloscope.Checked) {
                SelectEquipmentType(tabOscilloscope);
            }
        }
        private void rbPlc_CheckedChanged(object sender, EventArgs e) {
            if (rbPlc.Checked) {
                SelectEquipmentType(tabPlc);
            }
        }
        private void btClean_Click(object sender, EventArgs e) {
            CleanFields();
        }
        private async void btSave_Click(object sender, EventArgs e) {
            if (CheckMandatoryFields()) {
                if (Id == null) {
                    if (await controller.Create(loggedUser, CheckedEquipmentType())) {
                        MessageBox.Show("O instrumento foi criado com sucesso!");
                        CleanFields();
                    }
                } else {
                    if (await controller.Update(loggedUser, CheckedEquipmentType())) {
                        MessageBox.Show("O instrumento foi alterado com sucesso!");
                        CleanFields();
                    }
                }
            }
        }
        private async void tabMeasuringInstruments_SelectedIndexChanged(object sender, EventArgs e) {
            if (tabMeasuringInstruments.SelectedTab == tabMeasuringInstruments.TabPages["tabSearch"]) {
                CleanFields();
                tbSearchSerialNumber.Text = "";
                tbSearchBrand.Text = "";
                await LoadEquipment();
            }
        }
        private async void tbSearchSerialNumber_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadEquipment();
            }
        }
        private async void tbSearchBrand_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadEquipment();
            }
        }
        private async void btSearch_Click(object sender, EventArgs e) {
            await LoadEquipment();
        }
        private async void dgMeasuringInstruments_DoubleClick(object sender, EventArgs e) {
            try {
                CleanFields();
                Id = dgMeasuringInstruments.SelectedRows[0].Cells[0].Value.ToString();
                await controller.Read(loggedUser, dgMeasuringInstruments.SelectedRows[0].Cells[1].Value.ToString());
                tabMeasuringInstruments.SelectedTab = tabRegister;
            } catch {
                new FormException("Selecione a linha do instrumento que deseja alterar.");
            }
        }
        private async void btDelete_Click(object sender, EventArgs e) {
            try {
                Id = dgMeasuringInstruments.SelectedRows[0].Cells[0].Value.ToString();
                string type = dgMeasuringInstruments.SelectedRows[0].Cells[1].Value.ToString();
                var result = MessageBox.Show(
                    "Tem certeza de que deseja remover este instrumento de medição?",
                    "Atenção!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                );
                if (result == DialogResult.Yes) {
                    if (await controller.Delete(loggedUser, type)) {
                        MessageBox.Show("O instrumento foi removido com sucesso!");
                        CleanDataGrid();
                        await LoadEquipment();
                    }
                }
            } catch {
                new FormException("Selecione a linha do instrumento de medição que deseja remover!");
            }
        }
        private async void btUpdate_Click(object sender, EventArgs e) {
            await GetInstrument();
        }
        #endregion

        #region Functions
        private void SelectEquipmentType(TabPage tab) {
            bool tabIsVisible = false; ;
            foreach (TabPage page in tabInstrumentsType.TabPages) {
                if (page != tab) {
                    tabInstrumentsType.TabPages.Remove(page);
                } else {
                    tabIsVisible = true;
                }
            }
            if (!tabIsVisible) {
                tabInstrumentsType.TabPages.Add(tab);
            }
        }
        private void CleanFields() {
            Id = null;
            tbSerialNumber.Text = "";
            tbBrand.Text = "";
            tbModel.Text = "";
            tbIp.Text = "";
            tbPort.Text = "";
            tbDescription.Text = "";
            tbChannels.Text = "";
            tbInstrument.Text = "";
            tbAcquisitioninterval.Text = "";
        }
        private bool CheckMandatoryFields() {
            if (ValidationHandler.IsTextBoxNull(tbSerialNumber, lbSerialNumber.Text)) {
                return false;
            }
            if (ValidationHandler.IsTextBoxNull(tbBrand, lbBrand.Text)) {
                return false;
            }
            if (ValidationHandler.IsTextBoxNull(tbModel, lbModel.Text)) {
                return false;
            }
            if (ValidationHandler.IsMaskedBoxNull(tbIp, lbIp.Text)) {
                return false;
            }
            if (ValidationHandler.IsTextBoxNull(tbPort, lbPort.Text)) {
                return false;
            }
            if (ValidationHandler.IsTextBoxNull(tbDescription, tbDescription.Text)) {
                return false;
            }
            if ((rbOscilloscope.Checked) && (ValidationHandler.IsTextBoxNull(tbChannels, tbChannels.Text))) {
                return false;
            }
            if ((rbOscilloscope.Checked) && (ValidationHandler.IsTextBoxNull(tbInstrument, tbInstrument.Text))) {
                return false;
            }
            if ((rbPlc.Checked) && (ValidationHandler.IsTextBoxNull(tbAcquisitioninterval, tbAcquisitioninterval.Text))) {
                return false;
            }
            return true;
        }
        private string CheckedEquipmentType() {
            string equipmentType = "";
            if (rbOscilloscope.Checked) {
                equipmentType = "Oscilloscope";
            } else if (rbPlc.Checked) {
                equipmentType = "Plc";
            }
            return equipmentType;
        }
        private async Task LoadEquipment() {
            CleanDataGrid();
            if (cbOrder.SelectedIndex != -1) {
                await controller.List(
                    loggedUser,
                    tbSearchSerialNumber.Text,
                    tbSearchBrand.Text,
                    cbOrder.SelectedItem.ToString()
                );
            } else {
                await controller.List(
                    loggedUser,
                    tbSearchSerialNumber.Text,
                    tbSearchBrand.Text
                );
            }
        }
        private void CleanDataGrid() {
            dgMeasuringInstruments.DataSource = null;
        }
        private async Task GetInstrument() {
            try {
                CleanFields();
                Id = dgMeasuringInstruments.SelectedRows[0].Cells[0].Value.ToString();
                string type = dgMeasuringInstruments.SelectedRows[0].Cells[1].Value.ToString();
                await controller.Read(loggedUser, type);
                tabMeasuringInstruments.SelectedTab = tabRegister;
            } catch {
                new FormException("Selecione a linha do instrumento de medição que deseja alterar.");
            }
        }
        #endregion
        
    }
}
