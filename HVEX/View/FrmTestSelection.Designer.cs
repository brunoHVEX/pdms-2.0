﻿
namespace HVEX.View {
    partial class FrmTestSelection {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTestSelection));
            this.pnTestSelection = new System.Windows.Forms.Panel();
            this.cbTestFra = new System.Windows.Forms.CheckBox();
            this.pnTestSelection.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTestSelection
            // 
            this.pnTestSelection.BackColor = System.Drawing.Color.White;
            this.pnTestSelection.Controls.Add(this.cbTestFra);
            this.pnTestSelection.Location = new System.Drawing.Point(12, 12);
            this.pnTestSelection.Name = "pnTestSelection";
            this.pnTestSelection.Size = new System.Drawing.Size(606, 339);
            this.pnTestSelection.TabIndex = 0;
            // 
            // cbTestFra
            // 
            this.cbTestFra.AutoSize = true;
            this.cbTestFra.Location = new System.Drawing.Point(31, 23);
            this.cbTestFra.Name = "cbTestFra";
            this.cbTestFra.Size = new System.Drawing.Size(47, 17);
            this.cbTestFra.TabIndex = 0;
            this.cbTestFra.Text = "FRA";
            this.cbTestFra.UseVisualStyleBackColor = true;
            // 
            // FrmTestSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(630, 363);
            this.Controls.Add(this.pnTestSelection);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmTestSelection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Seleção de ensaios";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTestSelection_FormClosed);
            this.pnTestSelection.ResumeLayout(false);
            this.pnTestSelection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTestSelection;
        public System.Windows.Forms.CheckBox cbTestFra;
    }
}