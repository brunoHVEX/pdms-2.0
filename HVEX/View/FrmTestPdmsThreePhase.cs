﻿using HVEX.Controller;
using HVEX.Model;
using HVEX.Service;
using HVEX.View.Interface;
using HVEX.View.SubForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;
using Timer = System.Windows.Forms.Timer;

namespace HVEX.View {
    public partial class FrmTestPdmsThreePhase : Form, ITestPdmsThreePhase {

        private string loggedUser;
        TestPdmsThreePhaseController controller;
        Stopwatch testTimer = new Stopwatch();
        OscilloscopePdms oscilloscope = new OscilloscopePdms();

        

        #region Interface Attributes
        public string Id { get; set; }
        public string IdConfiguration { get; set; }
        public string EquipmentId { get; set; }
        public string EquipmentDescription {
            get { return (string)tbTransformer.Invoke(new Func<string>(() => tbTransformer.Text)); }
            set {
                tbTransformer.Invoke((MethodInvoker)delegate {
                    tbTransformer.Text = value;
                });
            }
        }
        public string InstrumentId {
            get { return (string)cbOsciloscope.Invoke(new Func<string>(() => cbOsciloscope.SelectedValue.ToString())); }
            set {
                cbOsciloscope.Invoke((MethodInvoker)delegate {
                    cbOsciloscope.Text = value.ToString();
                });
            }
        }
        public string InstrumentConnection {
            get { return (string)lbConnection.Invoke(new Func<string>(() => lbConnection.Text)); }
            set {
                try {
                    if (value == "Conectado") {
                        lbConnection.Invoke((MethodInvoker)delegate {
                            lbConnection.BackColor = System.Drawing.Color.LightGreen;
                            lbConnection.Text = value;
                        });
                        btConnection.Invoke((MethodInvoker)delegate {
                            btConnection.Text = "Desconectar";
                        });
                    } else {
                        lbConnection.Invoke((MethodInvoker)delegate {
                            lbConnection.BackColor = System.Drawing.Color.Salmon;
                            lbConnection.Text = value;
                        });
                        btConnection.Invoke((MethodInvoker)delegate {
                            btConnection.Text = "Conectar";
                        });
                    }
                } catch {

                }
            }
        }
        public string InstrumentIp {
            get { return tbIp.Text; }
            set { tbIp.Text = value; }
        }

        public int InstrumentPort {
            get { return int.Parse(tbPort.Text); }
            set { tbPort.Text = value.ToString(); }
        }
        public DataTable OsciloscopeList {
            set {
                cbOsciloscope.DisplayMember = "Descrição";
                cbOsciloscope.ValueMember = "Id";
                cbOsciloscope.DataSource = value;
            }
        }
        public string Channels {
            get {return (string)cbChannels.Invoke(new Func<string>(() => cbChannels.SelectedItem.ToString()));}
            set {cbChannels.Invoke((MethodInvoker)delegate {cbChannels.SelectedItem = value;});}
        }
        public WaveForm SignalPhase1 {
            set { SetGraph(gcSignalPhase1, value); }
        }
        public WaveForm SignalPhase2 {
            set { SetGraph(gcSignalPhase2, value); }
        }
        public WaveForm SignalPhase3 {
            set { SetGraph(gcSignalPhase3, value); }
        }
        public WaveForm SignalFiltredPhase1 {
            set { SetGraph(gcSignalFiltredPhase1, value); }
        }
        public WaveForm SignalFiltredPhase2 {
            set { SetGraph(gcSignalFiltredPhase2, value); }
        }
        public WaveForm SignalFiltredPhase3 {
            set { SetGraph(gcSignalFiltredPhase3, value); }
        }
        public List<double>[] DispersionPhase1 {
            set { SetPointsGraph(gcDispersionPhase1, value); }
        }
        public List<double>[] DispersionPhase2 {
            set { SetPointsGraph(gcDispersionPhase2, value); }
        }
        public List<double>[] DispersionPhase3 {
            set { SetPointsGraph(gcDispersionPhase3, value); }
        }
        public List<double>[] DispersionDetailedPhase1 {
            set { SetPointsGraph(gcDispersionDetailedPhase1, value); }
        }
        public List<double>[] DispersionDetailedPhase2 {
            set { SetPointsGraph(gcDispersionDetailedPhase2, value); }
        }
        public List<double>[] DispersionDetailedPhase3 {
            set { SetPointsGraph(gcDispersionDetailedPhase3, value); }
        }
        public List<double>[] PartialDischargeTrendPhase1 {
            set { SetBarGraph(gcPartialDischargeTrendPhase1, value); }
        }
        public List<double>[] PartialDischargeTrendPhase2 {
            set { SetBarGraph(gcPartialDischargeTrendPhase2, value); }
        }
        public List<double>[] PartialDischargeTrendPhase3 {
            set { SetBarGraph(gcPartialDischargeTrendPhase3, value); }
        }
        public WaveForm FftPhase1 {
            set { SetGraph(gcFftPhase1, value); }
        }
        public WaveForm FftPhase2 {
            set { SetGraph(gcFftPhase2, value); }
        }
        public WaveForm FftPhase3 {
            set { SetGraph(gcFftPhase3, value); }
        }
        public double GainPhase1 {
            get { return (double)tbGainPhase1.Invoke(new Func<double>(() => double.Parse(tbGainPhase1.Text))); }
            set {
                tbGainPhase1.Invoke((MethodInvoker)delegate {
                    tbGainPhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double GainPhase2 {
            get { return (double)tbGainPhase2.Invoke(new Func<double>(() => double.Parse(tbGainPhase2.Text))); }
            set {
                tbGainPhase2.Invoke((MethodInvoker)delegate {
                    tbGainPhase2.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double GainPhase3 {
            get { return (double)tbGainPhase3.Invoke(new Func<double>(() => double.Parse(tbGainPhase3.Text))); }
            set {
                tbGainPhase3.Invoke((MethodInvoker)delegate {
                    tbGainPhase3.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double GatePhase1 {
            get { return (double)tbGatePhase1.Invoke(new Func<double>(() => double.Parse(tbGatePhase1.Text))); }
            set {
                tbGatePhase1.Invoke((MethodInvoker)delegate {
                    tbGatePhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double GatePhase2 {
            get { return (double)tbGatePhase2.Invoke(new Func<double>(() => double.Parse(tbGatePhase2.Text))); }
            set {
                tbGatePhase2.Invoke((MethodInvoker)delegate {
                    tbGatePhase2.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double GatePhase3 {
            get { return (double)tbGatePhase3.Invoke(new Func<double>(() => double.Parse(tbGatePhase3.Text))); }
            set {
                tbGatePhase3.Invoke((MethodInvoker)delegate {
                    tbGatePhase3.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public string Filter {
            get {
                 return (string)cbFilter.Invoke(new Func<string>(() => cbFilter.SelectedItem.ToString()));
            }
            set { cbFilter.Invoke((MethodInvoker)delegate { cbFilter.SelectedItem = value; }); }
        }
        public double Order {
            get { return (double)tbOrder.Invoke(new Func<double>(() => double.Parse(tbOrder.Text))); }
            set {
                tbOrder.Invoke((MethodInvoker)delegate {
                    tbOrder.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double FrequencyUndercut {
            get { return (double)tbFrequencyUndercut.Invoke(new Func<double>(() => double.Parse(tbFrequencyUndercut.Text))); }
            set {
                tbFrequencyUndercut.Invoke((MethodInvoker)delegate {
                    tbFrequencyUndercut.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public string UnitFrequencyUndercut {
            get { return (string)cbFrequencyUndercut.Invoke(new Func<string>(() => cbFrequencyUndercut.SelectedItem.ToString())); }
            set { cbFrequencyUndercut.Invoke((MethodInvoker)delegate { cbFrequencyUndercut.SelectedItem = value; }); }
        }
        public double FrequencyTopCut {
            get { return (double)tbFrequencyTopCut.Invoke(new Func<double>(() => double.Parse(tbFrequencyTopCut.Text))); }
            set {
                tbFrequencyTopCut.Invoke((MethodInvoker)delegate {
                    tbFrequencyTopCut.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public string UnitFrequencyTopCut {
            get { return (string)cbFrequencyTopCut.Invoke(new Func<string>(() => cbFrequencyTopCut.SelectedItem.ToString())); }
            set { cbFrequencyTopCut.Invoke((MethodInvoker)delegate { cbFrequencyTopCut.SelectedItem = value; }); }
        }
        public double ChargePhase1 {
            get { return (double)tbChargePhase1.Invoke(new Func<double>(() => double.Parse(tbChargePhase1.Text))); }
            set {
                tbChargePhase1.Invoke((MethodInvoker)delegate {
                    tbChargePhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double OccurrencesPhase1 {
            get { return (double)tbOccurrencesPhase1.Invoke(new Func<double>(() => double.Parse(tbOccurrencesPhase1.Text))); }
            set {
                tbOccurrencesPhase1.Invoke((MethodInvoker)delegate {
                    tbOccurrencesPhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double ChargePhase2 {
            get { return (double)tbChargePhase2.Invoke(new Func<double>(() => double.Parse(tbChargePhase2.Text))); }
            set {
                tbChargePhase2.Invoke((MethodInvoker)delegate {
                    tbChargePhase2.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double OccurrencesPhase2 {
            get { return (double)tbOccurrencesPhase2.Invoke(new Func<double>(() => double.Parse(tbOccurrencesPhase2.Text))); }
            set {
                tbOccurrencesPhase2.Invoke((MethodInvoker)delegate {
                    tbOccurrencesPhase2.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double ChargePhase3 {
            get { return (double)tbChargePhase3.Invoke(new Func<double>(() => double.Parse(tbChargePhase3.Text))); }
            set {
                tbChargePhase3.Invoke((MethodInvoker)delegate {
                    tbChargePhase3.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double OccurrencesPhase3 {
            get { return (double)tbOccurrencesPhase3.Invoke(new Func<double>(() => double.Parse(tbOccurrencesPhase3.Text))); }
            set {
                tbOccurrencesPhase3.Invoke((MethodInvoker)delegate {
                    tbOccurrencesPhase3.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double TotalPositiveDischarges {
            get { return (double)tbTotalPositiveDischarges.Invoke(new Func<double>(() => double.Parse(tbTotalPositiveDischarges.Text))); }
            set {
                tbTotalPositiveDischarges.Invoke((MethodInvoker)delegate {
                    tbTotalPositiveDischarges.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double TotalNegativeDischarges {
            get { return (double)tbTotalNegativeDischarges.Invoke(new Func<double>(() => double.Parse(tbTotalNegativeDischarges.Text))); }
            set {
                tbTotalNegativeDischarges.Invoke((MethodInvoker)delegate {
                    tbTotalNegativeDischarges.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double TotalOccurrences {
            get { return (double)tbTotalOccurrences.Invoke(new Func<double>(() => double.Parse(tbTotalOccurrences.Text))); }
            set {
                tbTotalOccurrences.Invoke((MethodInvoker)delegate {
                    tbTotalOccurrences.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double CalibrationPhase1 {
            get { return (double)tbCalibrationPhase1.Invoke(new Func<double>(() => double.Parse(tbCalibrationPhase1.Text))); }
            set {
                tbCalibrationPhase1.Invoke((MethodInvoker)delegate {
                    tbCalibrationPhase1.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double CalibrationPhase2 {
            get { return (double)tbCalibrationPhase2.Invoke(new Func<double>(() => double.Parse(tbCalibrationPhase2.Text))); }
            set {
                tbCalibrationPhase2.Invoke((MethodInvoker)delegate {
                    tbCalibrationPhase2.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public double CalibrationPhase3 {
            get { return (double)tbCalibrationPhase3.Invoke(new Func<double>(() => double.Parse(tbCalibrationPhase3.Text))); }
            set {
                tbCalibrationPhase3.Invoke((MethodInvoker)delegate {
                    tbCalibrationPhase3.Text = value.ToString(CultureInfo.GetCultureInfo("pt-BR"));
                });
            }
        }
        public bool Osciloscope { get; set; }
        #endregion

        public FrmTestPdmsThreePhase(string userId) {
            loggedUser = userId;
            controller = new TestPdmsThreePhaseController(this);
            InitializeComponent();
            setGraphics();
        }

        #region Events
        private async void FrmTestPdmsThreePhase_Load(object sender, EventArgs e) {
            await controller.ListOsciloscopes(loggedUser);
            if (Osciloscope) {
                cbOsciloscope.SelectedIndex = 0;
                cbChannels.SelectedIndex = 0;
            } else {
                tbIp.Enabled = false;
                tbPort.Enabled = false;
                cbChannels.Enabled = false;
                btConnection.Enabled = false;
            }
            await ReadConfigurationAsync();
            timerForm.Start();
        }
        private void btGetEquipment_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmChoseEquipment")) {
                FrmChoseEquipment frm = new FrmChoseEquipment(loggedUser, this);
                frm.Show();
            }
        }
        private async void cbOsciloscope_SelectedIndexChanged(object sender, EventArgs e) {
            CleanInstrument();
            if (cbOsciloscope.SelectedIndex != -1) {
                await controller.Disconnect(loggedUser);
                await controller.GetOsciloscope(loggedUser, cbOsciloscope.SelectedValue.ToString());
            }
        }
        private async void FrmTestPdmsThreePhase_FormClosed(object sender, FormClosedEventArgs e) {
            try {
                await controller.Disconnect(loggedUser);
                FormHandler.CloseForm();
            } catch (Exception ex) {
                Console.WriteLine(ex);
            }
        }
        private async void btConnection_Click(object sender, EventArgs e) {
            Cursor.Current = Cursors.WaitCursor;
            if (btConnection.Text == "Conectar") {
                await controller.Connect(loggedUser);
            } else {
                await controller.Disconnect(loggedUser);
            }
            Cursor.Current = Cursors.Default;
        }
        private void timerForm_Tick(object sender, EventArgs e) {
            UnitFrequencyUndercut = cbFrequencyUndercut.Text;
            UnitFrequencyTopCut = cbFrequencyTopCut.Text;
            if (!string.IsNullOrEmpty(tbTransformer.Text)) {
                tbTransformer.BackColor = System.Drawing.SystemColors.Window;
            }
            //if (testTimer.IsRunning) {
            //    lbTime.Text = testTimer.Elapsed.ToString("mm\\:ss");
            //}
        }
        private void tbPort_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbPort_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbGainPhase1_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbGainPhase1_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbGainPhase2_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbGainPhase2_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbGainPhase3_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbGainPhase3_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbGatePhase1_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbGatePhase1_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            //ValidationHandler.IsDot(e);
        }
        private void tbGatePhase2_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbGatePhase2_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbGatePhase3_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbGatePhase3_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbOrder_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbOrder_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbFrequencyUndercut_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbFrequencyUndercut_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        private void tbFrequencyTopCut_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbFrequencyTopCut_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
            ValidationHandler.IsDot(e);
        }
        #endregion

        #region Functions
        private void CleanInstrument() {
            tbIp.Text = "";
            tbPort.Text = "";
        }
        private void setGraphics() {
            gcSignalPhase1.GraphPane.Title.Text = "Sinal F1";
            gcSignalPhase1.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcSignalPhase1.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcSignalPhase2.GraphPane.Title.Text = "Sinal F2";
            gcSignalPhase2.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcSignalPhase2.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcSignalPhase3.GraphPane.Title.Text = "Sinal F3";
            gcSignalPhase3.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcSignalPhase3.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcSignalFiltredPhase1.GraphPane.Title.Text = "Filtrado F1";
            gcSignalFiltredPhase1.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcSignalFiltredPhase1.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcSignalFiltredPhase2.GraphPane.Title.Text = "Filtrado F2";
            gcSignalFiltredPhase2.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcSignalFiltredPhase2.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcSignalFiltredPhase3.GraphPane.Title.Text = "Filtrado F3";
            gcSignalFiltredPhase3.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcSignalFiltredPhase3.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcDispersionPhase1.GraphPane.Title.Text = "Dispersão F1";
            gcDispersionPhase1.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcDispersionPhase1.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcDispersionPhase2.GraphPane.Title.Text = "Dispersão F2";
            gcDispersionPhase2.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcDispersionPhase2.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcDispersionPhase3.GraphPane.Title.Text = "Dispersão F3";
            gcDispersionPhase3.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcDispersionPhase3.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcDispersionDetailedPhase1.GraphPane.Title.Text = "Dispersão Detalhada F1";
            gcDispersionDetailedPhase1.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcDispersionDetailedPhase1.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcDispersionDetailedPhase2.GraphPane.Title.Text = "Dispersão Detalhada F2";
            gcDispersionDetailedPhase2.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcDispersionDetailedPhase2.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcDispersionDetailedPhase3.GraphPane.Title.Text = "Dispersão Detalhada F3";
            gcDispersionDetailedPhase3.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcDispersionDetailedPhase3.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcPartialDischargeTrendPhase1.GraphPane.Title.Text = "Tendência Descargas Parciais F1";
            gcPartialDischargeTrendPhase1.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcPartialDischargeTrendPhase1.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcPartialDischargeTrendPhase2.GraphPane.Title.Text = "Tendência Descargas Parciais F2";
            gcPartialDischargeTrendPhase2.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcPartialDischargeTrendPhase2.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcPartialDischargeTrendPhase3.GraphPane.Title.Text = "Tendência Descargas Parciais F3";
            gcPartialDischargeTrendPhase3.GraphPane.XAxis.Title.Text = "Tempo (s)";
            gcPartialDischargeTrendPhase3.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcFftPhase1.GraphPane.Title.Text = "FFT F1";
            gcFftPhase1.GraphPane.XAxis.Title.Text = "Frequência (Hz)";
            gcFftPhase1.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcFftPhase2.GraphPane.Title.Text = "FFT F2";
            gcFftPhase2.GraphPane.XAxis.Title.Text = "Frequência (Hz)";
            gcFftPhase2.GraphPane.YAxis.Title.Text = "Amplitude (V)";

            gcFftPhase3.GraphPane.Title.Text = "FFT F3";
            gcFftPhase3.GraphPane.XAxis.Title.Text = "Frequência (Hz)";
            gcFftPhase3.GraphPane.YAxis.Title.Text = "Amplitude (V)";
        }
        public void SetGraph(ZedGraph.ZedGraphControl graph, WaveForm value) {
            graph.GraphPane.CurveList.Clear();
                graph.GraphPane.AddCurve(
                    null,
                    value.x.ToArray(),
                    value.y.ToArray(),
                    System.Drawing.Color.Black,
                    ZedGraph.SymbolType.None
                );
                graph.AxisChange();
                graph.Invalidate();
        }
        public void SetPointsGraph(ZedGraph.ZedGraphControl graph, List<double>[] value) {
            graph.GraphPane.CurveList.Clear();
            LineItem curve = graph.GraphPane.AddCurve(
                                                null,
                                                value[0].ToArray(),
                                                value[1].ToArray(),
                                                System.Drawing.Color.Black,
                                                ZedGraph.SymbolType.Circle
                                                );
            curve.Line.IsVisible = false;
            graph.GraphPane.AddCurve(
                                    null,
                                    value[2].ToArray(),
                                    value[3].ToArray(),
                                    System.Drawing.Color.Gray,
                                    ZedGraph.SymbolType.None);
            graph.AxisChange();
            graph.Invalidate();
        }
        public void SetBarGraph(ZedGraph.ZedGraphControl graph, List<double>[] value) {
            graph.GraphPane.CurveList.Clear();
            graph.GraphPane.AddBar(
                                    null,
                                    value[0].ToArray(),
                                    value[1].ToArray(),
                                    System.Drawing.Color.Black);
            graph.AxisChange();
            graph.Invalidate();
        }
        private void StartTest() {
            controller.TestRunning = true;
            testTimer.Reset();
            testTimer.Start();
        }
        private async void StopTest() {
            controller.TestRunning = false;
            await controller.StopTest(loggedUser);
            testTimer.Stop();
        }
        #endregion

        private async void btStart_Click(object sender, EventArgs e) {
            StartTest();
            cbChannels.Enabled = false;
            tbIp.Enabled = false;
            tbPort.Enabled = false;
            bool result = await Task.Run(() => controller.RunTest(loggedUser));
        }

        private async void btStop_Click(object sender, EventArgs e) {
            StopTest();
            cbChannels.Enabled = true;
            tbIp.Enabled = true;
            tbPort.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e) {
            oscilloscope.partialDischargesPhase1.Clear();        
        }

        private void btStartTest_Click(object sender, EventArgs e) {
            Timer relogio = new Timer();
            string hr = "";
            string min = "";
            string sec = "";
            relogio.Interval = 1000;
            int timeEndTest = (int)nudTestTime.Value;

            //Convertendo em hora, minuto e segundo
            int hour = (int)(timeEndTest / (60 * 60));
            int minute = (int)((timeEndTest - (hour * 60 * 60)) / 60);
            int second = (int)(timeEndTest - (hour * 60 * 60) - (minute * 60));

            if (hour < 10) {
                hr = "0" + hour.ToString();
            } else {
                hr = hour.ToString();
            }
            if (minute < 10) {
                min = "0" + minute.ToString();
            } else {
                min = minute.ToString();
            }
            if (second < 10) {
                sec = "0" + second.ToString();
            } else {
                sec = second.ToString();
            }
            lbTimer.Text = hr + ":" + min + ":" + sec;

            relogio.Tick += delegate {
                timeEndTest -= 1;
                //Convertendo em hora, minuto e segundo
                hour = (int)(timeEndTest / (60 * 60));
                minute = (int)((timeEndTest - (hour * 60 * 60)) / 60);
                second = (int)(timeEndTest - (hour * 60 * 60) - (minute * 60));

                if (hour < 10) {
                    hr = "0" + hour.ToString();
                } else {
                    hr = hour.ToString();
                }
                if (minute < 10) {
                    min = "0" + minute.ToString();
                } else {
                    min = minute.ToString();
                }
                if (second < 10) {
                    sec = "0" + second.ToString();
                } else {
                    sec = second.ToString();
                }

                //Apresentando o relógio decrescente
                lbTimer.Text = hr + ":" + min + ":" + sec;

                

                if (timeEndTest!=0) {
                    Console.WriteLine("teste");
                } else {
                    relogio.Stop();
                }
                
            };

            relogio.Start();

        }

        private void btCalibration_Click(object sender, EventArgs e) {
            oscilloscope.calibrating = true;
            if (!string.IsNullOrEmpty(tbCalibrationPhase1.Text)) {
                controller.UpGain(Convert.ToDouble(tbCalibrationPhase1.Text),1);
            }
            if (!string.IsNullOrEmpty(tbCalibrationPhase2.Text)) {
                controller.UpGain(Convert.ToDouble(tbCalibrationPhase2.Text),2);
            }
            if (!string.IsNullOrEmpty(tbCalibrationPhase3.Text)) {
                controller.UpGain(Convert.ToDouble(tbCalibrationPhase3.Text),3);
            }
            oscilloscope.calibrating = false;
        }

        private async void btSaveCalibration_Click(object sender, EventArgs e) {

            if (await controller.SaveConfigurations(loggedUser)) {
                MessageBox.Show("As configurações foram registradas com sucesso!");
            }
        }

        private async Task ReadConfigurationAsync() {
            ConfigurationThreePhase configurationThreePhase = new ConfigurationThreePhase();
            await configurationThreePhase.Read();
            IdConfiguration = configurationThreePhase.Id.ToString();
            tbGainPhase1.Value = Convert.ToDecimal(configurationThreePhase.Gain[0]);
            tbGainPhase2.Value = Convert.ToDecimal(configurationThreePhase.Gain[1]);
            tbGainPhase3.Value = Convert.ToDecimal(configurationThreePhase.Gain[2]);
            tbGatePhase1.Value = Convert.ToDecimal(configurationThreePhase.Gate[0]);
            tbGatePhase2.Value = Convert.ToDecimal(configurationThreePhase.Gate[1]);
            tbGatePhase3.Value = Convert.ToDecimal(configurationThreePhase.Gate[2]);
            cbFilter.SelectedIndex = configurationThreePhase.Filter;
            tbOrder.Value = Convert.ToDecimal(configurationThreePhase.Order);
            tbFrequencyUndercut.Value = Convert.ToDecimal(configurationThreePhase.FrequencyUndercut[0]);
            cbFrequencyUndercut.SelectedIndex = Convert.ToInt32(configurationThreePhase.FrequencyUndercut[1]);
            tbFrequencyTopCut.Value = Convert.ToDecimal(configurationThreePhase.FrequencyTopCut[0]);
            cbFrequencyTopCut.SelectedIndex = Convert.ToInt32(configurationThreePhase.FrequencyTopCut[1]);
            tbCalibrationPhase1.Value = Convert.ToDecimal(configurationThreePhase.Calibration[0]);
            tbCalibrationPhase2.Value = Convert.ToDecimal(configurationThreePhase.Calibration[1]);
            tbCalibrationPhase3.Value = Convert.ToDecimal(configurationThreePhase.Calibration[2]);
        }

        private async void btResetConfigurations_Click(object sender, EventArgs e) {
            if (await controller.ResetConfigurations(loggedUser)) {
                MessageBox.Show("As configurações foram resetadas com sucesso!");
            }
        }
    }
}
