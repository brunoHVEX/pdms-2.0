﻿using HVEX.Service;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmAbout : Form {
        public FrmAbout() {
            InitializeComponent();
        }

        private void FrmAbout_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }

        private void lbWebsite_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            System.Diagnostics.Process.Start("http://www.hvex.com.br");
        }
    }
}
