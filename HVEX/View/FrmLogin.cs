﻿using HVEX.Controller;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmLogin : Form {

        private UserController controller;

        public FrmLogin() {
            InitializeComponent();
            controller = new UserController();
        }

        #region Events
        private void btLogout_Click(object sender, EventArgs e) {
            Environment.Exit(1);
        }
        private async void btLogin_Click(object sender, EventArgs e) {
            await Login();
        }
        private async void tbUserName_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                await Login();
            }
        }
        private async void tbPassword_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode == Keys.Enter) {
                await Login();
            }
        }
        #endregion

        #region Functions
        private async Task Login() {
            if (!string.IsNullOrEmpty(tbUserName.Text) && !string.IsNullOrEmpty(tbPassword.Text)) {
                var userId = await controller.Login(tbUserName.Text, tbPassword.Text);
                if (userId != null) {
                    FrmMain frm = new FrmMain(userId);
                    frm.Show();
                    Close();
                }
            }
        }
        #endregion
    }
}
