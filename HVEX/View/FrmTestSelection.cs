﻿using HVEX.Service;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmTestSelection : Form {
        public FrmTestSelection() {
            InitializeComponent();
        }
        private void FrmTestSelection_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }
    }
}
