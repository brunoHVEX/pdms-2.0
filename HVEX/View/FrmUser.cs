﻿using HVEX.Controller;
using HVEX.Model;
using HVEX.Service;
using HVEX.View.Interface;
using System.Data;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmUser : Form, IUser {

        private UserController controller;
        private string loggedUser;

        #region Interface Attributes
        public string Id { get; set; }
        public string Name {
            get { return tbName.Text; }
            set { tbName.Text = value; }
        }
        public string UserName {
            get { return tbUserName.Text; }
            set { tbUserName.Text = value; }
        }
        public string Password {
            get { return tbPassword.Text; }
            set { tbPassword.Text = value; }
        }
        public string AccessLevel {
            get {
                if (cbAccessLevel.SelectedItem.ToString().Equals("Master")) {   
                    return cbAccessLevel.SelectedItem.ToString();
                } else {
                    cbAccessLevel.Items.Remove("Master");
                    return cbAccessLevel.SelectedItem.ToString();
                }
            }
            set { cbAccessLevel.SelectedItem = value; }
        }
        public Image Image {
            get { return pbUser.Image; }
            set { pbUser.Image = value; }
        }
        public DataTable List {
            set {
                if (value != null) {
                    BindingSource binding = new BindingSource();
                    binding.DataSource = value;
                    dgUser.DataSource = binding;
                    dgUser.Columns[0].Visible = false;
                }
            }
        }
        #endregion

        public FrmUser(string userId) {
            InitializeComponent();
            controller = new UserController(this);
            loggedUser = userId;
        }

        #region Events
        private async void FrmUser_Load(object sender, System.EventArgs e) {
            Id = loggedUser;
            await controller.Read(loggedUser);
            if ((AccessLevel != "Administrador") && (AccessLevel != "Master")){
                lbAccessLevel.Visible = false;
                cbAccessLevel.Visible = false;
                btClean.Visible = false;
                btSave.Top = tbConfirmPassword.Top;
                tabUser.TabPages.Remove(tabSearch);
            } if (AccessLevel == "Master") {
                CleanFields();
            }
        }
        private void FrmUser_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }
        private void tbName_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
            }
        }
        private void tbUserName_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
            }
        }
        private void tbPassword_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
            }
        }
        private void tbConfirmPassword_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
            }
        }
        private void pbUser_Click(object sender, System.EventArgs e) {
            OpenFileDialog imgDialog = new OpenFileDialog();
            imgDialog.Filter = "Arquivos de Imagem (*.jpg;*.png)|*.JPG;*.PNG";
            if (imgDialog.ShowDialog() == DialogResult.OK) {
                pbUser.ImageLocation = imgDialog.FileName.ToString();
            }
        }
        private void btClean_Click(object sender, System.EventArgs e) {
            CleanFields();
        }
        private async void btSave_Click(object sender, System.EventArgs e) {
            if (ValidatePasswords()) {
                if (Id == null) {
                    if (cbAccessLevel.SelectedItem.ToString().Equals("Master")) {
                        MessageBox.Show(
                        "Não é possível criar um usuário nível Master!",
                        "Erro!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error
                    );
                    } else {
                        if (await controller.Create(loggedUser)) {
                            MessageBox.Show("O usuário foi cadastrado com sucesso!");
                            CleanFields();
                        }
                    }
                } else {
                    if (await controller.Update(loggedUser)) {
                        MessageBox.Show("O usuário foi alterado com sucesso!");
                        CleanFields();
                    }
                }
            } else {
                MessageBox.Show(
                    "As senhas não coincidem!",
                    "Erro!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
            }
        }
        private async void tabUser_SelectedIndexChanged(object sender, System.EventArgs e) {
            if (tabUser.SelectedTab == tabUser.TabPages["tabSearch"]) {
                CleanFields();
                CleanDataGrid();
                tbSearch.Text = "";
                cbOrder.SelectedIndex = -1;
                await LoadUsers();
            }
        }
        private async void tbSearch_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadUsers();
            }
        }
        private async void btSearch_Click(object sender, System.EventArgs e) {
            await LoadUsers();
        }
        private async void btDelete_Click(object sender, System.EventArgs e) {
            try {
                Id = dgUser.SelectedRows[0].Cells[0].Value.ToString();
                var result = MessageBox.Show(
                    "Tem certeza de que deseja excluir o usuário selecionado?",
                    "Atenção!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                );
                if (result == DialogResult.Yes) {
                    if (await controller.Delete(loggedUser)) {
                        Id = null;
                        MessageBox.Show("O usuário foi removido com sucesso!");
                        CleanDataGrid();
                        await LoadUsers();
                    }
                }
            } catch {
                new FormException("Selecione a linha do usuário que deseja remover.");
            }
        }
        private async void btUpdate_Click(object sender, System.EventArgs e) {
            try {
                CleanFields();
                Id = dgUser.SelectedRows[0].Cells[0].Value.ToString();
                await controller.Read(loggedUser);
                tabUser.SelectedTab = tabRegister;
            } catch {
                new FormException("Selecione a linha do usuário que deseja alterar.");
            }
        }
        private async void dgUser_MouseDoubleClick(object sender, MouseEventArgs e) {
            try {
                CleanFields();
                Id = dgUser.SelectedRows[0].Cells[0].Value.ToString();
                await controller.Read(loggedUser);
                tabUser.SelectedTab = tabRegister;
            } catch {
                new FormException("Selecione a linha do usuário que deseja alterar.");
            }
        }
        #endregion

        #region Functions
        private void CleanFields() {
            Id = null;
            tbName.Text = "";
            tbUserName.Text = "";
            tbPassword.Text = "";
            tbConfirmPassword.Text = "";
            cbAccessLevel.SelectedIndex = -1;
            pbUser.Image = Properties.Resources.user;
        }
        private bool ValidatePasswords() {
            if (
                !string.IsNullOrEmpty(tbPassword.Text) &&
                !string.IsNullOrEmpty(tbConfirmPassword.Text) &&
                tbPassword.Text == tbConfirmPassword.Text
            ) {
                return true;
            } else {
                return false;
            }
        }
        private async Task LoadUsers() {
            CleanDataGrid();
            if (cbOrder.SelectedIndex == -1) {
                await controller.List(loggedUser, tbSearch.Text);
            } else {
                await controller.List(loggedUser, tbSearch.Text, cbOrder.SelectedItem.ToString());
            }
        }
        private void CleanDataGrid() {
            dgUser.DataSource = null;
        }

        #endregion
    }
}
