﻿
namespace HVEX.View.SubForms {
    partial class FrmChoseEquipment {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmChoseEquipment));
            this.pnChooseTrafo = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSearchSerial = new System.Windows.Forms.TextBox();
            this.btSelect = new System.Windows.Forms.Button();
            this.dgTransformers = new System.Windows.Forms.DataGridView();
            this.btSearch = new System.Windows.Forms.Button();
            this.tbSearchModel = new System.Windows.Forms.TextBox();
            this.lbSearch = new System.Windows.Forms.Label();
            this.pnChooseTrafo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTransformers)).BeginInit();
            this.SuspendLayout();
            // 
            // pnChooseTrafo
            // 
            this.pnChooseTrafo.BackColor = System.Drawing.Color.White;
            this.pnChooseTrafo.Controls.Add(this.label1);
            this.pnChooseTrafo.Controls.Add(this.tbSearchSerial);
            this.pnChooseTrafo.Controls.Add(this.btSelect);
            this.pnChooseTrafo.Controls.Add(this.dgTransformers);
            this.pnChooseTrafo.Controls.Add(this.btSearch);
            this.pnChooseTrafo.Controls.Add(this.tbSearchModel);
            this.pnChooseTrafo.Controls.Add(this.lbSearch);
            this.pnChooseTrafo.Location = new System.Drawing.Point(12, 12);
            this.pnChooseTrafo.Name = "pnChooseTrafo";
            this.pnChooseTrafo.Size = new System.Drawing.Size(760, 417);
            this.pnChooseTrafo.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(188, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Modelo";
            // 
            // tbSearchSerial
            // 
            this.tbSearchSerial.Location = new System.Drawing.Point(13, 25);
            this.tbSearchSerial.Multiline = true;
            this.tbSearchSerial.Name = "tbSearchSerial";
            this.tbSearchSerial.Size = new System.Drawing.Size(169, 21);
            this.tbSearchSerial.TabIndex = 12;
            this.tbSearchSerial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSearchSerial_KeyDown);
            // 
            // btSelect
            // 
            this.btSelect.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btSelect.FlatAppearance.BorderSize = 0;
            this.btSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSelect.ForeColor = System.Drawing.Color.White;
            this.btSelect.Image = global::HVEX.Properties.Resources.white_select;
            this.btSelect.Location = new System.Drawing.Point(666, 385);
            this.btSelect.Name = "btSelect";
            this.btSelect.Size = new System.Drawing.Size(80, 23);
            this.btSelect.TabIndex = 9;
            this.btSelect.Text = "Selecionar";
            this.btSelect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btSelect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSelect.UseVisualStyleBackColor = false;
            this.btSelect.Click += new System.EventHandler(this.btSelect_Click);
            // 
            // dgTransformers
            // 
            this.dgTransformers.AllowUserToAddRows = false;
            this.dgTransformers.AllowUserToDeleteRows = false;
            this.dgTransformers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgTransformers.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgTransformers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgTransformers.Location = new System.Drawing.Point(13, 52);
            this.dgTransformers.Name = "dgTransformers";
            this.dgTransformers.Size = new System.Drawing.Size(733, 327);
            this.dgTransformers.TabIndex = 8;
            this.dgTransformers.RowHeaderMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgTransformers_RowHeaderMouseDoubleClick);
            // 
            // btSearch
            // 
            this.btSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btSearch.FlatAppearance.BorderSize = 0;
            this.btSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSearch.ForeColor = System.Drawing.Color.White;
            this.btSearch.Image = global::HVEX.Properties.Resources.white_search;
            this.btSearch.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btSearch.Location = new System.Drawing.Point(666, 25);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(80, 21);
            this.btSearch.TabIndex = 5;
            this.btSearch.Text = "Pesquisar";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.UseVisualStyleBackColor = false;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // tbSearchModel
            // 
            this.tbSearchModel.Location = new System.Drawing.Point(188, 25);
            this.tbSearchModel.Multiline = true;
            this.tbSearchModel.Name = "tbSearchModel";
            this.tbSearchModel.Size = new System.Drawing.Size(472, 21);
            this.tbSearchModel.TabIndex = 1;
            this.tbSearchModel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSearchModel_KeyDown);
            // 
            // lbSearch
            // 
            this.lbSearch.AutoSize = true;
            this.lbSearch.Location = new System.Drawing.Point(10, 10);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(86, 13);
            this.lbSearch.TabIndex = 0;
            this.lbSearch.Text = "Número de Série";
            // 
            // FrmChoseEquipment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(784, 441);
            this.Controls.Add(this.pnChooseTrafo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmChoseEquipment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Equipamentos";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmChoseEquipment_FormClosed);
            this.Load += new System.EventHandler(this.FrmChoseEquipment_Load);
            this.pnChooseTrafo.ResumeLayout(false);
            this.pnChooseTrafo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTransformers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnChooseTrafo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbSearchSerial;
        private System.Windows.Forms.Button btSelect;
        private System.Windows.Forms.DataGridView dgTransformers;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.TextBox tbSearchModel;
        private System.Windows.Forms.Label lbSearch;
    }
}