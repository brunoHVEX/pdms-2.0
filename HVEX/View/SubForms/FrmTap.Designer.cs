﻿
namespace HVEX.View.SubForms {
    partial class FrmTap {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTap));
            this.btOk = new System.Windows.Forms.Button();
            this.tbTap = new System.Windows.Forms.TextBox();
            this.lbVoltage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btOk
            // 
            this.btOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btOk.FlatAppearance.BorderSize = 0;
            this.btOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btOk.ForeColor = System.Drawing.Color.White;
            this.btOk.Location = new System.Drawing.Point(112, 67);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(60, 23);
            this.btOk.TabIndex = 8;
            this.btOk.Text = "OK";
            this.btOk.UseVisualStyleBackColor = false;
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // tbTap
            // 
            this.tbTap.Location = new System.Drawing.Point(15, 35);
            this.tbTap.Name = "tbTap";
            this.tbTap.Size = new System.Drawing.Size(157, 20);
            this.tbTap.TabIndex = 7;
            this.tbTap.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbTap_KeyDown);
            this.tbTap.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbTap_KeyPress);
            // 
            // lbVoltage
            // 
            this.lbVoltage.AutoSize = true;
            this.lbVoltage.Location = new System.Drawing.Point(12, 17);
            this.lbVoltage.Name = "lbVoltage";
            this.lbVoltage.Size = new System.Drawing.Size(65, 13);
            this.lbVoltage.TabIndex = 6;
            this.lbVoltage.Text = "Tensão ( V )";
            // 
            // FrmTap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(184, 106);
            this.Controls.Add(this.btOk);
            this.Controls.Add(this.tbTap);
            this.Controls.Add(this.lbVoltage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Taps";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTap_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.TextBox tbTap;
        private System.Windows.Forms.Label lbVoltage;
    }
}