﻿
namespace HVEX.View.SubForms {
    partial class FrmConnection {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmConnection));
            this.pnConnection = new System.Windows.Forms.Panel();
            this.lb11 = new System.Windows.Forms.Label();
            this.lb10 = new System.Windows.Forms.Label();
            this.lb08 = new System.Windows.Forms.Label();
            this.lb07 = new System.Windows.Forms.Label();
            this.lb06 = new System.Windows.Forms.Label();
            this.lb05 = new System.Windows.Forms.Label();
            this.lb04 = new System.Windows.Forms.Label();
            this.lb02 = new System.Windows.Forms.Label();
            this.lb01 = new System.Windows.Forms.Label();
            this.lb00 = new System.Windows.Forms.Label();
            this.pn11 = new System.Windows.Forms.Panel();
            this.cbYz11 = new System.Windows.Forms.CheckBox();
            this.cbDy11 = new System.Windows.Forms.CheckBox();
            this.cbYd11 = new System.Windows.Forms.CheckBox();
            this.pbYz11 = new System.Windows.Forms.PictureBox();
            this.pbDy11 = new System.Windows.Forms.PictureBox();
            this.pbYd11 = new System.Windows.Forms.PictureBox();
            this.pn10 = new System.Windows.Forms.Panel();
            this.cbDz10 = new System.Windows.Forms.CheckBox();
            this.cbDd10 = new System.Windows.Forms.CheckBox();
            this.pbDz10 = new System.Windows.Forms.PictureBox();
            this.pbDd10 = new System.Windows.Forms.PictureBox();
            this.pn08 = new System.Windows.Forms.Panel();
            this.cbDz8 = new System.Windows.Forms.CheckBox();
            this.cbDd8 = new System.Windows.Forms.CheckBox();
            this.pbDz8 = new System.Windows.Forms.PictureBox();
            this.pbDd8 = new System.Windows.Forms.PictureBox();
            this.pn07 = new System.Windows.Forms.Panel();
            this.cbYz7 = new System.Windows.Forms.CheckBox();
            this.cbDy7 = new System.Windows.Forms.CheckBox();
            this.cbYd7 = new System.Windows.Forms.CheckBox();
            this.pbYz7 = new System.Windows.Forms.PictureBox();
            this.pbDy7 = new System.Windows.Forms.PictureBox();
            this.Yd7 = new System.Windows.Forms.PictureBox();
            this.pn06 = new System.Windows.Forms.Panel();
            this.cbDz6 = new System.Windows.Forms.CheckBox();
            this.cbDd6 = new System.Windows.Forms.CheckBox();
            this.cbYy6 = new System.Windows.Forms.CheckBox();
            this.pbDz6 = new System.Windows.Forms.PictureBox();
            this.pbDd6 = new System.Windows.Forms.PictureBox();
            this.pbYy6 = new System.Windows.Forms.PictureBox();
            this.pn05 = new System.Windows.Forms.Panel();
            this.cbYz5 = new System.Windows.Forms.CheckBox();
            this.cbDy5 = new System.Windows.Forms.CheckBox();
            this.cbYd5 = new System.Windows.Forms.CheckBox();
            this.pbYz5 = new System.Windows.Forms.PictureBox();
            this.pbDy5 = new System.Windows.Forms.PictureBox();
            this.pbYd5 = new System.Windows.Forms.PictureBox();
            this.pn04 = new System.Windows.Forms.Panel();
            this.cbDz4 = new System.Windows.Forms.CheckBox();
            this.cbDd4 = new System.Windows.Forms.CheckBox();
            this.pbDz4 = new System.Windows.Forms.PictureBox();
            this.pbDd4 = new System.Windows.Forms.PictureBox();
            this.pn02 = new System.Windows.Forms.Panel();
            this.cbDz2 = new System.Windows.Forms.CheckBox();
            this.cbDd2 = new System.Windows.Forms.CheckBox();
            this.pbDz2 = new System.Windows.Forms.PictureBox();
            this.pbDd2 = new System.Windows.Forms.PictureBox();
            this.pn01 = new System.Windows.Forms.Panel();
            this.cbYz1 = new System.Windows.Forms.CheckBox();
            this.cbDy1 = new System.Windows.Forms.CheckBox();
            this.cbYd1 = new System.Windows.Forms.CheckBox();
            this.pbYz1 = new System.Windows.Forms.PictureBox();
            this.pbDy1 = new System.Windows.Forms.PictureBox();
            this.pbYd1 = new System.Windows.Forms.PictureBox();
            this.pn00 = new System.Windows.Forms.Panel();
            this.cbZz0 = new System.Windows.Forms.CheckBox();
            this.cbDz0 = new System.Windows.Forms.CheckBox();
            this.cbDd0 = new System.Windows.Forms.CheckBox();
            this.cbYy0 = new System.Windows.Forms.CheckBox();
            this.pbZz0 = new System.Windows.Forms.PictureBox();
            this.pbDz0 = new System.Windows.Forms.PictureBox();
            this.pbDd0 = new System.Windows.Forms.PictureBox();
            this.pcYy0 = new System.Windows.Forms.PictureBox();
            this.pnConnection.SuspendLayout();
            this.pn11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbYz11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDy11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYd11)).BeginInit();
            this.pn10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd10)).BeginInit();
            this.pn08.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd8)).BeginInit();
            this.pn07.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbYz7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDy7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Yd7)).BeginInit();
            this.pn06.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYy6)).BeginInit();
            this.pn05.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbYz5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDy5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYd5)).BeginInit();
            this.pn04.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd4)).BeginInit();
            this.pn02.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd2)).BeginInit();
            this.pn01.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbYz1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDy1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYd1)).BeginInit();
            this.pn00.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbZz0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcYy0)).BeginInit();
            this.SuspendLayout();
            // 
            // pnConnection
            // 
            this.pnConnection.BackColor = System.Drawing.Color.White;
            this.pnConnection.Controls.Add(this.lb11);
            this.pnConnection.Controls.Add(this.lb10);
            this.pnConnection.Controls.Add(this.lb08);
            this.pnConnection.Controls.Add(this.lb07);
            this.pnConnection.Controls.Add(this.lb06);
            this.pnConnection.Controls.Add(this.lb05);
            this.pnConnection.Controls.Add(this.lb04);
            this.pnConnection.Controls.Add(this.lb02);
            this.pnConnection.Controls.Add(this.lb01);
            this.pnConnection.Controls.Add(this.lb00);
            this.pnConnection.Controls.Add(this.pn11);
            this.pnConnection.Controls.Add(this.pn10);
            this.pnConnection.Controls.Add(this.pn08);
            this.pnConnection.Controls.Add(this.pn07);
            this.pnConnection.Controls.Add(this.pn06);
            this.pnConnection.Controls.Add(this.pn05);
            this.pnConnection.Controls.Add(this.pn04);
            this.pnConnection.Controls.Add(this.pn02);
            this.pnConnection.Controls.Add(this.pn01);
            this.pnConnection.Controls.Add(this.pn00);
            this.pnConnection.Location = new System.Drawing.Point(12, 12);
            this.pnConnection.Name = "pnConnection";
            this.pnConnection.Size = new System.Drawing.Size(651, 2113);
            this.pnConnection.TabIndex = 2;
            // 
            // lb11
            // 
            this.lb11.AutoSize = true;
            this.lb11.Location = new System.Drawing.Point(25, 1900);
            this.lb11.Name = "lb11";
            this.lb11.Size = new System.Drawing.Size(19, 13);
            this.lb11.TabIndex = 26;
            this.lb11.Text = "11";
            // 
            // lb10
            // 
            this.lb10.AutoSize = true;
            this.lb10.Location = new System.Drawing.Point(25, 1690);
            this.lb10.Name = "lb10";
            this.lb10.Size = new System.Drawing.Size(19, 13);
            this.lb10.TabIndex = 25;
            this.lb10.Text = "10";
            // 
            // lb08
            // 
            this.lb08.AutoSize = true;
            this.lb08.Location = new System.Drawing.Point(25, 1480);
            this.lb08.Name = "lb08";
            this.lb08.Size = new System.Drawing.Size(19, 13);
            this.lb08.TabIndex = 24;
            this.lb08.Text = "08";
            // 
            // lb07
            // 
            this.lb07.AutoSize = true;
            this.lb07.Location = new System.Drawing.Point(25, 1270);
            this.lb07.Name = "lb07";
            this.lb07.Size = new System.Drawing.Size(19, 13);
            this.lb07.TabIndex = 23;
            this.lb07.Text = "07";
            // 
            // lb06
            // 
            this.lb06.AutoSize = true;
            this.lb06.Location = new System.Drawing.Point(25, 1061);
            this.lb06.Name = "lb06";
            this.lb06.Size = new System.Drawing.Size(19, 13);
            this.lb06.TabIndex = 22;
            this.lb06.Text = "06";
            // 
            // lb05
            // 
            this.lb05.AutoSize = true;
            this.lb05.Location = new System.Drawing.Point(25, 850);
            this.lb05.Name = "lb05";
            this.lb05.Size = new System.Drawing.Size(19, 13);
            this.lb05.TabIndex = 21;
            this.lb05.Text = "05";
            // 
            // lb04
            // 
            this.lb04.AutoSize = true;
            this.lb04.Location = new System.Drawing.Point(25, 640);
            this.lb04.Name = "lb04";
            this.lb04.Size = new System.Drawing.Size(19, 13);
            this.lb04.TabIndex = 20;
            this.lb04.Text = "04";
            // 
            // lb02
            // 
            this.lb02.AutoSize = true;
            this.lb02.Location = new System.Drawing.Point(25, 430);
            this.lb02.Name = "lb02";
            this.lb02.Size = new System.Drawing.Size(19, 13);
            this.lb02.TabIndex = 19;
            this.lb02.Text = "02";
            // 
            // lb01
            // 
            this.lb01.AutoSize = true;
            this.lb01.Location = new System.Drawing.Point(25, 220);
            this.lb01.Name = "lb01";
            this.lb01.Size = new System.Drawing.Size(19, 13);
            this.lb01.TabIndex = 18;
            this.lb01.Text = "01";
            // 
            // lb00
            // 
            this.lb00.AutoSize = true;
            this.lb00.Location = new System.Drawing.Point(20, 11);
            this.lb00.Name = "lb00";
            this.lb00.Size = new System.Drawing.Size(19, 13);
            this.lb00.TabIndex = 17;
            this.lb00.Text = "00";
            // 
            // pn11
            // 
            this.pn11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn11.Controls.Add(this.cbYz11);
            this.pn11.Controls.Add(this.cbDy11);
            this.pn11.Controls.Add(this.cbYd11);
            this.pn11.Controls.Add(this.pbYz11);
            this.pn11.Controls.Add(this.pbDy11);
            this.pn11.Controls.Add(this.pbYd11);
            this.pn11.Location = new System.Drawing.Point(11, 1910);
            this.pn11.Name = "pn11";
            this.pn11.Size = new System.Drawing.Size(472, 190);
            this.pn11.TabIndex = 16;
            // 
            // cbYz11
            // 
            this.cbYz11.AutoSize = true;
            this.cbYz11.Location = new System.Drawing.Point(315, 168);
            this.cbYz11.Name = "cbYz11";
            this.cbYz11.Size = new System.Drawing.Size(50, 17);
            this.cbYz11.TabIndex = 6;
            this.cbYz11.Text = "Yz11";
            this.cbYz11.UseVisualStyleBackColor = true;
            this.cbYz11.CheckedChanged += new System.EventHandler(this.cbYz11_CheckedChanged);
            // 
            // cbDy11
            // 
            this.cbDy11.AutoSize = true;
            this.cbDy11.Location = new System.Drawing.Point(159, 168);
            this.cbDy11.Name = "cbDy11";
            this.cbDy11.Size = new System.Drawing.Size(51, 17);
            this.cbDy11.TabIndex = 5;
            this.cbDy11.Text = "Dy11";
            this.cbDy11.UseVisualStyleBackColor = true;
            this.cbDy11.CheckedChanged += new System.EventHandler(this.cbDy11_CheckedChanged);
            // 
            // cbYd11
            // 
            this.cbYd11.AutoSize = true;
            this.cbYd11.Location = new System.Drawing.Point(3, 168);
            this.cbYd11.Name = "cbYd11";
            this.cbYd11.Size = new System.Drawing.Size(51, 17);
            this.cbYd11.TabIndex = 4;
            this.cbYd11.Text = "Yd11";
            this.cbYd11.UseVisualStyleBackColor = true;
            this.cbYd11.CheckedChanged += new System.EventHandler(this.cbYd11_CheckedChanged);
            // 
            // pbYz11
            // 
            this.pbYz11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbYz11.Image = global::HVEX.Properties.Resources.Yz11;
            this.pbYz11.Location = new System.Drawing.Point(315, 12);
            this.pbYz11.Name = "pbYz11";
            this.pbYz11.Size = new System.Drawing.Size(150, 150);
            this.pbYz11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbYz11.TabIndex = 2;
            this.pbYz11.TabStop = false;
            // 
            // pbDy11
            // 
            this.pbDy11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDy11.Image = global::HVEX.Properties.Resources.Dy11;
            this.pbDy11.Location = new System.Drawing.Point(159, 12);
            this.pbDy11.Name = "pbDy11";
            this.pbDy11.Size = new System.Drawing.Size(150, 150);
            this.pbDy11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDy11.TabIndex = 1;
            this.pbDy11.TabStop = false;
            // 
            // pbYd11
            // 
            this.pbYd11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbYd11.Image = global::HVEX.Properties.Resources.Yd11;
            this.pbYd11.Location = new System.Drawing.Point(3, 12);
            this.pbYd11.Name = "pbYd11";
            this.pbYd11.Size = new System.Drawing.Size(150, 150);
            this.pbYd11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbYd11.TabIndex = 0;
            this.pbYd11.TabStop = false;
            // 
            // pn10
            // 
            this.pn10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn10.Controls.Add(this.cbDz10);
            this.pn10.Controls.Add(this.cbDd10);
            this.pn10.Controls.Add(this.pbDz10);
            this.pn10.Controls.Add(this.pbDd10);
            this.pn10.Location = new System.Drawing.Point(11, 1700);
            this.pn10.Name = "pn10";
            this.pn10.Size = new System.Drawing.Size(314, 190);
            this.pn10.TabIndex = 15;
            // 
            // cbDz10
            // 
            this.cbDz10.AutoSize = true;
            this.cbDz10.Location = new System.Drawing.Point(159, 168);
            this.cbDz10.Name = "cbDz10";
            this.cbDz10.Size = new System.Drawing.Size(51, 17);
            this.cbDz10.TabIndex = 5;
            this.cbDz10.Text = "Dz10";
            this.cbDz10.UseVisualStyleBackColor = true;
            this.cbDz10.CheckedChanged += new System.EventHandler(this.cbDz10_CheckedChanged);
            // 
            // cbDd10
            // 
            this.cbDd10.AutoSize = true;
            this.cbDd10.Location = new System.Drawing.Point(3, 168);
            this.cbDd10.Name = "cbDd10";
            this.cbDd10.Size = new System.Drawing.Size(52, 17);
            this.cbDd10.TabIndex = 4;
            this.cbDd10.Text = "Dd10";
            this.cbDd10.UseVisualStyleBackColor = true;
            this.cbDd10.CheckedChanged += new System.EventHandler(this.cbDd10_CheckedChanged);
            // 
            // pbDz10
            // 
            this.pbDz10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDz10.Image = global::HVEX.Properties.Resources.Dz10;
            this.pbDz10.Location = new System.Drawing.Point(159, 12);
            this.pbDz10.Name = "pbDz10";
            this.pbDz10.Size = new System.Drawing.Size(150, 150);
            this.pbDz10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDz10.TabIndex = 1;
            this.pbDz10.TabStop = false;
            // 
            // pbDd10
            // 
            this.pbDd10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDd10.Image = global::HVEX.Properties.Resources.Dd10;
            this.pbDd10.Location = new System.Drawing.Point(3, 12);
            this.pbDd10.Name = "pbDd10";
            this.pbDd10.Size = new System.Drawing.Size(150, 150);
            this.pbDd10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDd10.TabIndex = 0;
            this.pbDd10.TabStop = false;
            // 
            // pn08
            // 
            this.pn08.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn08.Controls.Add(this.cbDz8);
            this.pn08.Controls.Add(this.cbDd8);
            this.pn08.Controls.Add(this.pbDz8);
            this.pn08.Controls.Add(this.pbDd8);
            this.pn08.Location = new System.Drawing.Point(11, 1490);
            this.pn08.Name = "pn08";
            this.pn08.Size = new System.Drawing.Size(314, 190);
            this.pn08.TabIndex = 14;
            // 
            // cbDz8
            // 
            this.cbDz8.AutoSize = true;
            this.cbDz8.Location = new System.Drawing.Point(159, 168);
            this.cbDz8.Name = "cbDz8";
            this.cbDz8.Size = new System.Drawing.Size(45, 17);
            this.cbDz8.TabIndex = 5;
            this.cbDz8.Text = "Dz8";
            this.cbDz8.UseVisualStyleBackColor = true;
            this.cbDz8.CheckedChanged += new System.EventHandler(this.cbDz8_CheckedChanged);
            // 
            // cbDd8
            // 
            this.cbDd8.AutoSize = true;
            this.cbDd8.Location = new System.Drawing.Point(3, 168);
            this.cbDd8.Name = "cbDd8";
            this.cbDd8.Size = new System.Drawing.Size(46, 17);
            this.cbDd8.TabIndex = 4;
            this.cbDd8.Text = "Dd8";
            this.cbDd8.UseVisualStyleBackColor = true;
            this.cbDd8.CheckedChanged += new System.EventHandler(this.cbDd8_CheckedChanged);
            // 
            // pbDz8
            // 
            this.pbDz8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDz8.Image = global::HVEX.Properties.Resources.Dz8;
            this.pbDz8.Location = new System.Drawing.Point(159, 12);
            this.pbDz8.Name = "pbDz8";
            this.pbDz8.Size = new System.Drawing.Size(150, 150);
            this.pbDz8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDz8.TabIndex = 1;
            this.pbDz8.TabStop = false;
            // 
            // pbDd8
            // 
            this.pbDd8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDd8.Image = global::HVEX.Properties.Resources.Dd8;
            this.pbDd8.Location = new System.Drawing.Point(3, 12);
            this.pbDd8.Name = "pbDd8";
            this.pbDd8.Size = new System.Drawing.Size(150, 150);
            this.pbDd8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDd8.TabIndex = 0;
            this.pbDd8.TabStop = false;
            // 
            // pn07
            // 
            this.pn07.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn07.Controls.Add(this.cbYz7);
            this.pn07.Controls.Add(this.cbDy7);
            this.pn07.Controls.Add(this.cbYd7);
            this.pn07.Controls.Add(this.pbYz7);
            this.pn07.Controls.Add(this.pbDy7);
            this.pn07.Controls.Add(this.Yd7);
            this.pn07.Location = new System.Drawing.Point(11, 1280);
            this.pn07.Name = "pn07";
            this.pn07.Size = new System.Drawing.Size(472, 190);
            this.pn07.TabIndex = 13;
            // 
            // cbYz7
            // 
            this.cbYz7.AutoSize = true;
            this.cbYz7.Location = new System.Drawing.Point(315, 168);
            this.cbYz7.Name = "cbYz7";
            this.cbYz7.Size = new System.Drawing.Size(44, 17);
            this.cbYz7.TabIndex = 6;
            this.cbYz7.Text = "Yz7";
            this.cbYz7.UseVisualStyleBackColor = true;
            this.cbYz7.CheckedChanged += new System.EventHandler(this.cbYz7_CheckedChanged);
            // 
            // cbDy7
            // 
            this.cbDy7.AutoSize = true;
            this.cbDy7.Location = new System.Drawing.Point(159, 168);
            this.cbDy7.Name = "cbDy7";
            this.cbDy7.Size = new System.Drawing.Size(45, 17);
            this.cbDy7.TabIndex = 5;
            this.cbDy7.Text = "Dy7";
            this.cbDy7.UseVisualStyleBackColor = true;
            this.cbDy7.CheckedChanged += new System.EventHandler(this.cbDy7_CheckedChanged);
            // 
            // cbYd7
            // 
            this.cbYd7.AutoSize = true;
            this.cbYd7.Location = new System.Drawing.Point(3, 168);
            this.cbYd7.Name = "cbYd7";
            this.cbYd7.Size = new System.Drawing.Size(45, 17);
            this.cbYd7.TabIndex = 4;
            this.cbYd7.Text = "Yd7";
            this.cbYd7.UseVisualStyleBackColor = true;
            this.cbYd7.CheckedChanged += new System.EventHandler(this.cbYd7_CheckedChanged);
            // 
            // pbYz7
            // 
            this.pbYz7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbYz7.Image = global::HVEX.Properties.Resources.Yz7;
            this.pbYz7.Location = new System.Drawing.Point(315, 12);
            this.pbYz7.Name = "pbYz7";
            this.pbYz7.Size = new System.Drawing.Size(150, 150);
            this.pbYz7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbYz7.TabIndex = 2;
            this.pbYz7.TabStop = false;
            // 
            // pbDy7
            // 
            this.pbDy7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDy7.Image = global::HVEX.Properties.Resources.Dy7;
            this.pbDy7.Location = new System.Drawing.Point(159, 12);
            this.pbDy7.Name = "pbDy7";
            this.pbDy7.Size = new System.Drawing.Size(150, 150);
            this.pbDy7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDy7.TabIndex = 1;
            this.pbDy7.TabStop = false;
            // 
            // Yd7
            // 
            this.Yd7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Yd7.Image = global::HVEX.Properties.Resources.Yd7;
            this.Yd7.Location = new System.Drawing.Point(3, 12);
            this.Yd7.Name = "Yd7";
            this.Yd7.Size = new System.Drawing.Size(150, 150);
            this.Yd7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Yd7.TabIndex = 0;
            this.Yd7.TabStop = false;
            // 
            // pn06
            // 
            this.pn06.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn06.Controls.Add(this.cbDz6);
            this.pn06.Controls.Add(this.cbDd6);
            this.pn06.Controls.Add(this.cbYy6);
            this.pn06.Controls.Add(this.pbDz6);
            this.pn06.Controls.Add(this.pbDd6);
            this.pn06.Controls.Add(this.pbYy6);
            this.pn06.Location = new System.Drawing.Point(11, 1070);
            this.pn06.Name = "pn06";
            this.pn06.Size = new System.Drawing.Size(472, 190);
            this.pn06.TabIndex = 12;
            // 
            // cbDz6
            // 
            this.cbDz6.AutoSize = true;
            this.cbDz6.Location = new System.Drawing.Point(315, 168);
            this.cbDz6.Name = "cbDz6";
            this.cbDz6.Size = new System.Drawing.Size(45, 17);
            this.cbDz6.TabIndex = 6;
            this.cbDz6.Text = "Dz6";
            this.cbDz6.UseVisualStyleBackColor = true;
            this.cbDz6.CheckedChanged += new System.EventHandler(this.cbDz6_CheckedChanged);
            // 
            // cbDd6
            // 
            this.cbDd6.AutoSize = true;
            this.cbDd6.Location = new System.Drawing.Point(159, 168);
            this.cbDd6.Name = "cbDd6";
            this.cbDd6.Size = new System.Drawing.Size(46, 17);
            this.cbDd6.TabIndex = 5;
            this.cbDd6.Text = "Dd6";
            this.cbDd6.UseVisualStyleBackColor = true;
            this.cbDd6.CheckedChanged += new System.EventHandler(this.cbDd6_CheckedChanged);
            // 
            // cbYy6
            // 
            this.cbYy6.AutoSize = true;
            this.cbYy6.Location = new System.Drawing.Point(3, 168);
            this.cbYy6.Name = "cbYy6";
            this.cbYy6.Size = new System.Drawing.Size(44, 17);
            this.cbYy6.TabIndex = 4;
            this.cbYy6.Text = "Yy6";
            this.cbYy6.UseVisualStyleBackColor = true;
            this.cbYy6.CheckedChanged += new System.EventHandler(this.cbYy6_CheckedChanged);
            // 
            // pbDz6
            // 
            this.pbDz6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDz6.Image = global::HVEX.Properties.Resources.Dz6;
            this.pbDz6.Location = new System.Drawing.Point(315, 12);
            this.pbDz6.Name = "pbDz6";
            this.pbDz6.Size = new System.Drawing.Size(150, 150);
            this.pbDz6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDz6.TabIndex = 2;
            this.pbDz6.TabStop = false;
            // 
            // pbDd6
            // 
            this.pbDd6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDd6.Image = global::HVEX.Properties.Resources.Dd6;
            this.pbDd6.Location = new System.Drawing.Point(159, 12);
            this.pbDd6.Name = "pbDd6";
            this.pbDd6.Size = new System.Drawing.Size(150, 150);
            this.pbDd6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDd6.TabIndex = 1;
            this.pbDd6.TabStop = false;
            // 
            // pbYy6
            // 
            this.pbYy6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbYy6.Image = global::HVEX.Properties.Resources.Yy6;
            this.pbYy6.Location = new System.Drawing.Point(3, 12);
            this.pbYy6.Name = "pbYy6";
            this.pbYy6.Size = new System.Drawing.Size(150, 150);
            this.pbYy6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbYy6.TabIndex = 0;
            this.pbYy6.TabStop = false;
            // 
            // pn05
            // 
            this.pn05.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn05.Controls.Add(this.cbYz5);
            this.pn05.Controls.Add(this.cbDy5);
            this.pn05.Controls.Add(this.cbYd5);
            this.pn05.Controls.Add(this.pbYz5);
            this.pn05.Controls.Add(this.pbDy5);
            this.pn05.Controls.Add(this.pbYd5);
            this.pn05.Location = new System.Drawing.Point(11, 860);
            this.pn05.Name = "pn05";
            this.pn05.Size = new System.Drawing.Size(472, 190);
            this.pn05.TabIndex = 11;
            // 
            // cbYz5
            // 
            this.cbYz5.AutoSize = true;
            this.cbYz5.Location = new System.Drawing.Point(315, 168);
            this.cbYz5.Name = "cbYz5";
            this.cbYz5.Size = new System.Drawing.Size(44, 17);
            this.cbYz5.TabIndex = 6;
            this.cbYz5.Text = "Yz5";
            this.cbYz5.UseVisualStyleBackColor = true;
            this.cbYz5.CheckedChanged += new System.EventHandler(this.cbYz5_CheckedChanged);
            // 
            // cbDy5
            // 
            this.cbDy5.AutoSize = true;
            this.cbDy5.Location = new System.Drawing.Point(159, 168);
            this.cbDy5.Name = "cbDy5";
            this.cbDy5.Size = new System.Drawing.Size(45, 17);
            this.cbDy5.TabIndex = 5;
            this.cbDy5.Text = "Dy5";
            this.cbDy5.UseVisualStyleBackColor = true;
            this.cbDy5.CheckedChanged += new System.EventHandler(this.cbDy5_CheckedChanged);
            // 
            // cbYd5
            // 
            this.cbYd5.AutoSize = true;
            this.cbYd5.Location = new System.Drawing.Point(3, 168);
            this.cbYd5.Name = "cbYd5";
            this.cbYd5.Size = new System.Drawing.Size(45, 17);
            this.cbYd5.TabIndex = 4;
            this.cbYd5.Text = "Yd5";
            this.cbYd5.UseVisualStyleBackColor = true;
            this.cbYd5.CheckedChanged += new System.EventHandler(this.cbYd5_CheckedChanged);
            // 
            // pbYz5
            // 
            this.pbYz5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbYz5.Image = global::HVEX.Properties.Resources.Yz5;
            this.pbYz5.Location = new System.Drawing.Point(315, 12);
            this.pbYz5.Name = "pbYz5";
            this.pbYz5.Size = new System.Drawing.Size(150, 150);
            this.pbYz5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbYz5.TabIndex = 2;
            this.pbYz5.TabStop = false;
            // 
            // pbDy5
            // 
            this.pbDy5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDy5.Image = global::HVEX.Properties.Resources.Dy5;
            this.pbDy5.Location = new System.Drawing.Point(159, 12);
            this.pbDy5.Name = "pbDy5";
            this.pbDy5.Size = new System.Drawing.Size(150, 150);
            this.pbDy5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDy5.TabIndex = 1;
            this.pbDy5.TabStop = false;
            // 
            // pbYd5
            // 
            this.pbYd5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbYd5.Image = global::HVEX.Properties.Resources.Yd5;
            this.pbYd5.Location = new System.Drawing.Point(3, 12);
            this.pbYd5.Name = "pbYd5";
            this.pbYd5.Size = new System.Drawing.Size(150, 150);
            this.pbYd5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbYd5.TabIndex = 0;
            this.pbYd5.TabStop = false;
            // 
            // pn04
            // 
            this.pn04.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn04.Controls.Add(this.cbDz4);
            this.pn04.Controls.Add(this.cbDd4);
            this.pn04.Controls.Add(this.pbDz4);
            this.pn04.Controls.Add(this.pbDd4);
            this.pn04.Location = new System.Drawing.Point(11, 650);
            this.pn04.Name = "pn04";
            this.pn04.Size = new System.Drawing.Size(314, 190);
            this.pn04.TabIndex = 10;
            // 
            // cbDz4
            // 
            this.cbDz4.AutoSize = true;
            this.cbDz4.Location = new System.Drawing.Point(159, 168);
            this.cbDz4.Name = "cbDz4";
            this.cbDz4.Size = new System.Drawing.Size(45, 17);
            this.cbDz4.TabIndex = 5;
            this.cbDz4.Text = "Dz4";
            this.cbDz4.UseVisualStyleBackColor = true;
            this.cbDz4.CheckedChanged += new System.EventHandler(this.cbDz4_CheckedChanged);
            // 
            // cbDd4
            // 
            this.cbDd4.AutoSize = true;
            this.cbDd4.Location = new System.Drawing.Point(3, 168);
            this.cbDd4.Name = "cbDd4";
            this.cbDd4.Size = new System.Drawing.Size(46, 17);
            this.cbDd4.TabIndex = 4;
            this.cbDd4.Text = "Dd4";
            this.cbDd4.UseVisualStyleBackColor = true;
            this.cbDd4.CheckedChanged += new System.EventHandler(this.cbDd4_CheckedChanged);
            // 
            // pbDz4
            // 
            this.pbDz4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDz4.Image = global::HVEX.Properties.Resources.Dz4;
            this.pbDz4.Location = new System.Drawing.Point(159, 12);
            this.pbDz4.Name = "pbDz4";
            this.pbDz4.Size = new System.Drawing.Size(150, 150);
            this.pbDz4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDz4.TabIndex = 1;
            this.pbDz4.TabStop = false;
            // 
            // pbDd4
            // 
            this.pbDd4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDd4.Image = global::HVEX.Properties.Resources.Dd4;
            this.pbDd4.Location = new System.Drawing.Point(3, 12);
            this.pbDd4.Name = "pbDd4";
            this.pbDd4.Size = new System.Drawing.Size(150, 150);
            this.pbDd4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDd4.TabIndex = 0;
            this.pbDd4.TabStop = false;
            // 
            // pn02
            // 
            this.pn02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn02.Controls.Add(this.cbDz2);
            this.pn02.Controls.Add(this.cbDd2);
            this.pn02.Controls.Add(this.pbDz2);
            this.pn02.Controls.Add(this.pbDd2);
            this.pn02.Location = new System.Drawing.Point(11, 440);
            this.pn02.Name = "pn02";
            this.pn02.Size = new System.Drawing.Size(315, 190);
            this.pn02.TabIndex = 9;
            // 
            // cbDz2
            // 
            this.cbDz2.AutoSize = true;
            this.cbDz2.Location = new System.Drawing.Point(159, 168);
            this.cbDz2.Name = "cbDz2";
            this.cbDz2.Size = new System.Drawing.Size(45, 17);
            this.cbDz2.TabIndex = 5;
            this.cbDz2.Text = "Dz2";
            this.cbDz2.UseVisualStyleBackColor = true;
            this.cbDz2.CheckedChanged += new System.EventHandler(this.cbDz2_CheckedChanged);
            // 
            // cbDd2
            // 
            this.cbDd2.AutoSize = true;
            this.cbDd2.Location = new System.Drawing.Point(3, 168);
            this.cbDd2.Name = "cbDd2";
            this.cbDd2.Size = new System.Drawing.Size(46, 17);
            this.cbDd2.TabIndex = 4;
            this.cbDd2.Text = "Dd2";
            this.cbDd2.UseVisualStyleBackColor = true;
            this.cbDd2.CheckedChanged += new System.EventHandler(this.cbDd2_CheckedChanged);
            // 
            // pbDz2
            // 
            this.pbDz2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDz2.Image = global::HVEX.Properties.Resources.Dz2;
            this.pbDz2.Location = new System.Drawing.Point(159, 12);
            this.pbDz2.Name = "pbDz2";
            this.pbDz2.Size = new System.Drawing.Size(150, 150);
            this.pbDz2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDz2.TabIndex = 1;
            this.pbDz2.TabStop = false;
            // 
            // pbDd2
            // 
            this.pbDd2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDd2.Image = global::HVEX.Properties.Resources.Dd2;
            this.pbDd2.Location = new System.Drawing.Point(3, 12);
            this.pbDd2.Name = "pbDd2";
            this.pbDd2.Size = new System.Drawing.Size(150, 150);
            this.pbDd2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDd2.TabIndex = 0;
            this.pbDd2.TabStop = false;
            // 
            // pn01
            // 
            this.pn01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn01.Controls.Add(this.cbYz1);
            this.pn01.Controls.Add(this.cbDy1);
            this.pn01.Controls.Add(this.cbYd1);
            this.pn01.Controls.Add(this.pbYz1);
            this.pn01.Controls.Add(this.pbDy1);
            this.pn01.Controls.Add(this.pbYd1);
            this.pn01.Location = new System.Drawing.Point(11, 230);
            this.pn01.Name = "pn01";
            this.pn01.Size = new System.Drawing.Size(472, 190);
            this.pn01.TabIndex = 8;
            // 
            // cbYz1
            // 
            this.cbYz1.AutoSize = true;
            this.cbYz1.Location = new System.Drawing.Point(315, 168);
            this.cbYz1.Name = "cbYz1";
            this.cbYz1.Size = new System.Drawing.Size(44, 17);
            this.cbYz1.TabIndex = 6;
            this.cbYz1.Text = "Yz1";
            this.cbYz1.UseVisualStyleBackColor = true;
            this.cbYz1.CheckedChanged += new System.EventHandler(this.cbYz1_CheckedChanged);
            // 
            // cbDy1
            // 
            this.cbDy1.AutoSize = true;
            this.cbDy1.Location = new System.Drawing.Point(159, 168);
            this.cbDy1.Name = "cbDy1";
            this.cbDy1.Size = new System.Drawing.Size(45, 17);
            this.cbDy1.TabIndex = 5;
            this.cbDy1.Text = "Dy1";
            this.cbDy1.UseVisualStyleBackColor = true;
            this.cbDy1.CheckedChanged += new System.EventHandler(this.cbDy1_CheckedChanged);
            // 
            // cbYd1
            // 
            this.cbYd1.AutoSize = true;
            this.cbYd1.Location = new System.Drawing.Point(3, 168);
            this.cbYd1.Name = "cbYd1";
            this.cbYd1.Size = new System.Drawing.Size(45, 17);
            this.cbYd1.TabIndex = 4;
            this.cbYd1.Text = "Yd1";
            this.cbYd1.UseVisualStyleBackColor = true;
            this.cbYd1.CheckedChanged += new System.EventHandler(this.cbYd1_CheckedChanged);
            // 
            // pbYz1
            // 
            this.pbYz1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbYz1.Image = global::HVEX.Properties.Resources.Yz1;
            this.pbYz1.Location = new System.Drawing.Point(315, 12);
            this.pbYz1.Name = "pbYz1";
            this.pbYz1.Size = new System.Drawing.Size(150, 150);
            this.pbYz1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbYz1.TabIndex = 2;
            this.pbYz1.TabStop = false;
            // 
            // pbDy1
            // 
            this.pbDy1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDy1.Image = global::HVEX.Properties.Resources.Dy1;
            this.pbDy1.Location = new System.Drawing.Point(159, 12);
            this.pbDy1.Name = "pbDy1";
            this.pbDy1.Size = new System.Drawing.Size(150, 150);
            this.pbDy1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDy1.TabIndex = 1;
            this.pbDy1.TabStop = false;
            // 
            // pbYd1
            // 
            this.pbYd1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbYd1.Image = global::HVEX.Properties.Resources.Yd1;
            this.pbYd1.Location = new System.Drawing.Point(3, 12);
            this.pbYd1.Name = "pbYd1";
            this.pbYd1.Size = new System.Drawing.Size(150, 150);
            this.pbYd1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbYd1.TabIndex = 0;
            this.pbYd1.TabStop = false;
            // 
            // pn00
            // 
            this.pn00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pn00.Controls.Add(this.cbZz0);
            this.pn00.Controls.Add(this.cbDz0);
            this.pn00.Controls.Add(this.cbDd0);
            this.pn00.Controls.Add(this.cbYy0);
            this.pn00.Controls.Add(this.pbZz0);
            this.pn00.Controls.Add(this.pbDz0);
            this.pn00.Controls.Add(this.pbDd0);
            this.pn00.Controls.Add(this.pcYy0);
            this.pn00.Location = new System.Drawing.Point(11, 20);
            this.pn00.Name = "pn00";
            this.pn00.Size = new System.Drawing.Size(628, 190);
            this.pn00.TabIndex = 0;
            // 
            // cbZz0
            // 
            this.cbZz0.AutoSize = true;
            this.cbZz0.Location = new System.Drawing.Point(471, 168);
            this.cbZz0.Name = "cbZz0";
            this.cbZz0.Size = new System.Drawing.Size(44, 17);
            this.cbZz0.TabIndex = 7;
            this.cbZz0.Text = "Zz0";
            this.cbZz0.UseVisualStyleBackColor = true;
            this.cbZz0.CheckedChanged += new System.EventHandler(this.cbZz0_CheckedChanged);
            // 
            // cbDz0
            // 
            this.cbDz0.AutoSize = true;
            this.cbDz0.Location = new System.Drawing.Point(315, 168);
            this.cbDz0.Name = "cbDz0";
            this.cbDz0.Size = new System.Drawing.Size(45, 17);
            this.cbDz0.TabIndex = 6;
            this.cbDz0.Text = "Dz0";
            this.cbDz0.UseVisualStyleBackColor = true;
            this.cbDz0.CheckedChanged += new System.EventHandler(this.cbDz0_CheckedChanged);
            // 
            // cbDd0
            // 
            this.cbDd0.AutoSize = true;
            this.cbDd0.Location = new System.Drawing.Point(159, 168);
            this.cbDd0.Name = "cbDd0";
            this.cbDd0.Size = new System.Drawing.Size(46, 17);
            this.cbDd0.TabIndex = 5;
            this.cbDd0.Text = "Dd0";
            this.cbDd0.UseVisualStyleBackColor = true;
            this.cbDd0.CheckedChanged += new System.EventHandler(this.cbDd0_CheckedChanged);
            // 
            // cbYy0
            // 
            this.cbYy0.AutoSize = true;
            this.cbYy0.Location = new System.Drawing.Point(3, 168);
            this.cbYy0.Name = "cbYy0";
            this.cbYy0.Size = new System.Drawing.Size(44, 17);
            this.cbYy0.TabIndex = 4;
            this.cbYy0.Text = "Yy0";
            this.cbYy0.UseVisualStyleBackColor = true;
            this.cbYy0.CheckedChanged += new System.EventHandler(this.cbYy0_CheckedChanged);
            // 
            // pbZz0
            // 
            this.pbZz0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbZz0.Image = global::HVEX.Properties.Resources.Zz0;
            this.pbZz0.Location = new System.Drawing.Point(471, 12);
            this.pbZz0.Name = "pbZz0";
            this.pbZz0.Size = new System.Drawing.Size(150, 150);
            this.pbZz0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbZz0.TabIndex = 3;
            this.pbZz0.TabStop = false;
            // 
            // pbDz0
            // 
            this.pbDz0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDz0.Image = global::HVEX.Properties.Resources.Dz0;
            this.pbDz0.Location = new System.Drawing.Point(315, 12);
            this.pbDz0.Name = "pbDz0";
            this.pbDz0.Size = new System.Drawing.Size(150, 150);
            this.pbDz0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDz0.TabIndex = 2;
            this.pbDz0.TabStop = false;
            // 
            // pbDd0
            // 
            this.pbDd0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbDd0.Image = global::HVEX.Properties.Resources.Dd0;
            this.pbDd0.Location = new System.Drawing.Point(159, 12);
            this.pbDd0.Name = "pbDd0";
            this.pbDd0.Size = new System.Drawing.Size(150, 150);
            this.pbDd0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbDd0.TabIndex = 1;
            this.pbDd0.TabStop = false;
            // 
            // pcYy0
            // 
            this.pcYy0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pcYy0.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pcYy0.Image = global::HVEX.Properties.Resources.Yy0;
            this.pcYy0.Location = new System.Drawing.Point(3, 12);
            this.pcYy0.Name = "pcYy0";
            this.pcYy0.Size = new System.Drawing.Size(150, 150);
            this.pcYy0.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcYy0.TabIndex = 0;
            this.pcYy0.TabStop = false;
            // 
            // FrmConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(694, 441);
            this.Controls.Add(this.pnConnection);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmConnection";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Conexão";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmConnection_FormClosed);
            this.pnConnection.ResumeLayout(false);
            this.pnConnection.PerformLayout();
            this.pn11.ResumeLayout(false);
            this.pn11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbYz11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDy11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYd11)).EndInit();
            this.pn10.ResumeLayout(false);
            this.pn10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd10)).EndInit();
            this.pn08.ResumeLayout(false);
            this.pn08.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd8)).EndInit();
            this.pn07.ResumeLayout(false);
            this.pn07.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbYz7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDy7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Yd7)).EndInit();
            this.pn06.ResumeLayout(false);
            this.pn06.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYy6)).EndInit();
            this.pn05.ResumeLayout(false);
            this.pn05.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbYz5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDy5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYd5)).EndInit();
            this.pn04.ResumeLayout(false);
            this.pn04.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd4)).EndInit();
            this.pn02.ResumeLayout(false);
            this.pn02.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd2)).EndInit();
            this.pn01.ResumeLayout(false);
            this.pn01.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbYz1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDy1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbYd1)).EndInit();
            this.pn00.ResumeLayout(false);
            this.pn00.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbZz0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDz0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbDd0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcYy0)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnConnection;
        private System.Windows.Forms.Label lb11;
        private System.Windows.Forms.Label lb10;
        private System.Windows.Forms.Label lb08;
        private System.Windows.Forms.Label lb07;
        private System.Windows.Forms.Label lb06;
        private System.Windows.Forms.Label lb05;
        private System.Windows.Forms.Label lb04;
        private System.Windows.Forms.Label lb02;
        private System.Windows.Forms.Label lb01;
        private System.Windows.Forms.Label lb00;
        private System.Windows.Forms.Panel pn11;
        private System.Windows.Forms.CheckBox cbYz11;
        private System.Windows.Forms.CheckBox cbDy11;
        private System.Windows.Forms.CheckBox cbYd11;
        private System.Windows.Forms.PictureBox pbYz11;
        private System.Windows.Forms.PictureBox pbDy11;
        private System.Windows.Forms.PictureBox pbYd11;
        private System.Windows.Forms.Panel pn10;
        private System.Windows.Forms.CheckBox cbDz10;
        private System.Windows.Forms.CheckBox cbDd10;
        private System.Windows.Forms.PictureBox pbDz10;
        private System.Windows.Forms.PictureBox pbDd10;
        private System.Windows.Forms.Panel pn08;
        private System.Windows.Forms.CheckBox cbDz8;
        private System.Windows.Forms.CheckBox cbDd8;
        private System.Windows.Forms.PictureBox pbDz8;
        private System.Windows.Forms.PictureBox pbDd8;
        private System.Windows.Forms.Panel pn07;
        private System.Windows.Forms.CheckBox cbYz7;
        private System.Windows.Forms.CheckBox cbDy7;
        private System.Windows.Forms.CheckBox cbYd7;
        private System.Windows.Forms.PictureBox pbYz7;
        private System.Windows.Forms.PictureBox pbDy7;
        private System.Windows.Forms.PictureBox Yd7;
        private System.Windows.Forms.Panel pn06;
        private System.Windows.Forms.CheckBox cbDz6;
        private System.Windows.Forms.CheckBox cbDd6;
        private System.Windows.Forms.CheckBox cbYy6;
        private System.Windows.Forms.PictureBox pbDz6;
        private System.Windows.Forms.PictureBox pbDd6;
        private System.Windows.Forms.PictureBox pbYy6;
        private System.Windows.Forms.Panel pn05;
        private System.Windows.Forms.CheckBox cbYz5;
        private System.Windows.Forms.CheckBox cbDy5;
        private System.Windows.Forms.CheckBox cbYd5;
        private System.Windows.Forms.PictureBox pbYz5;
        private System.Windows.Forms.PictureBox pbDy5;
        private System.Windows.Forms.PictureBox pbYd5;
        private System.Windows.Forms.Panel pn04;
        private System.Windows.Forms.CheckBox cbDz4;
        private System.Windows.Forms.CheckBox cbDd4;
        private System.Windows.Forms.PictureBox pbDz4;
        private System.Windows.Forms.PictureBox pbDd4;
        private System.Windows.Forms.Panel pn02;
        private System.Windows.Forms.CheckBox cbDz2;
        private System.Windows.Forms.CheckBox cbDd2;
        private System.Windows.Forms.PictureBox pbDz2;
        private System.Windows.Forms.PictureBox pbDd2;
        private System.Windows.Forms.Panel pn01;
        private System.Windows.Forms.CheckBox cbYz1;
        private System.Windows.Forms.CheckBox cbDy1;
        private System.Windows.Forms.CheckBox cbYd1;
        private System.Windows.Forms.PictureBox pbYz1;
        private System.Windows.Forms.PictureBox pbDy1;
        private System.Windows.Forms.PictureBox pbYd1;
        private System.Windows.Forms.Panel pn00;
        private System.Windows.Forms.CheckBox cbZz0;
        private System.Windows.Forms.CheckBox cbDz0;
        private System.Windows.Forms.CheckBox cbDd0;
        private System.Windows.Forms.CheckBox cbYy0;
        private System.Windows.Forms.PictureBox pbZz0;
        private System.Windows.Forms.PictureBox pbDz0;
        private System.Windows.Forms.PictureBox pbDd0;
        private System.Windows.Forms.PictureBox pcYy0;
    }
}