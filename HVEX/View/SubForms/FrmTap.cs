﻿using HVEX.Service;
using HVEX.View.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;

namespace HVEX.View.SubForms {
    public partial class FrmTap : Form {

        private ITransformer view;

        public FrmTap (ITransformer frm) {
            view = frm;
            InitializeComponent();
        }

        #region Events
        private void FrmTap_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }
        private void btOk_Click(object sender, EventArgs e) {
            InsertTap();
        }
        private void tbTap_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                InsertTap();
            }
        }
        private void tbTap_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        #endregion

        #region Functions
        private void InsertTap() {
            try {
                List<double> taps = new List<double>();
                taps.Add(double.Parse(tbTap.Text, CultureInfo.GetCultureInfo("pt-BR")));
                view.TrafoTaps = taps;
                Close();
            } catch {
                new FormException("Insira o valor de tensão do Tap");
            }
        }

        #endregion
  
    }
}
