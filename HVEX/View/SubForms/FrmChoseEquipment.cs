﻿using HVEX.Controller;
using HVEX.Service;
using HVEX.View.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HVEX.View.SubForms {
    public partial class FrmChoseEquipment : Form, IEquipment {

        EquipmentController controller;
        private readonly string loggedUser;
        ITestShunt frmTestShunt;
        ITestFra frmTestFra;
        ITestPdmsThreePhase frmTestPdmsThreePhase;
        ITestPdmsSinglePhase frmTestPdmsSinglePhase;
        string control = "shunt";

        #region Interface Attributes
        public string Id { get; set; }
        public string SerialNumber { get; set; }
        public string Type { get; set; }
        public string Model { get; set; }
        public DataTable List {
            set {
                if (value != null) {
                    BindingSource binding = new BindingSource();
                    binding.DataSource = value;
                    dgTransformers.DataSource = binding;
                    dgTransformers.Columns[0].Visible = false;
                }
            }
        }
        #endregion

        public FrmChoseEquipment(string userId, ITestShunt frm) {
            controller = new EquipmentController(this);
            loggedUser = userId;
            frmTestShunt = frm;
            InitializeComponent();
            control = "shunt";
        }

        public FrmChoseEquipment(string userId, ITestFra frm) {
            controller = new EquipmentController(this);
            loggedUser = userId;
            frmTestFra = frm;
            InitializeComponent();
            control = "fra";
        }

        public FrmChoseEquipment(string userId, ITestPdmsThreePhase frm) {
            controller = new EquipmentController(this);
            loggedUser = userId;
            frmTestPdmsThreePhase = frm;
            InitializeComponent();
            control = "pdmsThreePhase";
        }

        public FrmChoseEquipment(string userId, ITestPdmsSinglePhase frm) {
            controller = new EquipmentController(this);
            loggedUser = userId;
            frmTestPdmsSinglePhase = frm;
            InitializeComponent();
            control = "pdmsSinglePhase";
        }

        #region Events
        private async void FrmChoseEquipment_Load(object sender, EventArgs e) {
            await LoadEquipments();
        }
        private void FrmChoseEquipment_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }
        private async void tbSearchSerial_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadEquipments();
            }
        }
        private async void tbSearchModel_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadEquipments();
            }
        }
        private void dgTransformers_RowHeaderMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e) {
            SelectEquipment();
        }
        private async void btSearch_Click(object sender, EventArgs e) {
            await LoadEquipments();
        }
        private void btSelect_Click(object sender, EventArgs e) {
            SelectEquipment();
        }
        #endregion

        #region Functions
        private async Task LoadEquipments() {
            await controller.List(loggedUser, tbSearchModel.Text, tbSearchModel.Text);
        }
        private void SelectEquipment() {
            try {
                if (control == "shunt") {
                    frmTestShunt.EquipmentId = dgTransformers.SelectedRows[0].Cells[0].Value.ToString();
                    frmTestShunt.EquipmentDescription = dgTransformers.SelectedRows[0].Cells[1].Value.ToString() + " - " + dgTransformers.SelectedRows[0].Cells[2].Value.ToString();
                    Close();
                } else if (control == "fra"){
                    frmTestFra.EquipmentId = dgTransformers.SelectedRows[0].Cells[0].Value.ToString();
                    frmTestFra.EquipmentDescription = dgTransformers.SelectedRows[0].Cells[1].Value.ToString() + " - " + dgTransformers.SelectedRows[0].Cells[2].Value.ToString();
                    Close();
                } else if (control == "pdmsThreePhase") {
                    frmTestPdmsThreePhase.EquipmentId = dgTransformers.SelectedRows[0].Cells[0].Value.ToString();
                    frmTestPdmsThreePhase.EquipmentDescription = dgTransformers.SelectedRows[0].Cells[1].Value.ToString() + " - " + dgTransformers.SelectedRows[0].Cells[2].Value.ToString();
                    Close();
                } else if (control == "pdmsSinglePhase") {
                    frmTestPdmsSinglePhase.EquipmentId = dgTransformers.SelectedRows[0].Cells[0].Value.ToString();
                    frmTestPdmsSinglePhase.EquipmentDescription = dgTransformers.SelectedRows[0].Cells[1].Value.ToString() + " - " + dgTransformers.SelectedRows[0].Cells[2].Value.ToString();
                    Close();
                }
                //Close();
            } catch {
                MessageBox.Show("Selecione a linha do equipamento que deseja testar.");
            }

        }
        #endregion 

    }
}
