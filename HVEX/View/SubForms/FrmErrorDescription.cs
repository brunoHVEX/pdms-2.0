﻿using HVEX.Service;
using HVEX.View.Interface;
using System;
using System.Data;
using System.Windows.Forms;

namespace HVEX.View.SubForms {
    public partial class FrmErrorDescription : Form, ILog {

        #region Interface Attributes
        public string Id { get; set; }
        public string Description { get; set; }
        public string Error { get; set; }
        public string Source { get; set; }
        public DateTime Date { get; set; }
        public string User { get; set; }
        public DataTable List { get; set; }
        #endregion

        public FrmErrorDescription() {
            InitializeComponent();
        }
        private void FrmErrorDescription_Load(object sender, EventArgs e) {
            rtErrorMessage.Text = Error;
        }
        private void FrmErrorDescription_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }
        private void btOk_Click(object sender, EventArgs e) {
            Close();
        }
    }
}
