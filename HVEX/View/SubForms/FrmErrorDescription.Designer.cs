﻿
namespace HVEX.View.SubForms {
    partial class FrmErrorDescription {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmErrorDescription));
            this.pnError = new System.Windows.Forms.Panel();
            this.btOk = new System.Windows.Forms.Button();
            this.rtErrorMessage = new System.Windows.Forms.RichTextBox();
            this.pnError.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnError
            // 
            this.pnError.BackColor = System.Drawing.Color.White;
            this.pnError.Controls.Add(this.btOk);
            this.pnError.Controls.Add(this.rtErrorMessage);
            this.pnError.Location = new System.Drawing.Point(12, 12);
            this.pnError.Name = "pnError";
            this.pnError.Size = new System.Drawing.Size(560, 297);
            this.pnError.TabIndex = 1;
            // 
            // btOk
            // 
            this.btOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btOk.ForeColor = System.Drawing.Color.White;
            this.btOk.Location = new System.Drawing.Point(487, 264);
            this.btOk.Name = "btOk";
            this.btOk.Size = new System.Drawing.Size(60, 23);
            this.btOk.TabIndex = 3;
            this.btOk.Text = "OK";
            this.btOk.UseVisualStyleBackColor = false;
            this.btOk.Click += new System.EventHandler(this.btOk_Click);
            // 
            // rtErrorMessage
            // 
            this.rtErrorMessage.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.rtErrorMessage.Location = new System.Drawing.Point(12, 12);
            this.rtErrorMessage.Name = "rtErrorMessage";
            this.rtErrorMessage.ReadOnly = true;
            this.rtErrorMessage.Size = new System.Drawing.Size(535, 246);
            this.rtErrorMessage.TabIndex = 0;
            this.rtErrorMessage.Text = "";
            // 
            // FrmErrorDescription
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(584, 321);
            this.Controls.Add(this.pnError);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmErrorDescription";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Erro";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmErrorDescription_FormClosed);
            this.Load += new System.EventHandler(this.FrmErrorDescription_Load);
            this.pnError.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnError;
        private System.Windows.Forms.Button btOk;
        private System.Windows.Forms.RichTextBox rtErrorMessage;
    }
}