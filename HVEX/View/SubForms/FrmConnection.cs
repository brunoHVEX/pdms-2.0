﻿using HVEX.Service;
using HVEX.View.Interface;
using System;
using System.Windows.Forms;

namespace HVEX.View.SubForms {
    public partial class FrmConnection : Form {

        private ITransformer view;

        public FrmConnection(ITransformer frm) {
            view = frm;
            InitializeComponent();
        }

        #region Events
        private void FrmConnection_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }
        private void cbYy0_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYy0);
        }
        private void cbDd0_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDd0);
        }
        private void cbDz0_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDz0);
        }
        private void cbZz0_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbZz0);
        }
        private void cbYd1_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYd1);
        }
        private void cbDy1_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDy1);
        }
        private void cbYz1_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYz1);
        }
        private void cbDd2_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDd2);
        }
        private void cbDz2_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDz2);
        }
        private void cbDd4_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDd4);
        }
        private void cbDz4_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDz4);
        }
        private void cbYd5_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYd5);
        }
        private void cbDy5_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDy5);
        }
        private void cbYz5_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYz5);
        }
        private void cbYy6_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYy6);
        }
        private void cbDd6_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDd6);
        }
        private void cbDz6_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDz6);
        }
        private void cbYd7_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYd7);
        }
        private void cbDy7_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDy7);
        }
        private void cbYz7_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYz7);
        }
        private void cbDd8_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDd8);
        }
        private void cbDz8_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDz8);
        }
        private void cbDd10_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDd10);
        }
        private void cbDz10_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDz10);
        }
        private void cbYd11_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYd11);
        }
        private void cbDy11_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbDy11);
        }
        private void cbYz11_CheckedChanged(object sender, EventArgs e) {
            SetConnection(cbYz11);
        }
        #endregion

        #region Functions
        private void SetConnection(CheckBox checkBox) {
            if (checkBox.Checked) {
                view.TrafoConnection = checkBox.Text;
                Close();
            }
        }
        #endregion

    }
}
