﻿
namespace HVEX.View {
    partial class FrmTestPdmsSinglePhase {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTestPdmsSinglePhase));
            this.btGetEquipment = new System.Windows.Forms.Button();
            this.tbTransformer = new System.Windows.Forms.TextBox();
            this.lbTransformer = new System.Windows.Forms.Label();
            this.pnTest = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tbGraphs = new System.Windows.Forms.TabControl();
            this.tpSignals = new System.Windows.Forms.TabPage();
            this.tlpSignal = new System.Windows.Forms.TableLayoutPanel();
            this.gcSignalFiltredPhase1 = new ZedGraph.ZedGraphControl();
            this.gcSignalPhase1 = new ZedGraph.ZedGraphControl();
            this.tpDispersion = new System.Windows.Forms.TabPage();
            this.tlpDispersion = new System.Windows.Forms.TableLayoutPanel();
            this.gcDispersionPhase1 = new ZedGraph.ZedGraphControl();
            this.tpDispersionDetailed = new System.Windows.Forms.TabPage();
            this.tlpDispersionDetailed = new System.Windows.Forms.TableLayoutPanel();
            this.gcDispersionDetailedPhase1 = new ZedGraph.ZedGraphControl();
            this.tpPartialDischargeTrend = new System.Windows.Forms.TabPage();
            this.tlpPartialDischargeTrend = new System.Windows.Forms.TableLayoutPanel();
            this.gcPartialDischargeTrendPhase1 = new ZedGraph.ZedGraphControl();
            this.tpFft = new System.Windows.Forms.TabPage();
            this.tlpFft = new System.Windows.Forms.TableLayoutPanel();
            this.gcFftPhase1 = new ZedGraph.ZedGraphControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gbTotalDischarges = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lbTotalOccurrences = new System.Windows.Forms.Label();
            this.lbTotalPositiveDischarges = new System.Windows.Forms.Label();
            this.tbTotalPositiveDischarges = new System.Windows.Forms.TextBox();
            this.lbTotalNegativeDischarges = new System.Windows.Forms.Label();
            this.tbTotalNegativeDischarges = new System.Windows.Forms.TextBox();
            this.tbTotalOccurrences = new System.Windows.Forms.TextBox();
            this.gbPartialDischargePhase1 = new System.Windows.Forms.GroupBox();
            this.tpPartialDischargePhase1 = new System.Windows.Forms.TableLayoutPanel();
            this.tbOccurrencesPhase1 = new System.Windows.Forms.TextBox();
            this.lbOccurrencesPhase1 = new System.Windows.Forms.Label();
            this.lbChargePhase1 = new System.Windows.Forms.Label();
            this.tbChargePhase1 = new System.Windows.Forms.TextBox();
            this.nudTestTime = new System.Windows.Forms.NumericUpDown();
            this.btStopTest = new System.Windows.Forms.Button();
            this.btStartTest = new System.Windows.Forms.Button();
            this.lbTimer = new System.Windows.Forms.Label();
            this.gbTestController = new System.Windows.Forms.GroupBox();
            this.btResetConfigurations = new System.Windows.Forms.Button();
            this.btSaveCalibration = new System.Windows.Forms.Button();
            this.btStop = new System.Windows.Forms.Button();
            this.btCalibration = new System.Windows.Forms.Button();
            this.gbCalibration = new System.Windows.Forms.GroupBox();
            this.tpCalibration = new System.Windows.Forms.TableLayoutPanel();
            this.tbCalibrationPhase1 = new System.Windows.Forms.NumericUpDown();
            this.lbCalibrationPhase1 = new System.Windows.Forms.Label();
            this.lbDp = new System.Windows.Forms.Label();
            this.lbPhase = new System.Windows.Forms.Label();
            this.tableCute = new System.Windows.Forms.TableLayoutPanel();
            this.cbFrequencyTopCut = new System.Windows.Forms.ComboBox();
            this.tbFrequencyTopCut = new System.Windows.Forms.NumericUpDown();
            this.lbTopCut = new System.Windows.Forms.Label();
            this.cbFrequencyUndercut = new System.Windows.Forms.ComboBox();
            this.lbUndercut = new System.Windows.Forms.Label();
            this.tbFrequencyUndercut = new System.Windows.Forms.NumericUpDown();
            this.tableFilter = new System.Windows.Forms.TableLayoutPanel();
            this.cbFilter = new System.Windows.Forms.ComboBox();
            this.lbFilter = new System.Windows.Forms.Label();
            this.lbOrder = new System.Windows.Forms.Label();
            this.tbOrder = new System.Windows.Forms.NumericUpDown();
            this.lbParamters = new System.Windows.Forms.Label();
            this.tableController = new System.Windows.Forms.TableLayoutPanel();
            this.lbPhase1 = new System.Windows.Forms.Label();
            this.lbGain = new System.Windows.Forms.Label();
            this.lbGate = new System.Windows.Forms.Label();
            this.tbGainPhase1 = new System.Windows.Forms.NumericUpDown();
            this.tbGatePhase1 = new System.Windows.Forms.NumericUpDown();
            this.cbOsciloscope = new System.Windows.Forms.ComboBox();
            this.btStart = new System.Windows.Forms.Button();
            this.lbOsciloscope = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lbChannel = new System.Windows.Forms.Label();
            this.btConnection = new System.Windows.Forms.Button();
            this.lbConnection = new System.Windows.Forms.Label();
            this.lbOsciloscopePort = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.tbIp = new System.Windows.Forms.MaskedTextBox();
            this.lbOsciloscopeIp = new System.Windows.Forms.Label();
            this.lbBrand = new System.Windows.Forms.Label();
            this.cbChannel = new System.Windows.Forms.ComboBox();
            this.timerForm = new System.Windows.Forms.Timer(this.components);
            this.pnTest.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tbGraphs.SuspendLayout();
            this.tpSignals.SuspendLayout();
            this.tlpSignal.SuspendLayout();
            this.tpDispersion.SuspendLayout();
            this.tlpDispersion.SuspendLayout();
            this.tpDispersionDetailed.SuspendLayout();
            this.tlpDispersionDetailed.SuspendLayout();
            this.tpPartialDischargeTrend.SuspendLayout();
            this.tlpPartialDischargeTrend.SuspendLayout();
            this.tpFft.SuspendLayout();
            this.tlpFft.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbTotalDischarges.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.gbPartialDischargePhase1.SuspendLayout();
            this.tpPartialDischargePhase1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTestTime)).BeginInit();
            this.gbTestController.SuspendLayout();
            this.gbCalibration.SuspendLayout();
            this.tpCalibration.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalibrationPhase1)).BeginInit();
            this.tableCute.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrequencyTopCut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrequencyUndercut)).BeginInit();
            this.tableFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOrder)).BeginInit();
            this.tableController.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbGainPhase1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGatePhase1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btGetEquipment
            // 
            this.btGetEquipment.Location = new System.Drawing.Point(527, 11);
            this.btGetEquipment.Name = "btGetEquipment";
            this.btGetEquipment.Size = new System.Drawing.Size(30, 25);
            this.btGetEquipment.TabIndex = 18;
            this.btGetEquipment.Text = "...";
            this.btGetEquipment.UseVisualStyleBackColor = true;
            this.btGetEquipment.Click += new System.EventHandler(this.btGetEquipment_Click);
            // 
            // tbTransformer
            // 
            this.tbTransformer.BackColor = System.Drawing.SystemColors.Window;
            this.tbTransformer.Enabled = false;
            this.tbTransformer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTransformer.Location = new System.Drawing.Point(177, 12);
            this.tbTransformer.Name = "tbTransformer";
            this.tbTransformer.ReadOnly = true;
            this.tbTransformer.Size = new System.Drawing.Size(350, 23);
            this.tbTransformer.TabIndex = 21;
            // 
            // lbTransformer
            // 
            this.lbTransformer.AutoSize = true;
            this.lbTransformer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTransformer.ForeColor = System.Drawing.Color.White;
            this.lbTransformer.Location = new System.Drawing.Point(29, 12);
            this.lbTransformer.Name = "lbTransformer";
            this.lbTransformer.Size = new System.Drawing.Size(142, 20);
            this.lbTransformer.TabIndex = 20;
            this.lbTransformer.Text = "EQUIPAMENTO:";
            // 
            // pnTest
            // 
            this.pnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnTest.BackColor = System.Drawing.Color.White;
            this.pnTest.Controls.Add(this.panel1);
            this.pnTest.Controls.Add(this.groupBox1);
            this.pnTest.Controls.Add(this.gbTestController);
            this.pnTest.Location = new System.Drawing.Point(14, 40);
            this.pnTest.Name = "pnTest";
            this.pnTest.Size = new System.Drawing.Size(1257, 659);
            this.pnTest.TabIndex = 19;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tbGraphs);
            this.panel1.Location = new System.Drawing.Point(236, 10);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(780, 640);
            this.panel1.TabIndex = 18;
            // 
            // tbGraphs
            // 
            this.tbGraphs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGraphs.Controls.Add(this.tpSignals);
            this.tbGraphs.Controls.Add(this.tpDispersion);
            this.tbGraphs.Controls.Add(this.tpDispersionDetailed);
            this.tbGraphs.Controls.Add(this.tpPartialDischargeTrend);
            this.tbGraphs.Controls.Add(this.tpFft);
            this.tbGraphs.ItemSize = new System.Drawing.Size(42, 18);
            this.tbGraphs.Location = new System.Drawing.Point(3, 6);
            this.tbGraphs.Name = "tbGraphs";
            this.tbGraphs.SelectedIndex = 0;
            this.tbGraphs.Size = new System.Drawing.Size(779, 634);
            this.tbGraphs.TabIndex = 0;
            // 
            // tpSignals
            // 
            this.tpSignals.Controls.Add(this.tlpSignal);
            this.tpSignals.Location = new System.Drawing.Point(4, 22);
            this.tpSignals.Name = "tpSignals";
            this.tpSignals.Padding = new System.Windows.Forms.Padding(3);
            this.tpSignals.Size = new System.Drawing.Size(771, 608);
            this.tpSignals.TabIndex = 0;
            this.tpSignals.Text = "Sinal";
            this.tpSignals.UseVisualStyleBackColor = true;
            // 
            // tlpSignal
            // 
            this.tlpSignal.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpSignal.ColumnCount = 1;
            this.tlpSignal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpSignal.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpSignal.Controls.Add(this.gcSignalFiltredPhase1, 0, 1);
            this.tlpSignal.Controls.Add(this.gcSignalPhase1, 0, 0);
            this.tlpSignal.Location = new System.Drawing.Point(3, 3);
            this.tlpSignal.Name = "tlpSignal";
            this.tlpSignal.RowCount = 2;
            this.tlpSignal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpSignal.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpSignal.Size = new System.Drawing.Size(765, 602);
            this.tlpSignal.TabIndex = 0;
            // 
            // gcSignalFiltredPhase1
            // 
            this.gcSignalFiltredPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSignalFiltredPhase1.Location = new System.Drawing.Point(3, 304);
            this.gcSignalFiltredPhase1.Name = "gcSignalFiltredPhase1";
            this.gcSignalFiltredPhase1.ScrollGrace = 0D;
            this.gcSignalFiltredPhase1.ScrollMaxX = 0D;
            this.gcSignalFiltredPhase1.ScrollMaxY = 0D;
            this.gcSignalFiltredPhase1.ScrollMaxY2 = 0D;
            this.gcSignalFiltredPhase1.ScrollMinX = 0D;
            this.gcSignalFiltredPhase1.ScrollMinY = 0D;
            this.gcSignalFiltredPhase1.ScrollMinY2 = 0D;
            this.gcSignalFiltredPhase1.Size = new System.Drawing.Size(759, 295);
            this.gcSignalFiltredPhase1.TabIndex = 2;
            this.gcSignalFiltredPhase1.UseExtendedPrintDialog = true;
            // 
            // gcSignalPhase1
            // 
            this.gcSignalPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSignalPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcSignalPhase1.Name = "gcSignalPhase1";
            this.gcSignalPhase1.ScrollGrace = 0D;
            this.gcSignalPhase1.ScrollMaxX = 0D;
            this.gcSignalPhase1.ScrollMaxY = 0D;
            this.gcSignalPhase1.ScrollMaxY2 = 0D;
            this.gcSignalPhase1.ScrollMinX = 0D;
            this.gcSignalPhase1.ScrollMinY = 0D;
            this.gcSignalPhase1.ScrollMinY2 = 0D;
            this.gcSignalPhase1.Size = new System.Drawing.Size(759, 295);
            this.gcSignalPhase1.TabIndex = 1;
            this.gcSignalPhase1.UseExtendedPrintDialog = true;
            // 
            // tpDispersion
            // 
            this.tpDispersion.Controls.Add(this.tlpDispersion);
            this.tpDispersion.Location = new System.Drawing.Point(4, 22);
            this.tpDispersion.Name = "tpDispersion";
            this.tpDispersion.Padding = new System.Windows.Forms.Padding(3);
            this.tpDispersion.Size = new System.Drawing.Size(771, 608);
            this.tpDispersion.TabIndex = 1;
            this.tpDispersion.Text = "Dispersão";
            this.tpDispersion.UseVisualStyleBackColor = true;
            // 
            // tlpDispersion
            // 
            this.tlpDispersion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpDispersion.ColumnCount = 1;
            this.tlpDispersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDispersion.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDispersion.Controls.Add(this.gcDispersionPhase1, 0, 0);
            this.tlpDispersion.Location = new System.Drawing.Point(3, 3);
            this.tlpDispersion.Name = "tlpDispersion";
            this.tlpDispersion.RowCount = 1;
            this.tlpDispersion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDispersion.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDispersion.Size = new System.Drawing.Size(765, 602);
            this.tlpDispersion.TabIndex = 0;
            // 
            // gcDispersionPhase1
            // 
            this.gcDispersionPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDispersionPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcDispersionPhase1.Name = "gcDispersionPhase1";
            this.gcDispersionPhase1.ScrollGrace = 0D;
            this.gcDispersionPhase1.ScrollMaxX = 0D;
            this.gcDispersionPhase1.ScrollMaxY = 0D;
            this.gcDispersionPhase1.ScrollMaxY2 = 0D;
            this.gcDispersionPhase1.ScrollMinX = 0D;
            this.gcDispersionPhase1.ScrollMinY = 0D;
            this.gcDispersionPhase1.ScrollMinY2 = 0D;
            this.gcDispersionPhase1.Size = new System.Drawing.Size(759, 596);
            this.gcDispersionPhase1.TabIndex = 1;
            this.gcDispersionPhase1.UseExtendedPrintDialog = true;
            // 
            // tpDispersionDetailed
            // 
            this.tpDispersionDetailed.Controls.Add(this.tlpDispersionDetailed);
            this.tpDispersionDetailed.Location = new System.Drawing.Point(4, 22);
            this.tpDispersionDetailed.Name = "tpDispersionDetailed";
            this.tpDispersionDetailed.Size = new System.Drawing.Size(771, 608);
            this.tpDispersionDetailed.TabIndex = 2;
            this.tpDispersionDetailed.Text = "Dispersão Detalhada";
            this.tpDispersionDetailed.UseVisualStyleBackColor = true;
            // 
            // tlpDispersionDetailed
            // 
            this.tlpDispersionDetailed.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpDispersionDetailed.ColumnCount = 1;
            this.tlpDispersionDetailed.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDispersionDetailed.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDispersionDetailed.Controls.Add(this.gcDispersionDetailedPhase1, 0, 0);
            this.tlpDispersionDetailed.Location = new System.Drawing.Point(3, 3);
            this.tlpDispersionDetailed.Name = "tlpDispersionDetailed";
            this.tlpDispersionDetailed.RowCount = 1;
            this.tlpDispersionDetailed.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDispersionDetailed.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpDispersionDetailed.Size = new System.Drawing.Size(765, 602);
            this.tlpDispersionDetailed.TabIndex = 1;
            // 
            // gcDispersionDetailedPhase1
            // 
            this.gcDispersionDetailedPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcDispersionDetailedPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcDispersionDetailedPhase1.Name = "gcDispersionDetailedPhase1";
            this.gcDispersionDetailedPhase1.ScrollGrace = 0D;
            this.gcDispersionDetailedPhase1.ScrollMaxX = 0D;
            this.gcDispersionDetailedPhase1.ScrollMaxY = 0D;
            this.gcDispersionDetailedPhase1.ScrollMaxY2 = 0D;
            this.gcDispersionDetailedPhase1.ScrollMinX = 0D;
            this.gcDispersionDetailedPhase1.ScrollMinY = 0D;
            this.gcDispersionDetailedPhase1.ScrollMinY2 = 0D;
            this.gcDispersionDetailedPhase1.Size = new System.Drawing.Size(759, 596);
            this.gcDispersionDetailedPhase1.TabIndex = 1;
            this.gcDispersionDetailedPhase1.UseExtendedPrintDialog = true;
            // 
            // tpPartialDischargeTrend
            // 
            this.tpPartialDischargeTrend.Controls.Add(this.tlpPartialDischargeTrend);
            this.tpPartialDischargeTrend.Location = new System.Drawing.Point(4, 22);
            this.tpPartialDischargeTrend.Name = "tpPartialDischargeTrend";
            this.tpPartialDischargeTrend.Size = new System.Drawing.Size(771, 608);
            this.tpPartialDischargeTrend.TabIndex = 3;
            this.tpPartialDischargeTrend.Text = "Tendência DP";
            this.tpPartialDischargeTrend.UseVisualStyleBackColor = true;
            // 
            // tlpPartialDischargeTrend
            // 
            this.tlpPartialDischargeTrend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpPartialDischargeTrend.ColumnCount = 1;
            this.tlpPartialDischargeTrend.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPartialDischargeTrend.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPartialDischargeTrend.Controls.Add(this.gcPartialDischargeTrendPhase1, 0, 0);
            this.tlpPartialDischargeTrend.Location = new System.Drawing.Point(3, 3);
            this.tlpPartialDischargeTrend.Name = "tlpPartialDischargeTrend";
            this.tlpPartialDischargeTrend.RowCount = 1;
            this.tlpPartialDischargeTrend.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPartialDischargeTrend.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPartialDischargeTrend.Size = new System.Drawing.Size(765, 602);
            this.tlpPartialDischargeTrend.TabIndex = 2;
            // 
            // gcPartialDischargeTrendPhase1
            // 
            this.gcPartialDischargeTrendPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcPartialDischargeTrendPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcPartialDischargeTrendPhase1.Name = "gcPartialDischargeTrendPhase1";
            this.gcPartialDischargeTrendPhase1.ScrollGrace = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMaxX = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMaxY = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMaxY2 = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMinX = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMinY = 0D;
            this.gcPartialDischargeTrendPhase1.ScrollMinY2 = 0D;
            this.gcPartialDischargeTrendPhase1.Size = new System.Drawing.Size(759, 596);
            this.gcPartialDischargeTrendPhase1.TabIndex = 1;
            this.gcPartialDischargeTrendPhase1.UseExtendedPrintDialog = true;
            // 
            // tpFft
            // 
            this.tpFft.Controls.Add(this.tlpFft);
            this.tpFft.Location = new System.Drawing.Point(4, 22);
            this.tpFft.Name = "tpFft";
            this.tpFft.Size = new System.Drawing.Size(771, 608);
            this.tpFft.TabIndex = 4;
            this.tpFft.Text = "FFT";
            this.tpFft.UseVisualStyleBackColor = true;
            // 
            // tlpFft
            // 
            this.tlpFft.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpFft.ColumnCount = 1;
            this.tlpFft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpFft.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpFft.Controls.Add(this.gcFftPhase1, 0, 0);
            this.tlpFft.Location = new System.Drawing.Point(3, 3);
            this.tlpFft.Name = "tlpFft";
            this.tlpFft.RowCount = 1;
            this.tlpFft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpFft.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpFft.Size = new System.Drawing.Size(765, 602);
            this.tlpFft.TabIndex = 3;
            // 
            // gcFftPhase1
            // 
            this.gcFftPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcFftPhase1.Location = new System.Drawing.Point(3, 3);
            this.gcFftPhase1.Name = "gcFftPhase1";
            this.gcFftPhase1.ScrollGrace = 0D;
            this.gcFftPhase1.ScrollMaxX = 0D;
            this.gcFftPhase1.ScrollMaxY = 0D;
            this.gcFftPhase1.ScrollMaxY2 = 0D;
            this.gcFftPhase1.ScrollMinX = 0D;
            this.gcFftPhase1.ScrollMinY = 0D;
            this.gcFftPhase1.ScrollMinY2 = 0D;
            this.gcFftPhase1.Size = new System.Drawing.Size(759, 596);
            this.gcFftPhase1.TabIndex = 1;
            this.gcFftPhase1.UseExtendedPrintDialog = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.gbTotalDischarges);
            this.groupBox1.Controls.Add(this.gbPartialDischargePhase1);
            this.groupBox1.Controls.Add(this.nudTestTime);
            this.groupBox1.Controls.Add(this.btStopTest);
            this.groupBox1.Controls.Add(this.btStartTest);
            this.groupBox1.Controls.Add(this.lbTimer);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(1025, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(220, 640);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ensaio";
            // 
            // gbTotalDischarges
            // 
            this.gbTotalDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbTotalDischarges.Controls.Add(this.tableLayoutPanel4);
            this.gbTotalDischarges.Location = new System.Drawing.Point(6, 165);
            this.gbTotalDischarges.Name = "gbTotalDischarges";
            this.gbTotalDischarges.Size = new System.Drawing.Size(208, 114);
            this.gbTotalDischarges.TabIndex = 27;
            this.gbTotalDischarges.TabStop = false;
            this.gbTotalDischarges.Text = "Descargas (Total)";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel4.Controls.Add(this.lbTotalOccurrences, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lbTotalPositiveDischarges, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tbTotalPositiveDischarges, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.lbTotalNegativeDischarges, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbTotalNegativeDischarges, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.tbTotalOccurrences, 1, 2);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(6, 19);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(196, 89);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // lbTotalOccurrences
            // 
            this.lbTotalOccurrences.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTotalOccurrences.AutoSize = true;
            this.lbTotalOccurrences.BackColor = System.Drawing.Color.Transparent;
            this.lbTotalOccurrences.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalOccurrences.Location = new System.Drawing.Point(1, 59);
            this.lbTotalOccurrences.Margin = new System.Windows.Forms.Padding(0);
            this.lbTotalOccurrences.Name = "lbTotalOccurrences";
            this.lbTotalOccurrences.Size = new System.Drawing.Size(67, 29);
            this.lbTotalOccurrences.TabIndex = 4;
            this.lbTotalOccurrences.Text = "Ocorrências:";
            this.lbTotalOccurrences.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbTotalPositiveDischarges
            // 
            this.lbTotalPositiveDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTotalPositiveDischarges.AutoSize = true;
            this.lbTotalPositiveDischarges.BackColor = System.Drawing.Color.Transparent;
            this.lbTotalPositiveDischarges.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalPositiveDischarges.Location = new System.Drawing.Point(1, 1);
            this.lbTotalPositiveDischarges.Margin = new System.Windows.Forms.Padding(0);
            this.lbTotalPositiveDischarges.Name = "lbTotalPositiveDischarges";
            this.lbTotalPositiveDischarges.Size = new System.Drawing.Size(67, 28);
            this.lbTotalPositiveDischarges.TabIndex = 0;
            this.lbTotalPositiveDischarges.Text = "Positiva:";
            this.lbTotalPositiveDischarges.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbTotalPositiveDischarges
            // 
            this.tbTotalPositiveDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTotalPositiveDischarges.Location = new System.Drawing.Point(72, 4);
            this.tbTotalPositiveDischarges.Name = "tbTotalPositiveDischarges";
            this.tbTotalPositiveDischarges.Size = new System.Drawing.Size(120, 20);
            this.tbTotalPositiveDischarges.TabIndex = 2;
            // 
            // lbTotalNegativeDischarges
            // 
            this.lbTotalNegativeDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTotalNegativeDischarges.AutoSize = true;
            this.lbTotalNegativeDischarges.BackColor = System.Drawing.Color.Transparent;
            this.lbTotalNegativeDischarges.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotalNegativeDischarges.Location = new System.Drawing.Point(1, 30);
            this.lbTotalNegativeDischarges.Margin = new System.Windows.Forms.Padding(0);
            this.lbTotalNegativeDischarges.Name = "lbTotalNegativeDischarges";
            this.lbTotalNegativeDischarges.Size = new System.Drawing.Size(67, 28);
            this.lbTotalNegativeDischarges.TabIndex = 1;
            this.lbTotalNegativeDischarges.Text = "Negativa:";
            this.lbTotalNegativeDischarges.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbTotalNegativeDischarges
            // 
            this.tbTotalNegativeDischarges.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTotalNegativeDischarges.Location = new System.Drawing.Point(72, 33);
            this.tbTotalNegativeDischarges.Name = "tbTotalNegativeDischarges";
            this.tbTotalNegativeDischarges.Size = new System.Drawing.Size(120, 20);
            this.tbTotalNegativeDischarges.TabIndex = 3;
            // 
            // tbTotalOccurrences
            // 
            this.tbTotalOccurrences.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbTotalOccurrences.Location = new System.Drawing.Point(72, 62);
            this.tbTotalOccurrences.Name = "tbTotalOccurrences";
            this.tbTotalOccurrences.Size = new System.Drawing.Size(120, 20);
            this.tbTotalOccurrences.TabIndex = 5;
            // 
            // gbPartialDischargePhase1
            // 
            this.gbPartialDischargePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPartialDischargePhase1.Controls.Add(this.tpPartialDischargePhase1);
            this.gbPartialDischargePhase1.Location = new System.Drawing.Point(6, 76);
            this.gbPartialDischargePhase1.Name = "gbPartialDischargePhase1";
            this.gbPartialDischargePhase1.Size = new System.Drawing.Size(208, 83);
            this.gbPartialDischargePhase1.TabIndex = 26;
            this.gbPartialDischargePhase1.TabStop = false;
            this.gbPartialDischargePhase1.Text = "Descarga Parcial";
            // 
            // tpPartialDischargePhase1
            // 
            this.tpPartialDischargePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tpPartialDischargePhase1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tpPartialDischargePhase1.ColumnCount = 2;
            this.tpPartialDischargePhase1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tpPartialDischargePhase1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tpPartialDischargePhase1.Controls.Add(this.tbOccurrencesPhase1, 1, 1);
            this.tpPartialDischargePhase1.Controls.Add(this.lbOccurrencesPhase1, 0, 1);
            this.tpPartialDischargePhase1.Controls.Add(this.lbChargePhase1, 0, 0);
            this.tpPartialDischargePhase1.Controls.Add(this.tbChargePhase1, 1, 0);
            this.tpPartialDischargePhase1.Location = new System.Drawing.Point(6, 19);
            this.tpPartialDischargePhase1.Name = "tpPartialDischargePhase1";
            this.tpPartialDischargePhase1.RowCount = 2;
            this.tpPartialDischargePhase1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpPartialDischargePhase1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpPartialDischargePhase1.Size = new System.Drawing.Size(196, 58);
            this.tpPartialDischargePhase1.TabIndex = 0;
            // 
            // tbOccurrencesPhase1
            // 
            this.tbOccurrencesPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOccurrencesPhase1.Location = new System.Drawing.Point(72, 32);
            this.tbOccurrencesPhase1.Name = "tbOccurrencesPhase1";
            this.tbOccurrencesPhase1.Size = new System.Drawing.Size(120, 20);
            this.tbOccurrencesPhase1.TabIndex = 3;
            // 
            // lbOccurrencesPhase1
            // 
            this.lbOccurrencesPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbOccurrencesPhase1.AutoSize = true;
            this.lbOccurrencesPhase1.BackColor = System.Drawing.Color.Transparent;
            this.lbOccurrencesPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOccurrencesPhase1.Location = new System.Drawing.Point(1, 29);
            this.lbOccurrencesPhase1.Margin = new System.Windows.Forms.Padding(0);
            this.lbOccurrencesPhase1.Name = "lbOccurrencesPhase1";
            this.lbOccurrencesPhase1.Size = new System.Drawing.Size(67, 28);
            this.lbOccurrencesPhase1.TabIndex = 1;
            this.lbOccurrencesPhase1.Text = "Ocorrências:";
            this.lbOccurrencesPhase1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbChargePhase1
            // 
            this.lbChargePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbChargePhase1.AutoSize = true;
            this.lbChargePhase1.BackColor = System.Drawing.Color.Transparent;
            this.lbChargePhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChargePhase1.Location = new System.Drawing.Point(1, 1);
            this.lbChargePhase1.Margin = new System.Windows.Forms.Padding(0);
            this.lbChargePhase1.Name = "lbChargePhase1";
            this.lbChargePhase1.Size = new System.Drawing.Size(67, 27);
            this.lbChargePhase1.TabIndex = 0;
            this.lbChargePhase1.Text = "Carga (pC):";
            this.lbChargePhase1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbChargePhase1
            // 
            this.tbChargePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbChargePhase1.Location = new System.Drawing.Point(72, 4);
            this.tbChargePhase1.Name = "tbChargePhase1";
            this.tbChargePhase1.Size = new System.Drawing.Size(120, 20);
            this.tbChargePhase1.TabIndex = 2;
            // 
            // nudTestTime
            // 
            this.nudTestTime.Location = new System.Drawing.Point(128, 49);
            this.nudTestTime.Name = "nudTestTime";
            this.nudTestTime.Size = new System.Drawing.Size(79, 20);
            this.nudTestTime.TabIndex = 25;
            this.nudTestTime.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // btStopTest
            // 
            this.btStopTest.Enabled = false;
            this.btStopTest.Location = new System.Drawing.Point(70, 47);
            this.btStopTest.Name = "btStopTest";
            this.btStopTest.Size = new System.Drawing.Size(52, 23);
            this.btStopTest.TabIndex = 24;
            this.btStopTest.Text = "Parar";
            this.btStopTest.UseVisualStyleBackColor = true;
            // 
            // btStartTest
            // 
            this.btStartTest.Location = new System.Drawing.Point(12, 47);
            this.btStartTest.Name = "btStartTest";
            this.btStartTest.Size = new System.Drawing.Size(52, 23);
            this.btStartTest.TabIndex = 23;
            this.btStartTest.Text = "Iniciar";
            this.btStartTest.UseVisualStyleBackColor = true;
            // 
            // lbTimer
            // 
            this.lbTimer.AutoSize = true;
            this.lbTimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTimer.Location = new System.Drawing.Point(12, 20);
            this.lbTimer.Name = "lbTimer";
            this.lbTimer.Size = new System.Drawing.Size(88, 24);
            this.lbTimer.TabIndex = 20;
            this.lbTimer.Text = "00:00:00";
            // 
            // gbTestController
            // 
            this.gbTestController.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gbTestController.Controls.Add(this.btResetConfigurations);
            this.gbTestController.Controls.Add(this.btSaveCalibration);
            this.gbTestController.Controls.Add(this.btStop);
            this.gbTestController.Controls.Add(this.btCalibration);
            this.gbTestController.Controls.Add(this.gbCalibration);
            this.gbTestController.Controls.Add(this.tableCute);
            this.gbTestController.Controls.Add(this.tableFilter);
            this.gbTestController.Controls.Add(this.lbParamters);
            this.gbTestController.Controls.Add(this.tableController);
            this.gbTestController.Controls.Add(this.cbOsciloscope);
            this.gbTestController.Controls.Add(this.btStart);
            this.gbTestController.Controls.Add(this.lbOsciloscope);
            this.gbTestController.Controls.Add(this.tableLayoutPanel1);
            this.gbTestController.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbTestController.Location = new System.Drawing.Point(10, 10);
            this.gbTestController.Name = "gbTestController";
            this.gbTestController.Size = new System.Drawing.Size(220, 640);
            this.gbTestController.TabIndex = 0;
            this.gbTestController.TabStop = false;
            this.gbTestController.Text = "Controle de Ensaio";
            // 
            // btResetConfigurations
            // 
            this.btResetConfigurations.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btResetConfigurations.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btResetConfigurations.Location = new System.Drawing.Point(127, 565);
            this.btResetConfigurations.Name = "btResetConfigurations";
            this.btResetConfigurations.Size = new System.Drawing.Size(75, 23);
            this.btResetConfigurations.TabIndex = 27;
            this.btResetConfigurations.Text = "Resetar";
            this.btResetConfigurations.UseVisualStyleBackColor = true;
            this.btResetConfigurations.Click += new System.EventHandler(this.btResetConfigurations_Click);
            // 
            // btSaveCalibration
            // 
            this.btSaveCalibration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btSaveCalibration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSaveCalibration.Location = new System.Drawing.Point(21, 565);
            this.btSaveCalibration.Name = "btSaveCalibration";
            this.btSaveCalibration.Size = new System.Drawing.Size(75, 23);
            this.btSaveCalibration.TabIndex = 26;
            this.btSaveCalibration.Text = "Salvar";
            this.btSaveCalibration.UseVisualStyleBackColor = true;
            this.btSaveCalibration.Click += new System.EventHandler(this.btSaveCalibration_Click);
            // 
            // btStop
            // 
            this.btStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStop.Location = new System.Drawing.Point(112, 19);
            this.btStop.Name = "btStop";
            this.btStop.Size = new System.Drawing.Size(101, 25);
            this.btStop.TabIndex = 25;
            this.btStop.Text = "Parar Aquisição";
            this.btStop.UseVisualStyleBackColor = true;
            // 
            // btCalibration
            // 
            this.btCalibration.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btCalibration.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCalibration.Location = new System.Drawing.Point(72, 535);
            this.btCalibration.Name = "btCalibration";
            this.btCalibration.Size = new System.Drawing.Size(75, 23);
            this.btCalibration.TabIndex = 24;
            this.btCalibration.Text = "Calibrar";
            this.btCalibration.UseVisualStyleBackColor = true;
            this.btCalibration.Click += new System.EventHandler(this.btCalibration_Click);
            // 
            // gbCalibration
            // 
            this.gbCalibration.Controls.Add(this.tpCalibration);
            this.gbCalibration.Location = new System.Drawing.Point(5, 445);
            this.gbCalibration.Name = "gbCalibration";
            this.gbCalibration.Size = new System.Drawing.Size(210, 78);
            this.gbCalibration.TabIndex = 19;
            this.gbCalibration.TabStop = false;
            this.gbCalibration.Text = "Calibração";
            // 
            // tpCalibration
            // 
            this.tpCalibration.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tpCalibration.ColumnCount = 2;
            this.tpCalibration.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpCalibration.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tpCalibration.Controls.Add(this.tbCalibrationPhase1, 1, 1);
            this.tpCalibration.Controls.Add(this.lbCalibrationPhase1, 0, 1);
            this.tpCalibration.Controls.Add(this.lbDp, 1, 0);
            this.tpCalibration.Controls.Add(this.lbPhase, 0, 0);
            this.tpCalibration.Location = new System.Drawing.Point(4, 17);
            this.tpCalibration.Name = "tpCalibration";
            this.tpCalibration.RowCount = 2;
            this.tpCalibration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tpCalibration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tpCalibration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tpCalibration.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tpCalibration.Size = new System.Drawing.Size(202, 56);
            this.tpCalibration.TabIndex = 18;
            // 
            // tbCalibrationPhase1
            // 
            this.tbCalibrationPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCalibrationPhase1.DecimalPlaces = 2;
            this.tbCalibrationPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCalibrationPhase1.Location = new System.Drawing.Point(104, 31);
            this.tbCalibrationPhase1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbCalibrationPhase1.Name = "tbCalibrationPhase1";
            this.tbCalibrationPhase1.Size = new System.Drawing.Size(94, 20);
            this.tbCalibrationPhase1.TabIndex = 28;
            // 
            // lbCalibrationPhase1
            // 
            this.lbCalibrationPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbCalibrationPhase1.AutoSize = true;
            this.lbCalibrationPhase1.BackColor = System.Drawing.Color.Transparent;
            this.lbCalibrationPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCalibrationPhase1.Location = new System.Drawing.Point(4, 28);
            this.lbCalibrationPhase1.Name = "lbCalibrationPhase1";
            this.lbCalibrationPhase1.Size = new System.Drawing.Size(93, 27);
            this.lbCalibrationPhase1.TabIndex = 6;
            this.lbCalibrationPhase1.Text = "Fase 1";
            this.lbCalibrationPhase1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbDp
            // 
            this.lbDp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbDp.AutoSize = true;
            this.lbDp.BackColor = System.Drawing.Color.Transparent;
            this.lbDp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDp.Location = new System.Drawing.Point(104, 1);
            this.lbDp.Name = "lbDp";
            this.lbDp.Size = new System.Drawing.Size(94, 26);
            this.lbDp.TabIndex = 5;
            this.lbDp.Text = "DP (pC)";
            this.lbDp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbPhase
            // 
            this.lbPhase.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPhase.AutoSize = true;
            this.lbPhase.BackColor = System.Drawing.Color.Transparent;
            this.lbPhase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhase.Location = new System.Drawing.Point(4, 1);
            this.lbPhase.Name = "lbPhase";
            this.lbPhase.Size = new System.Drawing.Size(93, 26);
            this.lbPhase.TabIndex = 4;
            this.lbPhase.Text = "Fase";
            this.lbPhase.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableCute
            // 
            this.tableCute.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableCute.ColumnCount = 3;
            this.tableCute.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44F));
            this.tableCute.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableCute.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28F));
            this.tableCute.Controls.Add(this.cbFrequencyTopCut, 2, 1);
            this.tableCute.Controls.Add(this.tbFrequencyTopCut, 1, 1);
            this.tableCute.Controls.Add(this.lbTopCut, 0, 1);
            this.tableCute.Controls.Add(this.cbFrequencyUndercut, 2, 0);
            this.tableCute.Controls.Add(this.lbUndercut, 0, 0);
            this.tableCute.Controls.Add(this.tbFrequencyUndercut, 1, 0);
            this.tableCute.Location = new System.Drawing.Point(6, 380);
            this.tableCute.Name = "tableCute";
            this.tableCute.RowCount = 2;
            this.tableCute.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableCute.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableCute.Size = new System.Drawing.Size(209, 52);
            this.tableCute.TabIndex = 15;
            // 
            // cbFrequencyTopCut
            // 
            this.cbFrequencyTopCut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFrequencyTopCut.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrequencyTopCut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFrequencyTopCut.FormattingEnabled = true;
            this.cbFrequencyTopCut.Items.AddRange(new object[] {
            "Hz",
            "kHz",
            "MHz",
            "GHz"});
            this.cbFrequencyTopCut.Location = new System.Drawing.Point(153, 29);
            this.cbFrequencyTopCut.Name = "cbFrequencyTopCut";
            this.cbFrequencyTopCut.Size = new System.Drawing.Size(52, 21);
            this.cbFrequencyTopCut.TabIndex = 15;
            // 
            // tbFrequencyTopCut
            // 
            this.tbFrequencyTopCut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFrequencyTopCut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFrequencyTopCut.Location = new System.Drawing.Point(95, 29);
            this.tbFrequencyTopCut.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbFrequencyTopCut.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbFrequencyTopCut.Name = "tbFrequencyTopCut";
            this.tbFrequencyTopCut.Size = new System.Drawing.Size(51, 20);
            this.tbFrequencyTopCut.TabIndex = 31;
            this.tbFrequencyTopCut.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lbTopCut
            // 
            this.lbTopCut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTopCut.AutoSize = true;
            this.lbTopCut.BackColor = System.Drawing.Color.Transparent;
            this.lbTopCut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTopCut.Location = new System.Drawing.Point(1, 26);
            this.lbTopCut.Margin = new System.Windows.Forms.Padding(0);
            this.lbTopCut.Name = "lbTopCut";
            this.lbTopCut.Size = new System.Drawing.Size(90, 25);
            this.lbTopCut.TabIndex = 28;
            this.lbTopCut.Text = "Corte superior:";
            this.lbTopCut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbFrequencyUndercut
            // 
            this.cbFrequencyUndercut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFrequencyUndercut.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFrequencyUndercut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFrequencyUndercut.FormattingEnabled = true;
            this.cbFrequencyUndercut.Items.AddRange(new object[] {
            "Hz",
            "kHz",
            "MHz",
            "GHz"});
            this.cbFrequencyUndercut.Location = new System.Drawing.Point(153, 4);
            this.cbFrequencyUndercut.Name = "cbFrequencyUndercut";
            this.cbFrequencyUndercut.Size = new System.Drawing.Size(52, 21);
            this.cbFrequencyUndercut.TabIndex = 13;
            // 
            // lbUndercut
            // 
            this.lbUndercut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbUndercut.AutoSize = true;
            this.lbUndercut.BackColor = System.Drawing.Color.Transparent;
            this.lbUndercut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUndercut.Location = new System.Drawing.Point(1, 1);
            this.lbUndercut.Margin = new System.Windows.Forms.Padding(0);
            this.lbUndercut.Name = "lbUndercut";
            this.lbUndercut.Size = new System.Drawing.Size(90, 24);
            this.lbUndercut.TabIndex = 27;
            this.lbUndercut.Text = "Corte inferior:";
            this.lbUndercut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbFrequencyUndercut
            // 
            this.tbFrequencyUndercut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFrequencyUndercut.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFrequencyUndercut.Location = new System.Drawing.Point(95, 4);
            this.tbFrequencyUndercut.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbFrequencyUndercut.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbFrequencyUndercut.Name = "tbFrequencyUndercut";
            this.tbFrequencyUndercut.Size = new System.Drawing.Size(51, 20);
            this.tbFrequencyUndercut.TabIndex = 30;
            this.tbFrequencyUndercut.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // tableFilter
            // 
            this.tableFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableFilter.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableFilter.ColumnCount = 2;
            this.tableFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableFilter.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableFilter.Controls.Add(this.cbFilter, 1, 0);
            this.tableFilter.Controls.Add(this.lbFilter, 0, 0);
            this.tableFilter.Controls.Add(this.lbOrder, 0, 1);
            this.tableFilter.Controls.Add(this.tbOrder, 1, 1);
            this.tableFilter.Location = new System.Drawing.Point(6, 317);
            this.tableFilter.Name = "tableFilter";
            this.tableFilter.RowCount = 2;
            this.tableFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 48.21429F));
            this.tableFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.78571F));
            this.tableFilter.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableFilter.Size = new System.Drawing.Size(209, 51);
            this.tableFilter.TabIndex = 12;
            // 
            // cbFilter
            // 
            this.cbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFilter.FormattingEnabled = true;
            this.cbFilter.Items.AddRange(new object[] {
            "Passa Baixa",
            "Passa Alta",
            "Passa Banda",
            "Passa Faixa"});
            this.cbFilter.Location = new System.Drawing.Point(108, 4);
            this.cbFilter.Name = "cbFilter";
            this.cbFilter.Size = new System.Drawing.Size(97, 21);
            this.cbFilter.TabIndex = 10;
            // 
            // lbFilter
            // 
            this.lbFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbFilter.AutoSize = true;
            this.lbFilter.BackColor = System.Drawing.Color.Transparent;
            this.lbFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFilter.Location = new System.Drawing.Point(1, 1);
            this.lbFilter.Margin = new System.Windows.Forms.Padding(0);
            this.lbFilter.Name = "lbFilter";
            this.lbFilter.Size = new System.Drawing.Size(103, 23);
            this.lbFilter.TabIndex = 16;
            this.lbFilter.Text = "Filtro:";
            this.lbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbOrder
            // 
            this.lbOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbOrder.AutoSize = true;
            this.lbOrder.BackColor = System.Drawing.Color.Transparent;
            this.lbOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOrder.Location = new System.Drawing.Point(4, 25);
            this.lbOrder.Name = "lbOrder";
            this.lbOrder.Size = new System.Drawing.Size(97, 25);
            this.lbOrder.TabIndex = 14;
            this.lbOrder.Text = "Ordem:";
            this.lbOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbOrder
            // 
            this.tbOrder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbOrder.Location = new System.Drawing.Point(108, 28);
            this.tbOrder.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbOrder.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbOrder.Name = "tbOrder";
            this.tbOrder.Size = new System.Drawing.Size(97, 20);
            this.tbOrder.TabIndex = 29;
            this.tbOrder.Value = new decimal(new int[] {
            500,
            0,
            0,
            0});
            // 
            // lbParamters
            // 
            this.lbParamters.AutoSize = true;
            this.lbParamters.Location = new System.Drawing.Point(6, 229);
            this.lbParamters.Name = "lbParamters";
            this.lbParamters.Size = new System.Drawing.Size(70, 13);
            this.lbParamters.TabIndex = 11;
            this.lbParamters.Text = "Parâmetros";
            // 
            // tableController
            // 
            this.tableController.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableController.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableController.ColumnCount = 4;
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableController.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableController.Controls.Add(this.lbPhase1, 0, 1);
            this.tableController.Controls.Add(this.lbGain, 1, 0);
            this.tableController.Controls.Add(this.lbGate, 2, 0);
            this.tableController.Controls.Add(this.tbGainPhase1, 1, 1);
            this.tableController.Controls.Add(this.tbGatePhase1, 2, 1);
            this.tableController.Location = new System.Drawing.Point(6, 247);
            this.tableController.Name = "tableController";
            this.tableController.RowCount = 5;
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableController.Size = new System.Drawing.Size(208, 57);
            this.tableController.TabIndex = 10;
            // 
            // lbPhase1
            // 
            this.lbPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbPhase1.AutoSize = true;
            this.lbPhase1.BackColor = System.Drawing.Color.Transparent;
            this.lbPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPhase1.Location = new System.Drawing.Point(1, 30);
            this.lbPhase1.Margin = new System.Windows.Forms.Padding(0);
            this.lbPhase1.Name = "lbPhase1";
            this.lbPhase1.Size = new System.Drawing.Size(20, 28);
            this.lbPhase1.TabIndex = 0;
            this.lbPhase1.Text = "F1";
            this.lbPhase1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbGain
            // 
            this.lbGain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGain.AutoSize = true;
            this.lbGain.BackColor = System.Drawing.Color.Transparent;
            this.lbGain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGain.Location = new System.Drawing.Point(22, 1);
            this.lbGain.Margin = new System.Windows.Forms.Padding(0);
            this.lbGain.Name = "lbGain";
            this.lbGain.Size = new System.Drawing.Size(81, 28);
            this.lbGain.TabIndex = 5;
            this.lbGain.Text = "Ganho";
            this.lbGain.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbGate
            // 
            this.lbGate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbGate.AutoSize = true;
            this.lbGate.BackColor = System.Drawing.Color.Transparent;
            this.lbGate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGate.Location = new System.Drawing.Point(104, 1);
            this.lbGate.Margin = new System.Windows.Forms.Padding(0);
            this.lbGate.Name = "lbGate";
            this.lbGate.Size = new System.Drawing.Size(81, 28);
            this.lbGate.TabIndex = 6;
            this.lbGate.Text = "Gate";
            this.lbGate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbGainPhase1
            // 
            this.tbGainPhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGainPhase1.DecimalPlaces = 2;
            this.tbGainPhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGainPhase1.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.tbGainPhase1.Location = new System.Drawing.Point(25, 33);
            this.tbGainPhase1.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.tbGainPhase1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.tbGainPhase1.Name = "tbGainPhase1";
            this.tbGainPhase1.Size = new System.Drawing.Size(75, 20);
            this.tbGainPhase1.TabIndex = 23;
            this.tbGainPhase1.Value = new decimal(new int[] {
            10000,
            0,
            0,
            131072});
            // 
            // tbGatePhase1
            // 
            this.tbGatePhase1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbGatePhase1.DecimalPlaces = 3;
            this.tbGatePhase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbGatePhase1.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.tbGatePhase1.Location = new System.Drawing.Point(107, 33);
            this.tbGatePhase1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            196608});
            this.tbGatePhase1.Name = "tbGatePhase1";
            this.tbGatePhase1.Size = new System.Drawing.Size(75, 20);
            this.tbGatePhase1.TabIndex = 26;
            this.tbGatePhase1.Value = new decimal(new int[] {
            500,
            0,
            0,
            196608});
            // 
            // cbOsciloscope
            // 
            this.cbOsciloscope.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbOsciloscope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOsciloscope.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbOsciloscope.FormattingEnabled = true;
            this.cbOsciloscope.Location = new System.Drawing.Point(6, 72);
            this.cbOsciloscope.Name = "cbOsciloscope";
            this.cbOsciloscope.Size = new System.Drawing.Size(208, 21);
            this.cbOsciloscope.TabIndex = 5;
            this.cbOsciloscope.SelectedIndexChanged += new System.EventHandler(this.cbOsciloscope_SelectedIndexChanged);
            // 
            // btStart
            // 
            this.btStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStart.Location = new System.Drawing.Point(5, 19);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(102, 25);
            this.btStart.TabIndex = 1;
            this.btStart.Text = "Iniciar Aquisição";
            this.btStart.UseVisualStyleBackColor = true;
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // lbOsciloscope
            // 
            this.lbOsciloscope.AutoSize = true;
            this.lbOsciloscope.Location = new System.Drawing.Point(6, 56);
            this.lbOsciloscope.Name = "lbOsciloscope";
            this.lbOsciloscope.Size = new System.Drawing.Size(79, 13);
            this.lbOsciloscope.TabIndex = 6;
            this.lbOsciloscope.Text = "Osciloscópio";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.71844F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.28156F));
            this.tableLayoutPanel1.Controls.Add(this.lbChannel, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.btConnection, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbConnection, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbOsciloscopePort, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbPort, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tbIp, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbOsciloscopeIp, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbBrand, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbChannel, 1, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(6, 99);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(208, 118);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // lbChannel
            // 
            this.lbChannel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbChannel.AutoSize = true;
            this.lbChannel.BackColor = System.Drawing.Color.Transparent;
            this.lbChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChannel.Location = new System.Drawing.Point(1, 88);
            this.lbChannel.Margin = new System.Windows.Forms.Padding(0);
            this.lbChannel.Name = "lbChannel";
            this.lbChannel.Size = new System.Drawing.Size(87, 28);
            this.lbChannel.TabIndex = 14;
            this.lbChannel.Text = "Canal:";
            this.lbChannel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btConnection
            // 
            this.btConnection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btConnection.Location = new System.Drawing.Point(4, 4);
            this.btConnection.Name = "btConnection";
            this.btConnection.Size = new System.Drawing.Size(81, 22);
            this.btConnection.TabIndex = 1;
            this.btConnection.Text = "Conectar";
            this.btConnection.UseVisualStyleBackColor = true;
            this.btConnection.Click += new System.EventHandler(this.btConnection_Click);
            // 
            // lbConnection
            // 
            this.lbConnection.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbConnection.AutoSize = true;
            this.lbConnection.BackColor = System.Drawing.Color.LightSalmon;
            this.lbConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbConnection.Location = new System.Drawing.Point(89, 1);
            this.lbConnection.Margin = new System.Windows.Forms.Padding(0);
            this.lbConnection.Name = "lbConnection";
            this.lbConnection.Size = new System.Drawing.Size(118, 28);
            this.lbConnection.TabIndex = 13;
            this.lbConnection.Text = "Desconectado";
            this.lbConnection.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbOsciloscopePort
            // 
            this.lbOsciloscopePort.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbOsciloscopePort.AutoSize = true;
            this.lbOsciloscopePort.BackColor = System.Drawing.Color.Transparent;
            this.lbOsciloscopePort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOsciloscopePort.Location = new System.Drawing.Point(1, 59);
            this.lbOsciloscopePort.Margin = new System.Windows.Forms.Padding(0);
            this.lbOsciloscopePort.Name = "lbOsciloscopePort";
            this.lbOsciloscopePort.Size = new System.Drawing.Size(87, 28);
            this.lbOsciloscopePort.TabIndex = 8;
            this.lbOsciloscopePort.Text = "Porta:";
            this.lbOsciloscopePort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbPort
            // 
            this.tbPort.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPort.Location = new System.Drawing.Point(92, 62);
            this.tbPort.Multiline = true;
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(111, 22);
            this.tbPort.TabIndex = 3;
            // 
            // tbIp
            // 
            this.tbIp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tbIp.Culture = new System.Globalization.CultureInfo("");
            this.tbIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbIp.Location = new System.Drawing.Point(92, 33);
            this.tbIp.Mask = "000.000.000.000";
            this.tbIp.Name = "tbIp";
            this.tbIp.Size = new System.Drawing.Size(111, 22);
            this.tbIp.TabIndex = 2;
            // 
            // lbOsciloscopeIp
            // 
            this.lbOsciloscopeIp.AutoSize = true;
            this.lbOsciloscopeIp.BackColor = System.Drawing.Color.Transparent;
            this.lbOsciloscopeIp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbOsciloscopeIp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOsciloscopeIp.Location = new System.Drawing.Point(1, 30);
            this.lbOsciloscopeIp.Margin = new System.Windows.Forms.Padding(0);
            this.lbOsciloscopeIp.Name = "lbOsciloscopeIp";
            this.lbOsciloscopeIp.Size = new System.Drawing.Size(87, 28);
            this.lbOsciloscopeIp.TabIndex = 2;
            this.lbOsciloscopeIp.Text = "IP:";
            this.lbOsciloscopeIp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbBrand
            // 
            this.lbBrand.AutoSize = true;
            this.lbBrand.BackColor = System.Drawing.Color.Transparent;
            this.lbBrand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBrand.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbBrand.Location = new System.Drawing.Point(1, 117);
            this.lbBrand.Margin = new System.Windows.Forms.Padding(0);
            this.lbBrand.Name = "lbBrand";
            this.lbBrand.Size = new System.Drawing.Size(87, 20);
            this.lbBrand.TabIndex = 10;
            this.lbBrand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbChannel
            // 
            this.cbChannel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChannel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChannel.FormattingEnabled = true;
            this.cbChannel.Items.AddRange(new object[] {
            "Canal 1",
            "Canal 2",
            "Canal 3",
            "Canal 4"});
            this.cbChannel.Location = new System.Drawing.Point(92, 91);
            this.cbChannel.Name = "cbChannel";
            this.cbChannel.Size = new System.Drawing.Size(112, 23);
            this.cbChannel.TabIndex = 15;
            // 
            // timerForm
            // 
            this.timerForm.Tick += new System.EventHandler(this.timerForm_Tick);
            // 
            // FrmTestPdmsSinglePhase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(1284, 711);
            this.Controls.Add(this.btGetEquipment);
            this.Controls.Add(this.tbTransformer);
            this.Controls.Add(this.lbTransformer);
            this.Controls.Add(this.pnTest);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmTestPdmsSinglePhase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Controle de Ensaio";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmTestPdmsSinglePhase_FormClosed);
            this.Load += new System.EventHandler(this.FrmTestPdmsSinglePhase_Load);
            this.pnTest.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tbGraphs.ResumeLayout(false);
            this.tpSignals.ResumeLayout(false);
            this.tlpSignal.ResumeLayout(false);
            this.tpDispersion.ResumeLayout(false);
            this.tlpDispersion.ResumeLayout(false);
            this.tpDispersionDetailed.ResumeLayout(false);
            this.tlpDispersionDetailed.ResumeLayout(false);
            this.tpPartialDischargeTrend.ResumeLayout(false);
            this.tlpPartialDischargeTrend.ResumeLayout(false);
            this.tpFft.ResumeLayout(false);
            this.tlpFft.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbTotalDischarges.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.gbPartialDischargePhase1.ResumeLayout(false);
            this.tpPartialDischargePhase1.ResumeLayout(false);
            this.tpPartialDischargePhase1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudTestTime)).EndInit();
            this.gbTestController.ResumeLayout(false);
            this.gbTestController.PerformLayout();
            this.gbCalibration.ResumeLayout(false);
            this.tpCalibration.ResumeLayout(false);
            this.tpCalibration.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbCalibrationPhase1)).EndInit();
            this.tableCute.ResumeLayout(false);
            this.tableCute.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrequencyTopCut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbFrequencyUndercut)).EndInit();
            this.tableFilter.ResumeLayout(false);
            this.tableFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbOrder)).EndInit();
            this.tableController.ResumeLayout(false);
            this.tableController.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbGainPhase1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbGatePhase1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btGetEquipment;
        private System.Windows.Forms.TextBox tbTransformer;
        private System.Windows.Forms.Label lbTransformer;
        private System.Windows.Forms.Panel pnTest;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox gbTestController;
        private System.Windows.Forms.ComboBox cbOsciloscope;
        private System.Windows.Forms.Button btStart;
        private System.Windows.Forms.Label lbOsciloscope;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btConnection;
        private System.Windows.Forms.Label lbConnection;
        private System.Windows.Forms.Label lbOsciloscopePort;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label lbBrand;
        private System.Windows.Forms.MaskedTextBox tbIp;
        private System.Windows.Forms.Label lbOsciloscopeIp;
        private System.Windows.Forms.Timer timerForm;
        private System.Windows.Forms.Label lbChannel;
        private System.Windows.Forms.ComboBox cbChannel;
        private System.Windows.Forms.Label lbParamters;
        private System.Windows.Forms.TableLayoutPanel tableController;
        private System.Windows.Forms.Label lbPhase1;
        private System.Windows.Forms.Label lbGain;
        private System.Windows.Forms.Label lbGate;
        private System.Windows.Forms.NumericUpDown tbGainPhase1;
        private System.Windows.Forms.NumericUpDown tbGatePhase1;
        private System.Windows.Forms.TableLayoutPanel tableFilter;
        private System.Windows.Forms.ComboBox cbFilter;
        private System.Windows.Forms.Label lbFilter;
        private System.Windows.Forms.Label lbOrder;
        private System.Windows.Forms.NumericUpDown tbOrder;
        private System.Windows.Forms.TableLayoutPanel tableCute;
        private System.Windows.Forms.ComboBox cbFrequencyTopCut;
        private System.Windows.Forms.NumericUpDown tbFrequencyTopCut;
        private System.Windows.Forms.Label lbTopCut;
        private System.Windows.Forms.ComboBox cbFrequencyUndercut;
        private System.Windows.Forms.Label lbUndercut;
        private System.Windows.Forms.NumericUpDown tbFrequencyUndercut;
        private System.Windows.Forms.GroupBox gbCalibration;
        private System.Windows.Forms.TableLayoutPanel tpCalibration;
        private System.Windows.Forms.Label lbCalibrationPhase1;
        private System.Windows.Forms.Label lbDp;
        private System.Windows.Forms.Label lbPhase;
        private System.Windows.Forms.Button btCalibration;
        private System.Windows.Forms.Button btStop;
        private System.Windows.Forms.Label lbTimer;
        private System.Windows.Forms.NumericUpDown nudTestTime;
        private System.Windows.Forms.Button btStopTest;
        private System.Windows.Forms.Button btStartTest;
        private System.Windows.Forms.GroupBox gbPartialDischargePhase1;
        private System.Windows.Forms.TableLayoutPanel tpPartialDischargePhase1;
        private System.Windows.Forms.TextBox tbOccurrencesPhase1;
        private System.Windows.Forms.Label lbOccurrencesPhase1;
        private System.Windows.Forms.Label lbChargePhase1;
        private System.Windows.Forms.TextBox tbChargePhase1;
        private System.Windows.Forms.GroupBox gbTotalDischarges;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label lbTotalOccurrences;
        private System.Windows.Forms.Label lbTotalPositiveDischarges;
        private System.Windows.Forms.TextBox tbTotalPositiveDischarges;
        private System.Windows.Forms.Label lbTotalNegativeDischarges;
        private System.Windows.Forms.TextBox tbTotalNegativeDischarges;
        private System.Windows.Forms.TextBox tbTotalOccurrences;
        private System.Windows.Forms.TabControl tbGraphs;
        private System.Windows.Forms.TabPage tpSignals;
        private System.Windows.Forms.TabPage tpDispersion;
        private System.Windows.Forms.TabPage tpDispersionDetailed;
        private System.Windows.Forms.TabPage tpPartialDischargeTrend;
        private System.Windows.Forms.TabPage tpFft;
        private System.Windows.Forms.TableLayoutPanel tlpSignal;
        private ZedGraph.ZedGraphControl gcSignalPhase1;
        private ZedGraph.ZedGraphControl gcSignalFiltredPhase1;
        private System.Windows.Forms.TableLayoutPanel tlpDispersion;
        private System.Windows.Forms.TableLayoutPanel tlpDispersionDetailed;
        private System.Windows.Forms.TableLayoutPanel tlpPartialDischargeTrend;
        private System.Windows.Forms.TableLayoutPanel tlpFft;
        private ZedGraph.ZedGraphControl gcDispersionPhase1;
        private ZedGraph.ZedGraphControl gcDispersionDetailedPhase1;
        private ZedGraph.ZedGraphControl gcPartialDischargeTrendPhase1;
        private ZedGraph.ZedGraphControl gcFftPhase1;
        private System.Windows.Forms.Button btSaveCalibration;
        private System.Windows.Forms.Button btResetConfigurations;
        private System.Windows.Forms.NumericUpDown tbCalibrationPhase1;
    }
}