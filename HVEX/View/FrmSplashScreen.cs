﻿using HVEX.Service;
using System;
using System.ComponentModel;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmSplashScreen : Form {
        public delegate void UpdateBar(int value);
        public UpdateBar delegateUpdateBar;
        private ServiceController[] services = ServiceController.GetServices();
        public FrmSplashScreen() {
            InitializeComponent();
            bwLoader.WorkerReportsProgress = true;
            bwLoader.WorkerSupportsCancellation = true;
        }
        private void FrmSplashScreen_Load(object sender, EventArgs e) {
            if (bwLoader.IsBusy != true) {
                pbLoader.Value = 0;
                pbLoader.Minimum = 0;
                pbLoader.Maximum = services.Count();
                bwLoader.RunWorkerAsync();
            }
        }
        //Procura pelo serviço do MongoDB dentre aqueles sendo executados no computador.
        //Habilita o mesmo, caso ele se encontre desabilitado, e realiza uma consulta aos
        //bancos nele instalado (para os acessos subsequentes sejam mais rápidos)
        private void bwLoader_DoWork(object sender, DoWorkEventArgs e) {
            Cursor.Current = Cursors.WaitCursor;
            BackgroundWorker worker = sender as BackgroundWorker;
            for (int i = 0; i < services.Count(); i++) {
                if (services[i].ServiceName == "MongoDB") {
                    if (services[i].Status.Equals(ServiceControllerStatus.Stopped)) {
                        try {
                            services[i].Start();
                            services[i].WaitForStatus(ServiceControllerStatus.Running);
                            StartDatabase();
                            worker.ReportProgress(services.Count() - 1);
                        } catch (Exception ex) {
                            new DatabaseException(ex.Message);
                        }
                    } else {
                        StartDatabase();
                        worker.ReportProgress(services.Count() - 1);
                        Thread.Sleep(500);
                    }
                } else {
                    worker.ReportProgress(i);
                }
            }
        }
        private void bwLoader_ProgressChanged(object sender, ProgressChangedEventArgs e) {
            pbLoader.Value = e.ProgressPercentage;
        }
        private void bwLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (pbLoader.Value == services.Count() - 1) {
                Cursor.Current = Cursors.Default;
                FrmLogin login = new FrmLogin();
                login.FormClosed += new FormClosedEventHandler(
                    (object obj, FormClosedEventArgs evt) => {
                        Close();
                    }
                );
                login.Show();
            }
        }
        private void StartDatabase() {
            Mongo.ListDatabases();
        }
    }
}
