﻿
namespace HVEX.View {
    partial class FrmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.pnHeader = new System.Windows.Forms.Panel();
            this.lbAccessLevel = new System.Windows.Forms.Label();
            this.lbUserName = new System.Windows.Forms.Label();
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.análiseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.controleDeEnsaioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shuntToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fRAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pDMSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.monofásicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trifásicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipamentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testBodiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.measuringInstrumentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fabricantesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuáriosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sairToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sobreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ensaiosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnHeader.SuspendLayout();
            this.msMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnHeader
            // 
            this.pnHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.pnHeader.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.pnHeader.Controls.Add(this.lbAccessLevel);
            this.pnHeader.Controls.Add(this.lbUserName);
            this.pnHeader.Location = new System.Drawing.Point(0, 24);
            this.pnHeader.Name = "pnHeader";
            this.pnHeader.Size = new System.Drawing.Size(984, 50);
            this.pnHeader.TabIndex = 4;
            // 
            // lbAccessLevel
            // 
            this.lbAccessLevel.AutoSize = true;
            this.lbAccessLevel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAccessLevel.ForeColor = System.Drawing.Color.White;
            this.lbAccessLevel.Location = new System.Drawing.Point(10, 25);
            this.lbAccessLevel.Name = "lbAccessLevel";
            this.lbAccessLevel.Size = new System.Drawing.Size(122, 16);
            this.lbAccessLevel.TabIndex = 1;
            this.lbAccessLevel.Text = "Nível de Acesso";
            // 
            // lbUserName
            // 
            this.lbUserName.AutoSize = true;
            this.lbUserName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUserName.ForeColor = System.Drawing.Color.White;
            this.lbUserName.Location = new System.Drawing.Point(10, 4);
            this.lbUserName.Name = "lbUserName";
            this.lbUserName.Size = new System.Drawing.Size(49, 16);
            this.lbUserName.TabIndex = 0;
            this.lbUserName.Text = "Nome";
            // 
            // msMain
            // 
            this.msMain.BackColor = System.Drawing.SystemColors.ControlLight;
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.análiseToolStripMenuItem,
            this.controleDeEnsaioToolStripMenuItem,
            this.equipamentosToolStripMenuItem,
            this.fabricantesToolStripMenuItem,
            this.usuáriosToolStripMenuItem,
            this.sistemaToolStripMenuItem,
            this.ensaiosToolStripMenuItem});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(984, 24);
            this.msMain.TabIndex = 3;
            this.msMain.Text = "menuStrip1";
            // 
            // análiseToolStripMenuItem
            // 
            this.análiseToolStripMenuItem.Name = "análiseToolStripMenuItem";
            this.análiseToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.análiseToolStripMenuItem.Text = "Análise";
            // 
            // controleDeEnsaioToolStripMenuItem
            // 
            this.controleDeEnsaioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.shuntToolStripMenuItem,
            this.fRAToolStripMenuItem,
            this.pDMSToolStripMenuItem});
            this.controleDeEnsaioToolStripMenuItem.Name = "controleDeEnsaioToolStripMenuItem";
            this.controleDeEnsaioToolStripMenuItem.Size = new System.Drawing.Size(118, 20);
            this.controleDeEnsaioToolStripMenuItem.Text = "Controle de Ensaio";
            // 
            // shuntToolStripMenuItem
            // 
            this.shuntToolStripMenuItem.Name = "shuntToolStripMenuItem";
            this.shuntToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.shuntToolStripMenuItem.Text = "Shunt";
            this.shuntToolStripMenuItem.Click += new System.EventHandler(this.shuntToolStripMenuItem_Click);
            // 
            // fRAToolStripMenuItem
            // 
            this.fRAToolStripMenuItem.Name = "fRAToolStripMenuItem";
            this.fRAToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.fRAToolStripMenuItem.Text = "FRA";
            this.fRAToolStripMenuItem.Click += new System.EventHandler(this.fRAToolStripMenuItem_Click);
            // 
            // pDMSToolStripMenuItem
            // 
            this.pDMSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.monofásicoToolStripMenuItem,
            this.trifásicoToolStripMenuItem});
            this.pDMSToolStripMenuItem.Name = "pDMSToolStripMenuItem";
            this.pDMSToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.pDMSToolStripMenuItem.Text = "PDMS";
            // 
            // monofásicoToolStripMenuItem
            // 
            this.monofásicoToolStripMenuItem.Name = "monofásicoToolStripMenuItem";
            this.monofásicoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.monofásicoToolStripMenuItem.Text = "Monofásico";
            this.monofásicoToolStripMenuItem.Click += new System.EventHandler(this.monofásicoToolStripMenuItem_Click);
            // 
            // trifásicoToolStripMenuItem
            // 
            this.trifásicoToolStripMenuItem.Name = "trifásicoToolStripMenuItem";
            this.trifásicoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.trifásicoToolStripMenuItem.Text = "Trifásico";
            this.trifásicoToolStripMenuItem.Click += new System.EventHandler(this.trifásicoToolStripMenuItem_Click);
            // 
            // equipamentosToolStripMenuItem
            // 
            this.equipamentosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testBodiesToolStripMenuItem,
            this.measuringInstrumentsToolStripMenuItem});
            this.equipamentosToolStripMenuItem.Name = "equipamentosToolStripMenuItem";
            this.equipamentosToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.equipamentosToolStripMenuItem.Text = "Equipamentos";
            // 
            // testBodiesToolStripMenuItem
            // 
            this.testBodiesToolStripMenuItem.Name = "testBodiesToolStripMenuItem";
            this.testBodiesToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.testBodiesToolStripMenuItem.Text = "Corpos de Prova";
            this.testBodiesToolStripMenuItem.Click += new System.EventHandler(this.testBodiesToolStripMenuItem_Click);
            // 
            // measuringInstrumentsToolStripMenuItem
            // 
            this.measuringInstrumentsToolStripMenuItem.Name = "measuringInstrumentsToolStripMenuItem";
            this.measuringInstrumentsToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.measuringInstrumentsToolStripMenuItem.Text = "Instrumentos de Medição";
            this.measuringInstrumentsToolStripMenuItem.Click += new System.EventHandler(this.measuringInstrumentsToolStripMenuItem_Click);
            // 
            // fabricantesToolStripMenuItem
            // 
            this.fabricantesToolStripMenuItem.Name = "fabricantesToolStripMenuItem";
            this.fabricantesToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.fabricantesToolStripMenuItem.Text = "Fabricantes";
            this.fabricantesToolStripMenuItem.Click += new System.EventHandler(this.fabricantesToolStripMenuItem_Click);
            // 
            // usuáriosToolStripMenuItem
            // 
            this.usuáriosToolStripMenuItem.Name = "usuáriosToolStripMenuItem";
            this.usuáriosToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.usuáriosToolStripMenuItem.Text = "Usuários";
            this.usuáriosToolStripMenuItem.Click += new System.EventHandler(this.usuáriosToolStripMenuItem_Click);
            // 
            // sistemaToolStripMenuItem
            // 
            this.sistemaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logsToolStripMenuItem,
            this.sairToolStripMenuItem,
            this.sobreToolStripMenuItem});
            this.sistemaToolStripMenuItem.Name = "sistemaToolStripMenuItem";
            this.sistemaToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.sistemaToolStripMenuItem.Text = "Sistema";
            // 
            // logsToolStripMenuItem
            // 
            this.logsToolStripMenuItem.Name = "logsToolStripMenuItem";
            this.logsToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.logsToolStripMenuItem.Text = "Logs";
            this.logsToolStripMenuItem.Click += new System.EventHandler(this.logsToolStripMenuItem_Click);
            // 
            // sairToolStripMenuItem
            // 
            this.sairToolStripMenuItem.Name = "sairToolStripMenuItem";
            this.sairToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.sairToolStripMenuItem.Text = "Sair";
            this.sairToolStripMenuItem.Click += new System.EventHandler(this.sairToolStripMenuItem_Click);
            // 
            // sobreToolStripMenuItem
            // 
            this.sobreToolStripMenuItem.Name = "sobreToolStripMenuItem";
            this.sobreToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.sobreToolStripMenuItem.Text = "Sobre";
            this.sobreToolStripMenuItem.Click += new System.EventHandler(this.sobreToolStripMenuItem_Click);
            // 
            // ensaiosToolStripMenuItem
            // 
            this.ensaiosToolStripMenuItem.Name = "ensaiosToolStripMenuItem";
            this.ensaiosToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.ensaiosToolStripMenuItem.Text = "Ensaios";
            this.ensaiosToolStripMenuItem.Click += new System.EventHandler(this.ensaiosToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(984, 561);
            this.Controls.Add(this.msMain);
            this.Controls.Add(this.pnHeader);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msMain;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HVEX";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMain_FormClosed);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.pnHeader.ResumeLayout(false);
            this.pnHeader.PerformLayout();
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnHeader;
        private System.Windows.Forms.Label lbAccessLevel;
        private System.Windows.Forms.Label lbUserName;
        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.ToolStripMenuItem análiseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem controleDeEnsaioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipamentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testBodiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem measuringInstrumentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fabricantesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sistemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sairToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sobreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuáriosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ensaiosToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem shuntToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem fRAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pDMSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem monofásicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trifásicoToolStripMenuItem;
    }
}