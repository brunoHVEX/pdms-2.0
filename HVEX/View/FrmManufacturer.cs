﻿using HVEX.Controller;
using HVEX.Service;
using HVEX.View.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmManufacturer : Form, IManufacturer {

        private string loggedUser;
        private ManufacturerController controller;

        #region Interface Attributes
        public string Id { get; set; }
        public string CompanyName {
            get { return tbCompanyName.Text; }
            set { tbCompanyName.Text = value; }
        }
        public string FantasyName {
            get { return tbFantasyName.Text; }
            set { tbFantasyName.Text = value; }
        }
        public string Email {
            get { return tbEmail.Text; }
            set { tbEmail.Text = value; }
        }
        public string Phone {
            get { return tbPhone.Text; }
            set { tbPhone.Text = value; }
        }
        public string Contact {
            get { return tbContact.Text; }
            set { tbContact.Text = value; }
        }
        public string Address {
            get { return tbAddress.Text; }
            set { tbAddress.Text = value; }
        }
        public string District {
            get { return tbDistrict.Text; }
            set { tbDistrict.Text = value; }
        }
        public string City {
            get { return tbCity.Text; }
            set { tbCity.Text = value; }
        }
        public string State {
            get { return tbState.Text; }
            set { tbState.Text = value; }
        }
        public string Country {
            get { return tbCountry.Text; }
            set { tbCountry.Text = value; }
        }
        public Image Image {
            get { return pbManufacturer.Image; }
            set { pbManufacturer.Image = value; }
        }
        public DataTable List {
            set {
                if (value != null) {
                    BindingSource binding = new BindingSource();
                    binding.DataSource = value;
                    dgManufacturer.DataSource = binding;
                    dgManufacturer.Columns[0].Visible = false;
                }
            }
        }
        #endregion

        public FrmManufacturer(string userId) {
            InitializeComponent();
            loggedUser = userId;
            controller = new ManufacturerController(this);
        }

        #region Events
        private void FrmManufacturer_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }
        private void pbManufacturer_Click(object sender, EventArgs e) {
            OpenFileDialog imgDialog = new OpenFileDialog();
            imgDialog.Filter = "Arquivos de Imagem (*.jpg;*.png)|*.JPG;*.PNG";
            if (imgDialog.ShowDialog() == DialogResult.OK) {
                pbManufacturer.ImageLocation = imgDialog.FileName.ToString();
            }
        }
        private void btClean_Click(object sender, EventArgs e) {
            CleanFields();
        }
        private async void btSave_Click(object sender, EventArgs e) {
            if (CheckMandatoryFields()) {
                if (Id == null) {
                    if (await controller.Create(loggedUser)) {
                        MessageBox.Show("O fabricante foi cadastrado com sucesso!");
                        CleanFields();
                    }
                } else {
                    if (await controller.Update(loggedUser)) {
                        MessageBox.Show("O fabricante foi alterado com sucesso!");
                        CleanFields();
                    }
                }
            }
        }
        private async void tabManufacturer_SelectedIndexChanged(object sender, EventArgs e) {
            if (tabManufacturer.SelectedTab == tabManufacturer.TabPages["tabSearch"]) {
                CleanFields();
                CleanDataGrid();
                tbSearch.Text = "";
                cbOrder.SelectedIndex = -1;
                await LoadManufacturers();
            }
        }
        private async void tbSearch_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadManufacturers();
            }
        }
        private async void btSearch_Click(object sender, EventArgs e) {
            await LoadManufacturers();
        }
        private async void btDelete_Click(object sender, EventArgs e) {
            try {
                Id = dgManufacturer.SelectedRows[0].Cells[0].Value.ToString();
                var result = MessageBox.Show(
                    "Tem certeza de que deseja excluir o fabricante selecionado?",
                    "Atenção!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                );
                if (result == DialogResult.Yes) {
                    if (await controller.Delete(loggedUser)) {
                        MessageBox.Show("O fabricante foi removido com sucesso!");
                        CleanFields();
                        await LoadManufacturers();
                    }
                }
            } catch {
                new FormException("Selecione a linha do fabricante que deseja remover.");
            }
        }
        private void btUpdate_Click(object sender, EventArgs e) {
            GetManufacturer();
        }
        private async void dgManufacturer_MouseDoubleClick(object sender, MouseEventArgs e) {
            GetManufacturer();
        }
        #endregion

        #region Functions
        private void CleanFields() {
            Id = null;
            CompanyName = "";
            FantasyName = "";
            Email = "";
            Phone = "";
            Contact = "";
            Address = "";
            District = "";
            City = "";
            State = "";
            Country = "";
            Image = Properties.Resources.company_image;
        }
        private bool CheckMandatoryFields() {
            if (ValidationHandler.IsTextBoxNull(tbCompanyName, lbCompanyName.Text)) {
                return false;
            }
            if (ValidationHandler.IsTextBoxNull(tbFantasyName, lbFantasyName.Text)) {
                return false;
            }
            if (ValidationHandler.IsMaskedBoxNull(tbPhone, lbPhone.Text)) {
                return false;
            }
            if (ValidationHandler.IsTextBoxNull(tbContact, lbContact.Text)) {
                return false;
            }
            return true;
        }
        private void CleanDataGrid() {
            dgManufacturer.DataSource = null;
        }
        private async Task LoadManufacturers() {
            CleanDataGrid();
            if (cbOrder.SelectedIndex == -1) {
                await controller.List(loggedUser, tbSearch.Text);
            } else {
                await controller.List(loggedUser, tbSearch.Text, cbOrder.SelectedItem.ToString());
            }
        }
        private async void GetManufacturer() {
            try {
                CleanFields();
                Id = dgManufacturer.SelectedRows[0].Cells[0].Value.ToString();
                await controller.Read(loggedUser);
                tabManufacturer.SelectedTab = tabRegister;
            } catch {
                new FormException("Selecione o fabricante que deseja alterar.");
            }
        }
        #endregion

        
    }
}
