﻿using HVEX.Controller;
using HVEX.Service;
using HVEX.View.Interface;
using System;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmMain : Form, IUser {

        private UserController controller;

        #region Interface Attributes
        public string Id { get; set; }
        public string Name {
            get { return lbUserName.Text; }
            set { lbUserName.Text = value; }
        }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AccessLevel {
            get { return lbAccessLevel.Text; }
            set { lbAccessLevel.Text = value; }
        }
        public Image Image { get; set; }
        public string Search { get; set; }
        public DataTable List { get; set; }
        #endregion 

        public FrmMain(string userId) {
            InitializeComponent();
            controller = new UserController(this);
            Id = userId;
        }

        #region Events
        private void ensaiosToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmTestSelection")) {
                FrmTestSelection frm = new FrmTestSelection();
                frm.Show();
            }
        }
        private async void FrmMain_Load(object sender, System.EventArgs e) {
            FrmTestSelection frm = new FrmTestSelection();
            await controller.Read(Id);
            if (AccessLevel == "Administrador") {
                ensaiosToolStripMenuItem.Visible = false;
            } else if (AccessLevel == "Engenheiro") {
                ensaiosToolStripMenuItem.Visible = false;
                logsToolStripMenuItem.Visible = false;
            } else if (AccessLevel == "Técnico") {
                ensaiosToolStripMenuItem.Visible = false;
                logsToolStripMenuItem.Visible = false;
                controleDeEnsaioToolStripMenuItem.Visible = false;
            }
        }
        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e) {
            if (Application.OpenForms.Count == 0) {
                Environment.Exit(1);
            }
        }
        private void sobreToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmAbout")) {
                FrmAbout frm = new FrmAbout();
                frm.Show();
            }
        }
        private void sairToolStripMenuItem_Click(object sender, EventArgs e) {
            CloseAllForms();
        }
        private void logsToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmLogger")) {
                FrmLogger frm = new FrmLogger();
                frm.Show();
            }
        }
        private void usuáriosToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmUser")) {
                FrmUser frm = new FrmUser(Id);
                frm.Show();
            }
        }
        private void fabricantesToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmManufacturer")) {
                FrmManufacturer frm = new FrmManufacturer(Id);
                frm.Show();
            }
        }
        private void testBodiesToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmTestBody")) {
                FrmEquipment frm = new FrmEquipment(Id);
                frm.Show();
            }
        }
        private void measuringInstrumentsToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmMeasuringInstrument")) {
                FrmMeasuringInstrument frm = new FrmMeasuringInstrument(Id);
                frm.Show();
            }
        }
        private void fRAToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmTestFra")) {
                FrmTestFra frm = new FrmTestFra(Id);
                frm.Show();
            }
        }
        private void shuntToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmTestShunt")) {
                FrmTestShunt frm = new FrmTestShunt(Id);
                frm.Show();
            }
        }
        private void trifásicoToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmTestPdmsThreePhase")) {
                FrmTestPdmsThreePhase frm = new FrmTestPdmsThreePhase(Id);
                frm.Show();
            }
        }
        private void monofásicoToolStripMenuItem_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmTestPdmsSinglePhase")) {
                FrmTestPdmsSinglePhase frm = new FrmTestPdmsSinglePhase(Id);
                frm.Show();
            }
        }
        #endregion

        #region Functions
        private void CloseAllForms() {
            Form[] OpenForms = Application.OpenForms.Cast<Form>().ToArray();
            foreach (Form form in OpenForms) {
                if (form.Name != "FrmMain") {
                    form.Close();
                }
            }
            FrmLogin frmLogin = new FrmLogin();
            frmLogin.Show();
            Close();
        }




        #endregion
        
    }
}
