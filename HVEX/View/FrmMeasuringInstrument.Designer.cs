﻿
namespace HVEX.View {
    partial class FrmMeasuringInstrument {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMeasuringInstrument));
            this.tabMeasuringInstruments = new System.Windows.Forms.TabControl();
            this.tabRegister = new System.Windows.Forms.TabPage();
            this.btClean = new System.Windows.Forms.Button();
            this.btSave = new System.Windows.Forms.Button();
            this.rbPlc = new System.Windows.Forms.RadioButton();
            this.rbOscilloscope = new System.Windows.Forms.RadioButton();
            this.lbGeneral = new System.Windows.Forms.Label();
            this.tabInstrumentsType = new System.Windows.Forms.TabControl();
            this.tabOscilloscope = new System.Windows.Forms.TabPage();
            this.tbInstrument = new System.Windows.Forms.TextBox();
            this.tbChannels = new System.Windows.Forms.TextBox();
            this.lbInstrument = new System.Windows.Forms.Label();
            this.lbChannels = new System.Windows.Forms.Label();
            this.tabPlc = new System.Windows.Forms.TabPage();
            this.tbAcquisitioninterval = new System.Windows.Forms.TextBox();
            this.lbAcquisitioninterval = new System.Windows.Forms.Label();
            this.pnGeneral = new System.Windows.Forms.Panel();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.lbPort = new System.Windows.Forms.Label();
            this.tbIp = new System.Windows.Forms.MaskedTextBox();
            this.lbIp = new System.Windows.Forms.Label();
            this.tbBrand = new System.Windows.Forms.TextBox();
            this.lbBrand = new System.Windows.Forms.Label();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.lbModel = new System.Windows.Forms.Label();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.lbDescription = new System.Windows.Forms.Label();
            this.tbSerialNumber = new System.Windows.Forms.TextBox();
            this.lbSerialNumber = new System.Windows.Forms.Label();
            this.tabSearch = new System.Windows.Forms.TabPage();
            this.dgMeasuringInstruments = new System.Windows.Forms.DataGridView();
            this.btUpdate = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.btSearch = new System.Windows.Forms.Button();
            this.cbOrder = new System.Windows.Forms.ComboBox();
            this.tbSearchBrand = new System.Windows.Forms.TextBox();
            this.tbSearchSerialNumber = new System.Windows.Forms.TextBox();
            this.lbSearchOrder = new System.Windows.Forms.Label();
            this.lbSearchBrand = new System.Windows.Forms.Label();
            this.lbSearchSerialNumber = new System.Windows.Forms.Label();
            this.tabMeasuringInstruments.SuspendLayout();
            this.tabRegister.SuspendLayout();
            this.tabInstrumentsType.SuspendLayout();
            this.tabOscilloscope.SuspendLayout();
            this.tabPlc.SuspendLayout();
            this.pnGeneral.SuspendLayout();
            this.tabSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMeasuringInstruments)).BeginInit();
            this.SuspendLayout();
            // 
            // tabMeasuringInstruments
            // 
            this.tabMeasuringInstruments.Controls.Add(this.tabRegister);
            this.tabMeasuringInstruments.Controls.Add(this.tabSearch);
            this.tabMeasuringInstruments.Location = new System.Drawing.Point(12, 13);
            this.tabMeasuringInstruments.Name = "tabMeasuringInstruments";
            this.tabMeasuringInstruments.SelectedIndex = 0;
            this.tabMeasuringInstruments.Size = new System.Drawing.Size(493, 345);
            this.tabMeasuringInstruments.TabIndex = 1;
            this.tabMeasuringInstruments.SelectedIndexChanged += new System.EventHandler(this.tabMeasuringInstruments_SelectedIndexChanged);
            // 
            // tabRegister
            // 
            this.tabRegister.Controls.Add(this.btClean);
            this.tabRegister.Controls.Add(this.btSave);
            this.tabRegister.Controls.Add(this.rbPlc);
            this.tabRegister.Controls.Add(this.rbOscilloscope);
            this.tabRegister.Controls.Add(this.lbGeneral);
            this.tabRegister.Controls.Add(this.tabInstrumentsType);
            this.tabRegister.Controls.Add(this.pnGeneral);
            this.tabRegister.Location = new System.Drawing.Point(4, 22);
            this.tabRegister.Name = "tabRegister";
            this.tabRegister.Padding = new System.Windows.Forms.Padding(3);
            this.tabRegister.Size = new System.Drawing.Size(485, 319);
            this.tabRegister.TabIndex = 0;
            this.tabRegister.Text = "Registrar";
            this.tabRegister.UseVisualStyleBackColor = true;
            // 
            // btClean
            // 
            this.btClean.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btClean.FlatAppearance.BorderSize = 0;
            this.btClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btClean.ForeColor = System.Drawing.Color.White;
            this.btClean.Location = new System.Drawing.Point(404, 289);
            this.btClean.Name = "btClean";
            this.btClean.Size = new System.Drawing.Size(75, 23);
            this.btClean.TabIndex = 17;
            this.btClean.Text = "Limpar";
            this.btClean.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btClean.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btClean.UseVisualStyleBackColor = false;
            this.btClean.Click += new System.EventHandler(this.btClean_Click);
            // 
            // btSave
            // 
            this.btSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btSave.FlatAppearance.BorderSize = 0;
            this.btSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSave.ForeColor = System.Drawing.Color.White;
            this.btSave.Location = new System.Drawing.Point(323, 289);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(75, 23);
            this.btSave.TabIndex = 16;
            this.btSave.Text = "Salvar";
            this.btSave.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btSave.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSave.UseVisualStyleBackColor = false;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // rbPlc
            // 
            this.rbPlc.AutoSize = true;
            this.rbPlc.Location = new System.Drawing.Point(431, 191);
            this.rbPlc.Name = "rbPlc";
            this.rbPlc.Size = new System.Drawing.Size(45, 17);
            this.rbPlc.TabIndex = 15;
            this.rbPlc.Text = "CLP";
            this.rbPlc.UseVisualStyleBackColor = true;
            this.rbPlc.CheckedChanged += new System.EventHandler(this.rbPlc_CheckedChanged);
            // 
            // rbOscilloscope
            // 
            this.rbOscilloscope.AutoSize = true;
            this.rbOscilloscope.Checked = true;
            this.rbOscilloscope.Location = new System.Drawing.Point(340, 190);
            this.rbOscilloscope.Name = "rbOscilloscope";
            this.rbOscilloscope.Size = new System.Drawing.Size(85, 17);
            this.rbOscilloscope.TabIndex = 14;
            this.rbOscilloscope.TabStop = true;
            this.rbOscilloscope.Text = "Osciloscópio";
            this.rbOscilloscope.UseVisualStyleBackColor = true;
            this.rbOscilloscope.CheckedChanged += new System.EventHandler(this.rbOscilloscope_CheckedChanged);
            // 
            // lbGeneral
            // 
            this.lbGeneral.AutoSize = true;
            this.lbGeneral.Location = new System.Drawing.Point(18, 8);
            this.lbGeneral.Name = "lbGeneral";
            this.lbGeneral.Size = new System.Drawing.Size(38, 13);
            this.lbGeneral.TabIndex = 13;
            this.lbGeneral.Text = " Geral ";
            // 
            // tabInstrumentsType
            // 
            this.tabInstrumentsType.Controls.Add(this.tabOscilloscope);
            this.tabInstrumentsType.Controls.Add(this.tabPlc);
            this.tabInstrumentsType.Location = new System.Drawing.Point(6, 191);
            this.tabInstrumentsType.Name = "tabInstrumentsType";
            this.tabInstrumentsType.SelectedIndex = 0;
            this.tabInstrumentsType.Size = new System.Drawing.Size(473, 88);
            this.tabInstrumentsType.TabIndex = 9;
            // 
            // tabOscilloscope
            // 
            this.tabOscilloscope.Controls.Add(this.tbInstrument);
            this.tabOscilloscope.Controls.Add(this.tbChannels);
            this.tabOscilloscope.Controls.Add(this.lbInstrument);
            this.tabOscilloscope.Controls.Add(this.lbChannels);
            this.tabOscilloscope.Location = new System.Drawing.Point(4, 22);
            this.tabOscilloscope.Name = "tabOscilloscope";
            this.tabOscilloscope.Padding = new System.Windows.Forms.Padding(3);
            this.tabOscilloscope.Size = new System.Drawing.Size(465, 62);
            this.tabOscilloscope.TabIndex = 0;
            this.tabOscilloscope.Text = "Osciloscópio";
            this.tabOscilloscope.UseVisualStyleBackColor = true;
            // 
            // tbInstrument
            // 
            this.tbInstrument.Location = new System.Drawing.Point(242, 29);
            this.tbInstrument.Name = "tbInstrument";
            this.tbInstrument.Size = new System.Drawing.Size(220, 20);
            this.tbInstrument.TabIndex = 8;
            // 
            // tbChannels
            // 
            this.tbChannels.Location = new System.Drawing.Point(5, 29);
            this.tbChannels.Name = "tbChannels";
            this.tbChannels.Size = new System.Drawing.Size(220, 20);
            this.tbChannels.TabIndex = 7;
            // 
            // lbInstrument
            // 
            this.lbInstrument.AutoSize = true;
            this.lbInstrument.Location = new System.Drawing.Point(239, 13);
            this.lbInstrument.Name = "lbInstrument";
            this.lbInstrument.Size = new System.Drawing.Size(62, 13);
            this.lbInstrument.TabIndex = 12;
            this.lbInstrument.Text = "Instrumento";
            // 
            // lbChannels
            // 
            this.lbChannels.AutoSize = true;
            this.lbChannels.Location = new System.Drawing.Point(8, 13);
            this.lbChannels.Name = "lbChannels";
            this.lbChannels.Size = new System.Drawing.Size(39, 13);
            this.lbChannels.TabIndex = 8;
            this.lbChannels.Text = "Canais";
            // 
            // tabPlc
            // 
            this.tabPlc.Controls.Add(this.tbAcquisitioninterval);
            this.tabPlc.Controls.Add(this.lbAcquisitioninterval);
            this.tabPlc.Location = new System.Drawing.Point(4, 22);
            this.tabPlc.Name = "tabPlc";
            this.tabPlc.Padding = new System.Windows.Forms.Padding(3);
            this.tabPlc.Size = new System.Drawing.Size(465, 62);
            this.tabPlc.TabIndex = 1;
            this.tabPlc.Text = "CLP";
            this.tabPlc.UseVisualStyleBackColor = true;
            // 
            // tbAcquisitioninterval
            // 
            this.tbAcquisitioninterval.Location = new System.Drawing.Point(5, 29);
            this.tbAcquisitioninterval.Name = "tbAcquisitioninterval";
            this.tbAcquisitioninterval.Size = new System.Drawing.Size(220, 20);
            this.tbAcquisitioninterval.TabIndex = 9;
            // 
            // lbAcquisitioninterval
            // 
            this.lbAcquisitioninterval.AutoSize = true;
            this.lbAcquisitioninterval.Location = new System.Drawing.Point(8, 13);
            this.lbAcquisitioninterval.Name = "lbAcquisitioninterval";
            this.lbAcquisitioninterval.Size = new System.Drawing.Size(112, 13);
            this.lbAcquisitioninterval.TabIndex = 10;
            this.lbAcquisitioninterval.Text = "Intervalo de Aquisição";
            // 
            // pnGeneral
            // 
            this.pnGeneral.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnGeneral.Controls.Add(this.tbPort);
            this.pnGeneral.Controls.Add(this.lbPort);
            this.pnGeneral.Controls.Add(this.tbIp);
            this.pnGeneral.Controls.Add(this.lbIp);
            this.pnGeneral.Controls.Add(this.tbBrand);
            this.pnGeneral.Controls.Add(this.lbBrand);
            this.pnGeneral.Controls.Add(this.tbModel);
            this.pnGeneral.Controls.Add(this.lbModel);
            this.pnGeneral.Controls.Add(this.tbDescription);
            this.pnGeneral.Controls.Add(this.lbDescription);
            this.pnGeneral.Controls.Add(this.tbSerialNumber);
            this.pnGeneral.Controls.Add(this.lbSerialNumber);
            this.pnGeneral.Location = new System.Drawing.Point(6, 16);
            this.pnGeneral.Name = "pnGeneral";
            this.pnGeneral.Size = new System.Drawing.Size(473, 162);
            this.pnGeneral.TabIndex = 1;
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(242, 128);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(100, 20);
            this.tbPort.TabIndex = 6;
            // 
            // lbPort
            // 
            this.lbPort.AutoSize = true;
            this.lbPort.Location = new System.Drawing.Point(242, 111);
            this.lbPort.Name = "lbPort";
            this.lbPort.Size = new System.Drawing.Size(32, 13);
            this.lbPort.TabIndex = 14;
            this.lbPort.Text = "Porta";
            // 
            // tbIp
            // 
            this.tbIp.Culture = new System.Globalization.CultureInfo("");
            this.tbIp.Location = new System.Drawing.Point(8, 128);
            this.tbIp.Mask = "000.000.000.000";
            this.tbIp.Name = "tbIp";
            this.tbIp.Size = new System.Drawing.Size(100, 20);
            this.tbIp.TabIndex = 5;
            // 
            // lbIp
            // 
            this.lbIp.AutoSize = true;
            this.lbIp.Location = new System.Drawing.Point(8, 111);
            this.lbIp.Name = "lbIp";
            this.lbIp.Size = new System.Drawing.Size(17, 13);
            this.lbIp.TabIndex = 11;
            this.lbIp.Text = "IP";
            // 
            // tbBrand
            // 
            this.tbBrand.Location = new System.Drawing.Point(242, 78);
            this.tbBrand.Name = "tbBrand";
            this.tbBrand.Size = new System.Drawing.Size(220, 20);
            this.tbBrand.TabIndex = 4;
            // 
            // lbBrand
            // 
            this.lbBrand.AutoSize = true;
            this.lbBrand.Location = new System.Drawing.Point(242, 62);
            this.lbBrand.Name = "lbBrand";
            this.lbBrand.Size = new System.Drawing.Size(37, 13);
            this.lbBrand.TabIndex = 10;
            this.lbBrand.Text = "Marca";
            // 
            // tbModel
            // 
            this.tbModel.Location = new System.Drawing.Point(8, 78);
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(220, 20);
            this.tbModel.TabIndex = 3;
            // 
            // lbModel
            // 
            this.lbModel.AutoSize = true;
            this.lbModel.Location = new System.Drawing.Point(8, 62);
            this.lbModel.Name = "lbModel";
            this.lbModel.Size = new System.Drawing.Size(42, 13);
            this.lbModel.TabIndex = 8;
            this.lbModel.Text = "Modelo";
            // 
            // tbDescription
            // 
            this.tbDescription.Location = new System.Drawing.Point(242, 29);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.Size = new System.Drawing.Size(220, 20);
            this.tbDescription.TabIndex = 2;
            // 
            // lbDescription
            // 
            this.lbDescription.AutoSize = true;
            this.lbDescription.Location = new System.Drawing.Point(242, 13);
            this.lbDescription.Name = "lbDescription";
            this.lbDescription.Size = new System.Drawing.Size(55, 13);
            this.lbDescription.TabIndex = 7;
            this.lbDescription.Text = "Descrição";
            // 
            // tbSerialNumber
            // 
            this.tbSerialNumber.Location = new System.Drawing.Point(8, 29);
            this.tbSerialNumber.Name = "tbSerialNumber";
            this.tbSerialNumber.Size = new System.Drawing.Size(220, 20);
            this.tbSerialNumber.TabIndex = 1;
            // 
            // lbSerialNumber
            // 
            this.lbSerialNumber.AutoSize = true;
            this.lbSerialNumber.Location = new System.Drawing.Point(8, 13);
            this.lbSerialNumber.Name = "lbSerialNumber";
            this.lbSerialNumber.Size = new System.Drawing.Size(86, 13);
            this.lbSerialNumber.TabIndex = 5;
            this.lbSerialNumber.Text = "Número de Série";
            // 
            // tabSearch
            // 
            this.tabSearch.Controls.Add(this.dgMeasuringInstruments);
            this.tabSearch.Controls.Add(this.btUpdate);
            this.tabSearch.Controls.Add(this.btDelete);
            this.tabSearch.Controls.Add(this.btSearch);
            this.tabSearch.Controls.Add(this.cbOrder);
            this.tabSearch.Controls.Add(this.tbSearchBrand);
            this.tabSearch.Controls.Add(this.tbSearchSerialNumber);
            this.tabSearch.Controls.Add(this.lbSearchOrder);
            this.tabSearch.Controls.Add(this.lbSearchBrand);
            this.tabSearch.Controls.Add(this.lbSearchSerialNumber);
            this.tabSearch.Location = new System.Drawing.Point(4, 22);
            this.tabSearch.Name = "tabSearch";
            this.tabSearch.Padding = new System.Windows.Forms.Padding(3);
            this.tabSearch.Size = new System.Drawing.Size(485, 319);
            this.tabSearch.TabIndex = 1;
            this.tabSearch.Text = "Pesquisar";
            this.tabSearch.UseVisualStyleBackColor = true;
            // 
            // dgMeasuringInstruments
            // 
            this.dgMeasuringInstruments.AllowUserToAddRows = false;
            this.dgMeasuringInstruments.AllowUserToDeleteRows = false;
            this.dgMeasuringInstruments.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMeasuringInstruments.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dgMeasuringInstruments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMeasuringInstruments.Location = new System.Drawing.Point(8, 55);
            this.dgMeasuringInstruments.Name = "dgMeasuringInstruments";
            this.dgMeasuringInstruments.ReadOnly = true;
            this.dgMeasuringInstruments.Size = new System.Drawing.Size(471, 228);
            this.dgMeasuringInstruments.TabIndex = 7;
            this.dgMeasuringInstruments.DoubleClick += new System.EventHandler(this.dgMeasuringInstruments_DoubleClick);
            // 
            // btUpdate
            // 
            this.btUpdate.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btUpdate.FlatAppearance.BorderSize = 0;
            this.btUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btUpdate.ForeColor = System.Drawing.Color.White;
            this.btUpdate.Location = new System.Drawing.Point(404, 289);
            this.btUpdate.Name = "btUpdate";
            this.btUpdate.Size = new System.Drawing.Size(75, 23);
            this.btUpdate.TabIndex = 6;
            this.btUpdate.Text = "Editar";
            this.btUpdate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btUpdate.UseVisualStyleBackColor = false;
            this.btUpdate.Click += new System.EventHandler(this.btUpdate_Click);
            // 
            // btDelete
            // 
            this.btDelete.BackColor = System.Drawing.Color.DarkRed;
            this.btDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btDelete.FlatAppearance.BorderSize = 0;
            this.btDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btDelete.ForeColor = System.Drawing.Color.White;
            this.btDelete.Image = ((System.Drawing.Image)(resources.GetObject("btDelete.Image")));
            this.btDelete.Location = new System.Drawing.Point(323, 289);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(75, 23);
            this.btDelete.TabIndex = 5;
            this.btDelete.Text = "Excluir";
            this.btDelete.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btDelete.UseVisualStyleBackColor = false;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btSearch
            // 
            this.btSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.btSearch.FlatAppearance.BorderSize = 0;
            this.btSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btSearch.ForeColor = System.Drawing.Color.White;
            this.btSearch.Location = new System.Drawing.Point(399, 28);
            this.btSearch.Name = "btSearch";
            this.btSearch.Size = new System.Drawing.Size(80, 21);
            this.btSearch.TabIndex = 4;
            this.btSearch.Text = "Pesquisar";
            this.btSearch.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btSearch.UseVisualStyleBackColor = false;
            this.btSearch.Click += new System.EventHandler(this.btSearch_Click);
            // 
            // cbOrder
            // 
            this.cbOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOrder.DropDownWidth = 110;
            this.cbOrder.FormattingEnabled = true;
            this.cbOrder.Items.AddRange(new object[] {
            "Número de Série",
            "Marca",
            "Modelo"});
            this.cbOrder.Location = new System.Drawing.Point(278, 28);
            this.cbOrder.Name = "cbOrder";
            this.cbOrder.Size = new System.Drawing.Size(110, 21);
            this.cbOrder.TabIndex = 3;
            // 
            // tbSearchBrand
            // 
            this.tbSearchBrand.Location = new System.Drawing.Point(108, 28);
            this.tbSearchBrand.Multiline = true;
            this.tbSearchBrand.Name = "tbSearchBrand";
            this.tbSearchBrand.Size = new System.Drawing.Size(160, 21);
            this.tbSearchBrand.TabIndex = 2;
            this.tbSearchBrand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSearchBrand_KeyDown);
            // 
            // tbSearchSerialNumber
            // 
            this.tbSearchSerialNumber.Location = new System.Drawing.Point(8, 28);
            this.tbSearchSerialNumber.Multiline = true;
            this.tbSearchSerialNumber.Name = "tbSearchSerialNumber";
            this.tbSearchSerialNumber.Size = new System.Drawing.Size(90, 21);
            this.tbSearchSerialNumber.TabIndex = 1;
            this.tbSearchSerialNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tbSearchSerialNumber_KeyDown);
            // 
            // lbSearchOrder
            // 
            this.lbSearchOrder.AutoSize = true;
            this.lbSearchOrder.Location = new System.Drawing.Point(275, 12);
            this.lbSearchOrder.Name = "lbSearchOrder";
            this.lbSearchOrder.Size = new System.Drawing.Size(38, 13);
            this.lbSearchOrder.TabIndex = 5;
            this.lbSearchOrder.Text = "Ordem";
            // 
            // lbSearchBrand
            // 
            this.lbSearchBrand.AutoSize = true;
            this.lbSearchBrand.Location = new System.Drawing.Point(105, 12);
            this.lbSearchBrand.Name = "lbSearchBrand";
            this.lbSearchBrand.Size = new System.Drawing.Size(37, 13);
            this.lbSearchBrand.TabIndex = 3;
            this.lbSearchBrand.Text = "Marca";
            // 
            // lbSearchSerialNumber
            // 
            this.lbSearchSerialNumber.AutoSize = true;
            this.lbSearchSerialNumber.Location = new System.Drawing.Point(5, 12);
            this.lbSearchSerialNumber.Name = "lbSearchSerialNumber";
            this.lbSearchSerialNumber.Size = new System.Drawing.Size(84, 13);
            this.lbSearchSerialNumber.TabIndex = 0;
            this.lbSearchSerialNumber.Text = "Número de série";
            // 
            // FrmMeasuringInstrument
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(111)))), ((int)(((byte)(159)))));
            this.BackgroundImage = global::HVEX.Properties.Resources.grid;
            this.ClientSize = new System.Drawing.Size(517, 370);
            this.Controls.Add(this.tabMeasuringInstruments);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FrmMeasuringInstrument";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Instrumentos de medição";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmMeasuringInstrument_FormClosed);
            this.Load += new System.EventHandler(this.FrmMeasuringInstrument_Load);
            this.tabMeasuringInstruments.ResumeLayout(false);
            this.tabRegister.ResumeLayout(false);
            this.tabRegister.PerformLayout();
            this.tabInstrumentsType.ResumeLayout(false);
            this.tabOscilloscope.ResumeLayout(false);
            this.tabOscilloscope.PerformLayout();
            this.tabPlc.ResumeLayout(false);
            this.tabPlc.PerformLayout();
            this.pnGeneral.ResumeLayout(false);
            this.pnGeneral.PerformLayout();
            this.tabSearch.ResumeLayout(false);
            this.tabSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMeasuringInstruments)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabMeasuringInstruments;
        private System.Windows.Forms.TabPage tabRegister;
        private System.Windows.Forms.Button btClean;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.RadioButton rbPlc;
        private System.Windows.Forms.RadioButton rbOscilloscope;
        private System.Windows.Forms.Label lbGeneral;
        private System.Windows.Forms.TabControl tabInstrumentsType;
        private System.Windows.Forms.TabPage tabOscilloscope;
        private System.Windows.Forms.TextBox tbInstrument;
        private System.Windows.Forms.TextBox tbChannels;
        private System.Windows.Forms.Label lbInstrument;
        private System.Windows.Forms.Label lbChannels;
        private System.Windows.Forms.TabPage tabPlc;
        private System.Windows.Forms.TextBox tbAcquisitioninterval;
        private System.Windows.Forms.Label lbAcquisitioninterval;
        private System.Windows.Forms.Panel pnGeneral;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label lbPort;
        private System.Windows.Forms.MaskedTextBox tbIp;
        private System.Windows.Forms.Label lbIp;
        private System.Windows.Forms.TextBox tbBrand;
        private System.Windows.Forms.Label lbBrand;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.Label lbModel;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label lbDescription;
        private System.Windows.Forms.TextBox tbSerialNumber;
        private System.Windows.Forms.Label lbSerialNumber;
        private System.Windows.Forms.TabPage tabSearch;
        private System.Windows.Forms.DataGridView dgMeasuringInstruments;
        private System.Windows.Forms.Button btUpdate;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btSearch;
        private System.Windows.Forms.ComboBox cbOrder;
        private System.Windows.Forms.TextBox tbSearchBrand;
        private System.Windows.Forms.TextBox tbSearchSerialNumber;
        private System.Windows.Forms.Label lbSearchOrder;
        private System.Windows.Forms.Label lbSearchBrand;
        private System.Windows.Forms.Label lbSearchSerialNumber;
    }
}