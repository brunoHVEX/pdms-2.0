﻿using HVEX.Controller;
using HVEX.Service;
using HVEX.View.Interface;
using HVEX.View.SubForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HVEX.View {
    public partial class FrmEquipment : Form, ITransformer {

        private string loggedUser;
        private EquipmentController controller;

        #region Interface Attributes
        public string Id { get; set; }

        public string SerialNumber {
            get { return tbSerialNumber.Text; }
            set { tbSerialNumber.Text = value; }
        }

        public string InternalNumber {
            get { return tbInternalNumber.Text; }
            set { tbInternalNumber.Text = value; }
        }

        public string Description {
            get { return tbDescription.Text; }
            set { tbDescription.Text = value; }
        }

        public string Model {
            get { return tbModel.Text; }
            set { tbModel.Text = value; }
        }

        public DateTime ManufactureDate {
            get { return DateTime.Parse(tbManufactureDate.Text); }
            set { tbManufactureDate.Text = value.ToString(); }
        }

        public string Manufacturer {
            get { return cbManufacturer.SelectedValue.ToString(); }
            set { cbManufacturer.SelectedValue = value; }
        }

        public string Customer {
            get { return cbCustomer.SelectedValue.ToString(); }
            set { cbCustomer.SelectedValue = value; }
        }

        public string SpecimenType {
            get {
                if (rbTransformer.Checked) {
                    return "Transformer";
                } else {
                    return "Generic";
                }
            }
            set {
                switch (value) {
                    case "Transformer":
                        rbTransformer.Checked = true;
                        break;
                    case "Generic":
                        rbGeneric.Checked = true;
                        break;
                }
            }
        }

        public string TrafoEfficiencyLevel {
            get { return tbEfficiencyLevel.Text; }
            set { tbEfficiencyLevel.Text = value; }
        }

        public string TrafoType {
            get { return tbType.Text; }
            set { tbType.Text = value; }
        }

        public double TrafoWeight {
            get {
                if (!string.IsNullOrEmpty(tbWeight.Text.Trim())) {
                    return double.Parse(tbWeight.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    return 0;
                }
            }
            set { tbWeight.Text = value.ToString(); }
        }

        public double TrafoRegulationRange {
            get {
                if (!string.IsNullOrEmpty(tbRegulationRange.Text.Trim())) {
                    return double.Parse(tbRegulationRange.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    return 0;
                }
            }
            set { tbRegulationRange.Text = value.ToString(); }
        }

        public int TrafoPhases {
            get {
                if (cbPhases.SelectedIndex != -1) {
                    return cbPhases.SelectedIndex + 1;
                } else {
                    return 1;
                }
            }
            set { cbPhases.SelectedIndex = value - 1; }
        }

        public int TrafoFrequency {
            get {
                if (cbFrequency.SelectedIndex != -1) {
                    return int.Parse(cbFrequency.SelectedItem.ToString());
                } else {
                    return 0;
                }
            }
            set { cbFrequency.SelectedItem = value.ToString(); }
        }

        public string TrafoInsulatingOil {
            get {
                if (cbInsulatingOil.SelectedIndex != -1) {
                    return cbInsulatingOil.SelectedItem.ToString();
                } else {
                    return null;
                }
            }
            set { cbInsulatingOil.SelectedItem = value; }
        }

        public string TrafoPolarity {
            get {
                if (cbPolarity.SelectedIndex != -1) {
                    return cbPolarity.SelectedItem.ToString();
                } else {
                    return null;
                }
            }
            set { cbPolarity.SelectedItem = value; }
        }

        public double TrafoVoltageClass {
            get {
                if (cbVoltageClass.SelectedIndex != -1) {
                    return double.Parse(
                        cbVoltageClass.SelectedItem.ToString(), CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    return 0;
                }
            }
            set { cbVoltageClass.SelectedItem = string.Format("{0:0.0}", value); }
        }

        public string TrafoState {
            get {
                if (cbState.SelectedIndex != -1) {
                    return cbState.SelectedItem.ToString();
                } else {
                    return null;
                }
            }
            set { cbState.SelectedItem = value; }
        }

        public double TrafoTemperatureLimit {
            get {
                if (!string.IsNullOrEmpty(tbTemperatureLimit.Text.Trim())) {
                    return double.Parse(tbTemperatureLimit.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    return 0;
                }
            }
            set { tbTemperatureLimit.Text = value.ToString(); }
        }

        public double TrafoResistanceTemperature {
            get {
                if (!string.IsNullOrEmpty(tbResistanceTemperature.Text.Trim())) {
                    return double.Parse(tbResistanceTemperature.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    return 0;
                }
            }
            set { tbResistanceTemperature.Text = value.ToString(); }
        }

        public double[] TrafoPower {
            get {
                double[] power = new double[3];
                if (!string.IsNullOrEmpty(tbFirstPower.Text)) {
                    power[0] = double.Parse(tbFirstPower.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    power[0] = 0;
                }
                if (!string.IsNullOrEmpty(tbSecondPower.Text)) {
                    power[1] = double.Parse(tbSecondPower.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    power[1] = 0;
                }
                if (!string.IsNullOrEmpty(tbThirdPower.Text)) {
                    power[2] = double.Parse(tbThirdPower.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    power[2] = 0;
                }
                return power;
            }
            set {
                tbFirstPower.Text = String.Format("{0:0.00}", value[0]);
                tbSecondPower.Text = String.Format("{0:0.00}", value[1]);
                tbThirdPower.Text = String.Format("{0:0.00}", value[2]);
            }
        }

        public string[] TrafoPowerMeasurement {
            get {
                string[] powerMeasurement = new string[3];
                if (cbFirstPower.SelectedIndex != -1) {
                    powerMeasurement[0] = cbFirstPower.SelectedItem.ToString();
                } else {
                    powerMeasurement[0] = null;
                }
                if (cbSecondPower.SelectedIndex != -1) {
                    powerMeasurement[1] = cbSecondPower.SelectedItem.ToString();
                } else {
                    powerMeasurement[1] = null;
                }
                if (cbThirdPower.SelectedIndex != -1) {
                    powerMeasurement[2] = cbThirdPower.SelectedItem.ToString();
                } else {
                    powerMeasurement[2] = null;
                }
                return powerMeasurement;
            }
            set {
                if (value != null) {
                    cbFirstPower.SelectedItem = value[0];
                    cbSecondPower.SelectedItem = value[1];
                    cbThirdPower.SelectedItem = value[2];
                }
            }
        }

        public double[] TrafoVoltage {
            get {
                double[] voltage = new double[3];
                if (!string.IsNullOrEmpty(tbFirstVoltage.Text)) {
                    voltage[0] = double.Parse(tbFirstVoltage.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    voltage[0] = 0;
                }
                if (!string.IsNullOrEmpty(tbSecondVoltage.Text)) {
                    voltage[1] = double.Parse(tbSecondVoltage.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    voltage[1] = 0;
                }
                if (!string.IsNullOrEmpty(tbThirdVoltage.Text)) {
                    voltage[2] = double.Parse(tbThirdVoltage.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    voltage[2] = 0;
                }
                return voltage;
            }
            set {
                tbFirstVoltage.Text = String.Format("{0:0.00}", value[0]);
                tbSecondVoltage.Text = String.Format("{0:0.00}", value[1]);
                tbThirdVoltage.Text = String.Format("{0:0.00}", value[2]);
            }
        }

        public string[] TrafoVoltageMeasurement {
            get {
                string[] voltageMeasurement = new string[3];
                if (cbFirstVoltage.SelectedIndex != -1) {
                    voltageMeasurement[0] = cbFirstVoltage.SelectedItem.ToString();
                } else {
                    voltageMeasurement[0] = null;
                }
                if (cbSecondVoltage.SelectedIndex != -1) {
                    voltageMeasurement[1] = cbSecondVoltage.SelectedItem.ToString();
                } else {
                    voltageMeasurement[1] = null;
                }
                if (cbThirdVoltage.SelectedIndex != -1) {
                    voltageMeasurement[2] = cbSecondVoltage.SelectedItem.ToString();
                } else {
                    voltageMeasurement[2] = null;
                }
                return voltageMeasurement;
            }
            set {
                if (value != null) {
                    cbFirstVoltage.SelectedItem = value[0];
                    cbSecondVoltage.SelectedItem = value[1];
                    cbThirdVoltage.SelectedItem = value[2];
                }
            }
        }

        public double[] TrafoCurrent {
            get {
                double[] current = new double[3];
                if (!string.IsNullOrEmpty(tbFirstCurrent.Text)) {
                    current[0] = double.Parse(tbFirstCurrent.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    current[0] = 0;
                }
                if (!string.IsNullOrEmpty(tbSecondCurrent.Text)) {
                    current[1] = double.Parse(tbSecondCurrent.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    current[1] = 0;
                }
                if (!string.IsNullOrEmpty(tbThirdCurrent.Text)) {
                    current[2] = double.Parse(tbThirdCurrent.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    current[2] = 0;
                }
                return current;
            }
            set {
                tbFirstCurrent.Text = String.Format("{0:0.00}", value[0]);
                tbSecondCurrent.Text = String.Format("{0:0.00}", value[1]);
                tbThirdCurrent.Text = String.Format("{0:0.00}", value[2]);
            }
        }

        public string[] TrafoCurrentMeasurement {
            get {
                string[] currentMeasurement = new string[3];
                if (cbFirstCurrent.SelectedIndex != -1) {
                    currentMeasurement[0] = cbFirstCurrent.SelectedItem.ToString();
                } else {
                    currentMeasurement[0] = null;
                }
                if (cbSecondCurrent.SelectedIndex != -1) {
                    currentMeasurement[1] = cbSecondCurrent.SelectedItem.ToString();
                } else {
                    currentMeasurement[1] = null;
                }
                if (cbThirdCurrent.SelectedIndex != -1) {
                    currentMeasurement[2] = cbThirdCurrent.SelectedItem.ToString();
                } else {
                    currentMeasurement[2] = null;
                }
                return currentMeasurement;
            }
            set {
                if (value != null) {
                    cbFirstCurrent.SelectedItem = value[0];
                    cbSecondCurrent.SelectedItem = value[1];
                    cbThirdCurrent.SelectedItem = value[2];
                }
            }
        }

        public double[] TrafoResistance {
            get {
                double[] resistance = new double[3];
                if (!string.IsNullOrEmpty(tbFirstResistance.Text)) {
                    resistance[0] = double.Parse(tbFirstResistance.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    resistance[0] = 0;
                }
                if (!string.IsNullOrEmpty(tbSecondResistance.Text)) {
                    resistance[1] = double.Parse(tbSecondResistance.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    resistance[1] = 0;
                }
                if (!string.IsNullOrEmpty(tbThirdResistance.Text)) {
                    resistance[2] = double.Parse(tbThirdResistance.Text, CultureInfo.GetCultureInfo("pt-BR"));
                } else {
                    resistance[2] = 0;
                }
                return resistance;
            }
            set {
                tbFirstResistance.Text = String.Format("{0:0.00}", value[0]);
                tbSecondResistance.Text = String.Format("{0:0.00}", value[1]);
                tbThirdResistance.Text = String.Format("{0:0.00}", value[2]);
            }
        }

        public string[] TrafoResistanceMeasurement {
            get {
                string[] resistanceMeasurement = new string[3];
                if (cbFirstResistance.SelectedIndex != -1) {
                    resistanceMeasurement[0] = cbFirstResistance.SelectedItem.ToString();
                } else {
                    resistanceMeasurement[0] = null;
                }
                if (cbSecondResistance.SelectedIndex != -1) {
                    resistanceMeasurement[1] = cbSecondResistance.SelectedItem.ToString();
                } else {
                    resistanceMeasurement[1] = null;
                }
                if (cbThirdResistance.SelectedIndex != -1) {
                    resistanceMeasurement[2] = cbThirdResistance.SelectedItem.ToString();
                } else {
                    resistanceMeasurement[2] = null;
                }
                return resistanceMeasurement;
            }
            set {
                if (value != null) {
                    cbFirstResistance.SelectedItem = value[0];
                    cbSecondResistance.SelectedItem = value[1];
                    cbThirdResistance.SelectedItem = value[2];
                }
            }
        }

        public string[] TrafoWindingMaterial {
            get {
                string[] windingMaterial = new string[3];
                if (cbFirstWindingMaterial.SelectedIndex != -1) {
                    windingMaterial[0] = cbFirstWindingMaterial.SelectedItem.ToString();
                } else {
                    windingMaterial[0] = null;
                }
                if (cbSecondWindingMaterial.SelectedIndex != -1) {
                    windingMaterial[1] = cbSecondWindingMaterial.SelectedItem.ToString();
                } else {
                    windingMaterial[1] = null;
                }
                if (cbThirdWindingMaterial.SelectedIndex != -1) {
                    windingMaterial[2] = cbThirdWindingMaterial.SelectedItem.ToString();
                } else {
                    windingMaterial[2] = null;
                }
                return windingMaterial;
            }
            set {
                if (value != null) {
                    cbFirstWindingMaterial.SelectedItem = value[0];
                    cbSecondWindingMaterial.SelectedItem = value[1];
                    cbThirdWindingMaterial.SelectedItem = value[2];
                }
            }
        }

        public List<double> TrafoTaps {
            get {
                List<double> taps = new List<double>();
                foreach (DataGridViewRow row in dgTaps.Rows) {
                    taps.Add(double.Parse(row.Cells[0].Value.ToString(), CultureInfo.GetCultureInfo("pt-BR")));
                }
                return taps;
            }
            set {
                foreach (double tap in value) {
                    dgTaps.Rows.Add(tap);
                }
            }
        }

        public string TrafoConnection {
            get { return tbConnection.Text; }
            set { tbConnection.Text = value; }
        }

        public DataTable CustomerList {
            set {
                cbCustomer.DataSource = value;
                cbCustomer.DisplayMember = "Nome Fantasia";
                cbCustomer.ValueMember = "Id";
            }
        }

        public DataTable ManufacturerList {
            set {
                cbManufacturer.DisplayMember = "Nome Fantasia";
                cbManufacturer.ValueMember = "Id";
                cbManufacturer.DataSource = value;
            }
        }

        public DataTable List {
            set {
                if (value != null) {
                    BindingSource binding = new BindingSource();
                    binding.DataSource = value;
                    dgTestBodies.DataSource = binding;
                    dgTestBodies.Columns[0].Visible = false;
                    dgTestBodies.Columns[1].Visible = false;
                }
            }
        }
        #endregion 

        public FrmEquipment(string userId) {
            InitializeComponent();
            loggedUser = userId;
            controller = new EquipmentController(this);
        }

        #region Events
        private async void FrmTestBody_Load(object sender, EventArgs e) {
            tabTestBodyType.TabPages.Remove(tabTransformer);
            await controller.ListManufacturers(loggedUser);
            await controller.ListCustomers(loggedUser);
        }
        private void FrmTestBody_FormClosed(object sender, FormClosedEventArgs e) {
            FormHandler.CloseForm();
        }
        private async void tabTestBodies_SelectedIndexChanged(object sender, EventArgs e) {
            if (tabTestBodies.SelectedTab == tabTestBodies.TabPages["tabSearch"]) {
          
                CleanFields();
                CleanDataGrid();
                tbSearchDescription.Text = "";
                tbSearchSerialNumber.Text = "";
                await LoadTestBody();
            }
        }
        private void rbTransformer_CheckedChanged(object sender, EventArgs e) {
            if (rbTransformer.Checked) {
                SelectEquipmentType(tabTransformer);
            }
        }
        private void rbGeneric_CheckedChanged(object sender, EventArgs e) {
            if (rbGeneric.Checked) {
                SelectEquipmentType(tabGeneric);
            }
        }
        private void cbSecondary_CheckedChanged(object sender, EventArgs e) {
            if (cbTerciary.Checked) {
                pnThirdWinding.Enabled = true;
            } else {
                pnThirdWinding.Enabled = false;
            }
        }
        private void cbTerciary_CheckedChanged(object sender, EventArgs e) {
            if (cbSecondary.Checked) {
                pnSecondWinding.Enabled = true;
            } else {
                pnSecondWinding.Enabled = false;
            }
        }
        private void tbFirstPower_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }

        private void tbFirstPower_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbFirstVoltage_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbFirstVoltage_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbFirstCurrent_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbFirstCurrent_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbFirstResistance_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbFirstResistance_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbSecondPower_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbSecondPower_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbSecondVoltage_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }

        private void tbSecondVoltage_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbSecondCurrent_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbSecondCurrent_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbSecondResistance_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbSecondResistance_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbThirdPower_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbThirdPower_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbThirdVoltage_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbThirdVoltage_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbThirdCurrent_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }

        private void tbThirdCurrent_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbThirdResistance_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbThirdResistance_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbEfficiencyLevel_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbEfficiencyLevel_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbTemperatureLimit_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }

        private void tbTemperatureLimit_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbWeight_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbWeight_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbType_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbRegulationRange_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbRegulationRange_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private void tbResistanceTemperature_KeyPress(object sender, KeyPressEventArgs e) {
            ValidationHandler.IsNumber(e);
        }
        private void tbResistanceTemperature_KeyDown(object sender, KeyEventArgs e) {
            ValidationHandler.SupressEnter(e);
        }
        private async void btSave_Click(object sender, EventArgs e) {
            if (CheckMandatoryFields()) {
                if (Id == null) {
                    if (await controller.Create(loggedUser, CheckedEquipmentType())) {
                        MessageBox.Show("O corpo de prova foi cadastrado com sucesso!");
                        CleanFields();
                    }
                } else {
                    if (await controller.Update(loggedUser, CheckedEquipmentType())) {
                        MessageBox.Show("O corpo de prova foi alterado com sucesso!");
                        CleanFields();
                    }
                }
            }
        }
        private void btClean_Click(object sender, EventArgs e) {
            CleanFields();
        }
        private void btConnection_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmConnection")) {
                FrmConnection frm = new FrmConnection(this);
                frm.Show();
            }
        }
        private void btAdd_Click(object sender, EventArgs e) {
            if (!FormHandler.IsFormOpened("FrmTap")) {
                FrmTap frm = new FrmTap(this);
                frm.Show();
            }
        }
        private void btDelete_Click(object sender, EventArgs e) {
            try {
                dgTaps.Rows.RemoveAt(dgTaps.SelectedRows[0].Index);
            } catch {
                new FormException("Selecione a linha que deseja remover.");
            }
        }
        private async void tbSearchSerialNumber_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadTestBody();
            }
        }
        private async void tbSearchDescription_KeyDown(object sender, KeyEventArgs e) {
            if (e.KeyCode.Equals(Keys.Enter)) {
                e.SuppressKeyPress = true;
                await LoadTestBody();
            }
        }
        private async void btSearch_Click(object sender, EventArgs e) {
            await LoadTestBody();
        }
        private async void dgTestBodies_MouseDoubleClick(object sender, MouseEventArgs e) {
            try {
                CleanFields();
                Id = dgTestBodies.SelectedRows[0].Cells[0].Value.ToString();
                await controller.Read(loggedUser, dgTestBodies.SelectedRows[0].Cells[1].Value.ToString());
                tabTestBodies.SelectedTab = tabRegister;
            } catch {
                new FormException("Selecione a linha do corpo de prova que deseja alterar.");
            }
        }
        private async void btRemove_Click(object sender, EventArgs e) {
            try {
                Id = dgTestBodies.SelectedRows[0].Cells[0].Value.ToString();
                var result = MessageBox.Show(
                    "Tem certeza de que deseja remover este corpo de prova?",
                    "Atenção!",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning
                );
                if (result == DialogResult.Yes) {
                    if (await controller.Delete(loggedUser, CheckedEquipmentType())) {
                        MessageBox.Show("O corpo de prova foi deletedo com sucesso!");
                        CleanDataGrid();
                        await LoadTestBody();
                    }
                }
            } catch {
                new FormException("Selecione a linha do corpo de prova que deseja remover!");
            }
        }
        private async void btUpdate_Click(object sender, EventArgs e) {
            try {
                CleanFields();
                Id = dgTestBodies.SelectedRows[0].Cells[0].Value.ToString();
                await controller.Read(loggedUser, dgTestBodies.SelectedRows[0].Cells[1].Value.ToString());
                tabTestBodies.SelectedTab = tabRegister;
            } catch {
                new FormException("Selecione a linha do corpo de prova que deseja alterar.");
            }
        }
        #endregion

        #region Functions
        private void CleanFields() {
            Id = null;
            tbSerialNumber.Text = "";
            tbInternalNumber.Text = "";
            tbDescription.Text = "";
            tbModel.Text = "";
            tbEfficiencyLevel.Text = "";
            tbTemperatureLimit.Text = "";
            tbType.Text = "";
            tbManufactureDate.Text = "";
            tbWeight.Text = "";
            tbRegulationRange.Text = "";
            tbResistanceTemperature.Text = "";
            tbConnection.Text = "";
            tbFirstPower.Text = "";
            tbSecondPower.Text = "";
            tbThirdPower.Text = "";
            tbFirstVoltage.Text = "";
            tbSecondVoltage.Text = "";
            tbThirdVoltage.Text = "";
            tbFirstCurrent.Text = "";
            tbSecondCurrent.Text = "";
            tbThirdCurrent.Text = "";
            tbFirstResistance.Text = "";
            tbSecondResistance.Text = "";
            tbThirdResistance.Text = "";
            cbManufacturer.SelectedIndex = -1;
            cbCustomer.SelectedIndex = -1;
            cbFirstPower.SelectedIndex = -1;
            cbSecondPower.SelectedIndex = -1;
            cbThirdPower.SelectedIndex = -1;
            cbFirstVoltage.SelectedIndex = -1;
            cbSecondVoltage.SelectedIndex = -1;
            cbThirdVoltage.SelectedIndex = -1;
            cbFirstCurrent.SelectedIndex = -1;
            cbSecondCurrent.SelectedIndex = -1;
            cbThirdCurrent.SelectedIndex = -1;
            cbFirstResistance.SelectedIndex = -1;
            cbSecondResistance.SelectedIndex = -1;
            cbThirdResistance.SelectedIndex = -1;
            cbFirstWindingMaterial.SelectedIndex = -1;
            cbSecondWindingMaterial.SelectedIndex = -1;
            cbThirdWindingMaterial.SelectedIndex = -1;
            cbPhases.SelectedIndex = -1;
            cbFrequency.SelectedIndex = -1;
            cbPolarity.SelectedIndex = -1;
            cbInsulatingOil.SelectedIndex = -1;
            cbVoltageClass.SelectedIndex = -1;
            cbState.SelectedIndex = -1;
            dgTaps.Rows.Clear();
        }
        private async Task LoadTestBody() {
            CleanDataGrid();
            if (cbOrder.SelectedIndex != -1) {
                await controller.List(
                    loggedUser,
                    tbSearchSerialNumber.Text,
                    tbSearchDescription.Text,
                    cbOrder.SelectedItem.ToString()
                );
            } else {
                await controller.List(
                    loggedUser,
                    tbSearchSerialNumber.Text,
                    tbSearchDescription.Text
                );
            }
        }
        private void CleanDataGrid() {
            dgTestBodies.DataSource = null;
        }
        private void SelectEquipmentType(TabPage tab) {
            bool tabIsVisible = false; ;
            foreach (TabPage page in tabTestBodyType.TabPages) {
                if (page != tab) {
                    tabTestBodyType.TabPages.Remove(page);
                } else {
                    tabIsVisible = true;
                }
            }
            if (!tabIsVisible) {
                tabTestBodyType.TabPages.Add(tab);
            }
        }
        private bool CheckMandatoryFields() {
            if (ValidationHandler.IsTextBoxNull(tbSerialNumber, lbSerialNumber.Text)) {
                return false;
            }
            if (ValidationHandler.IsTextBoxNull(tbInternalNumber, lbInternalNumber.Text)) {
                return false;
            }
            if (ValidationHandler.IsTextBoxNull(tbDescription, lbDescription.Text)) {
                return false;
            }
            if (ValidationHandler.IsTextBoxNull(tbModel, lbModel.Text)) {
                return false;
            }
            if (ValidationHandler.IsMaskedBoxNull(tbManufactureDate, lbManufactureDate.Text)) {
                return false;
            }
            if (ValidationHandler.IsComboBoxNull(cbManufacturer, lbManufacturer.Text)) {
                return false;
            }
            if (ValidationHandler.IsComboBoxNull(cbCustomer, lbCustomer.Text)) {
                return false;
            }
            return true;
        }
        private string CheckedEquipmentType() {
            string equipmentType;
            if (rbTransformer.Checked) {
                equipmentType = "Transformer";
            } else {
                equipmentType = "Generic";
            }
            return equipmentType;
        }




        #endregion
        
    }
}
